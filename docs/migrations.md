# New Migration script

- main documentation of `db-migrate` https://db-migrate.readthedocs.io/en/latest/

## Using NPM

- **Important** before runnig the migrations the ENV variables `POSTGRES_CONN` must be set.
- command to run all migrations
```
npm run migrate-db
```

### PostgreSQL migrations

- creating the new migration (creates js file and sql up and down files)
  - files for migration scripts are saved in directory `db/postgresql-migrations/sqls/`
```
npx db-migrate create new-migration-name --env postgresql --migrations-dir db/postgresql-migrations --sql-file
```
- checking done/waitning migrations
```
npx db-migrate check --env postgresql --migrations-dir db/postgresql-migrations
```
- running all waiting migrations (up)
```
npx db-migrate up --env postgresql --migrations-dir db/postgresql-migrations
```
- undoing last migration (down)
```
npx db-migrate down --env postgresql --migrations-dir db/postgresql-migrations
```

## Using Docker

- Go to project root directory `cd schema-definition`.
- Build the docker image which containing required libraries `docker build -t schema-definition .`.

### PostgreSQL migrations

- Creating the new migration using Docker image (creates js file and sql up and down files).
  - Files for migration scripts are saved in directory `db/postgresql-migrations/sqls/`.
```
docker run -ti --rm -v $(pwd):/home/node/app/schema-definitions schema-definition bash -c 'export POSTGRES_CONN="db";npx db-migrate create YOUR_ISSUE --env postgresql --migrations-dir db/postgresql-migrations --sql-file'
```
- Adding sql scripts to the new sql migration files.
