-- analytic.v_pedestrians_flow_quality source
drop VIEW analytic.v_pedestrians_flow_quality;
CREATE OR REPLACE VIEW analytic.v_pedestrians_flow_quality
AS WITH timeline_category AS (
         SELECT DISTINCT fs.cube_id,
            fs.id AS sink_id,
            timeline."timestamp"
           FROM flow_sinks fs
             LEFT JOIN ( SELECT generate_series(date_trunc('month'::text, CURRENT_DATE - '180 days'::interval)::timestamp with time zone, date_trunc('hour'::text, CURRENT_DATE::timestamp with time zone), '01:00:00'::interval) AS "timestamp") timeline ON true
        ), real_nrow AS (
         SELECT date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) AS date_trunc,
            flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(*) AS n_row,
            count(*) FILTER (WHERE flow_measurements.category::text = 'pedestrian'::text) AS n_row_pedestrian
           FROM flow_measurements
          WHERE flow_measurements.start_timestamp::double precision >= (1000::double precision * date_part('epoch'::text, CURRENT_DATE - '220 days'::interval))
          GROUP BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))), flow_measurements.cube_id, flow_measurements.sink_id
        ), expected_nrow AS (
         SELECT flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(DISTINCT flow_measurements.category) * 12 AS expected_nrow,
            12 AS expected_nrow_pedestrian
           FROM flow_measurements
          WHERE flow_measurements.start_timestamp::double precision >= (1000::double precision * date_part('epoch'::text, CURRENT_DATE - '220 days'::interval))
          GROUP BY flow_measurements.cube_id, flow_measurements.sink_id
        )
 SELECT t."timestamp",
    date_trunc('month'::text, t."timestamp")::date AS month,
    t.cube_id,
    t.sink_id,
    concat(t.cube_id, ' - ', t.sink_id) AS cube_sink,
    COALESCE(r.n_row, 0::bigint) AS n_row,
    COALESCE(r.n_row_pedestrian, 0::bigint) AS n_row_pedestrian,
    er.expected_nrow,
    er.expected_nrow_pedestrian
   FROM timeline_category t
     LEFT JOIN real_nrow r ON t."timestamp" = r.date_trunc AND t.cube_id = r.cube_id AND t.sink_id = r.sink_id
     LEFT JOIN expected_nrow er ON t.cube_id = er.cube_id AND t.sink_id = er.sink_id;