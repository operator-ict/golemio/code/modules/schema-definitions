/* Replace with your SQL commands */

-- Odstranění metadat
delete from  meta."extract"
where id_dataset = (select id_dataset from meta.dataset where code_dataset = 'rsd_tmc');

delete from meta.dataset
where code_dataset = 'rsd_tmc';

drop TABLE public.rsd_tmc_segments;
drop TABLE public.rsd_tmc_roads;
drop TABLE public.rsd_tmc_points;
drop TABLE public.rsd_tmc_admins;