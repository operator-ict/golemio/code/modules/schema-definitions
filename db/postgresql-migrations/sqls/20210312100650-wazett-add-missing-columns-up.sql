ALTER TABLE public.wazett_jams_stats ADD COLUMN create_batch_id bigint;
ALTER TABLE public.wazett_jams_stats ADD COLUMN created_by character varying(150);
ALTER TABLE public.wazett_jams_stats ADD COLUMN update_batch_id bigint;
ALTER TABLE public.wazett_jams_stats ADD COLUMN updated_by character varying(150);
