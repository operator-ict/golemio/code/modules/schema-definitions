/* Replace with your SQL commands */

create or replace view analytic.v_ropidbi_ticket_with_rollingmean as
select "date"::date,
		count(tariff_name) as ticket_count,
		sum (cena_s_dph) as total_sale,
		avg(count(tariff_name)) over(order by "date" rows between 6 preceding and current row) as roll_ticket_count,
		avg(sum (cena_s_dph)) over(order by "date" rows between 6 preceding and current row) as roll_sale
from (
		select "date"::date,
			   tariff_name,
				case when tariff_name like '%pes%' then 16 else cen.cena_s_dph end as cena_s_dph 
		from public.mos_ma_ticketpurchases pur
		left join (select distinct * from analytic.pid_cenik) cen
				on pur.tariff_name = cen.tarif
				) tab
where "date"::date > '2020-02-29'
group by "date"::date;


CREATE OR REPLACE VIEW analytic.v_ropidbi_coupon_channels_sale
AS WITH calendar AS (
         SELECT generate_series('2019-01-01 00:00:00+01'::timestamp with time zone, now()::date::timestamp with time zone - '1 day'::interval, '1 day'::interval)::date AS on_date
        ), channel AS (
         SELECT unnest(string_to_array('Mobilní aplikace,Přepážka,Eshop'::text, ','::text)) AS channel
        ), payment_method AS (
         SELECT unnest(string_to_array('Platební karta,Hotově,Převodem'::text, ','::text)) AS pmethod
        ), seller AS (
         SELECT unnest(string_to_array('OICT,DPP,Nezařazeno'::text, ','::text)) AS seller
        ), mos AS (
         SELECT c.created::date AS created,
                CASE
                    WHEN c.created_by_id::integer = 188 THEN 'Mobilní aplikace'::text
                    WHEN c.order_payment_type = ANY (ARRAY[1, 2]) THEN 'Eshop'::text
                    WHEN c.order_payment_type = ANY (ARRAY[3, 4]) THEN 'Přepážka'::text
                    ELSE 'Nezařazeno'::text
                END AS channel,
                CASE
                    WHEN c.order_status = 2 THEN 'Platební karta'::text
                    WHEN c.order_status = 5 THEN 'Hotově'::text
                    WHEN c.order_status = 3 THEN 'Převodem'::text
                    ELSE 'Nezařazeno'::text
                END AS payment_method,
                CASE
                    WHEN c.seller_id = 1 THEN 'OICT'::text
                    WHEN c.seller_id = 29 THEN 'DPP'::text
                    ELSE 'Nezařazeno'::text
                END AS seller,
            count(*) AS coupon_count,
            sum(c.price) AS sale
           FROM mos_be_coupons c
          GROUP BY (c.created::date), (
                CASE
                    WHEN c.created_by_id::integer = 188 THEN 'Mobilní aplikace'::text
                    WHEN c.order_payment_type = ANY (ARRAY[1, 2]) THEN 'Eshop'::text
                    WHEN c.order_payment_type = ANY (ARRAY[3, 4]) THEN 'Přepážka'::text
                    ELSE 'Nezařazeno'::text
                END), (
                CASE
                    WHEN c.order_status = 2 THEN 'Platební karta'::text
                    WHEN c.order_status = 5 THEN 'Hotově'::text
                    WHEN c.order_status = 3 THEN 'Převodem'::text
                    ELSE 'Nezařazeno'::text
                END), (
                CASE
                    WHEN c.seller_id = 1 THEN 'OICT'::text
                    WHEN c.seller_id = 29 THEN 'DPP'::text
                    ELSE 'Nezařazeno'::text
                END)
          ORDER BY (c.created::date) DESC
        )
 SELECT calendar.on_date AS created,
    channel.channel,
    payment_method.pmethod AS payment_method,
    seller.seller,
    COALESCE(mos.coupon_count, 0::bigint) AS coupon_count,
    COALESCE(mos.sale, 0::numeric) AS sale
   FROM calendar
     LEFT JOIN channel ON true
     LEFT JOIN payment_method ON true
     LEFT JOIN seller ON true
     LEFT JOIN mos ON calendar.on_date = mos.created AND mos.channel = channel.channel AND mos.payment_method = payment_method.pmethod AND mos.seller = seller.seller;
	 
	 
CREATE OR REPLACE VIEW analytic.v_parkomats_sales
AS SELECT date_part('year'::text, parkomats.ticket_bought) AS year,
    "substring"(split_part(parkomats.parking_zone::text, '-'::text, 1), 2, 2) AS parking_zone,
    'Praha '::text || "substring"(split_part(parkomats.parking_zone::text, '-'::text, 1), 2, 2) AS city_district,
        CASE
            WHEN parkomats.channel::text = 'PARKMACHINE'::text THEN 'Parkovací automat'::text
            ELSE 'VPH'::text
        END AS channel,
    sum(parkomats.price) AS sale
   FROM parkomats
  GROUP BY (date_part('year'::text, parkomats.ticket_bought)), ("substring"(split_part(parkomats.parking_zone::text, '-'::text, 1), 2, 2)), ('Praha '::text || "substring"(split_part(parkomats.parking_zone::text, '-'::text, 1), 2, 2)), (
        CASE
            WHEN parkomats.channel::text = 'PARKMACHINE'::text THEN 'Parkovací automat'::text
            ELSE 'VPH'::text
        END);


