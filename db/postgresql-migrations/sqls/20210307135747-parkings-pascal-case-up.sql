ALTER TABLE public.parkings RENAME COLUMN "sourceId" TO source_id;
ALTER TABLE public.parkings RENAME COLUMN "dataProvider" TO data_provider;
ALTER TABLE public.parkings RENAME COLUMN "usageScenario" TO usage_scenario;
ALTER TABLE public.parkings RENAME COLUMN "dateModified" TO date_modified;
ALTER TABLE public.parkings RENAME COLUMN "areaServed" TO area_served;
ALTER TABLE public.parkings RENAME COLUMN "paymentUrl" TO payment_url;
ALTER TABLE public.parkings RENAME COLUMN "totalSpotNumber" TO total_spot_number;

ALTER TABLE public.parkings_measurements RENAME COLUMN "sourceId" TO source_id;
ALTER TABLE public.parkings_measurements RENAME COLUMN "availableSpotNumber" TO available_spot_number;
ALTER TABLE public.parkings_measurements RENAME COLUMN "closedSpotNumber" TO closed_spot_number;
ALTER TABLE public.parkings_measurements RENAME COLUMN "occupiedSpotNumber" TO occupied_spot_number;
ALTER TABLE public.parkings_measurements RENAME COLUMN "totalSpotNumber" TO total_spot_number;
ALTER TABLE public.parkings_measurements RENAME COLUMN "dateModified" TO date_modified;
