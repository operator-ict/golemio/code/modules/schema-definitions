/* Replace with your SQL commands */

CREATE TABLE public.citydistricts (
	geom geometry(POLYGON, 4326) NULL,
	objectid int4 NULL,
	create_date varchar NULL,
	chnage_date varchar NULL,
	area float8 NULL,
	id int4 NOT NULL,
	zip int4 NULL,
	district_name varchar NULL,
	kod_mo int4 NULL,
	kod_so varchar NULL,
	tid_tm_district_p int4 NULL,
	provider varchar NULL,
	id_provider int4 NULL,
	chnage_statut varchar NULL,
	name_1 varchar NULL,
	shape_length float8 NULL,
	shape_area float8 NULL,
	CONSTRAINT citydistricts_pkey PRIMARY KEY (id)
);
CREATE INDEX sidx_citydistricts_geom ON public.citydistricts USING gist (geom);

update meta.extract
	set schema_extract = 'public'
	where name_extract = 'citydistricts';

insert into public.citydistricts
	select * from common.citydistricts ;

drop TABLE common.citydistricts;

drop schema IF EXISTS common;
