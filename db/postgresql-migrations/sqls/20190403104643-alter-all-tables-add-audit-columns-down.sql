DROP VIEW IF EXISTS v_vehiclepositions_last_position;

-- vehiclepositions_positions

ALTER TABLE vehiclepositions_positions DROP create_batch_id;
ALTER TABLE vehiclepositions_positions RENAME COLUMN created_at TO created;
ALTER TABLE vehiclepositions_positions DROP created_by;
ALTER TABLE vehiclepositions_positions DROP update_batch_id;
ALTER TABLE vehiclepositions_positions DROP updated_at;
ALTER TABLE vehiclepositions_positions DROP updated_by;

-- vehiclepositions_stops

ALTER TABLE vehiclepositions_stops DROP create_batch_id;
ALTER TABLE vehiclepositions_stops RENAME COLUMN created_at TO created;
ALTER TABLE vehiclepositions_stops DROP created_by;
ALTER TABLE vehiclepositions_stops DROP update_batch_id;
ALTER TABLE vehiclepositions_stops RENAME COLUMN updated_at TO modified;
ALTER TABLE vehiclepositions_stops DROP updated_by;

-- vehiclepositions_trips

ALTER TABLE vehiclepositions_trips DROP create_batch_id;
ALTER TABLE vehiclepositions_trips RENAME COLUMN created_at TO created;
ALTER TABLE vehiclepositions_trips DROP created_by;
ALTER TABLE vehiclepositions_trips DROP update_batch_id;
ALTER TABLE vehiclepositions_trips RENAME COLUMN updated_at TO modified;
ALTER TABLE vehiclepositions_trips DROP updated_by;

-- merakiaccesspoints_observations

ALTER TABLE merakiaccesspoints_observations DROP create_batch_id;
ALTER TABLE merakiaccesspoints_observations DROP created_at;
ALTER TABLE merakiaccesspoints_observations DROP created_by;
ALTER TABLE merakiaccesspoints_observations DROP update_batch_id;
ALTER TABLE merakiaccesspoints_observations DROP updated_at;
ALTER TABLE merakiaccesspoints_observations DROP updated_by;

-- merakiaccesspoints_tags

ALTER TABLE merakiaccesspoints_tags DROP create_batch_id;
ALTER TABLE merakiaccesspoints_tags DROP created_at;
ALTER TABLE merakiaccesspoints_tags DROP created_by;
ALTER TABLE merakiaccesspoints_tags DROP update_batch_id;
ALTER TABLE merakiaccesspoints_tags DROP updated_at;
ALTER TABLE merakiaccesspoints_tags DROP updated_by;


-- ropidgtfs_agency

ALTER TABLE ropidgtfs_agency ADD created_time time without time zone;
ALTER TABLE ropidgtfs_agency ADD last_modify time without time zone;
ALTER TABLE ropidgtfs_agency DROP create_batch_id;
ALTER TABLE ropidgtfs_agency DROP created_at;
ALTER TABLE ropidgtfs_agency DROP created_by;
ALTER TABLE ropidgtfs_agency DROP update_batch_id;
ALTER TABLE ropidgtfs_agency DROP updated_at;
ALTER TABLE ropidgtfs_agency DROP updated_by;

-- ropidgtfs_calendar

ALTER TABLE ropidgtfs_calendar ADD created_time time without time zone;
ALTER TABLE ropidgtfs_calendar ADD last_modify time without time zone;
ALTER TABLE ropidgtfs_calendar DROP create_batch_id;
ALTER TABLE ropidgtfs_calendar DROP created_at;
ALTER TABLE ropidgtfs_calendar DROP created_by;
ALTER TABLE ropidgtfs_calendar DROP update_batch_id;
ALTER TABLE ropidgtfs_calendar DROP updated_at;
ALTER TABLE ropidgtfs_calendar DROP updated_by;

-- ropidgtfs_calendar_dates

ALTER TABLE ropidgtfs_calendar_dates ADD created_time time without time zone;
ALTER TABLE ropidgtfs_calendar_dates ADD last_modify time without time zone;
ALTER TABLE ropidgtfs_calendar_dates DROP create_batch_id;
ALTER TABLE ropidgtfs_calendar_dates DROP created_at;
ALTER TABLE ropidgtfs_calendar_dates DROP created_by;
ALTER TABLE ropidgtfs_calendar_dates DROP update_batch_id;
ALTER TABLE ropidgtfs_calendar_dates DROP updated_at;
ALTER TABLE ropidgtfs_calendar_dates DROP updated_by;

-- ropidgtfs_cis_stop_groups

ALTER TABLE ropidgtfs_cis_stop_groups DROP create_batch_id;
ALTER TABLE ropidgtfs_cis_stop_groups RENAME COLUMN created_at TO time_created;
ALTER TABLE ropidgtfs_cis_stop_groups DROP created_by;
ALTER TABLE ropidgtfs_cis_stop_groups DROP update_batch_id;
ALTER TABLE ropidgtfs_cis_stop_groups DROP updated_at;
ALTER TABLE ropidgtfs_cis_stop_groups DROP updated_by;

-- ropidgtfs_cis_stops

ALTER TABLE ropidgtfs_cis_stops DROP create_batch_id;
ALTER TABLE ropidgtfs_cis_stops RENAME COLUMN created_at TO time_created;
ALTER TABLE ropidgtfs_cis_stops DROP created_by;
ALTER TABLE ropidgtfs_cis_stops DROP update_batch_id;
ALTER TABLE ropidgtfs_cis_stops DROP updated_at;
ALTER TABLE ropidgtfs_cis_stops DROP updated_by;

-- ropidgtfs_metadata

ALTER TABLE ropidgtfs_metadata DROP create_batch_id;
ALTER TABLE ropidgtfs_metadata DROP created_at;
ALTER TABLE ropidgtfs_metadata DROP created_by;
ALTER TABLE ropidgtfs_metadata DROP update_batch_id;
ALTER TABLE ropidgtfs_metadata DROP updated_at;
ALTER TABLE ropidgtfs_metadata DROP updated_by;

-- ropidgtfs_routes

ALTER TABLE ropidgtfs_routes ADD created_time time without time zone;
ALTER TABLE ropidgtfs_routes ADD last_modify time without time zone;
ALTER TABLE ropidgtfs_routes DROP create_batch_id;
ALTER TABLE ropidgtfs_routes DROP created_at;
ALTER TABLE ropidgtfs_routes DROP created_by;
ALTER TABLE ropidgtfs_routes DROP update_batch_id;
ALTER TABLE ropidgtfs_routes DROP updated_at;
ALTER TABLE ropidgtfs_routes DROP updated_by;

-- ropidgtfs_shapes

ALTER TABLE ropidgtfs_shapes ADD created_time time without time zone;
ALTER TABLE ropidgtfs_shapes ADD last_modify time without time zone;
ALTER TABLE ropidgtfs_shapes DROP create_batch_id;
ALTER TABLE ropidgtfs_shapes DROP created_at;
ALTER TABLE ropidgtfs_shapes DROP created_by;
ALTER TABLE ropidgtfs_shapes DROP update_batch_id;
ALTER TABLE ropidgtfs_shapes DROP updated_at;
ALTER TABLE ropidgtfs_shapes DROP updated_by;

-- ropidgtfs_stop_times

ALTER TABLE ropidgtfs_stop_times ADD created_time time without time zone;
ALTER TABLE ropidgtfs_stop_times ADD last_modify time without time zone;
ALTER TABLE ropidgtfs_stop_times DROP create_batch_id;
ALTER TABLE ropidgtfs_stop_times DROP created_at;
ALTER TABLE ropidgtfs_stop_times DROP created_by;
ALTER TABLE ropidgtfs_stop_times DROP update_batch_id;
ALTER TABLE ropidgtfs_stop_times DROP updated_at;
ALTER TABLE ropidgtfs_stop_times DROP updated_by;

-- ropidgtfs_stops

ALTER TABLE ropidgtfs_stops ADD created_time time without time zone;
ALTER TABLE ropidgtfs_stops ADD last_modify time without time zone;
ALTER TABLE ropidgtfs_stops DROP create_batch_id;
ALTER TABLE ropidgtfs_stops DROP created_at;
ALTER TABLE ropidgtfs_stops DROP created_by;
ALTER TABLE ropidgtfs_stops DROP update_batch_id;
ALTER TABLE ropidgtfs_stops DROP updated_at;
ALTER TABLE ropidgtfs_stops DROP updated_by;

-- ropidgtfs_trips

ALTER TABLE ropidgtfs_trips ADD created_time time without time zone;
ALTER TABLE ropidgtfs_trips ADD last_modify time without time zone;
ALTER TABLE ropidgtfs_trips DROP create_batch_id;
ALTER TABLE ropidgtfs_trips DROP created_at;
ALTER TABLE ropidgtfs_trips DROP created_by;
ALTER TABLE ropidgtfs_trips DROP update_batch_id;
ALTER TABLE ropidgtfs_trips DROP updated_at;
ALTER TABLE ropidgtfs_trips DROP updated_by;

CREATE OR REPLACE VIEW v_vehiclepositions_last_position
AS
SELECT
    DISTINCT ON ("vehiclepositions_positions"."trips_id") "vehiclepositions_positions".*
    FROM "vehiclepositions_positions" AS "vehiclepositions_positions"
    JOIN "vehiclepositions_trips" AS "vehiclepositions_trips"
    ON ("vehiclepositions_trips"."id" = "vehiclepositions_positions"."trips_id" AND
    "vehiclepositions_trips"."gtfs_trip_id" IS NOT NULL) --změna na gtfs_trip_id
    WHERE "vehiclepositions_positions"."created" > (NOW() - INTERVAL '5 min') AND "vehiclepositions_positions"."delay" IS NOT NULL --jen vypočtené delay (což zároveň vyfiltruje tracking=0)
    ORDER BY "vehiclepositions_positions"."trips_id", "vehiclepositions_positions"."created" DESC;
