/* Replace with your SQL commands */
drop view if exists analytic.v_ropidbi_ticket_activation_types;
drop view if exists analytic.v_ropidbi_ticket_activation_times;
drop view if exists analytic.v_ropidbi_ticket_activation_location;
drop view if exists analytic.v_ropidbi_ticket_sales;
drop view if exists analytic.v_ropidbi_device_counts;
drop view if exists analytic.v_ropidbi_customer_age;
drop view if exists analytic.v_ropidbi_ticket_zones;
drop view if exists analytic.v_ropidbi_soubeh_jizdenek;
drop table if exists analytic.pid_cenik;
DROP table if exists public.firebase_trasy;
drop materialized view if exists analytic.v_ropidbi_ticket;



CREATE materialized VIEW analytic.v_ropidbi_ticket
AS SELECT
	a.date AS activation_date,
	a.date::date AS act_date,
	date_trunc('hour'::text, a.date)::time AS act_time,
    a.lat AS act_lat,
    a.lon AS act_lon,
    a.ticket_id,
    CASE
            WHEN a.type::text = 'manualNow'::text THEN 'Ruční aktivace ihned'::text
            WHEN a.type::text = 'manualTime'::text THEN 'Ruční aktivace na daný čas'::text
            WHEN a.type::text = 'purchaseNow'::text THEN 'Nákup s aktivací ihned'::text
            WHEN a.type::text = 'purchaseTime'::text THEN 'Nákup s aktivací na daný čas'::text
            ELSE 'Zatím neaktivováno'::text
        END AS activation_type,
    a.zones,
    p.cptp,
    p.date AS purchase_date,
    p.duration AS duration_min,
    p.tariff_name,
    p.zone_count,
    CASE
            WHEN a.zones::text = ANY (ARRAY['P'::character varying, 'P,0'::character varying, 'P,0,B'::character varying, '0'::character varying, '0,B'::character varying, 'B'::character varying]::text[]) THEN 'Praha'::text
            ELSE 'Region'::text end as oblast
   FROM public.mos_ma_ticketactivations a
     RIGHT JOIN mos_ma_ticketpurchases p ON a.ticket_id = p.ticket_id;

-- activation_types

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_types
AS SELECT
		act_date,
        activation_type,
        oblast,
    	v_ropidbi_ticket.tariff_name,
    	count(*) AS count
  FROM analytic.v_ropidbi_ticket
  WHERE v_ropidbi_ticket.activation_type::text <> ''::text AND v_ropidbi_ticket.tariff_name::text <> ''::text
  GROUP BY act_date,
        activation_type,
        oblast,
    	v_ropidbi_ticket.tariff_name;

-- activation_times

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_times
AS SELECT
		  act_date,
    	  act_time,
    	  v_ropidbi_ticket.tariff_name,
    	  oblast,
          count(*) AS count
   FROM analytic.v_ropidbi_ticket
  GROUP BY act_date,
    	  act_time,
    	  v_ropidbi_ticket.tariff_name,
    	  oblast;

-- location

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_location
AS SELECT
		  act_date,
	      round (v_ropidbi_ticket.act_lat, 4) as act_lat,
	      round (v_ropidbi_ticket.act_lon, 4) as act_lon,
	      v_ropidbi_ticket.activation_type,
	      oblast,
          count(*) AS count
   FROM analytic.v_ropidbi_ticket
  WHERE act_date IS NOT NULL
  		AND v_ropidbi_ticket.activation_type IS NOT NULL
  		AND v_ropidbi_ticket.act_lat IS NOT NULL
  		AND v_ropidbi_ticket.act_lon IS NOT NULL
  GROUP BY act_date,
	       round (v_ropidbi_ticket.act_lat, 4),
	       round (v_ropidbi_ticket.act_lon, 4),
	       v_ropidbi_ticket.activation_type,
	       oblast;

-- sales

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales
AS SELECT v_ropidbi_ticket.purchase_date::date AS purchase_date,
		  tariff_name,
		  oblast,
		  count (*)
          FROM analytic.v_ropidbi_ticket
  WHERE purchase_date::date IS NOT NULL
  		AND tariff_name IS NOT NULL
  		AND v_ropidbi_ticket.act_lat IS NOT NULL
  		AND v_ropidbi_ticket.act_lon IS NOT NULL
  GROUP BY v_ropidbi_ticket.purchase_date::date,
		  tariff_name,
		  oblast;

-- devices

CREATE OR REPLACE VIEW analytic.v_ropidbi_device_counts
as SELECT
        trim(SUBSTR(replace(model,'_',' '),1, POSITION(' ' IN replace(model,'_',' ')))) brand,
        replace(model,'_',' ') as model,
        case when left(model, 1) = 'i' then 'iOS'
        	 else 'Android' end as operation_system,
        sum (count)
        from public.mos_ma_devicemodels
    where model is not null
   group by trim(SUBSTR(replace(model,'_',' '),1, POSITION(' ' IN replace(model,'_',' ')))),
        replace(model,'_',' '),
        case when left(model, 1) = 'i' then 'iOS'
        	 else 'Android' end;


-- customer age

CREATE OR REPLACE VIEW analytic.v_ropidbi_customer_age
as select date_part('year', age(date_of_birth::date)) as age,
		  count (*)
    from mos_be_customers
   where date_part('year', age(date_of_birth::date)) between 5 and 100
  group by date_part('year', age(date_of_birth::date));

-- coupons_zones

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_zones
as select c.created::date as valid_from,
		  c.customer_profile_name as customer,
		  c.tariff_profile_name as tarif,
		  case when z.zone_name in ('Praha', '0+B') then 'P+0+B'
		  	   else z.zone_name end as zona,
		  case when z.zone_name in ('Praha', '0+B') then '0'
		  	   else z.zone_name end as zone_order,
		  count (*)
    from mos_be_coupons as c
    left join mos_be_zones as z on z.coupon_id = c.coupon_id
   where c.created::date is not null
   		 and z.zone_name is not null
  group by c.created::date,
		  c.customer_profile_name,
		  c.tariff_profile_name,
		  case when z.zone_name in ('Praha', '0+B') then 'P+0+B'
		  	   else z.zone_name end,
		  case when z.zone_name in ('Praha', '0+B') then '0'
		  	   else z.zone_name end
  order by c.created::date asc;

-- soubeh jízdenek

CREATE OR REPLACE VIEW analytic.v_ropidbi_soubeh_jizdenek
AS SELECT celkem.datum,
    celkem.count_acc AS acc_all,
    celkem.count_glob AS ticket_all,
    dupl.count_acc AS acc_dupl,
    dupl.count_glob AS ticket_dupl
   FROM ( SELECT mos_ma_ticketpurchases.date::date AS datum,
            count(DISTINCT mos_ma_ticketpurchases.account_id) AS count_acc,
            count(*) AS count_glob
           FROM mos_ma_ticketpurchases
          GROUP BY (mos_ma_ticketpurchases.date::date)) celkem
     LEFT JOIN ( SELECT b.valid_from::date AS datum,
            count(DISTINCT b.account_id) AS count_acc,
            count(*) AS count_glob
           FROM ( SELECT a.account_id,
                    a.valid_from,
                    a.valid_to,
                    a.xx
                   FROM ( SELECT mos_ma_ticketpurchases.account_id,
                            mos_ma_ticketpurchases.date AS valid_from,
                            mos_ma_ticketpurchases.date + ((mos_ma_ticketpurchases.duration || ' minutes'::text)::interval) AS valid_to,
                            lead(mos_ma_ticketpurchases.date) OVER (PARTITION BY mos_ma_ticketpurchases.account_id ORDER BY mos_ma_ticketpurchases.date) AS xx
                           FROM mos_ma_ticketpurchases) a
                  WHERE a.xx >= a.valid_from AND a.xx <= a.valid_to) b
          GROUP BY (b.valid_from::date)) dupl ON celkem.datum = dupl.datum;

-- PID cenik

create table analytic.pid_cenik (
	Tarif varchar(100),
	Cestující varchar (50),
	Platnost varchar (50),
	Zóny varchar  (50),
	Číslo_tarifu_CPTP integer,
	Cena_s_DPH integer
	);

-- nutno nalít data z csv C:\Users\OP3218\Operátor ICT, a.s\Datova_Platforma - Datova_Platforma\Datové sady a projekty\Projekty\P0100. BI pro Ropid\2. DATOVÉ SADY

-- DROP TABLE public.firebase_trasy;

CREATE TABLE public.firebase_trasy (
	datum varchar(255) NOT NULL,
	s_from varchar(255) NOT NULL,
	s_to varchar(255) NOT NULL,
	pocet int4 NOT NULL,
	CONSTRAINT firebase_trasy_pkey PRIMARY KEY (datum, s_from, s_to)
);
