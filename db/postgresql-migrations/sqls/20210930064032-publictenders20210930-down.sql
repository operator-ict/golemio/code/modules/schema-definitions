-- analytic.v_public_tenders_internal_frequent_suppliers source

CREATE OR REPLACE VIEW analytic.v_public_tenders_internal_frequent_suppliers
AS SELECT count(DISTINCT pt.id_zakazky) AS pocet_zakazek,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
        CASE
            WHEN ptpi.ico IS NOT NULL THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.faze_zakazky,
        CASE
            WHEN pto.odbor IS NOT NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
        CASE
            WHEN pt.varianta_druhu_rizeni = 'Neveřejná výzva k podání nabídky'::text THEN s.spolecnosti_oslovene_k_podani_nabidky_nazev
            ELSE s2.nazev_smluvniho_partnera
        END AS spolecnost,
    sum(
        CASE
            WHEN s.spolecnosti_oslovene_k_podani_nabidky_nazev = s2.nazev_smluvniho_partnera OR s.spolecnosti_oslovene_k_podani_nabidky_nazev IS NULL THEN 1
            ELSE 0
        END) AS pocet_vyhranych_zakazek,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders pt
     LEFT JOIN LATERAL unnest(string_to_array(pt.spolecnosti_oslovene_k_podani_nabidky_nazev, '; '::text)) s(spolecnosti_oslovene_k_podani_nabidky_nazev) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.nazev_smluvniho_partnera, '; '::text)) s2(nazev_smluvniho_partnera) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.ico_smluvniho_partnera, '; '::text)) s3(ico_smluvniho_partnera) ON true
     LEFT JOIN python.public_tenders_odbory pto ON pt.organizacni_jednotka_spravy ~~ (('%'::text || pto.odbor::text) || '%'::text)
     LEFT JOIN python.public_tenders_partners_ico ptpi ON ptpi.ico::text = pt.ico_smluvniho_partnera
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND (pt.varianta_druhu_rizeni = ANY (ARRAY['Neveřejná výzva k podání nabídky'::text, 'Drobná objednávka'::text, 'Přímo objednáno u jednoho dodavatele'::text, 'Zakázka zadaná na základě výjimky'::text])) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text]))) AND (pt.datum_vytvoreni_zakazky >= '2020-09-01 01:00:00+02'::timestamp with time zone OR pt.datum_vytvoreni_zakazky >= '2021-01-01 00:00:00+01'::timestamp with time zone AND (pt.organizacni_jednotka_spravy !~~ ANY (ARRAY['%DNS%'::text, '%odbor dopravněsprávních činností%'::text, '%Městská Policie hl. m. Prahy%'::text, '%Archiv sekretariátů radních%'::text, '%odbor Archiv hl.m. Prahy%'::text, '%odbor bezpečnosti%'::text, '%odbor bytového fondu%'::text, '%odbor dopravy%'::text, '%odbor evidence majetku%'::text, '%odbor evropských fondů%'::text, '%odbor hospodaření s majetkem%'::text, '%odbor informatické infrastruktury%'::text, '%odbor informatických aplikací%'::text, '%odbor investiční%'::text, '%odbor Kancelář primátora%'::text, '%odbor Kancelář ředitele Magistrátu%'::text, '%odbor kontrolních činností%'::text, '%odbor kultury a cestovního ruchu%'::text, '%odbor legislativní a právní%'::text, '%Odbor médií a marketingu%'::text, '%odbor ochrany prostředí%'::text, '%odbor památkové péče%'::text, '%odbor personální%'::text, '%odbor pozemních komunikací a drah%'::text, '%odbor právní podpory%'::text, '%odbor projektového řízení%'::text, '%odbor rozpočtu%'::text, '%odbor služeb%'::text, '%odbor sociálních věcí MHMP%'::text, '%odbor stavebního řádu%'::text, '%odbor školství, mládeže a sportu%'::text, '%odbor územního rozvoje%'::text, '%odbor veřejných zakázek%'::text, '%odbor volených orgánů%'::text, '%odbor zdravotnictví%'::text, '%odbor živnostenský a občanskosprávní%'::text, '%sekretariát náměstka primátora%'::text, '%sekretariát radní HMP%'::text, '%sekretariát radního HMP%'::text, '%Útvar informačních a komunikačních technologií%'::text, '%Útvar provozního a logistického zabezpečení%'::text])))
  GROUP BY pt.organizacni_jednotka_spravy, pt.varianta_druhu_rizeni, (
        CASE
            WHEN ptpi.ico IS NOT NULL THEN 'Ano'::text
            ELSE 'Ne'::text
        END), pt.datum_uzavreni_smlouvy, pt.datum_vytvoreni_zakazky, pt.faze_zakazky, (
        CASE
            WHEN pto.odbor IS NOT NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END), (
        CASE
            WHEN pt.varianta_druhu_rizeni = 'Neveřejná výzva k podání nabídky'::text THEN s.spolecnosti_oslovene_k_podani_nabidky_nazev
            ELSE s2.nazev_smluvniho_partnera
        END), (
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END);
		
-- analytic.v_public_tenders_internal source

CREATE OR REPLACE VIEW analytic.v_public_tenders_internal
AS SELECT pt.id_zakazky,
    pt.nazev_zakazky,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
        CASE
            WHEN ptpi.ico IS NOT NULL THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.smluvni_cena_s_dph_kc > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.smluvni_cena_bez_dph_kc - NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_kc,
    (pt.smluvni_cena_bez_dph_kc - pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc) / NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_procent,
    pt.datum_uzavreni_smlouvy,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.datum_uzavreni_smlouvy - pt.datum_zahajeni_zadavaciho_rizeni AS delka_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu - pt.datum_zahajeni_zadavaciho_rizeni AS delka_lhuty_pro_podani_nabidek,
        CASE
            WHEN pt.hodnotici_kriteria ~~ '%100%'::text THEN 'Pouze cena'::text
            ELSE 'Více kritérií'::text
        END AS hodnotici_kriteria_typ,
    pt.externi_zastoupeni_zadavatele_v_rizeni,
    pt.externi_zastoupeni_zadavatele_v_rizeni_nazev,
    pt.externi_zastoupeni_zadavatele_v_rizeni_ico,
    "left"(pt.hlavni_nipez_kod_zakazky, 3) AS cpv_kod,
    pt.hlavni_nipez_kod_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.faze_zakazky,
        CASE
            WHEN pt.organizacni_jednotka_spravy ~~ ANY (ARRAY['%DNS%'::text, '%odbor dopravněsprávních činností%'::text, '%Městská Policie hl. m. Prahy%'::text, '%Archiv sekretariátů radních%'::text, '%odbor Archiv hl.m. Prahy%'::text, '%odbor bezpečnosti%'::text, '%odbor bytového fondu%'::text, '%odbor dopravy%'::text, '%odbor evidence majetku%'::text, '%odbor evropských fondů%'::text, '%odbor hospodaření s majetkem%'::text, '%odbor informatické infrastruktury%'::text, '%odbor informatických aplikací%'::text, '%odbor investiční%'::text, '%odbor Kancelář primátora%'::text, '%odbor Kancelář ředitele Magistrátu%'::text, '%odbor kontrolních činností%'::text, '%odbor kultury a cestovního ruchu%'::text, '%odbor legislativní a právní%'::text, '%Odbor médií a marketingu%'::text, '%odbor ochrany prostředí%'::text, '%odbor památkové péče%'::text, '%odbor personální%'::text, '%odbor pozemních komunikací a drah%'::text, '%odbor právní podpory%'::text, '%odbor projektového řízení%'::text, '%odbor rozpočtu%'::text, '%odbor služeb%'::text, '%odbor sociálních věcí MHMP%'::text, '%odbor stavebního řádu%'::text, '%odbor školství, mládeže a sportu%'::text, '%odbor územního rozvoje%'::text, '%odbor veřejných zakázek%'::text, '%odbor volených orgánů%'::text, '%odbor zdravotnictví%'::text, '%odbor živnostenský a občanskosprávní%'::text, '%sekretariát náměstka primátora%'::text, '%sekretariát radní HMP%'::text, '%sekretariát radního HMP%'::text, '%Útvar informačních a komunikačních technologií%'::text, '%Útvar provozního a logistického zabezpečení%'::text]) THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
    pt.updated_at,
    pt.created_at,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     LEFT JOIN python.public_tenders_odbory pto ON pt.organizacni_jednotka_spravy ~~ (('%'::text || pto.odbor::text) || '%'::text)
     LEFT JOIN python.public_tenders_partners_ico ptpi ON ptpi.ico::text = pt.ico_smluvniho_partnera
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text, 'Zrušena'::text])) AND (pt.datum_vytvoreni_zakazky >= '2020-09-01 01:00:00+02'::timestamp with time zone OR pt.datum_vytvoreni_zakazky >= '2021-01-01 00:00:00+01'::timestamp with time zone AND pto.odbor IS NULL) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));		