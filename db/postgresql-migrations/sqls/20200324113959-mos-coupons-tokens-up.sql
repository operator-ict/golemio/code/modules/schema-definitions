/*
úprava datové sady MOS_BE_*
	- rozšíření mos_be_coupons o nové atributy
	- zavedení nové tabulky mos_be_tokens
*/

-- evidence metadat
INSERT INTO meta.dataset(
	--	id_dataset, --nevyplňovat - autoincrement
	code_dataset,
	name_dataset,
	description,
	retention_days,
	h_retention_days,
	created_at
	)
	VALUES (
	--id_dataset,
	'mos_be',		--code_dataset,
	'mos dataset db MOS',	--name_dataset,
	'interní bd MOS',	--descr,
	null,			--retention_days,
	null,			--h_retention_days,
	now() 			--created_at
	)
ON CONFLICT DO NOTHING;



alter table public.mos_be_coupons
add created_by_id varchar(50),
add order_status integer,
add order_payment_type integer,
add token_id varchar(50);

COMMENT ON COLUMN public.mos_be_coupons.created_by_id IS 'CreatedByID - Uživatel který kupon vytvořil
-	1 - OICT
-	53 - DPP (eshop i přepážky - v budoucnu snad rozdělení právě pomocí nového uživatele na dva kanály)
-	188 - MobApp
';
COMMENT ON COLUMN public.mos_be_coupons.order_status IS 'Stav objednávky
-	1 - Ordered
-	2 - PaidByCard
-	3 - PaidByBankTransfer
-	4 - Cancelled
-	5 - PaidByCash';
COMMENT ON COLUMN public.mos_be_coupons.order_payment_type IS 'Způsob platby objednávky
-	1 - ByCard
-	2 - PaidByBankTransfer
-	3 - PaidByCardInOffice
-	4 - PaidByCashInOffice';
COMMENT ON COLUMN public.mos_be_coupons.token_id IS 'id tokenu - identifikátoru)';

create table public.mos_be_tokens (
	token_id varchar(50),
	identifier_type varchar(50),
	created timestamptz,
	---
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_tokens_pkey PRIMARY KEY (token_id)
);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract,
	id_dataset,
	id_db,
	description,
	schema_extract,
	retention_days,
	h_retention_days,
	created_at)

	VALUES (
	'mos_be_tokens',		-- name_extract,
	(select id_dataset from meta.dataset where code_dataset = 'mos_be'),		--;id_dataset,
	1,		--	1 - postgres, 2 mongodb_db	id_db,
	'tokeny - identifikátory',	--description,
	'public',	--schema_extract,
	null,	--retention_days,
	null,	--h_retention_days,
	now());

create or replace view analytic.v_ropidbi_coupon_tokens as
SELECT customer_profile_name,
		tariff_profile_name as tariff_name,
		case when seller_id = 1 then 'OICT' when seller_id = 29 then 'DPP' else 'jiný' end as seller,
		valid_from::date,
		case when t.identifier_type = 'MobApp' then 'Mobilní aplikace'
			 when t.identifier_type = 'BPK' then 'Platební karta'
			 when t.identifier_type = 'Litacka' then 'Lítačka'
			 when t.identifier_type = 'InKarta' then 'InKarta'
			 else 'jiný'
			 end as token_type,
		count (*) as coupon_count
FROM public.mos_be_coupons c
left join public.mos_be_tokens t on c.token_id = t.token_id
group by customer_profile_name,
		case when seller_id = 1 then 'OICT' when seller_id = 29 then 'DPP' else 'jiný' end,
		tariff_profile_name,
		valid_from::date,
		case when t.identifier_type = 'MobApp' then 'Mobilní aplikace'
			 when t.identifier_type = 'BPK' then 'Platební karta'
			 when t.identifier_type = 'Litacka' then 'Lítačka'
			 when t.identifier_type = 'InKarta' then 'InKarta'
			 else 'jiný'
		end;
