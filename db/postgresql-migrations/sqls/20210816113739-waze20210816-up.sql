/* Replace with your SQL commands */

drop view if exists public.v_waze_potholes_last_days;

create view public.v_waze_potholes_last_days as
SELECT DISTINCT ON (wa.uuid) wa.uuid,
    wa.city,
    wa.location,
    wa.street,
    wa.road_type,
    wa.magvar AS event_direction,
    wa.pub_utc_date at TIME zone 'Europe/Prague' AS published_at,
    wa.created_at at TIME zone 'Europe/Prague' AS last_reported,
    wa.reliability,
    wa.report_rating,
    wa.confidence
   FROM wazeccp_alerts wa
  WHERE wa.subtype = 'HAZARD_ON_ROAD_POT_HOLE'::text AND wa.pub_utc_date > (now()::date - '7 days'::interval)
  ORDER BY wa.uuid, wa.pub_utc_date, wa.created_at DESC;