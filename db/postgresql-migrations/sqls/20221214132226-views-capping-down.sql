DROP VIEW analytic.v_ropidbi_capping_agregation;

DROP VIEW analytic.v_ropidbi_capping_daily_counts;

DROP VIEW analytic.v_ropidbi_capping_distinct_accounts;

DROP VIEW analytic.v_ropidbi_capping_more_rides;

CREATE OR REPLACE VIEW analytic.v_ropidbi_capping
AS SELECT m.account_id,
    m.cptp,
    m.date,
    m.duration,
    m.lat AS misto_nakupu_lat,
    m.lon AS misto_nakupu_lon,
    m.tariff_id,
    m.tariff_name,
    m.ticket_id,
    m.zone_count,
    a.date AS datum_aktivace,
    a.zones AS zona_aktivace,
    a.type,
    a.lat AS misto_aktivace_lat,
    a.lon AS misto_aktivace_lon
   FROM mos_ma_ticketpurchases m
     LEFT JOIN mos_ma_ticketactivations a ON m.ticket_id = a.ticket_id
  WHERE m.date >= '2022-04-01 00:00:00+02'::timestamp with time zone;

  CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_more_rides
AS SELECT v.date::date AS date,
    v.cptp,
    v.account_id,
    v.n_ticket
   FROM ( SELECT mmt.cptp,
            count(mmt.ticket_id) AS n_ticket,
            mmt.account_id,
            mmt.date,
            row_number() OVER (PARTITION BY (mmt.date::date), mmt.account_id) AS row_number
           FROM mos_ma_ticketpurchases mmt
          WHERE mmt.cptp = 124 OR mmt.cptp = 624
          GROUP BY mmt.account_id, mmt.date, mmt.cptp) v
  WHERE v.row_number = 1;
  