-- App Store

INSERT INTO meta.dataset(
	--	id_dataset, --nevyplňovat - autoincrement
	code_dataset,
	name_dataset,
	description,
	retention_days,
	h_retention_days,
	created_at
	)
	VALUES (
	--id_dataset,
	'mobileappstatistics',		--code_dataset,
	'mobileappstatistics',	--name_dataset,
	'statistická data z mobilních aplikací',	--descr,
	null,			--retention_days,
	null,			--h_retention_days,
	now() 			--created_at
	)
ON CONFLICT DO NOTHING;

create table public.mobileappstatistics_appstore (
	begin_day 	date not null,
	app_id		varchar(50) not null,
	app_name 	varchar(50) null,
	version		varchar(50) null,
	event_type	varchar(50) null,
	device		varchar(50) null,
	country		varchar(50) null,
	event_count int,
	CONSTRAINT mobileappstatistics_appstore_pk PRIMARY KEY (begin_day,app_id,app_name,version,event_type,device,country)
);
CREATE INDEX idx_mobileappstatistics_appstore_begin_day ON public.mobileappstatistics_appstore USING btree (begin_day);

SELECT meta.add_audit_fields('public','mobileappstatistics_appstore');

COMMENT ON TABLE public.mobileappstatistics_appstore IS 'data z App Store';
COMMENT ON column public.mobileappstatistics_appstore.begin_day IS 'den za který máme data';
COMMENT ON column public.mobileappstatistics_appstore.app_id IS 'ID aplikace';
COMMENT ON column public.mobileappstatistics_appstore.app_name IS 'název aplikace';
COMMENT ON column public.mobileappstatistics_appstore.version IS 'verze aplikace';
COMMENT ON column public.mobileappstatistics_appstore.event_type IS 'id které určujě zda jde o placenou app/update apod';
COMMENT ON column public.mobileappstatistics_appstore.device IS 'typ zařízení na které bylo instalováno';
COMMENT ON column public.mobileappstatistics_appstore.country IS 'země původu uživatele';
COMMENT ON column public.mobileappstatistics_appstore.event_count IS 'počet unikátních kliknutí na button “Buy” or “Get”';

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract,
	id_dataset,
	id_db,
	description,
	schema_extract,
	retention_days,
	h_retention_days,
	created_at)

	VALUES (
	'mobileappstatistics_appstore',		-- name_extract,
	(select id_dataset from meta.dataset where code_dataset = 'mobileappstatistics'),		--;id_dataset,
	1,		--	1 - postgres, 2 mongodb_db	id_db,
	'data z mobile App Store a podobné',	--description,
	'public',	--schema_extract,
	null,	--retention_days,
	null,	--h_retention_days,
	now());



-- Play Store

create table public.mobileappstatistics_playstore (
	begin_day date NOT NULL,
	package_name character varying(256) NOT NULL,
	daily_device_installs integer,
	daily_user_installs integer,
	install_events integer,
	CONSTRAINT mobileappstatistics_playstore_pk PRIMARY KEY (begin_day, package_name)
);
CREATE INDEX idx_mobileappstatistics_playstore_begin_day ON public.mobileappstatistics_playstore USING btree (begin_day);

SELECT meta.add_audit_fields('public','mobileappstatistics_playstore');

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract,
	id_dataset,
	id_db,
	description,
	schema_extract,
	retention_days,
	h_retention_days,
	created_at)

	VALUES (
	'mobileappstatistics_playstore',		-- name_extract,
	(select id_dataset from meta.dataset where code_dataset = 'mobileappstatistics'),		--;id_dataset,
	1,		--	1 - postgres, 2 mongodb_db	id_db,
	'data z mobile Play Store a podobné',	--description,
	'public',	--schema_extract,
	null,	--retention_days,
	null,	--h_retention_days,
	now());

