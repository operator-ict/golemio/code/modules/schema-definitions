

drop TABLE IF EXISTS public.containers_opening_monitor;
drop TABLE IF EXISTS public.containers_measurement;
drop table IF EXISTS public.containers_containers;
drop TABLE IF EXISTS public.containers_picks;
drop TABLE IF EXISTS public.containers_scenar_items;
drop TABLE IF EXISTS public.containers_stations;
drop TABLE IF EXISTS public.containers_picks_dates;


DROP TABLE IF EXISTS public.containers_ksnko_notifications;

DROP INDEX IF EXISTS public.containers_picks_pick_at_utc;
DROP INDEX IF EXISTS public.containers_picks_station_code;
DROP INDEX IF EXISTS public.containers_measurement_measured_at_utc;
DROP INDEX IF EXISTS public.containers_measurement_station_code;
DROP INDEX IF EXISTS public.containers_stations_id;

drop index IF EXISTS public.containers_containers_station_code;
drop index IF EXISTS public.containers_containers_trash_type;