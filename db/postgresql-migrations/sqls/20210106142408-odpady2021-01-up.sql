--překladová tabulka MČ
create table analytic.containers_districts (
        district varchar(50),
        district_cz varchar (50) 
        );
comment on column analytic.containers_districts.district_cz is 'Jde o přidruženou tabulku s českým přepisem názvů městských částí (napsat funkci, která správně opraví interpunkci, nejde). Zdrojové csv se nachází na: https://operator.sharepoint.com/:x:/s/Datova_Platforma/EVIvMiZGvm1Ep4d5naJ5x3UBLrZctzUMuxt7RGgMn2VD9A?e=H3bDWz';


-- kontejnery
create or replace view analytic.v_containers_containers as
select cc.code as container_id,
        cc.total_volume,
        cc.network,
        cc.container_type,
        cc.bin_type,
        case when cc.trash_type = 1 then 'glass coloured'
            when cc.trash_type = 2 then 'electronics'
            when cc.trash_type = 3 then 'metal'
            when cc.trash_type = 4 then 'bevarage cartons'
            when cc.trash_type = 5 then 'paper'
            when cc.trash_type = 6 then 'plastic'
            when cc.trash_type = 7 then 'glass clear'
            when cc.trash_type = 8 then 'textile'
            else null end as trash_type,
        case when cc.trash_type = 1 then 'Sklo čiré'
            when cc.trash_type = 2 then 'Elektronika'
            when cc.trash_type = 3 then 'Kovy'
            when cc.trash_type = 4 then 'Nápojové kartony'
            when cc.trash_type = 5 then 'Papír'
            when cc.trash_type = 6 then 'Plast'
            when cc.trash_type = 7 then 'Sklo barevné'
            when cc.trash_type = 8 then 'Textil'
            else null end as typ_odpadu,
        case when cc.trash_type = 1 then 411
            when cc.trash_type = 3 then 120
            when cc.trash_type = 4 then 55
            when cc.trash_type = 5 then 76
            when cc.trash_type = 6 then 39
            when cc.trash_type = 7 then 411
            else null end as koeficient,
        cc.cleaning_frequency_frequency,
        cc.cleaning_frequency_interval,
        cs.latitude,
        cs.longitude,
        case when cs.address = ' Francouzská  240/76' then substring(cs.address from 2)
            else cs.address end as address,
        cd.district_cz,
        concat(1,right(split_part(cs.district, '-', 2),1),'000') as psc
from public.containers_containers as cc
left join public.containers_stations as cs on cc.station_code = cs.code
left join analytic.containers_districts as cd on cs.district = cd.district
where cc.code in (select distinct container_code from containers_measurement where percent_calculated is not null);

comment on column analytic.v_containers_containers.district_cz is 'Jde o přidruženou tabulku s českým přepisem názvů městských částí (napsat funkci, která správně opraví interpunkci, nejde). Zdrojové csv se nachází na: https://operator.sharepoint.com/:x:/s/Datova_Platforma/EVIvMiZGvm1Ep4d5naJ5x3UBLrZctzUMuxt7RGgMn2VD9A?e=H3bDWz';

-- měření
create or replace view analytic.v_containers_measurements as
select container_code,
        station_code,
        measured_at_utc,
        measured_at_utc::date as measured_at_utc_date,
        split_part(measured_at_utc::varchar(50) , ' ', 2) as measured_at_utc_time,
        percent_calculated,
        (coalesce(percent_calculated::decimal, 0)) / 100 as percent_calculated_decimal
from public.containers_measurement
where container_code in (select distinct code from containers_containers);


-- svozy
create or replace view analytic.v_containers_picks as
select 
    container_code,
    cc.total_volume,
    case when cc.trash_type = 1 then 411
            when cc.trash_type = 3 then 120
            when cc.trash_type = 4 then 55
            when cc.trash_type = 5 then 76
            when cc.trash_type = 6 then 39
            when cc.trash_type = 7 then 411
    else null end as koeficient,
    pick_at_utc,
    percent_before,
    percent_now,
    event_driven
from public.containers_picks cp
join public.containers_containers cc on cp.container_code = cc.code
where container_code in (select distinct code from containers_containers);

-- v_containers_full
CREATE OR REPLACE VIEW analytic.v_containers_full
AS WITH a AS (
         SELECT mer.container_code as container_id,
            lag(mer.percent_calculated) OVER (PARTITION BY mer.container_code ORDER BY mer.measured_at) AS pre_x,
            mer.percent_calculated AS actual,
            lead(mer.percent_calculated) OVER (PARTITION BY mer.container_code ORDER BY mer.measured_at) AS next_x,
            mer.measured_at
           FROM public.containers_measurement mer
        ), b AS (
         SELECT row_number() OVER (PARTITION BY a.container_id ORDER BY a.measured_at) AS rn,
                CASE
                    WHEN a.actual >= 95 AND a.pre_x < 95 THEN 'zacatek'::text
                    WHEN a.actual < 95 AND a.pre_x >= 95 THEN 'konec'::text
                    ELSE NULL::text
                END AS typ_zaznamu,
            a.container_id,
            a.pre_x,
            a.actual,
            a.next_x,
            a.measured_at
           FROM a
          WHERE a.actual >= 95 AND a.pre_x < 95 OR a.actual < 95 AND a.pre_x >= 95
        ), c AS (
         SELECT lag(b.measured_at) OVER (PARTITION BY b.container_id ORDER BY b.rn) AS pre_time,
            b.rn,
            b.typ_zaznamu,
            b.container_id,
            b.pre_x,
            b.actual,
            b.next_x,
            b.measured_at
           FROM b
        ), pocatek AS (
         SELECT containers_measurement.container_code as container_id,
            min(containers_measurement.measured_at) AS pocatek
           FROM public.containers_measurement
          GROUP BY containers_measurement.container_code
        )
 SELECT c.container_id,
    COALESCE(c.pre_time, pocatek.pocatek) AS start_time,
    c.measured_at AS end_time,
    round((date_part('epoch'::text, c.measured_at) - date_part('epoch'::text, COALESCE(c.pre_time, pocatek.pocatek))) / 60::double precision) AS trvani_mi
   FROM c
     LEFT JOIN pocatek ON pocatek.container_id = c.container_id
  WHERE c.typ_zaznamu = 'konec'::text;
  
  
-- containers_full_hour
CREATE TABLE public.containers_full_hour (
    container_id varchar(50) NOT NULL,
    full_hour timestamp NOT NULL,
    begin_full timestamp NULL,
    end_full timestamp NULL,
    CONSTRAINT containers_full_hod_pkey PRIMARY KEY (container_id, full_hour)
);

CREATE OR REPLACE FUNCTION analytic.set_containers_full_hour()
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare
    pocet integer;
    query_rec record;
    cas timestamp;
    poc timestamp;
    kon timestamp;
    poc_full timestamp;
    kon_full timestamp;
    id integer;
begin
    truncate table public.containers_full_hour;
    FOR  query_rec IN
        select 
        container_id,
        date_trunc('hour',start_time) pocatek,
        date_trunc('hour',end_time) konec,
        start_time poc_full, end_time kon_full
        from analytic.v_containers_full
    loop
        cas := query_rec.pocatek;
        
        LOOP
        -- some computations
            insert into public.containers_full_hour (
                container_id,
                full_hour,
                begin_full,
                end_full
            )
            values (
                query_rec.container_id,
                cas,
                query_rec.poc_full,
                query_rec.kon_full)
            on conflict (container_id, full_hour) do nothing;
            cas := cas + (interval '1 hour');
            EXIT WHEN cas >= query_rec.konec;
        END LOOP;
    END LOOP;
end;
$function$
;
COMMENT ON FUNCTION analytic.set_containers_full_hour() IS 'Fce vyplňuje tabulku containers_full_hour';
--

CREATE OR REPLACE VIEW analytic.v_containers_full_share as
    WITH calendar AS (
         SELECT generate_series((select min(measured_at_utc)::date from public.containers_measurement),--'2019-04-04'::date::timestamp with time zone, 
                                now() - '1 day'::interval, '1 day'::interval) AS xday
        ), 
        xx AS (
         SELECT calendar.xday,
            kont.code as id
           FROM public.containers_containers kont, calendar
           where kont.code in (select distinct container_code from public.containers_measurement where percent_calculated is not null)
        ), 
        ss AS (
         SELECT xx.id,
            xx.xday,
            zaplnenost.full_hour AS zaplneno_hod
           FROM xx
             LEFT JOIN public.containers_full_hour zaplnenost ON zaplnenost.container_id = xx.id AND xx.xday = zaplnenost.full_hour::date
        )
         SELECT ss.id AS container_id,
                ss.xday AS measurement_day,
                count(ss.zaplneno_hod) AS hour_count
         FROM ss
         GROUP BY ss.id, ss.xday;
        
create or replace view analytic.v_containers_24h_missing as
select container_code as container_id,
        MAX(measured_at)  measured_at 
from  public.containers_measurement
where measured_at  < current_date - 1 or measured_at is null
group by container_code;


CREATE OR REPLACE FUNCTION analytic.container_last_correct(p_continer_code text)
 RETURNS timestamp without time zone
 LANGUAGE plpgsql
AS $function$
declare 
    posledni integer;
begin
    posledni = (
        select percent_calculated   
        from public.containers_measurement
        where container_code = p_continer_code
        order by measured_at desc
        limit 1);
    return (
    select measured_at          
    from public.containers_measurement
    where container_code = p_continer_code
--  and not percent_calculated between p_min and p_max
    and not percent_calculated between posledni-3 and posledni+3
    order by measured_at desc
    limit 1);
end;
$function$
;

CREATE OR REPLACE VIEW analytic.v_containers_5d_same
AS SELECT s.container_id,
    s.min_percent,
    s.max_percent,
    analytic.container_last_correct(s.container_id::text) AS last_correct
   FROM ( SELECT containers_measurement.container_code AS container_id,
            min(containers_measurement.percent_calculated) AS min_percent,
            max(containers_measurement.percent_calculated) AS max_percent
           FROM public.containers_measurement
          WHERE containers_measurement.measured_at > (CURRENT_DATE - 5) AND NOT (containers_measurement.container_code::text IN ( SELECT "24h_missing".container_id
                   FROM ( SELECT containers_measurement_1.container_code AS container_id,
                            max(containers_measurement_1.measured_at) AS measured_at
                           FROM public.containers_measurement containers_measurement_1
                          GROUP BY containers_measurement_1.container_code
                         HAVING COALESCE(max(containers_measurement_1.measured_at), '2000-01-01 00:00:00'::timestamp without time zone) < (CURRENT_DATE - 1)) "24h_missing"))
          GROUP BY containers_measurement.container_code
         HAVING abs(min(containers_measurement.percent_calculated) - max(containers_measurement.percent_calculated)) <= 3) s;

		 
create or replace view analytic.v_containers_next_pick as
select cc.code as container_id, min(pick_date) as next_pick
from containers_picks_dates as pd
join containers_containers as cc on pd.container_id = cc.id
where pick_date > now() and cc.code in (select distinct container_code
                    from containers_measurement)
group by cc.code;

-- Permissions
/*
GRANT ALL ON analytic.v_containers_picks TO read_only;
GRANT ALL ON analytic.v_containers_containers TO read_only;
GRANT ALL ON analytic.v_containers_measurements TO read_only;
ALTER TABLE analytic.v_containers_full OWNER TO spravce;
GRANT ALL ON TABLE analytic.v_containers_full TO spravce;
GRANT ALL ON TABLE analytic.v_containers_full TO postgres;
GRANT SELECT ON TABLE analytic.v_containers_full TO read_only;
ALTER TABLE public.containers_full_hour OWNER TO spravce;
GRANT ALL ON TABLE public.containers_full_hour TO spravce;
GRANT SELECT ON TABLE public.containers_full_hour TO read_only;
ALTER TABLE analytic.v_containers_full_share OWNER TO spravce;
GRANT ALL ON TABLE analytic.v_containers_full_share TO spravce;
GRANT ALL ON TABLE analytic.v_containers_full_share TO postgres;
GRANT SELECT ON TABLE analytic.v_containers_full_share TO read_only;
GRANT SELECT ON TABLE analytic.v_containers_24h_missing TO read_only;
GRANT SELECT ON TABLE analytic.v_containers_5d_same TO read_only;
ALTER TABLE tmp.v_containers_5d_same OWNER TO postgres;
GRANT ALL ON TABLE tmp.v_containers_5d_same TO postgres;
GRANT SELECT, REFERENCES ON TABLE tmp.v_containers_5d_same TO dataplatform_tmp_ro;
GRANT ALL ON TABLE tmp.v_containers_5d_same TO dataplatform_tmp_rw;
GRANT ALL ON analytic.v_containers_next_pick TO read_only;
*/