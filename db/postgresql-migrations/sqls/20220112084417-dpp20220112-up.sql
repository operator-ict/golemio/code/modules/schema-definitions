-- vitekzkytek.dpp_travellers_bus_daily definition

CREATE TABLE python.dpp_travellers_bus_daily (
	datum timestamp NULL,
	pocet_cestujicich_ppc int8 NULL,
	podil_k_trvalemu_stavu float8 NULL,
	statisticky_vzorek_poctu_cestujicich_apc int8 NULL,
	podil_dat_apc float8 NULL
);

-- vitekzkytek.dpp_travellers_tram_daily definition

CREATE TABLE python.dpp_travellers_tram_daily (
	datum timestamp NULL,
	pocet_cestujicich_ppc int8 NULL,
	podil_k_trvalemu_stavu float8 NULL,
	statisticky_vzorek_poctu_cestujicich_apc int8 NULL,
	podil_dat_apc float8 NULL
);

-- vitekzkytek.dpp_travellers_metro_daily_details definition

CREATE TABLE python.dpp_travellers_metro_stations_daily (
	obdobi timestamp NULL,
	vstup float8 NULL,
	vystup float8 NULL,
	obrat float8 NULL,
	sheet_name text NULL,
	metro_name text NULL,
	entrance_name text NULL,
	sheet_no int8 NULL,
	excel_name text NULL,
	line text NULL
);


CREATE OR REPLACE VIEW analytic.v_dpp_metro_covid_comparison
AS WITH pre_covid AS (
         SELECT pre.metro_name,
            avg(pre.obrat) AS obrat
           FROM python.dpp_travellers_metro_stations_daily pre
          WHERE pre.obdobi < '2020-01-01 00:00:00'::timestamp without time zone
          GROUP BY pre.metro_name
        ), post_covid AS (
         SELECT date_trunc('Month'::text, post.obdobi) AS month,
            post.metro_name,
            avg(post.obrat) AS obrat
           FROM python.dpp_travellers_metro_stations_daily post
          WHERE post.obdobi >= '2020-01-01 00:00:00'::timestamp without time zone
          GROUP BY (date_trunc('Month'::text, post.obdobi)), post.metro_name
        )
 SELECT post_covid.month,
    post_covid.metro_name,
    post_covid.obrat,
    post_covid.obrat / pre_covid.obrat AS share_of_pre_covid
   FROM post_covid
     LEFT JOIN pre_covid ON post_covid.metro_name = pre_covid.metro_name
  ORDER BY post_covid.month;
