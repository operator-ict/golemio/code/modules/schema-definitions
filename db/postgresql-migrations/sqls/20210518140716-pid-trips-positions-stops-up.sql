ALTER TABLE "public"."vehiclepositions_positions"
    ADD COLUMN "this_stop_sequence" integer;

ALTER TABLE "public"."vehiclepositions_stops"
    ADD COLUMN "stop_id" character varying(50),
    ADD COLUMN "stop_sequence" integer,
    ADD COLUMN "arrival_timestamp_real" bigint,
    ADD COLUMN "departure_timestamp_real" bigint;

ALTER TABLE "public"."vehiclepositions_trips"
    ADD COLUMN "is_canceled" boolean,
    ADD COLUMN "end_timestamp" bigint;


DROP MATERIALIZED VIEW "public"."v_ropidgtfs_trips_minmaxsequences";
CREATE MATERIALIZED VIEW "public"."v_ropidgtfs_trips_minmaxsequences" AS 
    SELECT ropidgtfs_stop_times.trip_id,
        max(ropidgtfs_stop_times.stop_sequence) AS max_stop_sequence,
        min(ropidgtfs_stop_times.stop_sequence) AS min_stop_sequence,
        greatest(max(ropidgtfs_stop_times.arrival_time::INTERVAL), max(ropidgtfs_stop_times.departure_time::INTERVAL))::TEXT AS max_stop_time,
        least(min(ropidgtfs_stop_times.arrival_time::INTERVAL), min(ropidgtfs_stop_times.departure_time::INTERVAL))::TEXT AS min_stop_time
    FROM ropidgtfs_stop_times
    GROUP BY ropidgtfs_stop_times.trip_id;
