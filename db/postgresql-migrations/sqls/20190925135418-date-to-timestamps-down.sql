DROP VIEW IF EXISTS v_vehiclepositions_last_position;

--vehiclepositions_trips column start_timestamp
ALTER TABLE public.vehiclepositions_trips
ALTER COLUMN start_timestamp TYPE timestamp with time zone
USING to_timestamp(start_timestamp/1000);

--vehiclepositions_positions column origin_timestamp
ALTER TABLE public.vehiclepositions_positions
ALTER COLUMN origin_timestamp TYPE timestamp with time zone
USING to_timestamp(origin_timestamp/1000);

--vehiclepositions_stops column arrival_timestamp
ALTER TABLE public.vehiclepositions_stops
ALTER COLUMN arrival_timestamp TYPE timestamp with time zone
USING to_timestamp(arrival_timestamp/1000);

--vehiclepositions_stops column departure_timestamp
ALTER TABLE public.vehiclepositions_stops
ALTER COLUMN departure_timestamp TYPE timestamp with time zone
USING to_timestamp(departure_timestamp/1000);

CREATE OR REPLACE VIEW v_vehiclepositions_last_position
  AS
    SELECT
      DISTINCT ON ("vehiclepositions_positions"."trips_id") "vehiclepositions_positions".*
    FROM "vehiclepositions_positions" AS "vehiclepositions_positions"
      JOIN "vehiclepositions_trips" AS "vehiclepositions_trips"
        ON ("vehiclepositions_trips"."id" = "vehiclepositions_positions"."trips_id" AND
            "vehiclepositions_trips"."gtfs_trip_id" IS NOT NULL)
    WHERE "vehiclepositions_positions"."created_at" > (NOW() - INTERVAL '5 min') AND "vehiclepositions_positions"."delay" IS NOT NULL
    ORDER BY "vehiclepositions_positions"."trips_id", "vehiclepositions_positions"."created_at" DESC;
