--helping table:lkpr_dashboard_route_names
CREATE TABLE analytic.lkpr_dashboard_route_names (
	route_type text NULL,
	route_num text NULL,
	route_group_name text NULL,
	order_idx int4 NULL
);


-- iniciální load
INSERT 
	INTO analytic.lkpr_dashboard_route_names 
		 (route_type,route_num,route_group_name,order_idx) 
	VALUES
		 ('TN','01','Podbaba => Evropská => letiště',10),
		 ('TN','02','Vítězné nám. => Evropská => letiště',9),
		 ('TN','03','Letenské náměstí => Hradčanská => Evropská => letiště',7),
		 ('TN','04','Letenské náměstí => Bubeneč => Evropská => letiště',8),
		 ('TN','05','Smíchov => Horní Liboc => letiště',3),
		 ('TN','06','Motol => Horní Liboc => letiště',2),
		 ('TN','07','Letenské náměstí => Karlovarská => letiště',6),
		 ('TN','08','Smíchov => Karlovarská => letiště',4),
		 ('TN','09','Klárov => Karlovarská => letiště',5),
		 ('TN','10','Zličín/Řepy => Drnovská => letiště',1),
		 ('TN','11','Podbaba => Horoměřice => letiště',11);

	
	
CREATE VIEW analytic.v_lkpr_route_details
AS SELECT re.id,
    re.name,
    re.route_code,
    re.route_name,
    re.from_name,
    re.to_name,
    re.route_type,
    re.route_num,
    re.route_variant,
        CASE
            WHEN re.route_variant = 'A1'::text THEN 'Na letiště'::text
            WHEN re.route_variant = 'B1'::text THEN 'Z letiště'::text
            ELSE 'Ostatní'::text
        END AS direction,
    drn.route_group_name,
    drn.order_idx
   FROM ( SELECT rc.id,
            rc.name,
            rc.route_code,
            rc.route_name,
            rc.from_name,
            rc.to_name,
            split_part(rc.route_code, '-'::text, 1) AS route_type,
            split_part(rc.route_code, '-'::text, 2) AS route_num,
            split_part(rc.route_code, '-'::text, 3) AS route_variant
           FROM ( SELECT r.id,
                    r.name,
                    split_part(r.name, ' '::text, 1) AS route_code,
                    "substring"(r.name, "position"(r.name, ' '::text) + 1) AS route_name,
                    r.from_name,
                    r.to_name
                   FROM public.wazett_routes r
                  --WHERE r.feed_id = 0
				  ) rc) re
     LEFT JOIN analytic.lkpr_dashboard_route_names drn ON re.route_type = drn.route_type AND re.route_num = drn.route_num
  ORDER BY drn.order_idx;

