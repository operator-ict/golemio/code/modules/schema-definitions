-- franz.openproject_mhmp_projects definition

CREATE TABLE python.openproject_mhmp_projects (
	id int8 NULL,
	identifier text NULL,
	project_name text NULL,
	active bool NULL,
	public bool NULL,
	created_at timestamp NULL,
	updated_at timestamp NULL,
	project_start timestamp NULL,
	project_end timestamp NULL,
	supplier_name text NULL,
	budget_expected float8 NULL,
	budget_reality float8 NULL,
	council_rule_number text NULL,
	contract_number text NULL,
	tender_number text NULL,
	project_description text NULL,
	status_explanation text NULL,
	council_rule_url text NULL,
	contract_registry_url text NULL,
	tender_url text NULL,
	hashtag text NULL,
	solver_name text NULL,
	solver_id int4 NULL,
	mhmp_responsible_id int4 NULL,
	mhmp_responsible_name text NULL,
	councilor_id int4 NULL,
	councilor_name text NULL,
	parent_id int4 NULL,
	status text NULL,
	parent_project text NULL
);

-- franz.openproject_mhmp_wps definition

CREATE TABLE python.openproject_mhmp_wps (
	derived_start_date timestamp NULL,
	derived_due_date timestamp NULL,
	spent_time text NULL,
	labor_costs text NULL,
	material_costs text NULL,
	overall_costs text NULL,
	type_of_object text NULL,
	wp_id int8 NULL,
	wp_name text NULL,
	start_date timestamp NULL,
	due_date timestamp NULL,
	estimated_time text NULL,
	derived_estimated_time text NULL,
	percentage_done int8 NULL,
	created_at timestamp NULL,
	updated_at timestamp NULL,
	story_points float8 NULL,
	remaining_time text NULL,
	supplier_name text NULL,
	weight float8 NULL,
	contract_number text NULL,
	tender_number text NULL,
	council_rule_number text NULL,
	media_url text NULL,
	media_out_date timestamp NULL,
	wp_description text NULL,
	tender_url text NULL,
	contract_registry_url text NULL,
	council_rule_url text NULL,
	wp_type text NULL,
	priority text NULL,
	project_id int4 NULL,
	project_name text NULL,
	wp_status text NULL,
	author_id int4 NULL,
	author_name text NULL,
	responsible_id int4 NULL,
	responsible_name text NULL,
	assignee_id int4 NULL,
	assignee_name text NULL,
	department_resp_name text NULL,
	department_resp_id int4 NULL,
	supplier_resp_id int4 NULL,
	supplier_resp_name text NULL,
	parent_id int4 NULL,
	parent_name text NULL,
	completion_date timestamp NULL
);

-- franz.v_openproject_mhmp_domain source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_domain
AS SELECT openproject_mhmp_projects.id,
    openproject_mhmp_projects.project_name AS domain_name,
    openproject_mhmp_projects.councilor_name
   FROM python.openproject_mhmp_projects
  WHERE openproject_mhmp_projects.parent_id = 3;
  
  
-- franz.v_openproject_mhmp_projects source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_projects
AS WITH domain AS (
         SELECT openproject_mhmp_projects.id
           FROM python.openproject_mhmp_projects
          WHERE openproject_mhmp_projects.parent_id = 3
        ), topic AS (
         SELECT openproject_mhmp_projects.id
           FROM python.openproject_mhmp_projects
          WHERE (openproject_mhmp_projects.parent_id IN ( SELECT domain.id
                   FROM domain))
        ), projects AS (
         SELECT openproject_mhmp_projects.id,
            openproject_mhmp_projects.project_name,
            openproject_mhmp_projects.active,
            openproject_mhmp_projects.public,
            openproject_mhmp_projects.project_description,
            openproject_mhmp_projects.status,
            openproject_mhmp_projects.status_explanation,
            openproject_mhmp_projects.project_start,
            openproject_mhmp_projects.project_end,
            openproject_mhmp_projects.supplier_name,
            openproject_mhmp_projects.hashtag,
            openproject_mhmp_projects.budget_expected,
            openproject_mhmp_projects.budget_reality,
            openproject_mhmp_projects.council_rule_number,
            openproject_mhmp_projects.council_rule_url,
            openproject_mhmp_projects.contract_number,
            openproject_mhmp_projects.contract_registry_url,
            openproject_mhmp_projects.tender_number,
            openproject_mhmp_projects.tender_url,
            openproject_mhmp_projects.solver_name,
            openproject_mhmp_projects.councilor_name,
            openproject_mhmp_projects.mhmp_responsible_name,
            openproject_mhmp_projects.parent_id,
            openproject_mhmp_projects.parent_project,
            openproject_mhmp_projects.updated_at
           FROM python.openproject_mhmp_projects
          WHERE (openproject_mhmp_projects.parent_id IN ( SELECT topic.id
                   FROM topic))
        ), subprojects AS (
         SELECT sp.id,
            sp.project_name,
            sp.active,
            sp.public,
            sp.project_description,
            sp.status,
            sp.status_explanation,
            sp.project_start,
            sp.project_end,
            sp.supplier_name,
            sp.hashtag,
            sp.budget_expected,
            sp.budget_reality,
            sp.council_rule_number,
            sp.council_rule_url,
            sp.contract_number,
            sp.contract_registry_url,
            sp.tender_number,
            sp.tender_url,
            sp.solver_name,
            sp.councilor_name,
            sp.mhmp_responsible_name,
            p.parent_id,
            sp.parent_project,
            sp.updated_at
           FROM python.openproject_mhmp_projects sp
             LEFT JOIN projects p ON sp.parent_id = p.id
          WHERE (sp.parent_id IN ( SELECT projects.id
                   FROM projects))
        ), next_lvl AS (
         SELECT projects.id,
            projects.project_name,
            projects.active,
            projects.public,
            projects.project_description,
            projects.status,
            projects.status_explanation,
            projects.project_start,
            projects.project_end,
            projects.supplier_name,
            projects.hashtag,
            projects.budget_expected,
            projects.budget_reality,
            projects.council_rule_number,
            projects.council_rule_url,
            projects.contract_number,
            projects.contract_registry_url,
            projects.tender_number,
            projects.tender_url,
            projects.solver_name,
            projects.councilor_name,
            projects.mhmp_responsible_name,
            projects.parent_id,
            projects.parent_project,
            projects.updated_at
           FROM projects
        UNION ALL
         SELECT subprojects.id,
            subprojects.project_name,
            subprojects.active,
            subprojects.public,
            subprojects.project_description,
            subprojects.status,
            subprojects.status_explanation,
            subprojects.project_start,
            subprojects.project_end,
            subprojects.supplier_name,
            subprojects.hashtag,
            subprojects.budget_expected,
            subprojects.budget_reality,
            subprojects.council_rule_number,
            subprojects.council_rule_url,
            subprojects.contract_number,
            subprojects.contract_registry_url,
            subprojects.tender_number,
            subprojects.tender_url,
            subprojects.solver_name,
            subprojects.councilor_name,
            subprojects.mhmp_responsible_name,
            subprojects.parent_id,
            subprojects.parent_project,
            subprojects.updated_at
           FROM subprojects
        )
 SELECT nl.id,
    nl.project_name,
    nl.active,
    nl.public,
    nl.project_description,
    nl.status,
    nl.status_explanation,
    nl.project_start::timestamp with time zone AS project_start,
    nl.project_end::timestamp with time zone AS project_end,
    nl.supplier_name,
    nl.hashtag,
    nl.budget_expected,
    nl.budget_reality,
    nl.council_rule_number,
    nl.council_rule_url,
    nl.contract_number,
    nl.contract_registry_url,
    nl.tender_number,
    nl.tender_url,
    nl.solver_name,
    nl.councilor_name,
    nl.mhmp_responsible_name,
    nl.parent_id,
    nl.parent_project,
    COALESCE(nl.updated_at::timestamp with time zone, w.wps_up_at::timestamp with time zone) AS updated_at,
        CASE
            WHEN nl.project_start::timestamp with time zone IS NULL AND nl.project_end::timestamp with time zone IS NULL THEN 0
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[4::double precision, 5::double precision, 6::double precision, 7::double precision, 8::double precision, 9::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 4, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 4, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 10, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 3, 31)::date) THEN 1
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[1::double precision, 2::double precision, 3::double precision, 10::double precision, 11::double precision, 12::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200), 9, 30)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200), 9, 30)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 3, 31)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200), 10, 1)::date) THEN 1
            ELSE 0
        END AS this_period,
        CASE
            WHEN nl.project_start::timestamp with time zone IS NULL AND nl.project_end::timestamp with time zone IS NULL THEN 0
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[4::double precision, 5::double precision, 6::double precision, 7::double precision, 8::double precision, 9::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 9, 30)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 3, 31)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE), 10, 1)::date) THEN 1
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[1::double precision, 2::double precision, 3::double precision, 10::double precision, 11::double precision, 12::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 9, 30)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 4, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 9, 30)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 10, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200), 3, 31)::date) THEN 1
            ELSE 0
        END AS next_period,
        CASE
            WHEN nl.project_start::timestamp with time zone IS NULL AND nl.project_end::timestamp with time zone IS NULL THEN 0
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[4::double precision, 5::double precision, 6::double precision, 7::double precision, 8::double precision, 9::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 9, 30)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 4, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 9, 30)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 10, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE) + 1::double precision, 3, 31)::date) THEN 1
            WHEN (date_part('month'::text, CURRENT_DATE) = ANY (ARRAY[1::double precision, 2::double precision, 3::double precision, 10::double precision, 11::double precision, 12::double precision])) AND (nl.project_start::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200) + 1::double precision, 10, 1)::date AND nl.project_start::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200) + 1::double precision, 3, 31)::date OR nl.project_end::timestamp with time zone >= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200) + 1::double precision, 10, 1)::date AND nl.project_end::timestamp with time zone <= format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200) + 1::double precision, 3, 31)::date OR nl.project_start::timestamp with time zone < format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE + 200) + 1::double precision, 4, 1)::date AND nl.project_end::timestamp with time zone > format('%s-%s-%s'::text, date_part('year'::text, CURRENT_DATE - 200) + 1::double precision, 9, 30)::date) THEN 1
            ELSE 0
        END AS next_next_period
   FROM next_lvl nl
     LEFT JOIN ( SELECT openproject_mhmp_wps.project_id,
            max(openproject_mhmp_wps.updated_at) AS wps_up_at
           FROM python.openproject_mhmp_wps
          GROUP BY openproject_mhmp_wps.project_id) w ON w.project_id = nl.id;  
		  
-- franz.v_openproject_mhmp_topics source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_topics
AS WITH domain AS (
         SELECT openproject_mhmp_projects_1.id,
            openproject_mhmp_projects_1.project_name AS domain_name,
            openproject_mhmp_projects_1.councilor_name
           FROM python.openproject_mhmp_projects openproject_mhmp_projects_1
          WHERE openproject_mhmp_projects_1.parent_id = 3
        )
 SELECT openproject_mhmp_projects.id,
    openproject_mhmp_projects.project_name AS topic_name,
    openproject_mhmp_projects.active,
    openproject_mhmp_projects.public,
    openproject_mhmp_projects.status,
    openproject_mhmp_projects.councilor_name,
    openproject_mhmp_projects.mhmp_responsible_name,
    openproject_mhmp_projects.parent_id,
    openproject_mhmp_projects.parent_project
   FROM python.openproject_mhmp_projects
  WHERE (openproject_mhmp_projects.parent_id IN ( SELECT domain.id
           FROM domain));		  
		   
-- franz.v_openproject_mhmp_workpackages source

CREATE OR REPLACE VIEW analytic.v_openproject_mhmp_workpackages
AS SELECT openproject_mhmp_wps.derived_start_date,
    openproject_mhmp_wps.derived_due_date,
    openproject_mhmp_wps.spent_time,
    openproject_mhmp_wps.labor_costs,
    openproject_mhmp_wps.material_costs,
    openproject_mhmp_wps.overall_costs,
    openproject_mhmp_wps.type_of_object,
    openproject_mhmp_wps.wp_id,
    openproject_mhmp_wps.wp_name,
    openproject_mhmp_wps.start_date,
    openproject_mhmp_wps.due_date,
    openproject_mhmp_wps.estimated_time,
    openproject_mhmp_wps.derived_estimated_time,
    openproject_mhmp_wps.percentage_done,
    openproject_mhmp_wps.created_at,
    openproject_mhmp_wps.updated_at,
    openproject_mhmp_wps.story_points,
    openproject_mhmp_wps.remaining_time,
    openproject_mhmp_wps.supplier_name,
    openproject_mhmp_wps.weight,
    openproject_mhmp_wps.contract_number,
    openproject_mhmp_wps.tender_number,
    openproject_mhmp_wps.council_rule_number,
    openproject_mhmp_wps.media_url,
    openproject_mhmp_wps.media_out_date,
    openproject_mhmp_wps.wp_description,
    openproject_mhmp_wps.tender_url,
    openproject_mhmp_wps.contract_registry_url,
    openproject_mhmp_wps.council_rule_url,
    openproject_mhmp_wps.wp_type,
    openproject_mhmp_wps.priority,
    openproject_mhmp_wps.project_id,
    openproject_mhmp_wps.project_name,
    openproject_mhmp_wps.wp_status,
    openproject_mhmp_wps.author_id,
    openproject_mhmp_wps.author_name,
    openproject_mhmp_wps.responsible_id,
    openproject_mhmp_wps.responsible_name,
    openproject_mhmp_wps.assignee_id,
    openproject_mhmp_wps.assignee_name,
    openproject_mhmp_wps.department_resp_name,
    openproject_mhmp_wps.department_resp_id,
    openproject_mhmp_wps.supplier_resp_id,
    openproject_mhmp_wps.supplier_resp_name,
    openproject_mhmp_wps.parent_id,
    openproject_mhmp_wps.parent_name,
    openproject_mhmp_wps.completion_date
   FROM python.openproject_mhmp_wps
  WHERE (openproject_mhmp_wps.project_id IN ( SELECT v_openproject_mhmp_projects.id
           FROM analytic.v_openproject_mhmp_projects));		   