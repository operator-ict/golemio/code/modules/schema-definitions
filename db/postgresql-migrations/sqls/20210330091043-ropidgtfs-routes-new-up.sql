ALTER TABLE "public"."ropidgtfs_routes"
  ADD COLUMN "is_regional" character varying(255),
  ADD COLUMN "is_substitute_transport" character varying(255);