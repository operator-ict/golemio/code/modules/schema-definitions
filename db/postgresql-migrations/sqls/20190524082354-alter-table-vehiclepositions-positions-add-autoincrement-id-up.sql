
CREATE SEQUENCE public.vehiclepositions_positions_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE vehiclepositions_positions ADD COLUMN id bigint NOT NULL DEFAULT nextval('public.vehiclepositions_positions_id_seq'::regclass);
