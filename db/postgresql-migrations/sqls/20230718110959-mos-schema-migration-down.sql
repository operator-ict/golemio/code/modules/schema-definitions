-- public.mos_be_accounts definition

CREATE TABLE public.mos_be_accounts (
	count int4 NULL,
	"day" varchar(255) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_accounts_pkey PRIMARY KEY (day)
);


-- public.mos_be_coupons definition

CREATE TABLE public.mos_be_coupons (
	coupon_custom_status_id int4 NULL,
	coupon_id int4 NOT NULL,
	created varchar(255) NULL,
	customer_id int4 NULL,
	customer_profile_name varchar(255) NULL,
	description text NULL,
	price numeric NULL,
	seller_id int4 NULL,
	tariff_id int4 NULL,
	tariff_int_name varchar(255) NULL,
	tariff_name varchar(255) NULL,
	tariff_profile_name varchar(255) NULL,
	valid_from varchar(255) NULL,
	valid_till varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	created_by_id varchar(50) NULL, -- CreatedByID - Uživatel který kupon vytvořil¶- 1 - OICT¶- 53 - DPP (eshop i přepážky - v budoucnu snad rozdělení právě pomocí nového uživatele na dva kanály)¶- 188 - MobApp¶
	order_status int4 NULL, -- Stav objednávky¶- 1 - Ordered¶- 2 - PaidByCard¶- 3 - PaidByBankTransfer¶- 4 - Cancelled¶- 5 - PaidByCash
	order_payment_type int4 NULL, -- Způsob platby objednávky¶- 1 - ByCard¶- 2 - PaidByBankTransfer¶- 3 - PaidByCardInOffice¶- 4 - PaidByCashInOffice
	token_id varchar(50) NULL, -- id tokenu - identifikátoru)
	CONSTRAINT mos_be_coupons_pkey PRIMARY KEY (coupon_id)
);

-- Column comments

COMMENT ON COLUMN public.mos_be_coupons.created_by_id IS 'CreatedByID - Uživatel který kupon vytvořil
-	1 - OICT
-	53 - DPP (eshop i přepážky - v budoucnu snad rozdělení právě pomocí nového uživatele na dva kanály)
-	188 - MobApp
';
COMMENT ON COLUMN public.mos_be_coupons.order_status IS 'Stav objednávky
-	1 - Ordered
-	2 - PaidByCard
-	3 - PaidByBankTransfer
-	4 - Cancelled
-	5 - PaidByCash';
COMMENT ON COLUMN public.mos_be_coupons.order_payment_type IS 'Způsob platby objednávky
-	1 - ByCard
-	2 - PaidByBankTransfer
-	3 - PaidByCardInOffice
-	4 - PaidByCashInOffice';
COMMENT ON COLUMN public.mos_be_coupons.token_id IS 'id tokenu - identifikátoru)';


-- public.mos_be_customers definition

CREATE TABLE public.mos_be_customers (
	customer_id int4 NOT NULL,
	date_of_birth varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_customers_pkey PRIMARY KEY (customer_id)
);


-- public.mos_be_tokens definition

CREATE TABLE public.mos_be_tokens (
	token_id varchar(50) NOT NULL,
	identifier_type varchar(50) NULL,
	created timestamptz NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_tokens_pkey PRIMARY KEY (token_id)
);


-- public.mos_be_zones definition

CREATE TABLE public.mos_be_zones (
	coupon_id int4 NOT NULL,
	zone_name varchar(255) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_be_zones_pkey PRIMARY KEY (coupon_id, zone_name)
);


-- public.mos_ma_devicemodels definition

CREATE TABLE public.mos_ma_devicemodels (
	id bigserial NOT NULL,
	count int4 NULL,
	model varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_ma_devicemodels_pkey PRIMARY KEY (id)
);


-- public.mos_ma_ticketactivations definition

CREATE TABLE public.mos_ma_ticketactivations (
	"date" timestamptz NULL,
	lat numeric NULL,
	lon numeric NULL,
	ticket_id int4 NOT NULL,
	"type" varchar(255) NULL,
	zones varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_ma_ticketactivations_pkey PRIMARY KEY (ticket_id)
);


-- public.mos_ma_ticketinspections definition

CREATE TABLE public.mos_ma_ticketinspections (
	"date" timestamptz NULL,
	id bigserial NOT NULL,
	lat numeric NULL,
	lon numeric NULL,
	reason varchar(255) NULL,
	"result" bool NULL,
	user_id varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_ma_ticketinspections_pkey PRIMARY KEY (id)
);


-- public.mos_ma_ticketpurchases definition

CREATE TABLE public.mos_ma_ticketpurchases (
	account_id varchar(255) NULL,
	cptp int4 NULL,
	"date" timestamptz NULL,
	duration int4 NULL,
	lat numeric NULL,
	lon numeric NULL,
	tariff_id int4 NULL,
	tariff_name varchar(255) NULL,
	ticket_id int4 NOT NULL,
	zone_count int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT mos_ma_ticketpurchases_pkey PRIMARY KEY (ticket_id)
);

  -- analytic.v_ropidbi_ticket source

CREATE MATERIALIZED VIEW analytic.v_ropidbi_ticket
TABLESPACE pg_default
AS SELECT a.date AS activation_date,
    a.date::date AS act_date,
    date_trunc('hour'::text, a.date)::time without time zone AS act_time,
    a.lat AS act_lat,
    a.lon AS act_lon,
    a.ticket_id,
        CASE
            WHEN a.type::text = 'manualNow'::text THEN 'Ruční aktivace ihned'::text
            WHEN a.type::text = 'manualTime'::text THEN 'Ruční aktivace na daný čas'::text
            WHEN a.type::text = 'purchaseNow'::text THEN 'Nákup s aktivací ihned'::text
            WHEN a.type::text = 'purchaseTime'::text THEN 'Nákup s aktivací na daný čas'::text
            ELSE 'Zatím neaktivováno'::text
        END AS activation_type,
    a.zones,
    p.cptp,
    p.date AS purchase_date,
    p.duration AS duration_min,
    p.tariff_name,
    p.zone_count,
        CASE
            WHEN p.tariff_name::text ~~ '%Praha'::text THEN 'Praha'::text
            ELSE 'Region'::text
        END AS oblast
   FROM mos_ma_ticketactivations a
     RIGHT JOIN mos_ma_ticketpurchases p ON a.ticket_id = p.ticket_id
WITH DATA;


-- analytic.v_mos_coupons_all_channels source

CREATE OR REPLACE VIEW analytic.v_mos_coupons_all_channels
AS WITH mobapp AS (
         SELECT mbc.created::date AS on_date,
            count(*) AS count_mobapp_by_card,
            sum(mbc.price) AS price_mobapp_by_card
           FROM mos_be_coupons mbc
          WHERE mbc.order_payment_type = 1 AND mbc.order_status = 2 AND mbc.seller_id = 1 AND COALESCE(mbc.created_by_id::text, ''::text) = '188'::text
          GROUP BY (mbc.created::date)
        ), eshop_transfer AS (
         SELECT mbc.created::date AS on_date,
            count(*) AS count_eshop_by_transfer,
            sum(mbc.price) AS price_eshop_by_transfer
           FROM mos_be_coupons mbc
          WHERE mbc.order_payment_type = 2 AND mbc.order_status = 3 AND mbc.seller_id = 1 AND COALESCE(mbc.created_by_id::text, ''::text) <> '188'::text
          GROUP BY (mbc.created::date)
        ), eshop_card AS (
         SELECT mbc.created::date AS on_date,
            count(*) AS count_eshop_by_card,
            sum(mbc.price) AS price_eshop_by_card
           FROM mos_be_coupons mbc
          WHERE mbc.order_payment_type = 1 AND mbc.order_status = 2 AND mbc.seller_id = 1 AND COALESCE(mbc.created_by_id::text, ''::text) <> '188'::text
          GROUP BY (mbc.created::date)
        ), counter_cash AS (
         SELECT mbc.created::date AS on_date,
            count(*) AS count_counter_by_cash,
            sum(mbc.price) AS price_counter_by_cash
           FROM mos_be_coupons mbc
          WHERE mbc.order_payment_type = 4 AND mbc.order_status = 5 AND mbc.seller_id = 1 AND COALESCE(mbc.created_by_id::text, ''::text) <> '188'::text
          GROUP BY (mbc.created::date)
        ), counter_card AS (
         SELECT mbc.created::date AS on_date,
            count(*) AS count_counter_by_card,
            sum(mbc.price) AS price_counter_by_card
           FROM mos_be_coupons mbc
          WHERE mbc.order_payment_type = 3 AND mbc.order_status = 2 AND mbc.seller_id = 1 AND COALESCE(mbc.created_by_id::text, ''::text) <> '188'::text
          GROUP BY (mbc.created::date)
        ), calendar AS (
         SELECT generate_series(min(mbc.created::text)::date::timestamp with time zone, now()::date::timestamp with time zone - '1 day'::interval, '1 day'::interval)::date AS on_date
           FROM mos_be_coupons mbc
        )
 SELECT calendar.on_date,
    COALESCE(eshcard.count_eshop_by_card, 0::bigint) AS count_eshop_by_card,
    COALESCE(eshcard.price_eshop_by_card, 0::numeric) AS price_eshop_by_card,
    COALESCE(eshtrans.count_eshop_by_transfer, 0::bigint) AS count_eshop_by_transfer,
    COALESCE(eshtrans.price_eshop_by_transfer, 0::numeric) AS price_eshop_by_transfer,
    COALESCE(ctcard.count_counter_by_card, 0::bigint) AS count_counter_by_card,
    COALESCE(ctcard.price_counter_by_card, 0::numeric) AS price_counter_by_card,
    COALESCE(ctcash.count_counter_by_cash, 0::bigint) AS count_counter_by_cash,
    COALESCE(ctcash.price_counter_by_cash, 0::numeric) AS price_counter_by_cash,
    COALESCE(mobappcard.count_mobapp_by_card, 0::bigint) AS count_mobapp_by_card,
    COALESCE(mobappcard.price_mobapp_by_card, 0::numeric) AS price_mobapp_by_card,
    COALESCE(eshcard.price_eshop_by_card, 0::numeric) + COALESCE(eshtrans.price_eshop_by_transfer, 0::numeric) + COALESCE(ctcard.price_counter_by_card, 0::numeric) + COALESCE(ctcash.price_counter_by_cash, 0::numeric) + COALESCE(mobappcard.price_mobapp_by_card, 0::numeric) AS price_all_channels
   FROM calendar
     FULL JOIN counter_card ctcard USING (on_date)
     FULL JOIN counter_cash ctcash USING (on_date)
     FULL JOIN eshop_card eshcard USING (on_date)
     FULL JOIN eshop_transfer eshtrans USING (on_date)
     FULL JOIN mobapp mobappcard USING (on_date);

    -- analytic.v_ropidbi_coupon_channels_sale source

CREATE OR REPLACE VIEW analytic.v_ropidbi_coupon_channels_sale
AS WITH calendar AS (
         SELECT generate_series('2019-01-01 00:00:00+01'::timestamp with time zone, now()::date::timestamp with time zone - '1 day'::interval, '1 day'::interval)::date AS on_date
        ), channel AS (
         SELECT unnest(string_to_array('Mobilní aplikace,Přepážka,Eshop'::text, ','::text)) AS channel
        ), payment_method AS (
         SELECT unnest(string_to_array('Platební karta,Hotově,Převodem'::text, ','::text)) AS pmethod
        ), seller AS (
         SELECT unnest(string_to_array('OICT,DPP,Nezařazeno'::text, ','::text)) AS seller
        ), mos AS (
         SELECT c.created::date AS created,
                CASE
                    WHEN c.created_by_id::integer = 188 THEN 'Mobilní aplikace'::text
                    WHEN c.order_payment_type = ANY (ARRAY[1, 2]) THEN 'Eshop'::text
                    WHEN c.order_payment_type = ANY (ARRAY[3, 4]) THEN 'Přepážka'::text
                    ELSE 'Nezařazeno'::text
                END AS channel,
                CASE
                    WHEN c.order_status = 2 THEN 'Platební karta'::text
                    WHEN c.order_status = 5 THEN 'Hotově'::text
                    WHEN c.order_status = 3 THEN 'Převodem'::text
                    ELSE 'Nezařazeno'::text
                END AS payment_method,
                CASE
                    WHEN c.seller_id = 1 THEN 'OICT'::text
                    WHEN c.seller_id = 29 THEN 'DPP'::text
                    ELSE 'Nezařazeno'::text
                END AS seller,
            count(*) AS coupon_count,
            sum(c.price) AS sale
           FROM mos_be_coupons c
          WHERE c.tariff_profile_name::text !~~ '%PVP'::text
          GROUP BY (c.created::date), (
                CASE
                    WHEN c.created_by_id::integer = 188 THEN 'Mobilní aplikace'::text
                    WHEN c.order_payment_type = ANY (ARRAY[1, 2]) THEN 'Eshop'::text
                    WHEN c.order_payment_type = ANY (ARRAY[3, 4]) THEN 'Přepážka'::text
                    ELSE 'Nezařazeno'::text
                END), (
                CASE
                    WHEN c.order_status = 2 THEN 'Platební karta'::text
                    WHEN c.order_status = 5 THEN 'Hotově'::text
                    WHEN c.order_status = 3 THEN 'Převodem'::text
                    ELSE 'Nezařazeno'::text
                END), (
                CASE
                    WHEN c.seller_id = 1 THEN 'OICT'::text
                    WHEN c.seller_id = 29 THEN 'DPP'::text
                    ELSE 'Nezařazeno'::text
                END)
          ORDER BY (c.created::date) DESC
        )
 SELECT calendar.on_date AS created,
    channel.channel,
    payment_method.pmethod AS payment_method,
    seller.seller,
    COALESCE(mos.coupon_count, 0::bigint) AS coupon_count,
    COALESCE(mos.sale, 0::numeric) AS sale
   FROM calendar
     LEFT JOIN channel ON true
     LEFT JOIN payment_method ON true
     LEFT JOIN seller ON true
     LEFT JOIN mos ON calendar.on_date = mos.created AND mos.channel = channel.channel AND mos.payment_method = payment_method.pmethod AND mos.seller = seller.seller;

     -- analytic.v_ropidbi_coupon_tokens source

CREATE OR REPLACE VIEW analytic.v_ropidbi_coupon_tokens
AS SELECT c.customer_profile_name,
    c.tariff_profile_name AS tariff_name,
        CASE
            WHEN c.seller_id = 1 THEN 'OICT'::text
            WHEN c.seller_id = 29 THEN 'DPP'::text
            ELSE 'jiný'::text
        END AS seller,
    c.valid_from::date AS valid_from,
        CASE
            WHEN t.identifier_type::text = 'MobApp'::text THEN 'Mobilní aplikace'::text
            WHEN t.identifier_type::text = 'BPK'::text THEN 'Platební karta'::text
            WHEN t.identifier_type::text = 'Litacka'::text THEN 'Lítačka'::text
            WHEN t.identifier_type::text = 'InKarta'::text THEN 'InKarta'::text
            ELSE 'jiný'::text
        END AS token_type,
    count(*) AS coupon_count
   FROM mos_be_coupons c
     LEFT JOIN mos_be_tokens t ON c.token_id::text = t.token_id::text
  GROUP BY c.customer_profile_name, (
        CASE
            WHEN c.seller_id = 1 THEN 'OICT'::text
            WHEN c.seller_id = 29 THEN 'DPP'::text
            ELSE 'jiný'::text
        END), c.tariff_profile_name, (c.valid_from::date), (
        CASE
            WHEN t.identifier_type::text = 'MobApp'::text THEN 'Mobilní aplikace'::text
            WHEN t.identifier_type::text = 'BPK'::text THEN 'Platební karta'::text
            WHEN t.identifier_type::text = 'Litacka'::text THEN 'Lítačka'::text
            WHEN t.identifier_type::text = 'InKarta'::text THEN 'InKarta'::text
            ELSE 'jiný'::text
        END);

        -- analytic.v_ropidbi_customer_age source

CREATE OR REPLACE VIEW analytic.v_ropidbi_customer_age
AS SELECT date_part('year'::text, age(mos_be_customers.date_of_birth::date::timestamp with time zone)) AS age,
    count(*) AS count
   FROM mos_be_customers
  WHERE date_part('year'::text, age(mos_be_customers.date_of_birth::date::timestamp with time zone)) >= 5::double precision AND date_part('year'::text, age(mos_be_customers.date_of_birth::date::timestamp with time zone)) <= 100::double precision
  GROUP BY (date_part('year'::text, age(mos_be_customers.date_of_birth::date::timestamp with time zone)));

  -- analytic.v_ropidbi_device_counts source

CREATE OR REPLACE VIEW analytic.v_ropidbi_device_counts
AS SELECT btrim(substr(replace(mos_ma_devicemodels.model::text, '_'::text, ' '::text), 1, "position"(replace(mos_ma_devicemodels.model::text, '_'::text, ' '::text), ' '::text))) AS brand,
    replace(mos_ma_devicemodels.model::text, '_'::text, ' '::text) AS model,
        CASE
            WHEN "left"(mos_ma_devicemodels.model::text, 1) = 'i'::text THEN 'iOS'::text
            ELSE 'Android'::text
        END AS operation_system,
    sum(mos_ma_devicemodels.count) AS sum
   FROM mos_ma_devicemodels
  WHERE mos_ma_devicemodels.model IS NOT NULL
  GROUP BY (btrim(substr(replace(mos_ma_devicemodels.model::text, '_'::text, ' '::text), 1, "position"(replace(mos_ma_devicemodels.model::text, '_'::text, ' '::text), ' '::text)))), (replace(mos_ma_devicemodels.model::text, '_'::text, ' '::text)), (
        CASE
            WHEN "left"(mos_ma_devicemodels.model::text, 1) = 'i'::text THEN 'iOS'::text
            ELSE 'Android'::text
        END);

        -- analytic.v_ropidbi_soubeh_jizdenek source

CREATE OR REPLACE VIEW analytic.v_ropidbi_soubeh_jizdenek
AS SELECT celkem.datum,
    celkem.count_acc AS acc_all,
    celkem.count_glob AS ticket_all,
    dupl.count_acc AS acc_dupl,
    dupl.count_glob AS ticket_dupl
   FROM ( SELECT mos_ma_ticketpurchases.date::date AS datum,
            count(DISTINCT mos_ma_ticketpurchases.account_id) AS count_acc,
            count(*) AS count_glob
           FROM mos_ma_ticketpurchases
          GROUP BY (mos_ma_ticketpurchases.date::date)) celkem
     LEFT JOIN ( SELECT b.valid_from::date AS datum,
            count(DISTINCT b.account_id) AS count_acc,
            count(*) AS count_glob
           FROM ( SELECT a.account_id,
                    a.valid_from,
                    a.valid_to,
                    a.xx
                   FROM ( SELECT mos_ma_ticketpurchases.account_id,
                            mos_ma_ticketpurchases.date AS valid_from,
                            mos_ma_ticketpurchases.date + ((mos_ma_ticketpurchases.duration || ' minutes'::text)::interval) AS valid_to,
                            lead(mos_ma_ticketpurchases.date) OVER (PARTITION BY mos_ma_ticketpurchases.account_id ORDER BY mos_ma_ticketpurchases.date) AS xx
                           FROM mos_ma_ticketpurchases) a
                  WHERE a.xx >= a.valid_from AND a.xx <= a.valid_to) b
          GROUP BY (b.valid_from::date)) dupl ON celkem.datum = dupl.datum;

    -- analytic.v_ropidbi_ticket_activation_location source

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_location
AS SELECT v_ropidbi_ticket.act_date,
    round(v_ropidbi_ticket.act_lat, 4) AS act_lat,
    round(v_ropidbi_ticket.act_lon, 4) AS act_lon,
    v_ropidbi_ticket.oblast,
    count(*) AS count
   FROM analytic.v_ropidbi_ticket
  WHERE v_ropidbi_ticket.act_date IS NOT NULL AND v_ropidbi_ticket.act_lat >= 49.4 AND v_ropidbi_ticket.act_lat <= 50.7 AND v_ropidbi_ticket.act_lon >= 13::numeric AND v_ropidbi_ticket.act_lon <= 15.6
  GROUP BY v_ropidbi_ticket.act_date, (round(v_ropidbi_ticket.act_lat, 4)), (round(v_ropidbi_ticket.act_lon, 4)), v_ropidbi_ticket.oblast;
 
  -- analytic.v_ropidbi_ticket_activation_times source

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_times
AS SELECT rop.act_date,
    rop.act_time,
    rop.tariff_name,
    rop.oblast,
    count(*) AS count,
    pid.druh AS cestujici
   FROM analytic.v_ropidbi_ticket rop
     LEFT JOIN analytic.pid_price_list pid ON rop.tariff_name::text = pid.tarif::text AND rop.cptp = pid.cislo_tarifu_cptp AND rop.purchase_date::date >= pid.nabizeno_od AND rop.purchase_date::date <= pid.nabizeno_do
  GROUP BY rop.act_date, rop.act_time, rop.tariff_name, rop.oblast, pid.druh;

  -- analytic.v_ropidbi_ticket_activation_types source

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_types
AS SELECT rop.act_date,
    rop.activation_type,
    rop.oblast,
    rop.tariff_name,
    count(*) AS count,
    pid.druh AS cestujici
   FROM analytic.v_ropidbi_ticket rop
     LEFT JOIN analytic.pid_price_list pid ON rop.tariff_name::text = pid.tarif::text AND rop.cptp = pid.cislo_tarifu_cptp AND rop.purchase_date::date >= pid.nabizeno_od AND rop.purchase_date::date <= pid.nabizeno_do
  WHERE rop.activation_type <> 'zatím neaktivováno'::text AND rop.tariff_name::text <> ''::text
  GROUP BY rop.act_date, rop.activation_type, rop.oblast, rop.tariff_name, pid.druh;

  -- analytic.v_ropidbi_ticket_sales source

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales
AS SELECT rt.purchase_date::date AS purchase_date,
    rt.tariff_name,
    rt.oblast,
    count(*) AS count_ticket,
    pid.cena_s_dph,
    count(*) * pid.cena_s_dph AS sale,
    pid.nabizeno_od,
    pid.nabizeno_do,
    pid.druh AS cestujici
   FROM analytic.v_ropidbi_ticket rt
     LEFT JOIN analytic.pid_price_list pid ON rt.tariff_name::text = pid.tarif::text AND rt.cptp = pid.cislo_tarifu_cptp AND rt.purchase_date::date >= pid.nabizeno_od AND rt.purchase_date::date <= pid.nabizeno_do
  WHERE rt.purchase_date::date IS NOT NULL AND rt.tariff_name IS NOT NULL
  GROUP BY (rt.purchase_date::date), rt.tariff_name, rt.oblast, pid.cena_s_dph, pid.nabizeno_do, pid.nabizeno_od, pid.druh;

  -- analytic.v_ropidbi_ticket_with_rollingmean source

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_with_rollingmean
AS SELECT tab.date,
    count(tab.tariff_name) AS ticket_count,
    sum(tab.cena_s_dph) AS total_sale,
    avg(count(tab.tariff_name)) OVER (ORDER BY tab.date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS roll_ticket_count,
    avg(sum(tab.cena_s_dph)) OVER (ORDER BY tab.date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS roll_sale
   FROM ( SELECT pur.date::date AS date,
            pur.tariff_name,
            pur.cptp,
            cen.cena_s_dph
           FROM mos_ma_ticketpurchases pur
             LEFT JOIN analytic.pid_price_list cen ON pur.tariff_name::text = cen.tarif::text AND pur.cptp = cen.cislo_tarifu_cptp AND pur.date::date >= cen.nabizeno_od AND pur.date::date <= cen.nabizeno_do) tab
  GROUP BY tab.date;

  -- analytic.v_ropidbi_ticket_zones source

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_zones
AS SELECT tab.created AS valid_from,
    tab.customer_profile_name AS customer,
    tab.tariff_profile_name AS tarif,
    tab.zona,
    tab.zone_order,
    count(*) AS count
   FROM ( SELECT c.created::date AS created,
            c.coupon_id,
            c.tariff_profile_name,
            c.customer_profile_name,
            z.zona,
            z.zone_order
           FROM mos_be_coupons c
             JOIN ( SELECT z_1.coupon_id,
                        CASE
                            WHEN z_1.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN 'P+0+B'::character varying
                            ELSE z_1.zone_name
                        END AS zona,
                        CASE
                            WHEN z_1.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN '0'::character varying
                            ELSE z_1.zone_name
                        END AS zone_order
                   FROM mos_be_zones z_1
                  GROUP BY z_1.coupon_id, (
                        CASE
                            WHEN z_1.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN 'P+0+B'::character varying
                            ELSE z_1.zone_name
                        END), (
                        CASE
                            WHEN z_1.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN '0'::character varying
                            ELSE z_1.zone_name
                        END)) z ON c.coupon_id = z.coupon_id
          WHERE c.tariff_profile_name::text !~~ '%PVP'::text) tab
  GROUP BY tab.created, tab.customer_profile_name, tab.tariff_profile_name, tab.zona, tab.zone_order
  ORDER BY tab.created DESC;

-- analytic.v_ropidbi_coupon_sales_monthly_by_zones source

CREATE OR REPLACE VIEW analytic.v_ropidbi_coupon_sales_monthly_by_zones
AS WITH mos AS (
         SELECT mbc.coupon_id,
            mbc.created::date AS created,
            mbc.customer_profile_name,
                CASE
                    WHEN mbc.tariff_profile_name::text = ANY (ARRAY['Bezplatná4'::text, 'Bezplatná5'::text, 'Bezplatná6'::text]) THEN 'Bezplatná'::text
                    ELSE mbc.tariff_profile_name::text
                END AS tariff_profile_name,
            mbc.price,
                CASE
                    WHEN mbc.created_by_id::integer = 188 THEN 'Mobilní aplikace'::text
                    WHEN mbc.order_payment_type = ANY (ARRAY[1, 2]) THEN 'Eshop'::text
                    WHEN mbc.order_payment_type = ANY (ARRAY[3, 4]) THEN 'Přepážka'::text
                    ELSE 'Nezařazeno'::text
                END AS channel,
                CASE
                    WHEN mbc.seller_id = 1 THEN 'OICT'::text
                    WHEN mbc.seller_id = 29 THEN 'DPP'::text
                    ELSE 'Nezařazeno'::text
                END AS seller
           FROM mos_be_coupons mbc
          WHERE mbc.tariff_profile_name::text !~~ '%PVP'::text
        ), zones AS (
         SELECT z.coupon_id,
            string_agg(z.zona::text, ', '::text) AS zones,
            count(*) AS zones_count
           FROM ( SELECT mos_be_zones.coupon_id,
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN '0+B'::character varying
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 'P'::character varying
                            ELSE mos_be_zones.zone_name
                        END AS zona,
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN 0
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 0
                            ELSE mos_be_zones.zone_name::integer
                        END AS zone_order
                   FROM mos_be_zones
                  WHERE mos_be_zones.zone_name::text <> ''::text AND mos_be_zones.zone_name::text IS NOT NULL
                  GROUP BY mos_be_zones.coupon_id, (
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN '0+B'::character varying
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 'P'::character varying
                            ELSE mos_be_zones.zone_name
                        END), (
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN 0
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 0
                            ELSE mos_be_zones.zone_name::integer
                        END)
                  ORDER BY mos_be_zones.coupon_id, (
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['0'::text, '0+B'::character varying::text]) THEN 0
                            WHEN mos_be_zones.zone_name::text = 'Praha'::text THEN 0
                            ELSE mos_be_zones.zone_name::integer
                        END)) z
          GROUP BY z.coupon_id
        ), main AS (
         SELECT c.coupon_id,
            to_char(c.created::timestamp with time zone, 'YYYY-MM-01'::text)::date AS purchase_date,
            c.tariff_profile_name,
            c.customer_profile_name,
            c.price,
            c.channel,
            c.seller,
                CASE
                    WHEN zones.zones = 'P'::text THEN 'P, 0+B'::text
                    WHEN zones.zones IS NOT NULL THEN zones.zones
                    ELSE 'Nezařazeno'::text
                END AS zones,
                CASE
                    WHEN zones.zones_count IS NOT NULL AND zones.zones <> 'P, 0+B'::text THEN zones.zones_count::text
                    WHEN zones.zones = 'P, 0+B'::text THEN 'P, 0+B'::text
                    ELSE 0::text
                END AS zones_count
           FROM mos c
             LEFT JOIN zones ON c.coupon_id = zones.coupon_id
          ORDER BY c.coupon_id
        )
 SELECT m.tariff_profile_name,
    m.customer_profile_name,
    m.channel,
    m.seller,
    m.purchase_date,
    m.zones,
    m.zones_count,
    count(*) AS coupons_count,
    sum(m.price) AS sales
   FROM main m
  GROUP BY m.tariff_profile_name, m.customer_profile_name, m.channel, m.seller, m.purchase_date, m.zones_count, m.zones
  ORDER BY m.tariff_profile_name, m.customer_profile_name, m.channel, m.seller, m.zones_count, m.zones, m.purchase_date;

  -- analytic.v_ropidbi_capping_agregation source

CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_agregation
AS SELECT count(DISTINCT m.account_id) AS dis_acounts,
    count(m.account_id) AS accounts,
    m.cptp,
    date_trunc('month'::text, a.date) AS date_month,
    date_part('hour'::text, a.date) AS hour,
    p.active_users,
    count(m.ticket_id) AS nu_tickets
   FROM mos_ma_ticketpurchases m
     JOIN mos_ma_ticketactivations a ON m.ticket_id = a.ticket_id
     LEFT JOIN ( SELECT DISTINCT v.account_id,
            v.active_users
           FROM ( SELECT m_1.account_id,
                        CASE
                            WHEN m_1.cptp = 124 OR m_1.cptp = 624 THEN 'active'::text
                            ELSE 'non active'::text
                        END AS active_users
                   FROM mos_ma_ticketpurchases m_1
                     JOIN mos_ma_ticketactivations a_1 ON m_1.ticket_id = a_1.ticket_id
                  WHERE a_1.date >= '2022-04-01 00:00:00+02'::timestamp with time zone) v
          WHERE v.active_users = 'active'::text) p ON m.account_id::text = p.account_id::text
  WHERE a.date >= '2022-04-01 00:00:00+02'::timestamp with time zone
  GROUP BY (date_trunc('month'::text, a.date)), (date_part('hour'::text, a.date)), m.cptp, p.active_users;


  -- analytic.v_ropidbi_capping_daily_counts source

CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_daily_counts
AS SELECT a.date::date AS date,
    mmt.cptp,
    count(mmt.ticket_id) AS nu_tickets,
    count(DISTINCT mmt.account_id) AS nu_accounts
   FROM mos_ma_ticketpurchases mmt
     JOIN mos_ma_ticketactivations a ON mmt.ticket_id = a.ticket_id
  WHERE a.date >= '2022-04-01 00:00:00+02'::timestamp with time zone AND (mmt.cptp = 124 OR mmt.cptp = 624)
  GROUP BY (a.date::date), mmt.cptp
  ORDER BY (a.date::date) DESC;

  -- analytic.v_ropidbi_capping_distinct_accounts source

CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_distinct_accounts
AS SELECT mmt.cptp,
    date_trunc('month'::text, a.date) AS datum_mesic,
    mmt.account_id
   FROM mos_ma_ticketpurchases mmt
     JOIN mos_ma_ticketactivations a ON mmt.ticket_id = a.ticket_id
  WHERE a.date >= '2022-04-01 00:00:00+02'::timestamp with time zone AND (mmt.cptp = 124 OR mmt.cptp = 624)
  GROUP BY (date_trunc('month'::text, a.date)), mmt.account_id, mmt.cptp;

  -- analytic.v_ropidbi_capping_more_rides source

CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_more_rides
AS SELECT v.date::date AS date,
    v.cptp,
    v.account_id,
    v.n_tickets
   FROM ( SELECT mmt.cptp,
            count(mmt.ticket_id) AS n_tickets,
            mmt.account_id,
            a.date,
            row_number() OVER (PARTITION BY (a.date::date), mmt.account_id) AS row_number
           FROM mos_ma_ticketpurchases mmt
             JOIN mos_ma_ticketactivations a ON mmt.ticket_id = a.ticket_id
          WHERE (mmt.cptp = 124 OR mmt.cptp = 624) AND mmt.date >= '2022-04-01 00:00:00+02'::timestamp with time zone
          GROUP BY mmt.account_id, a.date, mmt.cptp) v
  WHERE v.row_number = 1;

  -- analytic.v_ropidbi_ticket_sales_monthly_by_zones source

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales_monthly_by_zones
AS WITH purchases_w_prices AS (
         SELECT p.ticket_id,
            p.tariff_name,
            p.date::date AS date,
            p.zone_count,
            COALESCE(pid.zony, 'nezařazeno'::character varying) AS zony,
            COALESCE(pid.cena_s_dph, 0) AS price
           FROM mos_ma_ticketpurchases p
             LEFT JOIN analytic.pid_price_list pid ON p.tariff_name::text = pid.tarif::text AND p.cptp = pid.cislo_tarifu_cptp AND p.date::date >= pid.nabizeno_od AND p.date::date <= pid.nabizeno_do
        ), activations AS (
         SELECT mos_ma_ticketactivations.ticket_id,
            btrim(unnest(string_to_array(mos_ma_ticketactivations.zones::text, ','::text))) AS zones
           FROM mos_ma_ticketactivations
          WHERE mos_ma_ticketactivations.zones::text <> ''::text AND mos_ma_ticketactivations.zones IS NOT NULL
          GROUP BY mos_ma_ticketactivations.ticket_id, mos_ma_ticketactivations.zones
        ), main AS (
         SELECT c.ticket_id,
            c.tariff_name,
            c.date,
            c.zone_count,
            c.zony,
            c.price,
            string_agg(c.zones, ', '::text) AS zones
           FROM ( SELECT DISTINCT p.ticket_id,
                    p.tariff_name,
                    to_char(p.date::timestamp with time zone, 'yyyy-mm-01'::text) AS date,
                    p.zone_count,
                    p.zony,
                    p.price,
                        CASE
                            WHEN a.zones = ANY (ARRAY['P'::text, '0'::text, 'B'::text, 'p'::text, 'b'::text]) THEN 'P+0+B'::text
                            WHEN a.zones IS NULL THEN 'Nezařazeno'::text
                            ELSE a.zones
                        END AS zones,
                        CASE
                            WHEN a.zones = ANY (ARRAY['P'::text, '0'::text, 'B'::text, 'p'::text, 'b'::text]) THEN 0
                            WHEN a.zones IS NULL THEN '-1'::integer
                            WHEN a.zones ~ '^\d+$'::text THEN a.zones::integer
                            ELSE NULL::integer
                        END AS zone_order
                   FROM purchases_w_prices p
                     LEFT JOIN activations a ON p.ticket_id = a.ticket_id
                  ORDER BY p.ticket_id, (
                        CASE
                            WHEN a.zones = ANY (ARRAY['P'::text, '0'::text, 'B'::text, 'p'::text, 'b'::text]) THEN 0
                            WHEN a.zones IS NULL THEN '-1'::integer
                            WHEN a.zones ~ '^\d+$'::text THEN a.zones::integer
                            ELSE NULL::integer
                        END)) c
          GROUP BY c.ticket_id, c.tariff_name, c.date, c.zone_count, c.zony, c.price
        )
 SELECT main.tariff_name,
    main.date,
    main.zone_count,
    main.zony,
    main.zones,
    count(*) AS tickets_count,
    sum(main.price) AS sales
   FROM main
  GROUP BY main.tariff_name, main.date, main.zone_count, main.zony, main.zones
  ORDER BY main.tariff_name, main.date, main.zone_count, main.zony, main.zones;