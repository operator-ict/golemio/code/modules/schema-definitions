-- franz.v_wazett_routes_lines source

CREATE OR REPLACE VIEW analytic.v_wazett_routes_lines
AS SELECT wazett_routes.id,
    wazett_routes.name,
    st_astext(wazett_routes.line) AS line,
    concat('#', "left"(lpad(to_hex((wazett_routes.id::double precision * character_length(wazett_routes.name)::double precision / (( SELECT max(wazett_routes_1.id * character_length(wazett_routes_1.name)) AS max
           FROM wazett_routes wazett_routes_1))::double precision * 10000000::double precision)::bigint), 6, '0'::text), 6)) AS color
   FROM wazett_routes;