-- franz.v_public_tenders_internal source
drop VIEW analytic.v_public_tenders_internal;
CREATE VIEW analytic.v_public_tenders_internal
AS SELECT pt.id_zakazky,
    pt.nazev_zakazky,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
        CASE
            WHEN ptpi.ico IS NOT NULL THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.smluvni_cena_s_dph_kc > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.smluvni_cena_bez_dph_kc - NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_kc,
    (pt.smluvni_cena_bez_dph_kc - pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc) / NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_procent,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.datum_uzavreni_smlouvy - pt.datum_zahajeni_zadavaciho_rizeni AS delka_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu - pt.datum_zahajeni_zadavaciho_rizeni AS delka_lhuty_pro_podani_nabidek,
        CASE
            WHEN pt.hodnotici_kriteria ~~ '%100%'::text THEN 'Pouze cena'::text
            ELSE 'Více kritérií'::text
        END AS hodnotici_kriteria_typ,
    pt.externi_zastoupeni_zadavatele_v_rizeni,
    pt.externi_zastoupeni_zadavatele_v_rizeni_nazev,
    pt.externi_zastoupeni_zadavatele_v_rizeni_ico,
    "left"(pt.hlavni_nipez_kod_zakazky, 3) AS cpv_kod,
    pt.hlavni_nipez_kod_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.faze_zakazky,
        CASE
            WHEN pto.odbor IS NOT NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
    pt.updated_at,
    pt.created_at,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     LEFT JOIN python.public_tenders_odbory pto ON pt.organizacni_jednotka_spravy ~~ (('%'::text || pto.odbor::text) || '%'::text)
     LEFT JOIN python.public_tenders_partners_ico ptpi ON ptpi.ico::text = pt.ico_smluvniho_partnera
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text, 'Zrušena'::text])) AND (pt.datum_vytvoreni_zakazky >= '2020-09-01 01:00:00+02'::timestamp with time zone OR pt.datum_vytvoreni_zakazky >= '2021-01-01 00:00:00+01'::timestamp with time zone AND pto.odbor IS NULL) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));