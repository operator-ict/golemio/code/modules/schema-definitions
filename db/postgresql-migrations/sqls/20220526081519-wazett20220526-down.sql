drop VIEW analytic.v_barrande_data_all;
drop VIEW analytic.v_barrande_union_time_interval;
drop VIEW analytic.v_barrande_route_hour;

drop MATERIALIZED VIEW analytic.mv_barrande_normal;

drop VIEW analytic.v_barrande_route_hour_core;
drop VIEW analytic.v_barrande_last_update;
drop VIEW analytic.v_barrande_route;
drop TABLE analytic.barrande_black_list_route;

-- analytic.v_bm_trasy source
CREATE OR REPLACE VIEW analytic.v_bm_trasy
AS SELECT DISTINCT ON (wazett_routes.name) wazett_routes.name,
    wazett_routes.id,
    wazett_routes.created_at,
        CASE
            WHEN "position"(wazett_routes.name, ' TN'::text) > 0 THEN true
            ELSE false
        END AS is_trasa,
        CASE
            WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN true
            ELSE false
        END AS is_usek,
        CASE
            WHEN "position"(wazett_routes.name, ' OS'::text) > 0 THEN true
            ELSE false
        END AS is_opacny_smer,
        CASE
            WHEN wazett_routes.id = ANY (ARRAY[32875, 32876]) THEN 'OICT 3 DO'::text
            WHEN wazett_routes.id = ANY (ARRAY[32888, 32889]) THEN 'OICT 3 OD'::text
            ELSE "substring"(wazett_routes.name, 4,
            CASE
                WHEN "position"(wazett_routes.name, ' TN'::text) > 0 THEN "position"(wazett_routes.name, ' TN'::text) - 3
                WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN "position"(wazett_routes.name, ' ÚS'::text) - 3
                ELSE NULL::integer
            END)
        END AS name_trasa,
    "substring"(wazett_routes.name,
        CASE
            WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN "position"(wazett_routes.name, ' ÚS'::text) + 4
            ELSE length(wazett_routes.name) + 1
        END,
        CASE
            WHEN "position"(wazett_routes.name, ' OS'::text) > 0 THEN "position"(wazett_routes.name, ' OS'::text) - "position"(wazett_routes.name, ' ÚS'::text) - 4
            ELSE length(wazett_routes.name) - "position"(wazett_routes.name, ' ÚS'::text)
        END) AS name_usek
   FROM wazett_routes
  WHERE "left"(wazett_routes.name, 2) = 'BM'::text
  ORDER BY wazett_routes.name, wazett_routes.created_at DESC;
  
  
CREATE OR REPLACE VIEW analytic.v_bm_last_update
AS SELECT to_timestamp((wrl.update_time / 1000)::double precision) AS last_update
   FROM wazett_route_lives wrl
     JOIN analytic.v_bm_trasy vbt ON wrl.route_id = vbt.id
  ORDER BY wrl.update_time DESC
 LIMIT 1;
 
  
-- analytic.v_bm_trasy_hod_part source

CREATE OR REPLACE VIEW analytic.v_bm_trasy_hod_part
AS SELECT rl.route_id,
    to_timestamp((rl.update_time / 1000)::double precision)::date AS datum,
    date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) AS hodina,
    (to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)), '09'::text) || ' -'::text) || to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) + 1::double precision, '09'::text) AS cas_interval,
    round(avg(rl."time"), 2) AS avg_time,
    round(avg(rl.length * 1000 / (rl."time" * 360)), 2) AS avg_speed
   FROM wazett_route_lives rl
     JOIN analytic.v_bm_trasy bm ON bm.id = rl.route_id
  WHERE rl.length > 0
  GROUP BY rl.route_id, (to_timestamp((rl.update_time / 1000)::double precision)::date), (date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)));  

-- analytic.mv_bm_normal source

CREATE MATERIALIZED VIEW analytic.mv_bm_normal
TABLESPACE pg_default
AS SELECT vtbh.route_id,
    vtbh.hodina,
    round(avg(vtbh.avg_time), 2) AS avg_time,
    round(avg(vtbh.avg_speed)) AS avg_speed
   FROM analytic.v_bm_trasy_hod_part vtbh
  WHERE vtbh.datum >= '2022-04-24'::date AND vtbh.datum <= '2022-05-05'::date AND (date_part('dow'::text, vtbh.datum) = ANY (ARRAY[2::double precision, 3::double precision, 4::double precision]))
  GROUP BY vtbh.route_id, vtbh.hodina
WITH DATA;

-- View indexes:
CREATE INDEX mv_bm_normal_idx ON analytic.mv_bm_normal USING btree (route_id, hodina);  

-- analytic.v_bm_trasy_hod source

CREATE OR REPLACE VIEW analytic.v_bm_trasy_hod
AS SELECT p.route_id,
    p.datum,
    p.hodina,
    p.cas_interval,
    p.avg_time,
    p.avg_speed,
    n.avg_time AS normal_avg_time,
    n.avg_speed AS normal_avg_speed
   FROM analytic.v_bm_trasy_hod_part p
     LEFT JOIN analytic.mv_bm_normal n ON n.route_id = p.route_id AND p.hodina = n.hodina;
	 
-- analytic.v_bm_union2 source

CREATE OR REPLACE VIEW analytic.v_bm_union2
AS WITH hodiny AS (
         SELECT v_bm_trasy_hod.route_id,
            v_bm_trasy_hod.datum,
            v_bm_trasy_hod.hodina,
            v_bm_trasy_hod.cas_interval,
            v_bm_trasy_hod.avg_time,
            v_bm_trasy_hod.avg_speed,
            v_bm_trasy_hod.normal_avg_time,
            v_bm_trasy_hod.normal_avg_speed
           FROM analytic.v_bm_trasy_hod
          WHERE v_bm_trasy_hod.datum < CURRENT_DATE OR v_bm_trasy_hod.hodina < date_part('hour'::text, now())
        ), den AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '0 - 24h'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.route_id, hodiny.datum
        ), h15_18 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '15 -18'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 15::double precision AND hodiny.hodina <= 17::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 17::double precision)
          GROUP BY hodiny.route_id, hodiny.datum
        ), h22_6 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '22 - 06'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE NOT (hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision) AND hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.route_id, hodiny.datum
        ), h6_22 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '06 - 22'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 21::double precision)
          GROUP BY hodiny.route_id, hodiny.datum
        ), h7_10 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '07 - 10'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 7::double precision AND hodiny.hodina <= 9::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 10::double precision)
          GROUP BY hodiny.route_id, hodiny.datum
        ), h7_11_15_19 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '07 - 11, 15 - 19'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE (hodiny.hodina >= 7::double precision AND hodiny.hodina <= 10::double precision OR hodiny.hodina >= 15::double precision AND hodiny.hodina <= 18::double precision) AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 18::double precision)
          GROUP BY hodiny.route_id, hodiny.datum
        )
 SELECT hodiny.route_id,
    hodiny.datum,
    hodiny.cas_interval,
    hodiny.avg_time,
    hodiny.avg_speed,
    hodiny.normal_avg_time,
    hodiny.normal_avg_speed
   FROM hodiny
UNION ALL
 SELECT den.route_id,
    den.datum,
    den.cas_interval,
    den.avg_time,
    den.avg_speed,
    den.normal_avg_time,
    den.normal_avg_speed
   FROM den
UNION ALL
 SELECT h15_18.route_id,
    h15_18.datum,
    h15_18.cas_interval,
    h15_18.avg_time,
    h15_18.avg_speed,
    h15_18.normal_avg_time,
    h15_18.normal_avg_speed
   FROM h15_18
UNION ALL
 SELECT h22_6.route_id,
    h22_6.datum,
    h22_6.cas_interval,
    h22_6.avg_time,
    h22_6.avg_speed,
    h22_6.normal_avg_time,
    h22_6.normal_avg_speed
   FROM h22_6
UNION ALL
 SELECT h6_22.route_id,
    h6_22.datum,
    h6_22.cas_interval,
    h6_22.avg_time,
    h6_22.avg_speed,
    h6_22.normal_avg_time,
    h6_22.normal_avg_speed
   FROM h6_22
UNION ALL
 SELECT h7_10.route_id,
    h7_10.datum,
    h7_10.cas_interval,
    h7_10.avg_time,
    h7_10.avg_speed,
    h7_10.normal_avg_time,
    h7_10.normal_avg_speed
   FROM h7_10
UNION ALL
 SELECT h7_11_15_19.route_id,
    h7_11_15_19.datum,
    h7_11_15_19.cas_interval,
    h7_11_15_19.avg_time,
    h7_11_15_19.avg_speed,
    h7_11_15_19.normal_avg_time,
    h7_11_15_19.normal_avg_speed
   FROM h7_11_15_19;	 
   
-- analytic.v_bm_data_all source

CREATE OR REPLACE VIEW analytic.v_bm_data_all
AS WITH data1 AS (
         SELECT trasy.name,
            trasy.name_trasa,
            trasy.name_usek,
                CASE
                    WHEN trasy.is_opacny_smer THEN 'Zpět'::text
                    ELSE 'Tam'::text
                END AS smer,
            vbu.datum,
            vbu.cas_interval,
            vbu.avg_time,
            vbu.avg_speed,
            vbu.normal_avg_time,
            vbu.normal_avg_speed
           FROM analytic.v_bm_union2 vbu
             JOIN analytic.v_bm_trasy trasy ON trasy.id = vbu.route_id
        ), data2 AS (
         SELECT data1.name_trasa,
            data1.name_usek,
            'Oba'::text AS smer,
            data1.datum,
            data1.cas_interval,
            sum(data1.avg_time) AS avg_time,
            avg(data1.avg_speed) AS avg_speed,
            sum(data1.normal_avg_time) AS normal_avg_time,
            avg(data1.normal_avg_speed) AS normal_avg_speed
           FROM data1
          GROUP BY data1.name_trasa, data1.name_usek, 'Oba'::text, data1.datum, data1.cas_interval
        )
 SELECT data1.name,
    data1.name_trasa,
    data1.name_usek,
    data1.smer,
    data1.datum,
    data1.cas_interval,
    data1.avg_time,
    data1.avg_speed,
    data1.normal_avg_time,
    data1.normal_avg_speed
   FROM data1
UNION
 SELECT vbt.name,
    data2.name_trasa,
    data2.name_usek,
    data2.smer,
    data2.datum,
    data2.cas_interval,
    data2.avg_time,
    data2.avg_speed,
    data2.normal_avg_time,
    data2.normal_avg_speed
   FROM data2
     JOIN analytic.v_bm_trasy vbt ON vbt.name_trasa = data2.name_trasa AND vbt.name_usek = data2.name_usek AND NOT vbt.is_opacny_smer;

-- nové řešení	 