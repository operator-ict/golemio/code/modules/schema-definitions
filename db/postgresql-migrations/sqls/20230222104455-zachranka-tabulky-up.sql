CREATE TABLE python.ambulance_measurements (
	id_call bigint ,
	date_time_call timestamptz NULL,
	date_time_dispatch timestamptz NULL,
	date_time_scene timestamptz NULL,
	dptr_base text NULL,
	indication text NULL,
	dptr_coordinates text NULL,
	lng float NULL,
	lat float NULL,
	hospital text NULL,
	date_time_arvl_hospital timestamptz NULL,
	date_time_handover timestamptz NULL,
	date_time_dptr_hospital timestamptz NULL
);

CREATE TABLE python.ambulance_indication (
	indication text,
	indication_type text,
	seriousness text,
	is_certain text,
	indication_count integer,
	indication_type_count integer);
