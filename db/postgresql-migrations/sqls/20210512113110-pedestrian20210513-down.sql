drop view analytic.v_pedestrians_detections_api;
drop view analytic.v_pedestrians_directions_api;
drop view analytic.v_pedestrians_locations_api;

drop table analytic.pedestrians_directions_api;
drop TABLE analytic.pedestrians_locations_api;
