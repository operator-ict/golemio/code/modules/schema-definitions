--drop FUNCTION meta.import_from_json;

CREATE OR REPLACE FUNCTION meta.import_from_json(p_batch_id bigint, p_data json, p_table_schema character varying, p_table_name character varying, p_pk json, p_sort json, p_worker_name character varying, OUT x_inserted json, OUT x_updated json)
 RETURNS record
 LANGUAGE plpgsql
AS $function$


/*
Updated by Pavel Procházka, 20.2.2020
Note:
Upraveno pořadí polí klíčů pro zápis do návratové tabulky
Updated: Pavel Procházka, 15.4.2020, zakomentování řádku debug
Updated: Pavel Procházka, 26.1.2021, odstranění temp tabulek
updated: Pavel Procházka, 6.4.2021, odstranění temp tabulek doladění
*/

declare
    pole_seznam varchar;
    pole_seznam_excluded varchar; --EXCLUDED
    pole_sort varchar;
    --pole_seznam_datatype varchar;
    pole_pk varchar;
    query_rec record;
    x_sql varchar;
    x_sql_sor varchar; -- source
begin

-- ladění
--create table if not exists tmp.debug_log (cas timestamp default now(), sql text);

if p_data::text = E'[]'::json::text then
    --insert into tmp.debug_log(SQL) VALUES(p_data);
    return;
end if;

-- vzor P_pk: ["pole1", "sloupec2" ,"dasi-sloupec"]

-- kontrola pokud je vyplněn p_sort, musí být i p_pk
if p_sort is not null and p_pk is null then
    raise EXCEPTION 'Nelze vyplnit p_sort(%) a nemít vyplněn PK', p_sort;
end if;

-- kontrola existence tabulky
    if not exists
    (
        select table_name
        from information_schema.tables
        where upper(table_name) = upper(p_table_name)
        and upper(table_schema) = upper(p_table_schema)
    )   then
        raise EXCEPTION 'Tabulka (%.%) neexistuje', p_table_schema,p_table_name;
    end if;
-- /kontrola existence tabulky
--drop table if exists pp_data;

--create temp table pp_data as
--select p_data  as data;


--execute 'select p_data as data';
-- pole z json
FOR  query_rec IN
    select json_object_keys(p_data->0) pole
LOOP
    if not exists
    (
        select column_name
        from information_schema.columns
        where upper(table_schema) = upper(p_table_schema)
        and upper(table_name) = upper(p_table_name)
        and upper (column_name) = upper(query_rec.pole)
    )   then
        raise EXCEPTION 'Tabulka %.% neobsahuje pole %', p_table_schema,p_table_name,query_rec.pole;
    else
        if pole_seznam is null then
            pole_seznam := query_rec.pole;
            pole_seznam_excluded := 'EXCLUDED.'||query_rec.pole;
        else
            pole_seznam := pole_seznam ||', '||query_rec.pole;
            pole_seznam_excluded := pole_seznam_excluded ||', '||'EXCLUDED.'||query_rec.pole;
        end if;
    end if;
end loop;
-- /pole z json

-- Kontrola PK
FOR  query_rec IN
    select cast(json_array_elements_text(p_pk) as varchar) pole order by pole
LOOP
    if not exists
    (
        select column_name
        from information_schema.columns
        where upper(table_schema) = upper(p_table_schema)
        and upper(table_name) = upper(p_table_name)
        and upper (column_name) = upper(query_rec.pole)
    )   then
        raise EXCEPTION 'Tabulka %.% neobsahuje PK pole %', p_table_schema,p_table_name,query_rec.pole;
    else
        if pole_pk is null then
            pole_pk := query_rec.pole;
        else
            pole_pk := pole_pk ||', '||query_rec.pole;
        end if;
    end if;
end loop;
-- insert into tmp.debug_log(sql) values('pole_pk: '||pole_pk);
-- pole_pk nyní obsahuje seznam pk odělený čárkami

-- /Kontrola PK

-- kontrola sort
FOR  query_rec IN
    select cast(json_array_elements_text(p_sort) as varchar) pole
LOOP
    if not exists
    (
        select column_name
        from information_schema.columns
        where upper(table_schema) = upper(p_table_schema)
        and upper(table_name) = upper(p_table_name)
        and upper (column_name) = upper(query_rec.pole)
    )   then
        raise EXCEPTION 'Tabulka %.% neobsahuje sort pole %', p_table_schema,p_table_name,query_rec.pole;
    else
        if pole_sort is null then
            pole_sort := query_rec.pole;
        else
            pole_sort := pole_sort ||', '||query_rec.pole;
        end if;
    end if;
end loop;
-- /kontrol sort
/*
drop table if exists mytemp;
x_sql := 'create temp table mytemp (';
FOR  query_rec IN
select b.pole, data_type, character_maximum_length
from (
    select
        cast(json_array_elements_text(data) as varchar) pole
        from (select p_pk as data) a
    )b
    join information_schema.columns inf
    on inf.table_schema = p_table_schema
        and inf.table_name = p_table_name
        and inf.column_name = b.pole
    order by b.pole
LOOP
    if not right(x_sql,1)='(' then
        x_sql := x_sql||',';
    end if;
    x_sql := x_sql||query_rec.pole||' '||query_rec.data_type;
    if query_rec.character_maximum_length is not null
    then
        x_sql := x_sql||'('||query_rec.character_maximum_length||')';
    end if;
end loop;
x_sql := x_sql||',upd boolean)';

insert into tmp.debug_log (sql) values (x_sql);
begin
	execute x_sql;
exception
 when others then
     raise EXCEPTION 'Chyba(1) při provádění xsql:%', x_sql;

--	insert into tmp.debug_log (sql) values ('Chyba 1:');
--	insert into tmp.debug_log (sql) values ('Chyba 1:'||x_sql);
--	insert into tmp.debug_log (sql) values ('Chyba 1:'||x_sql_sor);


	return;
end;
*/

x_sql_sor := ' select '||pole_seznam;
x_sql_sor := x_sql_sor||', $1 create_batch_id'||chr(10);
x_sql_sor := x_sql_sor||',now() created_at'||chr(10);
x_sql_sor := x_sql_sor||',$2 created_by'||chr(10);

x_sql_sor := x_sql_sor||' from (select '||chr(10);
    -- příklad:             cis_id = sor.cis_id::bigint,
    FOR  query_rec IN
        select b.pole
        ,data_type ,character_maximum_length
        from (
            select json_object_keys(p_data->0) pole
        )b
        join information_schema.columns inf
        on inf.table_schema = p_table_schema
            and inf.table_name = p_table_name
            and inf.column_name = b.pole
    LOOP
        if not right(x_sql_sor,8)='select '||chr(10) then
            x_sql_sor:= x_sql_sor||',';
        end if;
        x_sql_sor:= x_sql_sor||'cast(json_array_elements(data)->>'''||query_rec.pole||''' as '||query_rec.data_type||') "'||query_rec.pole||'"'||chr(10);
    end loop;
    if p_sort is not null then
        x_sql_sor:= x_sql_sor||', row_number() over(partition by '||pole_pk||' order by tar.'||pole_sort||') rn';
    else
        x_sql_sor:= x_sql_sor||',1 rn';
    end if;
    x_sql_sor:= x_sql_sor||' from pp_data
)a where rn = 1 ';

if p_pk is not null
then
    x_sql_sor:= x_sql_sor||chr(10)||'on conflict('||pole_pk||') do update'||chr(10);
    x_sql_sor:= x_sql_sor|| ' set ('||pole_seznam||',update_batch_id,updated_at,updated_by) ='||chr(10)||' ('||pole_seznam_excluded||',$1,now(),$2)';
end if;

-- x_sql_sor nyní obsahuje zdrojový select
-- /pouze zdroj

-- insert

x_sql:='with '||chr(10);
x_sql:=x_sql||'pp_data as (select $3  as data),'||chr(10);
x_sql:=x_sql||'rows as ('||chr(10);
x_sql:=x_sql||'insert into '||p_table_schema||'.'||p_table_name||chr(10)||' ('||pole_seznam||',create_batch_id, created_at, created_by)'||chr(10);
x_sql:=x_sql||x_sql_sor||chr(10);
x_sql:=x_sql||'RETURNING '||pole_pk||',updated_at is not null as upd)'||chr(10);
--x_sql:=x_sql||' insert into mytemp ('||pole_pk||') select '||pole_pk||' from rows '||chr(10);
--x_sql:=x_sql||' insert into mytemp  select * from rows '||chr(10);
x_sql:=x_sql||' select array_to_json(array_agg(row)) from (select * from rows) row';

--x_sql:=x_sql||'select * into tmp.mytemp from rows '||chr(10);

-- insert into tmp.debug_log (sql) values (x_sql);
begin
	execute x_sql using p_batch_id,p_worker_name ,p_data into x_inserted;
exception
 when others then
	raise EXCEPTION 'Chyba(2) při provádění xsql:%', x_sql;
	/*
	insert into tmp.debug_log (sql) values ('Chyba 2:'||x_sql);
	insert into tmp.debug_log (sql) values ('Chyba 2:'||x_sql_sor);
	*/
	return;
end;

--into x_inserted;
/*
if p_pk is not null then
        x_sql:='select array_to_json(array_agg(row)) from (select * from mytemp) row';
        --insert into tmp.debug_log (sql) values (x_sql);
        execute x_sql into x_inserted;
    end if;
*/

--drop table if exists pp_data;
--drop table if exists mytemp;

end;

$function$
;
