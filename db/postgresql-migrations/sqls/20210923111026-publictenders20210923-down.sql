drop VIEW analytic.v_public_tenders_internal_frequent_suppliers;
DROP VIEW analytic.v_public_tenders_internal;
DROP VIEW analytic.v_public_tenders;
DROP TABLE if exists python.public_tenders;

CREATE TABLE python.public_tenders (
	id_zakazky text NULL,
	id_casti_zakazky text NULL,
	systemove_cislo_zakazky text NULL,
	evidencni_cislo_zakazky text NULL,
	cislo_vysledku_zadavaciho_rizeni text NULL,
	interni_cisla_smluv_objednavek text NULL,
	nazev_zakazky text NULL,
	nazev_casti_zakazky text NULL,
	externi_zastoupeni_zadavatele_v_rizeni text NULL,
	externi_zastoupeni_zadavatele_v_rizeni_nazev text NULL,
	externi_zastoupeni_zadavatele_v_rizeni_ico float8 NULL,
	druh_verejne_zakazky text NULL,
	druh_zadavaciho_rizeni text NULL,
	typ_zakazky text NULL,
	varianta_druhu_rizeni text NULL,
	jedna_se_o_cast_vz text NULL,
	zakazka_zavadejici_dns text NULL,
	faze_zakazky text NULL,
	celkova_predpokladana_hodnota_zakazky_bez_dph_kc float8 NULL,
	zakazka_souvisejici_s_vykonem_relevantni_cinnosti text NULL,
	hlavni_nipez_kod_zakazky text NULL,
	organizacni_jednotka_spravy text NULL,
	datum_zahajeni_zadavaciho_rizeni timestamptz NULL,
	lhuta_pro_podani_nabidek_na_profilu timestamptz NULL,
	datum_otevirani_nabidek_na_profilu timestamptz NULL,
	lhuta_pro_doruceni_zadosti_o_ucast_na_profilu timestamptz NULL,
	hodnotici_kriteria text NULL,
	vysledkem_je_ramcova_dohoda text NULL,
	hodnoceni_formou_elektronicke_aukce_prostrednictvim_ta text NULL,
	hodnoceni_formou_elektronicke_aukce_mimo_aplikaci_ta text NULL,
	spolecnosti_oslovene_k_podani_nabidky_pocet int8 NULL,
	spolecnosti_oslovene_k_podani_nabidky_nazev text NULL,
	pocet_zadosti_o_ucast int8 NULL,
	seznam_zadosti_o_ucast text NULL,
	pocet_nabidek int8 NULL,
	seznam_nabidek_a_nabidkova_cena_bez_dph text NULL,
	ucastnik_zadavaciho_rizeni_nazev text NULL,
	nazev_smluvniho_partnera text NULL,
	ico_smluvniho_partnera text NULL,
	smluvni_cena_bez_dph_kc float8 NULL,
	smluvni_cena_s_dph_kc float8 NULL,
	datum_uzavreni_smlouvy timestamptz NULL,
	skutecne_uhrazena_cena_bez_dph float8 NULL,
	skutecne_uhrazena_cena_s_dph float8 NULL,
	updated_at timestamptz NULL,
	updated_by text NULL,
	created_at timestamptz NULL,
	created_by text NULL
);


-- analytic.v_public_tenders source

CREATE OR REPLACE VIEW analytic.v_public_tenders
AS SELECT pt.id_zakazky,
    pt.nazev_zakazky,
        CASE
            WHEN "position"(pt.organizacni_jednotka_spravy, '-'::text) = 0 THEN pt.organizacni_jednotka_spravy
            ELSE "left"(pt.organizacni_jednotka_spravy, "position"(pt.organizacni_jednotka_spravy, '-'::text) - 2)
        END AS organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
        CASE
            WHEN pt.ico_smluvniho_partnera = ANY (ARRAY['00005886'::text, '02795281'::text, '60194120'::text, '03447286'::text, '25656112'::text, '25672541'::text, '27251918'::text, '26442272'::text, '00409316'::text, '25649329'::text, '07312890'::text, '26714124'::text]) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.datum_uzavreni_smlouvy::date AS datum_uzavreni_smlouvy,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.smluvni_cena_s_dph_kc > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
    pt.datum_zahajeni_zadavaciho_rizeni,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.updated_at,
    pt.created_at
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND pt.datum_uzavreni_smlouvy >= '2020-09-01 00:00:00'::timestamp without time zone AND pt.datum_uzavreni_smlouvy < CURRENT_DATE AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));
  
  drop table python.public_tenders_partners_ico;
  drop table python.public_tenders_odbory;