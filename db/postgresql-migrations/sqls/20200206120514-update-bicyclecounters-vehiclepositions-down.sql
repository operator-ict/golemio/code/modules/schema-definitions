-- VEHICLEPOSITIONS

ALTER TABLE "public"."vehiclepositions_trips" RENAME COLUMN "cis_trip_number" TO "cis_line_number";

CREATE OR REPLACE VIEW "public"."v_vehiclepositions_last_position"
  AS
    SELECT
      DISTINCT ON ("vehiclepositions_positions"."trips_id") "vehiclepositions_positions".*,
      "vehiclepositions_trips"."gtfs_route_id", "vehiclepositions_trips"."gtfs_route_short_name", 
      "vehiclepositions_trips"."gtfs_trip_id", "vehiclepositions_trips"."gtfs_trip_headsign",
      "vehiclepositions_trips"."vehicle_type_id", "vehiclepositions_trips"."wheelchair_accessible"
    FROM "public"."vehiclepositions_positions" AS "vehiclepositions_positions"
      JOIN "vehiclepositions_trips" AS "vehiclepositions_trips"
        ON ("vehiclepositions_trips"."id" = "vehiclepositions_positions"."trips_id" AND
            "vehiclepositions_trips"."gtfs_trip_id" IS NOT NULL)
    WHERE "vehiclepositions_positions"."created_at" > (NOW() - INTERVAL '5 min') AND "vehiclepositions_positions"."delay" IS NOT NULL
    ORDER BY "vehiclepositions_positions"."trips_id", "vehiclepositions_positions"."created_at" DESC;

CREATE OR REPLACE VIEW "public"."v_vehiclepositions_trips_v1"
  AS
    SELECT "v_vehiclepositions_trips_v1"."cis_line_id" AS "cis_id",
      "v_vehiclepositions_trips_v1"."cis_line_number" AS "cis_number",
      "v_vehiclepositions_trips_v1"."sequence_id" AS "cis_order",
      "v_vehiclepositions_trips_v1"."cis_line_short_name" AS "cis_short_name",
      "v_vehiclepositions_trips_v1"."created_at",
      "v_vehiclepositions_trips_v1"."gtfs_route_id",
      "v_vehiclepositions_trips_v1"."gtfs_route_short_name",
      "v_vehiclepositions_trips_v1"."gtfs_trip_id",
      "v_vehiclepositions_trips_v1"."id",
      "v_vehiclepositions_trips_v1"."updated_at",
      "v_vehiclepositions_trips_v1"."start_cis_stop_id",
      "v_vehiclepositions_trips_v1"."start_cis_stop_platform_code",
      "v_vehiclepositions_trips_v1"."start_time",
      "v_vehiclepositions_trips_v1"."start_timestamp",
      "v_vehiclepositions_trips_v1"."vehicle_type_id" AS "vehicle_type",
      "v_vehiclepositions_trips_v1"."wheelchair_accessible",
      "v_vehiclepositions_trips_v1"."create_batch_id",
      "v_vehiclepositions_trips_v1"."created_by",
      "v_vehiclepositions_trips_v1"."update_batch_id",
      "v_vehiclepositions_trips_v1"."updated_by",
      "v_vehiclepositions_trips_v1"."agency_name_scheduled" AS "cis_agency_name",
      "v_vehiclepositions_trips_v1"."origin_route_name" AS "cis_parent_route_name",
      "v_vehiclepositions_trips_v1"."agency_name_real" AS "cis_real_agency_name",
      "v_vehiclepositions_trips_v1"."vehicle_registration_number" AS "cis_vehicle_registration_number"
    FROM "public"."vehiclepositions_trips" AS "v_vehiclepositions_trips_v1";

UPDATE "public"."vehiclepositions_vehicle_types" SET
    "id" = '4',
    "abbreviation" = 'BusReg',
    "description_cs" = 'regionalní autobus',
    "description_en" = 'regional bus'
WHERE "id" = '4';

-- BICYCLECOUNTERS

drop view if exists analytic.v_camea_last_update;
drop view if exists analytic.v_camea_data_quality;
drop view if exists analytic.v_camea_data_disbalance;
drop view if exists analytic.v_camea_bikecounters_all_data_today;
drop view if exists analytic.v_camea_bikecounters_all_data_table_today;
drop view if exists analytic.v_camea_bikecounters_all_data_history;
drop table if exists analytic.camea_bikecounters_in_contract;