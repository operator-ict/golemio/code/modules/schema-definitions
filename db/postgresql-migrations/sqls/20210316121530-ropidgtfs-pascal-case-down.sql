ALTER TABLE public.ropidgtfs_cis_stops RENAME COLUMN alt_idos_name TO "altIdosName";
ALTER TABLE public.ropidgtfs_cis_stops RENAME COLUMN jtsk_x TO "jtskX";
ALTER TABLE public.ropidgtfs_cis_stops RENAME COLUMN jtsk_y TO "jtskY";
ALTER TABLE public.ropidgtfs_cis_stops RENAME COLUMN wheelchair_access TO "wheelchairAccess";

ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN avg_jtsk_x TO "avgJtskX";
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN avg_jtsk_y TO "avgJtskY";
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN avg_lat TO "avgLat";
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN avg_lon TO "avgLon";
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN district_code TO "districtCode";
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN full_name TO "fullName";
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN idos_category TO "idosCategory";
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN idos_name TO "idosName";
ALTER TABLE public.ropidgtfs_cis_stop_groups RENAME COLUMN unique_name TO "uniqueName";
