CREATE OR REPLACE VIEW analytic.v_consumption_energy_deduct_month_ptv
AS SELECT date_part('year'::text, consumption_energy_consumption.time_utc) AS rok,
    date_part('month'::text, consumption_energy_consumption.time_utc) AS mesic,
    consumption_energy_consumption.addr,
    consumption_energy_consumption.var,
    consumption_energy_consumption.commodity,
    consumption_energy_consumption.unit,
    sum(consumption_energy_consumption.delta_value) AS value,
    max(consumption_energy_consumption.time_utc::date) AS data_do,
    count(*) AS count
   FROM analytic.v_consumption_energy_delta consumption_energy_consumption
   where (meter <> '300066393' or time_utc::date <= '2021-04-01')
--   			and addr = '/2.9/VF1'
  GROUP BY 
	(date_part('year'::text, consumption_energy_consumption.time_utc)), 
	(date_part('month'::text, consumption_energy_consumption.time_utc)), 
	consumption_energy_consumption.addr, 
	consumption_energy_consumption.var, 
	consumption_energy_consumption.commodity, 
	consumption_energy_consumption.unit;
