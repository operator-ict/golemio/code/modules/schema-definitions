ALTER TABLE public.parkings RENAME COLUMN source_id TO "sourceId";
ALTER TABLE public.parkings RENAME COLUMN data_provider TO "dataProvider";
ALTER TABLE public.parkings RENAME COLUMN usage_scenario TO "usageScenario";
ALTER TABLE public.parkings RENAME COLUMN date_modified TO "dateModified";
ALTER TABLE public.parkings RENAME COLUMN area_served TO "areaServed";
ALTER TABLE public.parkings RENAME COLUMN payment_url TO "paymentUrl";
ALTER TABLE public.parkings RENAME COLUMN total_spot_number TO "totalSpotNumber";

ALTER TABLE public.parkings_measurements RENAME COLUMN source_id TO "sourceId";
ALTER TABLE public.parkings_measurements RENAME COLUMN available_spot_number TO "availableSpotNumber";
ALTER TABLE public.parkings_measurements RENAME COLUMN closed_spot_number TO "closedSpotNumber";
ALTER TABLE public.parkings_measurements RENAME COLUMN occupied_spot_number TO "occupiedSpotNumber";
ALTER TABLE public.parkings_measurements RENAME COLUMN total_spot_number TO "totalSpotNumber";
ALTER TABLE public.parkings_measurements RENAME COLUMN date_modified TO "dateModified";
