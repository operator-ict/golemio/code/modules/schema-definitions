drop VIEW if exists analytic.v_sreality;
DROP TABLE if exists python.sreality;

CREATE TABLE python.sreality (
	week_label text NULL,
	week_number int8 NULL,
	offer_type text NULL,
	district text NULL,
	category text NULL,
	offers_count int8 NULL,
	median_price float8 NULL,
	week_start_dt date NULL,
	updated_at timestamp NULL
);

CREATE OR REPLACE VIEW analytic.v_sreality
AS SELECT s.week_start_dt,
    s.week_label,
    s.week_number,
    s.offer_type,
    s.district,
    s.category,
    s.offers_count,
    s.median_price,
        CASE
            WHEN s.district IS NULL OR s.district = ''::text OR s.district = 'All'::text THEN NULL::numeric
            WHEN "right"(s.district, 1)::numeric = 0::numeric THEN 10::numeric
            ELSE "right"(s.district, 1)::numeric
        END AS district_order
   FROM python.sreality s;
   

CREATE OR REPLACE VIEW analytic.v_airbnb_rooms_stays
AS SELECT o.o_year,
    o.o_month,
    count(DISTINCT o.room_id) AS active_roooms,
    round(sum(o.stays_count)) AS stays_count,
    o.nazev_mc,
    rank() OVER (ORDER BY o.o_month) AS month_order
   FROM analytic.v_airbnb_occupancy_monthly o
  GROUP BY o.o_year, o.o_month, o.nazev_mc;   
  

CREATE OR REPLACE VIEW analytic.v_airbnb_sreality_offers
AS WITH airbnb_offers AS (
         SELECT date_trunc('week'::text, l.room_created::timestamp with time zone) AS week_start_dt,
            date_part('week'::text, l.room_created) AS week_number,
                CASE
                    WHEN char_length(d.kod_mo::text) = 2 THEN concat('Praha ', "left"(d.kod_mo::text, 1))
                    ELSE concat('Praha ', "left"(d.kod_mo::text, 2))
                END AS district,
            count(l.room_id) AS offers_count
           FROM python.airbnb_listings l
             JOIN common.citydistricts d ON st_within(st_setsrid(st_point(l.longitude, l.latitude), 4326), d.geom)
          WHERE l.room_created >= '2020-01-06'::date
          GROUP BY d.kod_mo, (date_trunc('week'::text, l.room_created::timestamp with time zone)), (concat('Praha ', "left"(d.kod_mo::text, 1))), (date_part('week'::text, l.room_created))
        )
 SELECT s.week_start_dt,
    s.week_label,
    s.week_number,
    s.district,
    sum(
        CASE
            WHEN s.offer_type = 'Pronájem'::text THEN s.offers_count::numeric
            ELSE 0::numeric
        END) AS sreality_offers,
        CASE
            WHEN a.offers_count IS NULL THEN 0::bigint
            ELSE a.offers_count
        END AS airbnb_offers,
        CASE
            WHEN s.district IS NULL OR s.district = ''::text OR s.district = 'All'::text THEN NULL::numeric
            WHEN "right"(s.district, 1)::numeric = 0::numeric THEN 10::numeric
            ELSE "right"(s.district, 1)::numeric
        END AS district_order,
    sum(
        CASE
            WHEN s.offer_type = 'Prodej'::text THEN s.offers_count::numeric
            ELSE 0::numeric
        END) AS sreality_sales
   FROM python.sreality s
     LEFT JOIN airbnb_offers a ON a.week_start_dt = s.week_start_dt AND a.district = s.district
  WHERE s.district <> 'All'::text AND s.category = 'All'::text
  GROUP BY s.week_start_dt, s.week_label, s.week_number, a.offers_count, s.district
  ORDER BY s.week_start_dt DESC, s.district;  
  
  
drop view if exists analytic.v_covid19_airbnb;
drop view if exists analytic.v_covid19_airbnb_sreality_offers;
drop view if exists analytic.v_covid19_sreality;