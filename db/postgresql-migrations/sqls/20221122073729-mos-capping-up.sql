CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_more_rides
AS SELECT v.date::date AS date,
    v.cptp,
    v.account_id,
    v.n_ticket
   FROM ( SELECT mmt.cptp,
            count(mmt.ticket_id) AS n_ticket,
            mmt.account_id,
            mmt.date,
            row_number() OVER (PARTITION BY (mmt.date::date), mmt.account_id) AS row_number
           FROM mos_ma_ticketpurchases mmt
          WHERE mmt.cptp = 124 OR mmt.cptp = 624
          GROUP BY mmt.account_id, mmt.date, mmt.cptp) v
  WHERE v.row_number = 1;