DROP VIEW analytic.v_ropidbi_ticket_activation_types;

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_types
AS SELECT
    v_ropidbi_ticket.act_date,
    v_ropidbi_ticket.activation_type,
    v_ropidbi_ticket.oblast,
    v_ropidbi_ticket.tariff_name,
    count(*) AS count
FROM analytic.v_ropidbi_ticket
WHERE v_ropidbi_ticket.activation_type <> 'Zatím neaktivováno'::text AND v_ropidbi_ticket.tariff_name::text <> ''::text
GROUP BY v_ropidbi_ticket.act_date, v_ropidbi_ticket.activation_type, v_ropidbi_ticket.oblast, v_ropidbi_ticket.tariff_name;

DROP VIEW analytic.v_ropidbi_ticket_activation_times;

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_times
AS SELECT 
    v_ropidbi_ticket.act_date,
    v_ropidbi_ticket.act_time,
    v_ropidbi_ticket.tariff_name,
    v_ropidbi_ticket.oblast,
    count(*) AS count
FROM analytic.v_ropidbi_ticket
GROUP BY v_ropidbi_ticket.act_date, v_ropidbi_ticket.act_time, v_ropidbi_ticket.tariff_name, v_ropidbi_ticket.oblast;

DROP VIEW analytic.v_ropidbi_ticket_sales;

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales AS
SELECT
    v_ropidbi_ticket.purchase_date::date AS purchase_date,
    v_ropidbi_ticket.tariff_name,
    v_ropidbi_ticket.oblast,
    count(*) AS count
FROM analytic.v_ropidbi_ticket
WHERE v_ropidbi_ticket.purchase_date::date IS NOT NULL AND v_ropidbi_ticket.tariff_name IS NOT NULL
GROUP BY (v_ropidbi_ticket.purchase_date::date), v_ropidbi_ticket.tariff_name, v_ropidbi_ticket.oblast;
