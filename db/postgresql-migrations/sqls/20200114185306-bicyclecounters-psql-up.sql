-- Table: public.bicyclecounters_locations

CREATE TABLE public.bicyclecounters_locations
(
  id character varying(255) NOT NULL, /* "camea-BC_AT-STLA", "ecoCounter-100047647" */
  vendor_id character varying(255) NOT NULL, /* "BC_AT-STLA" */
  lat double precision NOT NULL,
  lng double precision NOT NULL,
  "name" character varying(255),
  "route" character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),

  CONSTRAINT bicyclecounters_locations_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- Table: public.bicyclecounters_directions

CREATE TABLE public.bicyclecounters_directions
(
  id character varying(255) NOT NULL, /* "camea-BC_AT-LA", "ecoCounter-103047647" */
  vendor_id character varying(255) NOT NULL, /* "BC_AT-LA", "103047647" */
  locations_id character varying(255) NOT NULL, /* camea-BC_AT-STLA", "ecoCounter-100047647" */
  "name" character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),

  CONSTRAINT bicyclecounters_directions_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX bicyclecounters_directions_locations_id ON public.bicyclecounters_directions USING btree (locations_id);

-- Table: public.bicyclecounters_detections

CREATE TABLE public.bicyclecounters_detections
(
  locations_id character varying(255) NOT NULL, /* camea-BC_AT-STLA" */
  directions_id character varying(255) NOT NULL, /* "camea-BC_AT-LA" */
  measured_from bigint NOT NULL,
  measured_to bigint NOT NULL,
  "value" integer,

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),

  CONSTRAINT bicyclecounters_detections_pkey PRIMARY KEY (locations_id, directions_id, measured_from)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX bicyclecounters_detections_locations_id ON public.bicyclecounters_detections USING btree (locations_id);

CREATE INDEX bicyclecounters_detections_directions_id ON public.bicyclecounters_detections USING btree (directions_id);


-- Table: public.bicyclecounters_temperatures

CREATE TABLE public.bicyclecounters_temperatures
(
  locations_id character varying(255) NOT NULL, /* camea-BC_AT-STLA" */
  measured_from bigint NOT NULL,
  measured_to bigint NOT NULL,
  "value" integer NOT NULL,

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),

  CONSTRAINT bicyclecounters_temperatures_pkey PRIMARY KEY (locations_id, measured_from)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX bicyclecounters_temperatures_locations_id ON public.bicyclecounters_temperatures USING btree (locations_id);