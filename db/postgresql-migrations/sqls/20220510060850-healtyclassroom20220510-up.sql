drop VIEW analytic.v_data_from_materialized_view_teachers;
drop VIEW analytic.v_data_from_materialized_view;

drop MATERIALIZED VIEW analytic.mv_data_with_timetables;

drop VIEW analytic.v_data_with_evaluations;
drop VIEW analytic.v_data_with_5min_intervals;
drop VIEW analytic.v_source_filtered;

drop TABLE python.values_evaluation;
drop TABLE python.timetables;
drop TABLE python.timetable_parameters;
drop TABLE python.source_csv_data;
drop TABLE python.classrooms ;

---

CREATE TABLE python.healthy_classroom_classrooms (
	classroom_id varchar(10) NOT NULL,
	device_id varchar(50) NOT NULL,
	cardinal_direction varchar(50) NULL,
	purpose varchar(100) NULL,
	floor_nr int4 NULL,
	new_technlogy bool NULL,
	CONSTRAINT healthy_classroom_classrooms_pk PRIMARY KEY (device_id)
);

CREATE TABLE python.healthy_classroom_source_data (
	device_id varchar(10) NOT NULL,
	nr int4 NULL,
	"date" timestamp NOT NULL,
	"action" varchar(50) NULL,
	product varchar(50) NULL,
	co2 float8 NULL,
	temperature float8 NULL,
	humidity float8 NULL,
	air_quality_level varchar(50) NULL,
	air_quality_level_source varchar(50) NULL,
	co2_level varchar(50) NULL,
	dryness_level varchar(50) NULL,
	mould_indicator varchar(50) NULL,
	dust_mites_indicator varchar(50) NULL,
	hci varchar(50) NULL,
	frame_nr varchar(50) NULL,
	short_message varchar(50) NULL,
	long_message varchar(50) NULL,
	lqi varchar(50) NULL,
	status varchar(50) NULL
);

CREATE TABLE python.healthy_classroom_timetable_parameters (
	hour_index int4 NOT NULL,
	begin_time time NULL,
	end_time time NULL,
	CONSTRAINT timetable_parameters_pk PRIMARY KEY (hour_index)
);

CREATE TABLE python.healthy_classroom_timetables (
	timetable_date date NOT NULL,
	hour_index int4 NOT NULL,
	day_index int4 NOT NULL,
	subject_id varchar(10) NULL,
	suject_abbrev varchar(10) NULL,
	subject_name varchar(100) NULL,
	teacher_id varchar(10) NULL,
	teacher_name varchar(100) NULL,
	room_id varchar(10) NULL,
	room_abbrev varchar(10) NULL
);

CREATE TABLE python.healthy_classroom_values_evaluation (
	metric text NOT NULL,
	low_limit int4 NULL,
	high_limit int4 NULL,
	evaluation text NULL,
	evaluation_order numeric NOT NULL,
	CONSTRAINT values_evaluation_pk2 PRIMARY KEY (metric, evaluation_order)
);

-- healthy_classroom_test.v_healthy_classroom_source_filtered source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_source_filtered
AS SELECT to_timestamp(healthy_classroom_source_data.date::text, 'YYYY-MM-DD HH24:MI:SS'::text) AS "timestamp",
    healthy_classroom_source_data.device_id,
    healthy_classroom_source_data.co2_level,
    healthy_classroom_source_data.dryness_level,
    healthy_classroom_source_data.air_quality_level,
    healthy_classroom_source_data.temperature,
    date_part('hour'::text, to_timestamp(healthy_classroom_source_data.date::text, 'YYYY-MM-DD HH24:MI:SS'::text)) AS measure_hour,
    date_trunc('hour'::text, to_timestamp(healthy_classroom_source_data.date::text, 'YYYY-MM-DD HH24:MI:SS'::text)) + (((date_part('minute'::text, to_timestamp(healthy_classroom_source_data.date::text, 'YYYY-MM-DD HH24:MI:SS'::text))::integer / 5 * 5) || ' minutes'::text)::interval) AS five_min_timestamp,
    healthy_classroom_source_data.humidity,
    healthy_classroom_source_data.co2
   FROM python.healthy_classroom_source_data
  WHERE healthy_classroom_source_data.co2 IS NOT NULL 
  OR healthy_classroom_source_data.humidity IS NOT NULL 
  OR healthy_classroom_source_data.air_quality_level IS NOT NULL 
  OR healthy_classroom_source_data.temperature IS NOT NULL;
  
-- healthy_classroom_test.v_healthy_classroom_5min_intervals source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_5min_intervals
AS WITH dta AS (
         SELECT DISTINCT i.generate_series,
            d.five_min_timestamp,
            i.device_id::text AS device_id,
            d.co2,
            d.humidity,
            d.temperature,
            date_part('hour'::text, i.generate_series) AS measure_hour,
            d.air_quality_level
           FROM ( SELECT generate_series.generate_series,
                    dev.device_id
                   FROM generate_series('2021-10-01 00:00:00+02'::timestamp with time zone, now(), '00:05:00'::interval) generate_series(generate_series)
                     CROSS JOIN ( SELECT DISTINCT v_healthy_classroom_source_filtered.device_id
                           FROM analytic.v_healthy_classroom_source_filtered) dev) i
             LEFT JOIN analytic.v_healthy_classroom_source_filtered d ON i.generate_series = d.five_min_timestamp AND i.device_id::text = d.device_id::text
          ORDER BY i.generate_series
        )
 SELECT DISTINCT c.generate_series,
    c.measure_hour,
    c.device_id,
    c.co2,
    max(c.co2::text) OVER (PARTITION BY c.co2_grp, c.device_id ORDER BY c.generate_series) AS co2_all,
    c.humidity,
    max(c.humidity::text) OVER (PARTITION BY c.humidity_grp, c.device_id ORDER BY c.generate_series) AS humidity_all,
    c.temperature,
    max(c.temperature::text) OVER (PARTITION BY c.temperature_grp, c.device_id ORDER BY c.generate_series) AS temperature_all,
    c.air_quality_level,
    max(c.air_quality_level::text) OVER (PARTITION BY c.air_quality_grp, c.device_id ORDER BY c.generate_series) AS air_quality_level_all,
    cr.classroom_id,
    cr.cardinal_direction,
    cr.floor_nr
   FROM ( SELECT dta.generate_series,
            dta.device_id,
            dta.co2,
            dta.measure_hour,
            dta.humidity,
            dta.temperature,
            dta.air_quality_level,
            sum(
                CASE
                    WHEN dta.co2 IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS co2_grp,
            sum(
                CASE
                    WHEN dta.humidity IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS humidity_grp,
            sum(
                CASE
                    WHEN dta.temperature IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS temperature_grp,
            sum(
                CASE
                    WHEN dta.air_quality_level IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS air_quality_grp
           FROM dta) c
     LEFT JOIN python.healthy_classroom_classrooms cr ON c.device_id = cr.device_id::text
  WHERE cr.new_technlogy = false
  ORDER BY c.generate_series, c.measure_hour;  
  
-- healthy_classroom_test.v_healthy_classroom_evaluations source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_evaluations
AS SELECT DISTINCT i.generate_series AS measure_time,
    i.measure_hour,
    i.device_id,
    i.air_quality_level,
    to_number(i.co2::text, 'L9G999g999.99'::text) AS co2,
    to_number(i.co2_all, 'L9G999g999.99'::text) AS co2_all,
    to_number(i.humidity::text, 'L9G999g999.99'::text) AS humidity,
    to_number(i.humidity_all, 'L9G999g999.99'::text) AS humidity_all,
    to_number(i.temperature::text, 'L9G999g999.99'::text) AS temperature,
    to_number(i.temperature_all, 'L9G999g999.99'::text) AS temperature_all,
    e_co2.evaluation AS co2_eval,
    e_hum.evaluation AS humidity_eval,
    e_tem.evaluation AS temperature_eval,
    date_trunc('day'::text, i.generate_series) AS measure_time_day,
        CASE
            WHEN lag(i.co2_all) OVER (PARTITION BY i.device_id ORDER BY i.generate_series) > i.co2_all THEN 1
            ELSE 0
        END AS co2_decrease,
    e_co2.evaluation_order AS co2_eval_order,
    e_hum.evaluation_order AS humidity_eval_order,
    e_tem.evaluation_order AS temperature_eval_order,
        CASE
            WHEN i.air_quality_level_all = 'bad'::text THEN 'Špatná'::text
            WHEN i.air_quality_level_all = 'poor'::text THEN 'Slabá'::text
            WHEN i.air_quality_level_all = 'fair'::text THEN 'Dostatečná'::text
            WHEN i.air_quality_level_all = 'good'::text THEN 'Dobrá'::text
            WHEN i.air_quality_level_all = 'excellent'::text THEN 'Vynikající'::text
            ELSE NULL::text
        END AS air_quality_level_all,
        CASE
            WHEN i.air_quality_level_all = 'Bad'::text THEN 1
            WHEN i.air_quality_level_all = 'Poor'::text THEN 2
            WHEN i.air_quality_level_all = 'Fair'::text THEN 3
            WHEN i.air_quality_level_all = 'Good'::text THEN 4
            WHEN i.air_quality_level_all = 'Excellent'::text THEN 5
            ELSE NULL::integer
        END AS air_quality_level_order,
    i.floor_nr,
    i.classroom_id,
    i.cardinal_direction
   FROM analytic.v_healthy_classroom_5min_intervals i
     LEFT JOIN python.healthy_classroom_values_evaluation e_co2 ON i.co2_all::numeric >= e_co2.low_limit::numeric AND i.co2_all::numeric < e_co2.high_limit::numeric AND e_co2.metric = 'co2'::text
     LEFT JOIN python.healthy_classroom_values_evaluation e_hum ON i.humidity_all::numeric >= e_hum.low_limit::numeric AND i.humidity_all::numeric < e_hum.high_limit::numeric AND e_hum.metric = 'humidity'::text
     LEFT JOIN python.healthy_classroom_values_evaluation e_tem ON i.temperature_all::numeric >= e_tem.low_limit::numeric AND i.temperature_all::numeric < e_tem.high_limit::numeric AND e_tem.metric = 'temperature'::text;  

-- healthy_classroom_test.mv_healthy_classroom_with_timetables source

CREATE MATERIALIZED VIEW analytic.mv_healthy_classroom_with_timetables
TABLESPACE pg_default
AS SELECT DISTINCT e.measure_time,
    e.measure_hour,
    e.device_id,
    e.air_quality_level,
    e.co2,
    e.co2_all,
    e.humidity,
    e.humidity_all,
    e.temperature,
    e.temperature_all,
    e.co2_eval,
    e.humidity_eval,
    e.temperature_eval,
    e.measure_time_day,
    e.co2_decrease,
    e.co2_eval_order,
    e.humidity_eval_order,
    e.temperature_eval_order,
    e.air_quality_level_all,
    e.air_quality_level_order,
    e.floor_nr,
    e.classroom_id,
    e.cardinal_direction,
    t.begin_datetime,
    t.end_datetime,
    t.suject_abbrev,
    t.subject_name,
    t.teacher_name
   FROM analytic.v_healthy_classroom_evaluations e
     LEFT JOIN ( SELECT DISTINCT t_1.room_abbrev,
            t_1.timetable_date + p.begin_time AS begin_datetime,
            t_1.timetable_date + p.end_time AS end_datetime,
            t_1.suject_abbrev,
            t_1.subject_name,
            t_1.teacher_name
           FROM python.healthy_classroom_timetables t_1
             LEFT JOIN python.healthy_classroom_timetable_parameters p 
			 ON t_1.hour_index = p.hour_index) t 
	ON t.room_abbrev::text = "right"(e.classroom_id::text, 3) AND e.measure_time >= t.begin_datetime AND e.measure_time <= t.end_datetime
WITH DATA;	 

-- healthy_classroom_test.v_healthy_classroom_mv_data source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_mv_data
AS SELECT DISTINCT mv_healthy_classroom_with_timetables.measure_time,
    mv_healthy_classroom_with_timetables.measure_hour,
    mv_healthy_classroom_with_timetables.device_id,
    mv_healthy_classroom_with_timetables.air_quality_level,
    mv_healthy_classroom_with_timetables.co2,
    mv_healthy_classroom_with_timetables.co2_all,
    mv_healthy_classroom_with_timetables.humidity,
    mv_healthy_classroom_with_timetables.humidity_all,
    mv_healthy_classroom_with_timetables.temperature,
    mv_healthy_classroom_with_timetables.temperature_all,
    mv_healthy_classroom_with_timetables.co2_eval,
    mv_healthy_classroom_with_timetables.humidity_eval,
    mv_healthy_classroom_with_timetables.temperature_eval,
    mv_healthy_classroom_with_timetables.measure_time_day,
    mv_healthy_classroom_with_timetables.co2_decrease,
    mv_healthy_classroom_with_timetables.co2_eval_order,
    mv_healthy_classroom_with_timetables.humidity_eval_order,
    mv_healthy_classroom_with_timetables.temperature_eval_order,
    mv_healthy_classroom_with_timetables.air_quality_level_all,
    mv_healthy_classroom_with_timetables.air_quality_level_order,
    mv_healthy_classroom_with_timetables.floor_nr,
    mv_healthy_classroom_with_timetables.classroom_id,
    mv_healthy_classroom_with_timetables.cardinal_direction
   FROM analytic.mv_healthy_classroom_with_timetables;

-- healthy_classroom_test.v_healthy_classroom_mv_data_teachers source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_mv_data_teachers
AS SELECT mv_healthy_classroom_with_timetables.measure_time_day,
    sum(mv_healthy_classroom_with_timetables.co2_decrease) AS co2_decrease,
    mv_healthy_classroom_with_timetables.floor_nr,
    mv_healthy_classroom_with_timetables.classroom_id,
    mv_healthy_classroom_with_timetables.begin_datetime,
    count(DISTINCT mv_healthy_classroom_with_timetables.begin_datetime) AS classes_cnt,
    mv_healthy_classroom_with_timetables.subject_name,
    min(mv_healthy_classroom_with_timetables.teacher_name::text) AS teacher_name
   FROM analytic.mv_healthy_classroom_with_timetables
  WHERE mv_healthy_classroom_with_timetables.begin_datetime IS NOT NULL
  GROUP BY mv_healthy_classroom_with_timetables.measure_time_day, mv_healthy_classroom_with_timetables.floor_nr, mv_healthy_classroom_with_timetables.classroom_id, mv_healthy_classroom_with_timetables.subject_name, mv_healthy_classroom_with_timetables.begin_datetime;   