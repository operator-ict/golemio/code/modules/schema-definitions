/* Replace with your SQL commands */

-- analytic.v_containers_24h_missing source

CREATE OR REPLACE VIEW analytic.v_containers_24h_missing
AS SELECT containers_measurement.container_code AS container_id,
    max(containers_measurement.measured_at) AS measured_at
   FROM containers_measurement
  WHERE containers_measurement.measured_at < (CURRENT_DATE - 1) OR containers_measurement.measured_at IS NULL
  GROUP BY containers_measurement.container_code;