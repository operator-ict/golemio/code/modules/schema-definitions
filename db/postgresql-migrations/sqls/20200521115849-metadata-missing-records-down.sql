/* Replace with your SQL commands */

-- wazeccp
delete from meta."extract" 
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='wazeccp');

delete from meta.dataset
where code_dataset ='wazeccp';

-- vehiclepositions
delete from meta."extract" 
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='vehiclepositions');

delete from meta.dataset
where code_dataset ='vehiclepositions';

-- ropidgtfs
delete from meta."extract" 
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='ropidgtfs');

delete from meta.dataset
where code_dataset ='ropidgtfs';

-- parkomats
delete from meta."extract" 
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='parkomats');

delete from meta.dataset
where code_dataset ='parkomats';

-- parkings
delete from meta."extract" 
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='parkings');

delete from meta.dataset
where code_dataset ='parkings';

-- MOS_MA
delete from meta."extract" 
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='mos_ma');

delete from meta.dataset
where code_dataset ='mos_ma';

-- bicyclecounters
delete from meta."extract" 
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='bicyclecounters');

delete from meta.dataset
where code_dataset ='bicyclecounters';

-- merakiaccesspoints
/*
delete from meta."extract" 
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='merakiaccesspoints');

delete from meta.dataset
where code_dataset ='merakiaccesspoints';
*/

-- mobileappstatistics
delete from meta."extract" 
where id_dataset = (select id_dataset from meta.dataset where code_dataset ='mobileappstatistics');

delete from meta.dataset
where code_dataset ='mobileappstatistics';

-- mos_be
-- dataset již zaregistrován
delete from meta."extract" 
where name_extract in ('mos_be_accounts','mos_be_coupons','mos_be_customers','mos_be_zones');
