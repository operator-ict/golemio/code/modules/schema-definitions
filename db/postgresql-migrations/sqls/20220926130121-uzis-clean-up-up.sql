drop view analytic.v_uzis_covid19_age_groups_prague_ratios;

drop view analytic.v_uzis_covid19_pes_regional;

-- creating existing tables that were made manually without migrations
CREATE TABLE IF NOT EXISTS uzis.covid19_vaccination_registrations (
	datum timestamp NULL,
	ockovaci_misto_id text NULL,
	ockovaci_misto_nazev text NULL,
	kraj_nuts_kod text NULL,
	kraj_nazev text NULL,
	vekova_skupina text NULL,
	povolani text NULL,
	stat text NULL,
	rezervace float8 NULL,
	datum_rezervace timestamp NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL,
	zavora_status text NULL,
	prioritni_skupina text NULL,
	zablokovano int4 NULL,
	duvod_blokace text NULL
);

CREATE TABLE IF NOT EXISTS uzis.covid19_vaccination_calendar_capacities (
	datum timestamp NULL,
	ockovaci_misto_id text NULL,
	ockovaci_misto_nazev text NULL,
	kraj_nuts_kod text NULL,
	kraj_nazev text NULL,
	volna_kapacita int8 NULL,
	maximalni_kapacita int8 NULL,
	kalendar_ockovani text NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);


ALTER TABLE uzis.covid19_vaccination_registrations
  RENAME TO history_covid19_vaccination_registrations;

ALTER TABLE uzis.covid19_vaccination_prague_details
  RENAME TO history_covid19_vaccination_prague_details;

ALTER TABLE uzis.covid19_vaccination_usage
  RENAME TO history_covid19_vaccination_usage;

ALTER TABLE uzis.covid19_vaccination_calendar_capacities
  RENAME TO history_covid19_vaccination_calendar_capacities;

ALTER TABLE uzis.covid19_pes_regional
  RENAME TO history_covid19_pes_regional;
