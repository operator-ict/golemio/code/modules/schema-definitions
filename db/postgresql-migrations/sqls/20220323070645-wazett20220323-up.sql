create view analytic.v_instatntomtom_days
as
select 
to_char(to_timestamp(update_time/1000),'yyyy-mm-dd') day_index,
route_id,
round(avg(time::numeric/historic_time::numeric),2) t_index_value,
count(*) t_index_count
from wazett_route_lives wrl 
where to_timestamp(update_time/1000)::time between '05:00' and '21:00'
group by 1,2;

COMMENT ON view analytic.v_instatntomtom_days IS 'Pokus vypočtu nahradního indexu kongesci po dnech a trasech na základě dat Waze.';