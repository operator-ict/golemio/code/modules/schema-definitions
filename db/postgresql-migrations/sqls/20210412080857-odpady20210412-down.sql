/* Replace with your SQL commands */

CREATE OR REPLACE FUNCTION analytic.set_containers_full_hour()
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare
    pocet integer;
    query_rec record;
    cas timestamp;
    poc timestamp;
    kon timestamp;
    poc_full timestamp;
    kon_full timestamp;
    id integer;
begin
    truncate table public.containers_full_hour;
    FOR  query_rec IN
        select 
        container_id,
        date_trunc('hour',start_time) pocatek,
        date_trunc('hour',end_time) konec,
        start_time poc_full, end_time kon_full
        from analytic.v_containers_full
    loop
        cas := query_rec.pocatek;
        
        LOOP
        -- some computations
            insert into public.containers_full_hour (
                container_id,
                full_hour,
                begin_full,
                end_full
            )
            values (
                query_rec.container_id,
                cas,
                query_rec.poc_full,
                query_rec.kon_full)
            on conflict (container_id, full_hour) do nothing;
            cas := cas + (interval '1 hour');
            EXIT WHEN cas >= query_rec.konec;
        END LOOP;
    END LOOP;
end;
$function$
;

-- public.containers_full_hour definition
CREATE TABLE public.containers_full_hour (
	container_id varchar(50) NOT NULL,
	full_hour timestamp NOT NULL,
	begin_full timestamp NULL,
	end_full timestamp NULL,
	CONSTRAINT containers_full_hod_pkey PRIMARY KEY (container_id, full_hour)
);  

-- analytic.containers_full_hour definition
CREATE TABLE analytic.containers_full_hour (
	container_id int4 NOT NULL,
	full_hour timestamp NOT NULL,
	begin_full timestamp NULL,
	end_full timestamp NULL,
	CONSTRAINT containers_full_hod_pkey PRIMARY KEY (container_id, full_hour)
);
CREATE INDEX containers_full_hour_container_id ON analytic.containers_full_hour USING btree (container_id);


-- analytic.v_containers_full_share source
CREATE OR REPLACE VIEW analytic.v_containers_full_share
AS WITH calendar AS (
         SELECT generate_series((( SELECT min(containers_measurement.measured_at_utc)::date AS min
                   FROM containers_measurement))::timestamp with time zone, now() - '1 day'::interval, '1 day'::interval) AS xday
        ), xx AS (
         SELECT calendar.xday,
            kont.code AS id
           FROM containers_containers kont,
            calendar
          WHERE (kont.code::text IN ( SELECT DISTINCT containers_measurement.container_code
                   FROM containers_measurement
                  WHERE containers_measurement.percent_calculated IS NOT NULL))
        ), ss AS (
         SELECT xx.id,
            xx.xday,
            zaplnenost.full_hour AS zaplneno_hod
           FROM xx
             LEFT JOIN containers_full_hour zaplnenost ON zaplnenost.container_id::text = xx.id::text AND xx.xday = zaplnenost.full_hour::date
        )
 SELECT ss.id AS container_id,
    ss.xday AS measurement_day,
    count(ss.zaplneno_hod) AS hour_count
   FROM ss
  GROUP BY ss.id, ss.xday;



-- analytic.containers_blockege_hour definition
CREATE TABLE analytic.containers_blockege_hour (
	container_id int4 NOT NULL,
	blocked_hour timestamptz NULL,
	begin_block timestamptz NULL,
	end_block timestamptz NULL
);
CREATE INDEX containers_blockege_hour_container_id ON analytic.containers_blockege_hour USING btree (container_id);


drop VIEW if exists analytic.v_containers_pick_dates;


drop VIEW if exists analytic.v_containers_full;
-- analytic.v_containers_full source
CREATE OR REPLACE VIEW analytic.v_containers_full
AS WITH a AS (
         SELECT mer.container_code AS container_id,
            lag(mer.percent_calculated) OVER (PARTITION BY mer.container_code ORDER BY mer.measured_at) AS pre_x,
            mer.percent_calculated AS actual,
            lead(mer.percent_calculated) OVER (PARTITION BY mer.container_code ORDER BY mer.measured_at) AS next_x,
            mer.measured_at
           FROM containers_measurement mer
        ), b AS (
         SELECT row_number() OVER (PARTITION BY a.container_id ORDER BY a.measured_at) AS rn,
                CASE
                    WHEN a.actual >= 95 AND a.pre_x < 95 THEN 'zacatek'::text
                    WHEN a.actual < 95 AND a.pre_x >= 95 THEN 'konec'::text
                    ELSE NULL::text
                END AS typ_zaznamu,
            a.container_id,
            a.pre_x,
            a.actual,
            a.next_x,
            a.measured_at
           FROM a
          WHERE a.actual >= 95 AND a.pre_x < 95 OR a.actual < 95 AND a.pre_x >= 95
        ), c AS (
         SELECT lag(b.measured_at) OVER (PARTITION BY b.container_id ORDER BY b.rn) AS pre_time,
            b.rn,
            b.typ_zaznamu,
            b.container_id,
            b.pre_x,
            b.actual,
            b.next_x,
            b.measured_at
           FROM b
        ), pocatek AS (
         SELECT containers_measurement.container_code AS container_id,
            min(containers_measurement.measured_at) AS pocatek
           FROM containers_measurement
          GROUP BY containers_measurement.container_code
        )
 SELECT c.container_id,
    COALESCE(c.pre_time, pocatek.pocatek) AS start_time,
    c.measured_at AS end_time,
    round((date_part('epoch'::text, c.measured_at) - date_part('epoch'::text, COALESCE(c.pre_time, pocatek.pocatek))) / 60::double precision) AS trvani_mi
   FROM c
     LEFT JOIN pocatek ON pocatek.container_id::text = c.container_id::text
  WHERE c.typ_zaznamu = 'konec'::text;
  
  
  drop view if exists analytic.v_containers_full_days;
  
  
  
  
  -- analytic.v_containers_containers source
drop view if exists public.v_containers_measurement_export;  
drop VIEW if exists analytic.v_containers_containers;
CREATE OR REPLACE VIEW analytic.v_containers_containers
AS SELECT cc.code AS container_id,
    cc.total_volume,
    cc.network,
    cc.container_type,
    cc.bin_type,
        CASE
            WHEN cc.trash_type = 1 THEN 'glass coloured'::text
            WHEN cc.trash_type = 2 THEN 'electronics'::text
            WHEN cc.trash_type = 3 THEN 'metal'::text
            WHEN cc.trash_type = 4 THEN 'bevarage cartons'::text
            WHEN cc.trash_type = 5 THEN 'paper'::text
            WHEN cc.trash_type = 6 THEN 'plastic'::text
            WHEN cc.trash_type = 7 THEN 'glass clear'::text
            WHEN cc.trash_type = 8 THEN 'textile'::text
            ELSE NULL::text
        END AS trash_type,
        CASE
            WHEN cc.trash_type = 1 THEN 'Sklo čiré'::text
            WHEN cc.trash_type = 2 THEN 'Elektronika'::text
            WHEN cc.trash_type = 3 THEN 'Kovy'::text
            WHEN cc.trash_type = 4 THEN 'Nápojové kartony'::text
            WHEN cc.trash_type = 5 THEN 'Papír'::text
            WHEN cc.trash_type = 6 THEN 'Plast'::text
            WHEN cc.trash_type = 7 THEN 'Sklo barevné'::text
            WHEN cc.trash_type = 8 THEN 'Textil'::text
            ELSE NULL::text
        END AS typ_odpadu,
        CASE
            WHEN cc.trash_type = 1 THEN 411
            WHEN cc.trash_type = 3 THEN 120
            WHEN cc.trash_type = 4 THEN 55
            WHEN cc.trash_type = 5 THEN 76
            WHEN cc.trash_type = 6 THEN 39
            WHEN cc.trash_type = 7 THEN 411
            ELSE NULL::integer
        END AS koeficient,
    cc.cleaning_frequency_frequency,
    cc.cleaning_frequency_interval,
    cs.latitude,
    cs.longitude,
        CASE
            WHEN cs.address::text = ' Francouzská  240/76'::text THEN "substring"(cs.address::text, 2)::character varying
            ELSE cs.address
        END AS address,
    cd.district_cz,
    concat(1, "right"(split_part(cs.district::text, '-'::text, 2), 1), '000') AS psc
   FROM containers_containers cc
     LEFT JOIN containers_stations cs ON cc.station_code::text = cs.code::text
     LEFT JOIN analytic.containers_districts cd ON cs.district::text = cd.district::text
  WHERE (cc.code::text IN ( SELECT DISTINCT containers_measurement.container_code
           FROM containers_measurement
          WHERE containers_measurement.percent_calculated IS NOT NULL));
		  
-- public.v_containers_measurement_export source

CREATE OR REPLACE VIEW public.v_containers_measurement_export
AS SELECT cc.container_id,
    cc.address,
    cc.district_cz,
    cc.psc,
    cc.trash_type,
    cm.measured_at,
    cm.percent_calculated
   FROM containers_measurement cm
     JOIN analytic.v_containers_containers cc ON cc.container_id::text = cm.container_code::text;		  