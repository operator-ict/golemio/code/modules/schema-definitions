create or replace view analytic.v_containers_containers as
with prep as (
				select *
				from public.containers_containers
				where sensoneo_code is not null
					and sensoneo_code != code
			),
unn as (
		select  sensoneo_code as code, 
				unnest(string_to_array(pick_days::text,','::text))::int as day_num
		from prep
			),
translate as (
			select code,
					day_num,
					case when day_num = 1 then 'Po'
						 when day_num = 2 then 'Út'
						 when day_num = 3 then 'St'
						 when day_num = 4 then 'Čt'
						 when day_num = 5 then 'Pá'
						 when day_num = 6 then 'So'
						 when day_num = 7 then 'Ne'
					end as day_name
			from unn
			),
		aa as (
			select code,
					string_agg(day_name,', ' order by day_num) pick_days
			from translate 
			group by code
			)
			select cc.sensoneo_code as container_id,
					cc.total_volume,
					cc.network,
					cc.container_type,
					cc.bin_type,
					case when cc.trash_type = 1 then 'glass coloured'
						when cc.trash_type = 2 then 'electronics'
						when cc.trash_type = 3 then 'metal'
						when cc.trash_type = 4 then 'bevarage cartons'
						when cc.trash_type = 5 then 'paper'
						when cc.trash_type = 6 then 'plastic'
						when cc.trash_type = 7 then 'glass clear'
						when cc.trash_type = 8 then 'textile'
						else null end as trash_type,
					case when cc.trash_type = 1 then 'Sklo barevné'
						when cc.trash_type = 2 then 'Elektronika'
						when cc.trash_type = 3 then 'Kovy'
						when cc.trash_type = 4 then 'Nápojové kartony'
						when cc.trash_type = 5 then 'Papír'
						when cc.trash_type = 6 then 'Plast'
						when cc.trash_type = 7 then 'Sklo čiré'
						when cc.trash_type = 8 then 'Textil'
						else null end as typ_odpadu,
					case when cc.trash_type = 1 then 411
						when cc.trash_type = 3 then 120
						when cc.trash_type = 4 then 55
						when cc.trash_type = 5 then 76
						when cc.trash_type = 6 then 39
						when cc.trash_type = 7 then 411
						else null end as koeficient,
					cc.cleaning_frequency_frequency,
					cc.cleaning_frequency_interval,
					cs.latitude,
					cs.longitude,
					case when cs.address = ' Francouzská  240/76' then substring(cs.address from 2)
						else cs.address end as address,
					cd.district_cz,
					concat(1,right(split_part(cs.district, '-', 2),1),'000') as psc,
					cc.sensor_id,
					cc.updated_at,
					aa.pick_days,
					row_number() over (partition by address, trash_type)
			from prep as cc
			left join public.containers_stations as cs on cc.station_code = cs.code
			left join analytic.containers_districts as cd on cs.district = cd.district
			left join aa on cc.sensoneo_code = aa.code;
