ALTER TABLE rsd_tmc_segments drop version_id;
ALTER TABLE rsd_tmc_segments ADD CONSTRAINT rsd_tmc_segments_pkey PRIMARY KEY (lcd);

ALTER TABLE rsd_tmc_roads drop version_id;
ALTER TABLE rsd_tmc_roads ADD CONSTRAINT rsd_tmc_roads_pkey PRIMARY KEY (lcd);

ALTER TABLE rsd_tmc_points drop version_id;
ALTER TABLE rsd_tmc_points ADD CONSTRAINT rsd_tmc_points_pkey PRIMARY KEY (lcd);

ALTER TABLE rsd_tmc_points
ALTER COLUMN wgs84_x TYPE varchar(1024) USING replace(wgs84_x::varchar(1024),'.',','),
ALTER COLUMN wgs84_y TYPE varchar(1024) USING replace(wgs84_y::varchar(1024),'.',','),
ALTER COLUMN sjtsk_x TYPE varchar(1024) USING replace(sjtsk_x::varchar(1024),'.',','),
ALTER COLUMN sjtsk_y TYPE varchar(1024) USING replace(sjtsk_y::varchar(1024),'.',',')
;

ALTER TABLE rsd_tmc_points drop wgs84_point;

ALTER TABLE rsd_tmc_admins drop version_id;
ALTER TABLE rsd_tmc_admins ADD CONSTRAINT rsd_tmc_admins_pkey PRIMARY KEY (lcd);


