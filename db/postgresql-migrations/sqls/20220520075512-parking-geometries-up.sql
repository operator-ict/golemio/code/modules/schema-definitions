CREATE TABLE public.parkings_location (
    "id" varchar PRIMARY KEY,
    "source" character varying(255) NOT NULL,
    "source_id" character varying(255) NOT NULL,
	"data_provider" character varying(255) NOT NULL,
	"location" geometry NOT NULL,
	"centroid" geometry NOT NULL,
	"address" json,
	"total_spot_number" integer NOT NULL,

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),

	CONSTRAINT "parkings_location_fkey" FOREIGN KEY ("source", "source_id")
        REFERENCES public.parkings ("source", "source_id")
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE INDEX "parkings_location_parking_idx" ON public.parkings_location ("source", "source_id");
