DROP VIEW analytic.v_ropidbi_ticket_activation_types;

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_types AS
SELECT 
	rop.act_date,
    rop.activation_type,
    rop.oblast,
    rop.tariff_name,
    count(*) AS count,
    pid.druh AS cestujici
FROM analytic.v_ropidbi_ticket rop
LEFT JOIN analytic.pid_price_list pid
ON rop.tariff_name = pid.tarif AND rop.cptp = pid.cislo_tarifu_cptp AND
to_char(rop.purchase_date::date, 'yyyy-mm-dd'::text) >= pid.nabizeno_od AND to_char(rop.purchase_date::date, 'yyyy-mm-dd'::text) <= nabizeno_do
WHERE rop.activation_type <> 'Zatím neaktivováno'::text AND rop.tariff_name::text <> ''::text
GROUP BY rop.act_date, rop.activation_type, rop.oblast, rop.tariff_name, pid.druh;

DROP VIEW analytic.v_ropidbi_ticket_activation_times;

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_activation_times AS
SELECT 
    rop.act_date,
    rop.act_time,
    rop.tariff_name,
    rop.oblast,
    count(*) AS count,
    pid.druh as cestujici
FROM analytic.v_ropidbi_ticket rop
LEFT JOIN analytic.pid_price_list pid
ON rop.tariff_name = pid.tarif AND rop.cptp = pid.cislo_tarifu_cptp AND
to_char(rop.purchase_date::date, 'yyyy-mm-dd'::text) >= pid.nabizeno_od AND to_char(rop.purchase_date::date, 'yyyy-mm-dd'::text) <= nabizeno_do
GROUP BY rop.act_date, rop.act_time, rop.tariff_name, rop.oblast, pid.druh;

DROP VIEW analytic.v_ropidbi_ticket_sales;

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales AS
SELECT
    to_char(rt.purchase_date::date, 'yyyy-mm-dd'::text) AS purchase_date,
    rt.tariff_name,
    rt.oblast,
    count(*) AS count_ticket,
    pid.cena_s_dph,
    count(*) * pid.cena_s_dph AS sale,
    pid.nabizeno_od::date AS nabizeno_od,
    pid.nabizeno_do::date AS nabizeno_do,
    pid.druh AS cestujici
FROM analytic.v_ropidbi_ticket rt
LEFT JOIN analytic.pid_price_list pid ON rt.tariff_name::text = pid.tarif::text AND rt.cptp = pid.cislo_tarifu_cptp AND to_char(rt.purchase_date::date, 'yyyy-mm-dd'::text) >= pid.nabizeno_od::text AND to_char(rt.purchase_date::date, 'yyyy-mm-dd'::text) <= pid.nabizeno_do::text
WHERE rt.purchase_date::date IS NOT NULL AND rt.tariff_name IS NOT NULL
GROUP BY (rt.purchase_date::date), rt.tariff_name, rt.oblast, pid.cena_s_dph, pid.nabizeno_do, pid.nabizeno_od, pid.druh;
