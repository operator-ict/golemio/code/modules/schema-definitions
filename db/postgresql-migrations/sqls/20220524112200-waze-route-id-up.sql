DROP VIEW public.v_lkpr_export;
DROP VIEW analytic.v_lkpr_route_details;
DROP VIEW analytic.v_wazett_routes_lines;
DROP VIEW analytic.v_route_travel_times;
DROP VIEW analytic.v_route_live;
DROP VIEW analytic.v_instatntomtom_days;

ALTER TABLE public.wazett_routes  
ALTER COLUMN id TYPE int8;

ALTER TABLE public.wazett_route_lives 
ALTER COLUMN route_id TYPE int8;

ALTER TABLE public.wazett_subroutes  
ALTER COLUMN route_id TYPE int8;

ALTER TABLE public.wazett_subroute_lives 
ALTER COLUMN route_id TYPE int8;


CREATE OR REPLACE VIEW analytic.v_instatntomtom_days
AS SELECT to_char(to_timestamp((wrl.update_time / 1000)::double precision), 'yyyy-mm-dd'::text) AS day_index,
    wrl.route_id,
    round(avg(wrl."time"::numeric / wrl.historic_time::numeric), 2) AS t_index_value,
    count(*) AS t_index_count
   FROM wazett_route_lives wrl
  WHERE to_timestamp((wrl.update_time / 1000)::double precision)::time without time zone >= '05:00:00'::time without time zone AND to_timestamp((wrl.update_time / 1000)::double precision)::time without time zone <= '21:00:00'::time without time zone
  GROUP BY (to_char(to_timestamp((wrl.update_time / 1000)::double precision), 'yyyy-mm-dd'::text)), wrl.route_id;
 
CREATE OR REPLACE VIEW analytic.v_route_live
AS SELECT wrl.route_id,
    wrl.update_time,
    wrl."time",
    wrl.length,
    wrl.historic_time,
    wrl.jam_level,
    wrl.create_batch_id,
    wrl.created_at,
    wrl.created_by,
    wrl.update_batch_id,
    wrl.updated_at,
    wrl.updated_by
   FROM wazett_route_lives wrl
  WHERE (wrl.route_id = ANY (ARRAY[24949, 24953, 24962, 24924, 24925, 24950, 24951, 24952, 24954, 24955, 24956, 24957, 24958, 24960, 24961, 24963, 24964, 24965, 24966, 24967, 24968, 24969])) AND wrl.update_time::double precision >= (( SELECT max(wazett_route_lives.update_time)::double precision - date_part('epoch'::text, '1 year'::interval) * 1000::double precision AS max
           FROM wazett_route_lives));
 
CREATE OR REPLACE VIEW analytic.v_route_travel_times
AS SELECT ts.route_id,
    ts.year,
    ts.month,
    ts.day,
    ts.dow,
    ts.hour,
    ts.quarter,
    ts.hour + (ts.quarter::numeric / 60::numeric)::double precision AS hour_quarter,
    ((((((((ts.year || '-'::text) || ts.month) || '-'::text) || ts.day) || ' '::text) || ts.hour) || ':'::text) || ts.quarter)::timestamp without time zone AS date,
    avg(ts.travel_time)::integer AS travel_time
   FROM ( SELECT raw.route_id,
            raw.update_time,
            raw.travel_time,
            date_part('year'::text, raw.update_time) AS year,
            date_part('month'::text, raw.update_time) AS month,
            date_part('day'::text, raw.update_time) AS day,
            date_part('dow'::text, raw.update_time) AS dow,
            date_part('hour'::text, raw.update_time) AS hour,
            date_part('minute'::text, raw.update_time) AS minute,
            date_part('minute'::text, raw.update_time)::integer / 15 * 15 AS quarter
           FROM ( SELECT wazett_route_lives.route_id,
                    timezone('Europe/Prague'::text, to_timestamp((wazett_route_lives.update_time / 1000)::double precision)::timestamp without time zone) AS update_time,
                    wazett_route_lives."time" AS travel_time
                   FROM wazett_route_lives
                     JOIN analytic.waze_dashboard_route_name wdrn ON wdrn.route_id = wazett_route_lives.route_id) raw) ts
  WHERE ts.year > 2000::double precision
  GROUP BY ts.route_id, ts.year, ts.month, ts.day, ts.dow, ts.hour, ts.quarter;
  
CREATE OR REPLACE VIEW analytic.v_wazett_routes_lines
AS SELECT wazett_routes.id,
    wazett_routes.name,
    st_astext(wazett_routes.line) AS line,
    concat('#', "left"(lpad(to_hex((wazett_routes.id::double precision * character_length(wazett_routes.name)::double precision / (( SELECT max(wazett_routes_1.id * character_length(wazett_routes_1.name)) AS max
           FROM wazett_routes wazett_routes_1))::double precision * 10000000::double precision)::bigint), 6, '0'::text), 6)) AS color
   FROM wazett_routes;

CREATE OR REPLACE VIEW analytic.v_lkpr_route_details
AS SELECT re.id,
    re.name,
    re.route_code,
    re.route_name,
    re.from_name,
    re.to_name,
    re.route_type,
    re.route_num,
    re.route_variant,
        CASE
            WHEN re.route_variant = 'A1'::text THEN 'Na letiště'::text
            WHEN re.route_variant = 'B1'::text THEN 'Z letiště'::text
            ELSE 'Ostatní'::text
        END AS direction,
    drn.route_group_name,
    drn.order_idx
   FROM ( SELECT rc.id,
            rc.name,
            rc.route_code,
            rc.route_name,
            rc.from_name,
            rc.to_name,
            split_part(rc.route_code, '-'::text, 1) AS route_type,
            split_part(rc.route_code, '-'::text, 2) AS route_num,
            split_part(rc.route_code, '-'::text, 3) AS route_variant
           FROM ( SELECT r.id,
                    r.name,
                    split_part(r.name, ' '::text, 1) AS route_code,
                    "substring"(r.name, "position"(r.name, ' '::text) + 1) AS route_name,
                    r.from_name,
                    r.to_name
                   FROM wazett_routes r
                  WHERE r.feed_id = 1) rc) re
     LEFT JOIN analytic.lkpr_dashboard_route_names drn ON re.route_type = drn.route_type AND re.route_num = drn.route_num
  ORDER BY drn.order_idx;
 
CREATE OR REPLACE VIEW public.v_lkpr_export
AS SELECT COALESCE(vrd.id, wrl.route_id) AS route_id,
    vrd.name,
    vrd.route_name,
    vrd.from_name,
    vrd.to_name,
    vrd.route_num,
    vrd.direction,
    vrd.order_idx,
    to_timestamp((wrl.update_time / 1000)::double precision) AS update_time,
    wrl."time" AS actual_time,
    round(wrl.length::numeric * 3.6 / wrl."time"::numeric) AS actual_speed,
    wrl.historic_time,
    round(wrl.length::numeric * 3.6 / wrl.historic_time::numeric) AS historic_speed,
    wrl.length
   FROM analytic.v_lkpr_route_details vrd
     FULL JOIN wazett_route_lives wrl ON vrd.id = wrl.route_id
  WHERE vrd.name ~~ 'TN-%'::text OR vrd.route_code ~~ 'TN-%'::text;


CREATE OR REPLACE VIEW analytic.v_bm_trasy
AS SELECT DISTINCT ON (wazett_routes.name) wazett_routes.name,
    wazett_routes.id,
    wazett_routes.created_at,
        CASE
            WHEN "position"(wazett_routes.name, ' TN'::text) > 0 THEN true
            ELSE false
        END AS is_trasa,
        CASE
            WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN true
            ELSE false
        END AS is_usek,
        CASE
            WHEN "position"(wazett_routes.name, ' OS'::text) > 0 THEN true
            ELSE false
        END AS is_opacny_smer,
        CASE
            WHEN wazett_routes.id = ANY (ARRAY[32875, 32876]) THEN 'OICT 3 DO'::text
            WHEN wazett_routes.id = ANY (ARRAY[32888, 32889]) THEN 'OICT 3 OD'::text
            ELSE "substring"(wazett_routes.name, 4,
            CASE
                WHEN "position"(wazett_routes.name, ' TN'::text) > 0 THEN "position"(wazett_routes.name, ' TN'::text) - 3
                WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN "position"(wazett_routes.name, ' ÚS'::text) - 3
                ELSE NULL::integer
            END)
        END AS name_trasa,
    "substring"(wazett_routes.name,
        CASE
            WHEN "position"(wazett_routes.name, ' ÚS'::text) > 0 THEN "position"(wazett_routes.name, ' ÚS'::text) + 4
            ELSE length(wazett_routes.name) + 1
        END,
        CASE
            WHEN "position"(wazett_routes.name, ' OS'::text) > 0 THEN "position"(wazett_routes.name, ' OS'::text) - "position"(wazett_routes.name, ' ÚS'::text) - 4
            ELSE length(wazett_routes.name) - "position"(wazett_routes.name, ' ÚS'::text)
        END) AS name_usek
   FROM wazett_routes
  WHERE "left"(wazett_routes.name, 2) = 'BM'::text
  ORDER BY wazett_routes.name, wazett_routes.created_at DESC;

 CREATE OR REPLACE VIEW analytic.v_bm_trasy_hod_part
AS SELECT rl.route_id,
    to_timestamp((rl.update_time / 1000)::double precision)::date AS datum,
    date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) AS hodina,
    (to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)), '09'::text) || ' -'::text) || to_char(date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)) + 1::double precision, '09'::text) AS cas_interval,
    round(avg(rl."time"), 2) AS avg_time,
    round(avg(rl.length * 1000 / (rl."time" * 360)), 2) AS avg_speed
   FROM wazett_route_lives rl
     JOIN analytic.v_bm_trasy bm ON bm.id = rl.route_id
  WHERE rl.length > 0
  GROUP BY rl.route_id, (to_timestamp((rl.update_time / 1000)::double precision)::date), (date_part('hour'::text, to_timestamp((rl.update_time / 1000)::double precision)));  

CREATE MATERIALIZED VIEW analytic.mv_bm_normal
AS SELECT vtbh.route_id,
    vtbh.hodina,
    round(avg(vtbh.avg_time), 2) AS avg_time,
    round(avg(vtbh.avg_speed)) AS avg_speed
   FROM analytic.v_bm_trasy_hod_part vtbh
  WHERE vtbh.datum >= '2022-04-24'::date AND vtbh.datum <= '2022-05-05'::date AND (date_part('dow'::text, vtbh.datum) = ANY (ARRAY[2::double precision, 3::double precision, 4::double precision]))
  GROUP BY vtbh.route_id, vtbh.hodina;

CREATE INDEX mv_bm_normal_idx ON analytic.mv_bm_normal USING btree (route_id, hodina);  

CREATE OR REPLACE VIEW analytic.v_bm_trasy_hod
AS SELECT p.route_id,
    p.datum,
    p.hodina,
    p.cas_interval,
    p.avg_time,
    p.avg_speed,
    n.avg_time AS normal_avg_time,
    n.avg_speed AS normal_avg_speed
   FROM analytic.v_bm_trasy_hod_part p
     LEFT JOIN analytic.mv_bm_normal n ON n.route_id = p.route_id AND p.hodina = n.hodina;

CREATE OR REPLACE  VIEW analytic.v_bm_union2
AS WITH hodiny AS (
         SELECT v_bm_trasy_hod.route_id,
            v_bm_trasy_hod.datum,
            v_bm_trasy_hod.hodina,
            v_bm_trasy_hod.cas_interval,
            v_bm_trasy_hod.avg_time,
            v_bm_trasy_hod.avg_speed,
            v_bm_trasy_hod.normal_avg_time,
            v_bm_trasy_hod.normal_avg_speed
           FROM analytic.v_bm_trasy_hod
          WHERE v_bm_trasy_hod.datum < CURRENT_DATE OR v_bm_trasy_hod.hodina < date_part('hour'::text, now())
        ), den AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '0 - 24h'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.route_id, hodiny.datum
        ), h15_18 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '15 -18'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 15::double precision AND hodiny.hodina <= 17::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 17::double precision)
          GROUP BY hodiny.route_id, hodiny.datum
        ), h22_6 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '22 - 06'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE NOT (hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision) AND hodiny.datum < CURRENT_DATE
          GROUP BY hodiny.route_id, hodiny.datum
        ), h6_22 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '06 - 22'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 6::double precision AND hodiny.hodina <= 21::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 21::double precision)
          GROUP BY hodiny.route_id, hodiny.datum
        ), h7_10 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '07 - 10'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE hodiny.hodina >= 7::double precision AND hodiny.hodina <= 9::double precision AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 10::double precision)
          GROUP BY hodiny.route_id, hodiny.datum
        ), h7_11_15_19 AS (
         SELECT hodiny.route_id,
            hodiny.datum,
            '07 - 11, 15 - 19'::text AS cas_interval,
            round(avg(hodiny.avg_time), 2) AS avg_time,
            round(avg(hodiny.avg_speed), 2) AS avg_speed,
            round(avg(hodiny.normal_avg_time), 2) AS normal_avg_time,
            round(avg(hodiny.normal_avg_speed), 2) AS normal_avg_speed
           FROM hodiny
          WHERE (hodiny.hodina >= 7::double precision AND hodiny.hodina <= 10::double precision OR hodiny.hodina >= 15::double precision AND hodiny.hodina <= 18::double precision) AND (hodiny.datum < CURRENT_DATE OR hodiny.hodina > 18::double precision)
          GROUP BY hodiny.route_id, hodiny.datum
        )
 SELECT hodiny.route_id,
    hodiny.datum,
    hodiny.cas_interval,
    hodiny.avg_time,
    hodiny.avg_speed,
    hodiny.normal_avg_time,
    hodiny.normal_avg_speed
   FROM hodiny
UNION ALL
 SELECT den.route_id,
    den.datum,
    den.cas_interval,
    den.avg_time,
    den.avg_speed,
    den.normal_avg_time,
    den.normal_avg_speed
   FROM den
UNION ALL
 SELECT h15_18.route_id,
    h15_18.datum,
    h15_18.cas_interval,
    h15_18.avg_time,
    h15_18.avg_speed,
    h15_18.normal_avg_time,
    h15_18.normal_avg_speed
   FROM h15_18
UNION ALL
 SELECT h22_6.route_id,
    h22_6.datum,
    h22_6.cas_interval,
    h22_6.avg_time,
    h22_6.avg_speed,
    h22_6.normal_avg_time,
    h22_6.normal_avg_speed
   FROM h22_6
UNION ALL
 SELECT h6_22.route_id,
    h6_22.datum,
    h6_22.cas_interval,
    h6_22.avg_time,
    h6_22.avg_speed,
    h6_22.normal_avg_time,
    h6_22.normal_avg_speed
   FROM h6_22
UNION ALL
 SELECT h7_10.route_id,
    h7_10.datum,
    h7_10.cas_interval,
    h7_10.avg_time,
    h7_10.avg_speed,
    h7_10.normal_avg_time,
    h7_10.normal_avg_speed
   FROM h7_10
UNION ALL
 SELECT h7_11_15_19.route_id,
    h7_11_15_19.datum,
    h7_11_15_19.cas_interval,
    h7_11_15_19.avg_time,
    h7_11_15_19.avg_speed,
    h7_11_15_19.normal_avg_time,
    h7_11_15_19.normal_avg_speed
   FROM h7_11_15_19;

CREATE OR REPLACE VIEW analytic.v_bm_data_all
AS WITH data1 AS (
         SELECT trasy.name,
            trasy.name_trasa,
            trasy.name_usek,
                CASE
                    WHEN trasy.is_opacny_smer THEN 'Zpět'::text
                    ELSE 'Tam'::text
                END AS smer,
            vbu.datum,
            vbu.cas_interval,
            vbu.avg_time,
            vbu.avg_speed,
            vbu.normal_avg_time,
            vbu.normal_avg_speed
           FROM analytic.v_bm_union2 vbu
             JOIN analytic.v_bm_trasy trasy ON trasy.id = vbu.route_id
        ), data2 AS (
         SELECT data1.name_trasa,
            data1.name_usek,
            'Oba'::text AS smer,
            data1.datum,
            data1.cas_interval,
            sum(data1.avg_time) AS avg_time,
            avg(data1.avg_speed) AS avg_speed,
            sum(data1.normal_avg_time) AS normal_avg_time,
            avg(data1.normal_avg_speed) AS normal_avg_speed
           FROM data1
          GROUP BY data1.name_trasa, data1.name_usek, 'Oba'::text, data1.datum, data1.cas_interval
        )
 SELECT data1.name,
    data1.name_trasa,
    data1.name_usek,
    data1.smer,
    data1.datum,
    data1.cas_interval,
    data1.avg_time,
    data1.avg_speed,
    data1.normal_avg_time,
    data1.normal_avg_speed
   FROM data1
UNION
 SELECT vbt.name,
    data2.name_trasa,
    data2.name_usek,
    data2.smer,
    data2.datum,
    data2.cas_interval,
    data2.avg_time,
    data2.avg_speed,
    data2.normal_avg_time,
    data2.normal_avg_speed
   FROM data2
     JOIN analytic.v_bm_trasy vbt ON vbt.name_trasa = data2.name_trasa AND vbt.name_usek = data2.name_usek AND NOT vbt.is_opacny_smer;

CREATE OR REPLACE VIEW analytic.v_bm_last_update
AS SELECT to_timestamp((wrl.update_time / 1000)::double precision) AS last_update
   FROM wazett_route_lives wrl
     JOIN analytic.v_bm_trasy vbt ON wrl.route_id = vbt.id
  ORDER BY wrl.update_time DESC
 LIMIT 1;
