CREATE TABLE public.ropidgtfs_run_numbers (
    "route_id" varchar(50) NOT NULL,
    "run_number" int NOT NULL,
    "service_id" varchar(50) NOT NULL,
    "trip_id" varchar(50) NOT NULL,
    "vehicle_type" int NOT NULL,

    create_batch_id bigint,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id bigint,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT "ropidgtfs_run_numbers_pkey" PRIMARY KEY ("run_number", "service_id", "trip_id")
)
