drop view analytic.v_ropidbi_ticket_sales_monthly_by_zones;

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales_monthly_by_zones
AS WITH purchases_w_prices AS (
         SELECT p.ticket_id,
            p.tariff_name,
            p.date::date AS date,
            p.zone_count,
            COALESCE(pid.zony, 'nezařazeno'::character varying(50)) AS zony,
            COALESCE(pid.cena_s_dph, 0) AS price
           FROM mos_ma_ticketpurchases p
             LEFT JOIN analytic.pid_price_list pid ON p.tariff_name::text = pid.tarif::text AND p.cptp = pid.cislo_tarifu_cptp AND p.date::date >= pid.nabizeno_od AND p.date::date <= pid.nabizeno_do
        ), activations AS (
         SELECT mos_ma_ticketactivations.ticket_id,
            btrim(unnest(string_to_array(mos_ma_ticketactivations.zones::text, ','::text))) AS zones
           FROM mos_ma_ticketactivations
          WHERE mos_ma_ticketactivations.zones::text <> ''::text AND mos_ma_ticketactivations.zones IS NOT NULL
          GROUP BY mos_ma_ticketactivations.ticket_id, mos_ma_ticketactivations.zones
        ), main AS (
         SELECT c.ticket_id,
            c.tariff_name,
            c.date,
            c.zone_count,
            c.zony,
            c.price,
            string_agg(c.zones, ', '::text) AS zones
           FROM ( SELECT DISTINCT p.ticket_id,
                    p.tariff_name,
                    to_char(p.date::timestamp with time zone, 'yyyy-mm-01'::text) AS date,
                    p.zone_count,
                    p.zony,
                    p.price,
                        CASE
                            WHEN a.zones = ANY (ARRAY['p'::character varying::text, '0'::text, 'b'::character varying::text]) THEN 'p+0+b'::character varying::text
                            WHEN a.zones IS NULL THEN 'nezařazeno'::text
                            ELSE a.zones
                        END AS zones,
                        CASE
                            WHEN a.zones = ANY (ARRAY['p'::character varying::text, '0'::text, 'b'::character varying::text]) THEN 0
                            WHEN a.zones IS NULL THEN '-1'::integer
                            ELSE a.zones::integer
                        END AS zone_order
                   FROM purchases_w_prices p
                     LEFT JOIN activations a ON p.ticket_id = a.ticket_id
                  ORDER BY p.ticket_id, (
                        CASE
                            WHEN a.zones = ANY (ARRAY['p'::character varying::text, '0'::text, 'b'::character varying::text]) THEN 0
                            WHEN a.zones IS NULL THEN '-1'::integer
                            ELSE a.zones::integer
                        END)) c
          GROUP BY c.ticket_id, c.tariff_name, c.date, c.zone_count, c.zony, c.price
        )
 SELECT main.tariff_name,
    main.date,
    main.zone_count,
    main.zony,
    main.zones,
    count(*) AS tickets_count,
    sum(main.price) AS sales
   FROM main
  GROUP BY main.tariff_name, main.date, main.zone_count, main.zony, main.zones
  ORDER BY main.tariff_name, main.zony, main.zone_count, main.zones, main.date;
