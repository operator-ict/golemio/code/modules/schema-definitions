drop view analytic.v_consumption_energy_reserve_day_electricity;

drop vIEW analytic.v_consumption_energy_reserve_day_gas ;
drop view analytic.v_consumption_gas_day;

drop view analytic.v_consumption_gas_reserve;

drop table analytic.consumption_electricity_daily_reserves;
drop table analytic.consumption_gas_daily_reserves  ;

create or replace view analytic.v_consumption_gas_reserve as
WITH kalendar AS (
         SELECT den.den::date AS den
           FROM generate_series('2019-01-01'::timestamp with time zone, now() - '1 day'::interval, '1 day'::interval) den(den)
        ),
   meraky AS (
         select distinct addr
		from public.consumption_energy_devices
		where meter_type = 'Plynoměr'
        ),
   daily_delta_core AS (
         SELECT date_trunc('day'::text, v_consumption_energy_delta.time_utc - '06:00:00'::interval)::date AS measured_from,
            v_consumption_energy_delta.addr,
            v_consumption_energy_delta.var,
            sum(v_consumption_energy_delta.delta_value) AS delta_value
           FROM analytic.v_consumption_energy_delta
          WHERE v_consumption_energy_delta.commodity::text = 'gas'::text AND date_part('year'::text, v_consumption_energy_delta.time_utc - '06:00:00'::interval) >= 2019::double precision
          GROUP BY (date_trunc('day'::text, v_consumption_energy_delta.time_utc - '06:00:00'::interval)::date), v_consumption_energy_delta.addr, v_consumption_energy_delta.var
        ), 
daily_delta_core2 AS (
         SELECT date_trunc('day'::text, consumption_energy_consumption.time_utc - '06:00:00'::interval)::date AS measured_from,
            consumption_energy_consumption.addr,
            consumption_energy_consumption.var,
            sum(consumption_energy_consumption.value) AS delta_value
           FROM public.consumption_energy_consumption
          WHERE consumption_energy_consumption.var::text = 'core2'::text AND consumption_energy_consumption.time_utc::date > '2018-12-31'::date
          GROUP BY (date_trunc('day'::text, consumption_energy_consumption.time_utc - '06:00:00'::interval)::date), consumption_energy_consumption.addr, consumption_energy_consumption.var
        ), 
delta_dates AS (
         SELECT daily_delta_core.measured_from,
            daily_delta_core.addr,
            daily_delta_core.var,
            daily_delta_core.delta_value,
            lag(daily_delta_core.measured_from, 1) OVER (PARTITION BY daily_delta_core.addr ORDER BY daily_delta_core.measured_from) AS measured_before,
            lead(daily_delta_core.measured_from, 1) OVER (PARTITION BY daily_delta_core.addr ORDER BY daily_delta_core.measured_from) AS measured_after,
            lag(daily_delta_core.delta_value, 1) OVER (PARTITION BY daily_delta_core.addr ORDER BY daily_delta_core.measured_from) AS delta_before,
            lead(daily_delta_core.delta_value, 1) OVER (PARTITION BY daily_delta_core.addr ORDER BY daily_delta_core.measured_from) AS delta_after
           FROM daily_delta_core
        ), 
delta_date_diff AS (
         SELECT delta_dates.measured_from,
            delta_dates.addr,
            delta_dates.var,
            delta_dates.delta_value,
            delta_dates.measured_before,
            delta_dates.measured_after,
            delta_dates.delta_before,
            delta_dates.delta_after,
                CASE
                    WHEN (delta_dates.measured_after - delta_dates.measured_from) > 1 AND (delta_dates.measured_from - delta_dates.measured_before) > 1 THEN 'mid'::text
                    WHEN (delta_dates.measured_after - delta_dates.measured_from) > 1 THEN 'begin'::text
                    WHEN (delta_dates.measured_from - delta_dates.measured_before) > 1 THEN 'end'::text
                    ELSE 'ok'::text
                END AS value_type,
                CASE
                    WHEN (delta_dates.measured_after - delta_dates.measured_from) > 1 AND (delta_dates.measured_from - delta_dates.measured_before) > 1 THEN (delta_dates.delta_value / 2::numeric + delta_dates.delta_after) / (delta_dates.measured_after - delta_dates.measured_from + 1)::numeric
                    WHEN (delta_dates.measured_after - delta_dates.measured_from) > 1 THEN (delta_dates.delta_value + delta_dates.delta_after) / (delta_dates.measured_after - delta_dates.measured_from + 1)::numeric
                    ELSE NULL::numeric
                END AS fix
           FROM delta_dates
        ), 
joined_table AS (
         SELECT kalendar.den AS measured_from,
            meraky.addr,
            diff.delta_value,
            diff.value_type,
            diff.fix,
            core2.delta_value AS core2_delta,
                CASE
                    WHEN diff.value_type = 'ok'::text THEN COALESCE(core2.delta_value, diff.delta_value)
                    ELSE diff.fix
                END AS final_fix,
                CASE
                    WHEN diff.value_type = 'ok'::text THEN COALESCE(core2.var, diff.var)
                    ELSE 'core'::character varying
                END AS final_var
           FROM kalendar
             LEFT JOIN meraky ON true
             LEFT JOIN delta_date_diff diff ON diff.measured_from = kalendar.den AND diff.addr::text = meraky.addr::text
             LEFT JOIN daily_delta_core2 core2 ON core2.measured_from = kalendar.den AND core2.addr::text = meraky.addr::text
          ORDER BY meraky.addr, kalendar.den
        ),
fixed_table AS (
         SELECT q.measured_from,
            q.addr,
            q.delta_value,
            q.value_type,
            q.fix,
            q.core2_delta,
            q.final_fix,
            q.final_var,
            q.value_partition,
            first_value(q.final_fix) OVER w AS value
           FROM ( SELECT joined_table.measured_from,
                    joined_table.addr,
                    joined_table.delta_value,
                    joined_table.value_type,
                    joined_table.fix,
                    joined_table.core2_delta,
                    joined_table.final_fix,
                    joined_table.final_var,
                    sum(
                        CASE
                            WHEN joined_table.final_fix IS NULL THEN 0
                            ELSE 1
                        END) OVER (PARTITION BY joined_table.addr ORDER BY joined_table.measured_from) AS value_partition
                   FROM joined_table) q
          WINDOW w AS (PARTITION BY q.addr, q.value_partition ORDER BY q.measured_from)
        )
 SELECT fixed_table.measured_from,
    fixed_table.addr,
    fixed_table.value AS delta_value,
    fixed_table.final_var,
    row_number() OVER (PARTITION BY fixed_table.addr, (date_trunc('month'::text, fixed_table.measured_from::timestamp with time zone)) ORDER BY fixed_table.value DESC) AS ran
   FROM fixed_table
   where fixed_table.value is not null;

drop VIEW analytic.v_vpalac_apartments_daily_consumption;
CREATE OR REPLACE VIEW analytic.v_vpalac_apartments_daily_consumption
as WITH last_measurement AS (
         SELECT date_part('year'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone) AS measure_year,
            date_trunc('day'::text, max(to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)) AS last_measurement,
            m.var_id
           FROM vpalac_measurement m
          GROUP BY (date_part('year'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)), m.var_id
        ), daily_status AS (
         SELECT
                CASE
                    WHEN me.me_serial::text = '5640201'::text THEN 'Prostor 109,111'::text
                    WHEN mm.location_name IS NOT NULL THEN mm.location_name::text
                    WHEN me.umisteni::text ~~ '%Byt%'::text THEN "right"(me.umisteni::text, length(me.umisteni::text) - "position"(me.umisteni::text, 'Byt'::text) + 1)
                    ELSE "right"(me.umisteni::text, length(me.umisteni::text) - "position"(me.umisteni::text, 'Prostor'::text) + 1)
                END AS apartment,
                CASE
                    WHEN mt.met_nazev::text = 'Sensor'::text THEN 'Teplo'::character varying
                    ELSE mt.met_nazev
                END AS met_nazev,
            me.mis_nazev,
            me.var_id,
            u.jed_nazev,
            date_trunc('hour'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone) AS measure_date,
            min(m.value) AS start_value
           FROM vpalac_measuring_equipment me
             JOIN vpalac_measurement m ON me.var_id = m.var_id
             LEFT JOIN vpalac_meter_type mt ON me.met_id = mt.met_id
             LEFT JOIN vpalac_units u ON me.pot_id = u.pot_id
             LEFT JOIN analytic.vpalac_meter_mapping mm ON mm.var_id = me.var_id
          WHERE to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone >= (now() - '3 years'::interval)
          GROUP BY mm.location_name, me.umisteni, me.me_serial, (date_part('month'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)), me.var_id, ("right"(me.umisteni::text, 6)), mt.met_nazev, me.mis_nazev, (date_trunc('hour'::text, to_timestamp((m.time_measurement / 1000)::double precision)::timestamp without time zone)), u.jed_nazev
        )
 SELECT ds.apartment,
    ds.met_nazev AS measure_type,
    ds.mis_nazev AS measure_detail,
    ds.jed_nazev,
    ds.measure_date,
    lm.last_measurement,
    CASE
            WHEN ds.met_nazev::text = 'Teplo'::text AND ds.measure_date = lm.last_measurement THEN lead(ds.start_value) OVER (PARTITION BY ds.var_id ORDER BY ds.measure_date)
            ELSE  ds.start_value - lag(ds.start_value) OVER (PARTITION BY ds.var_id ORDER BY ds.measure_date)
    END AS consumption,
    ds.var_id
 FROM daily_status ds
 JOIN last_measurement lm ON ds.var_id = lm.var_id AND date_part('year'::text, ds.measure_date) = lm.measure_year;
