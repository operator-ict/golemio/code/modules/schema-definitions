-- public.v_containers_picks_export source
drop VIEW public.v_containers_picks_export;
CREATE VIEW public.v_containers_picks_export
AS SELECT DISTINCT cc.code,
    cs.address,
    cs.district,
    cc.trash_type,
    cp.pick_at,
    cp.percent_before,
    cp.percent_now,
    cp.event_driven
   FROM containers_picks cp
     JOIN containers_containers cc ON cc.code::text = cp.container_code::text
     LEFT JOIN containers_stations cs ON cs.code::text = cc.station_code::text;
	 
-- public.v_containers_measurement_export source
drop VIEW public.v_containers_measurement_export;
CREATE VIEW public.v_containers_measurement_export
AS SELECT cc.container_id,
    cc.address,
    cc.district_cz,
    cc.psc,
    cc.trash_type,
    cm.measured_at,
    cm.percent_calculated
   FROM containers_measurement cm
     JOIN analytic.v_containers_containers cc ON cc.container_id::text = cm.container_code::text;	 