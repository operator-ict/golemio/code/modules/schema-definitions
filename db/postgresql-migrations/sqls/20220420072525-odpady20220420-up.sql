create table python.waste_assigned_pick_dates (
    container_code varchar(16),
    pick_date date,
    pick_at timestamptz,
    is_exact_pick_date boolean,
    is_tolerated_pick_date boolean, 
    is_pick_assigned boolean,
    tolerance interval, 
    trash_type varchar(20),
    category varchar(20)
);

ALTER TABLE python.waste_assigned_pick_dates ADD CONSTRAINT cnstr_waste_assigned_pick_dates UNIQUE (container_code, pick_date);

create table python.waste_assigned_picks (
 	container_code varchar(16),
 	pick_at timestamptz,
 	date date,
 	is_assigned boolean
);
ALTER TABLE python.waste_assigned_picks ADD CONSTRAINT cnstr_waste_assigned_picks UNIQUE (container_code, pick_at);
