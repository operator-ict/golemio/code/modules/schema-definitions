--merakiaccesspoints_observations column departure_timestamp
ALTER TABLE public.merakiaccesspoints_observations
ALTER COLUMN timestamp TYPE BIGINT
USING (EXTRACT(EPOCH FROM timestamp)*1000);