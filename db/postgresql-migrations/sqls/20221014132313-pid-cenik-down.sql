DROP VIEW analytic.v_ropidbi_ticket_with_rollingmean;

DROP TABLE analytic.pid_price_list;

CREATE TABLE analytic.pid_cenik (
	tarif varchar(100) NULL,
	cestující varchar(50) NULL,
	platnost varchar(50) NULL,
	zóny varchar(50) NULL,
	Číslo_tarifu_cptp int4 NULL,
	cena_s_dph int4 NULL
);

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_with_rollingmean
AS SELECT tab.date,
    COUNT(tab.tariff_name) AS ticket_count,
    SUM(tab.cena_s_dph) AS total_sale,
    AVG(COUNT(tab.tariff_name)) OVER (ORDER BY tab.date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS roll_ticket_count,
    AVG(SUM(tab.cena_s_dph)) OVER (ORDER BY tab.date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS roll_sale
   FROM ( SELECT pur.date::date AS date,
            pur.tariff_name,
                CASE
                    WHEN pur.tariff_name::text ~~ '%pes%'::text THEN 16
                    ELSE cen.cena_s_dph
                END AS cena_s_dph
           FROM mos_ma_ticketpurchases pur
             LEFT JOIN ( SELECT DISTINCT pid_cenik.tarif,
                    pid_cenik."cestující",
                    pid_cenik.platnost,
                    pid_cenik."zóny",
                    pid_cenik."Číslo_tarifu_cptp",
                    pid_cenik.cena_s_dph
                   FROM analytic.pid_cenik) cen ON pur.tariff_name::text = cen.tarif::text) tab
  GROUP BY tab.date;
