CREATE TABLE public.flow_od_measurements (
    "cube_id" integer NOT NULL, --
    "analytic_id" integer NOT NULL, --
    "sequence_number" integer NOT NULL, --
    "sink_id" integer NOT NULL, --
    "start_timestamp" bigint NOT NULL, --
    "end_timestamp" bigint NOT NULL, --
    "category" varchar(150), --
    "origin" varchar(150), --
    "destination" varchar(150), --
    "value" integer NOT NULL, --
    "data_validity" varchar(50), --

    "create_batch_id" bigint,
    "created_at" timestamp with time zone,
    "created_by" character varying(150),
    "update_batch_id" bigint,
    "updated_at" timestamp with time zone,
    "updated_by" character varying(150),
    CONSTRAINT flow_od_measurementspkey PRIMARY KEY (cube_id, sink_id, start_timestamp, end_timestamp, category, origin, destination, sequence_number)
);


CREATE INDEX flow_od_measurements_created_at 
ON public.flow_od_measurements 
USING btree (created_at);


ALTER TABLE public.flow_measurements ADD COLUMN IF NOT EXISTS data_validity varchar(50);



