CREATE OR REPLACE FUNCTION analytic.container_last_correct(p_continer_code text)
 RETURNS timestamp without time zone
 LANGUAGE plpgsql
AS $function$
declare 
    posledni integer;
begin
    posledni = (
        select percent_calculated   
        from public.containers_measurement
        where container_code = p_continer_code
        order by measured_at desc
        limit 1);
    return (
    select measured_at          
    from public.containers_measurement
    where container_code = p_continer_code
--  and not percent_calculated between p_min and p_max
    and not percent_calculated between posledni-3 and posledni+3
    order by measured_at desc
    limit 1);
end;
$function$
;