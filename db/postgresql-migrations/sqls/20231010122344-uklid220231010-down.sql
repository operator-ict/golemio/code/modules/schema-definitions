-- analytic.firebase_pidlitacka_events_table definition

CREATE TABLE analytic.firebase_pidlitacka_events_table (
	event_name varchar(255) NOT NULL,
	event_name_cz varchar(255) NULL,
	description varchar(5000) NULL,
	description_cz varchar(5000) NULL
);


-- analytic.lkpr_dashboard_route_names definition
/*
CREATE TABLE analytic.lkpr_dashboard_route_names (
	route_type text NULL,
	route_num text NULL,
	route_group_name text NULL,
	order_idx int4 NULL
);
*/


-- analytic.pedestrians_locations_gates definition

CREATE TABLE analytic.pedestrians_locations_gates (
	direction_id varchar(250) NOT NULL,
	cube_id varchar(250) NOT NULL,
	location_id int4 NOT NULL,
	"name" varchar(250) NOT NULL,
	direction_type varchar(250) NOT NULL,
	CONSTRAINT equipment_gates_pkey PRIMARY KEY (direction_id, location_id)
);
COMMENT ON TABLE analytic.pedestrians_locations_gates IS '-- tabulka směrů k jednotlivým lokacím, platí to samé co u lokations_list.
-- nachází se na  https://operator.sharepoint.com/:f:/s/Datova_Platforma/EsE5uSdyLzdImWgyrMY8nFcBzwtH7GOr6TXtzNBn3FLIJw?e=fOov5s a jmenuje se directions_list.csv';


-- analytic.pedestrians_locations_list definition

CREATE TABLE analytic.pedestrians_locations_list (
	id serial4 NOT NULL,
	location_name_plain varchar(250) NULL,
	location_name varchar(250) NULL,
	lat numeric NULL,
	lng numeric NULL,
	address varchar(250) NULL,
	city_district varchar(250) NULL,
	tech varchar(250) NULL,
	map_picture varchar(2500) NULL,
	gate_picture varchar(2500) NULL,
	measurement_start timestamptz NULL,
	measurement_end timestamptz NULL,
	id_equiptment varchar(250) NULL,
	cube_id varchar(250) NULL,
	lon varbit NULL,
	map_image varchar(55) NULL,
	place_image varchar(59) NULL,
	"table" varchar(19) NULL,
	CONSTRAINT equipment_list_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE analytic.pedestrians_locations_list IS '-- tabulka lokací. obsahuje lokace různých druhů a hodně custom dat, takže ji tvoříme ručně. sporadicky probíhají aktualizace
-- tabulka se nachází na https://operator.sharepoint.com/:f:/s/Datova_Platforma/EsE5uSdyLzdImWgyrMY8nFcBzwtH7GOr6TXtzNBn3FLIJw?e=fOov5s a jmenuje se locations_list.csv';


-- analytic.pid_price_list definition

CREATE TABLE analytic.pid_price_list (
	tarif varchar(50) NOT NULL,
	platnost_min int4 NOT NULL,
	platnost_text varchar(50) NULL,
	cislo_tarifu_cptp int4 NOT NULL,
	druh varchar(50) NULL,
	pasma varchar(50) NULL,
	zony varchar(50) NULL,
	cena_s_dph int4 NOT NULL,
	nabizeno_od date NOT NULL,
	nabizeno_do date NULL,
	stav varchar(50) NULL,
	zdroj varchar(50) NULL,
	CONSTRAINT pid_price_list_pkey PRIMARY KEY (tarif, cislo_tarifu_cptp, platnost_min, cena_s_dph, nabizeno_od)
);


-- analytic.vpalac_meter_mapping definition

CREATE TABLE analytic.vpalac_meter_mapping (
	location_code int4 NULL,
	met_nazev varchar(100) NULL,
	sublocation_id int4 NULL,
	me_extid varchar(100) NULL,
	location_name varchar(100) NULL,
	var_id int4 NULL
);

-- analytic.v_public_tenders source

CREATE OR REPLACE VIEW analytic.v_public_tenders
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT pt.id_zakazky,
    pt.nazev_zakazky,
        CASE
            WHEN "position"(pt.organizacni_jednotka_spravy, '-'::text) = 0 THEN pt.organizacni_jednotka_spravy
            ELSE "left"(pt.organizacni_jednotka_spravy, "position"(pt.organizacni_jednotka_spravy, '-'::text) - 2)
        END AS organizacni_jednotka_spravy,
        CASE
            WHEN pto.ico IS NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.datum_uzavreni_smlouvy,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.smluvni_cena_s_dph_kc > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
    pt.datum_zahajeni_zadavaciho_rizeni,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.updated_at,
    pt.created_at
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     LEFT JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND pt.datum_uzavreni_smlouvy >= '2020-09-01 00:00:00'::timestamp without time zone AND pt.datum_uzavreni_smlouvy < CURRENT_DATE AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));


-- analytic.v_public_tenders_dpp source

CREATE OR REPLACE VIEW analytic.v_public_tenders_dpp
AS SELECT pt.id_zakazky,
    pt.nazev_zakazky,
    pt.organizacni_jednotka_spravy,
    pt.zadavatel_nazev,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
    pt.zadavatel_ico,
    pt.datum_vytvoreni_zakazky,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.datum_uzavreni_smlouvy,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
    pt.datum_zahajeni_zadavaciho_rizeni,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.updated_at,
    pt.created_at
   FROM python.public_tenders_dpp pt
  WHERE (pt.faze_zakazky = ANY (ARRAY['Zadána'::text])) AND pt.datum_vytvoreni_zakazky >= '2021-10-01 00:00:00'::timestamp without time zone AND (pt.datum_uzavreni_smlouvy < CURRENT_DATE OR pt.datum_uzavreni_smlouvy IS NULL) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));


-- analytic.v_public_tenders_dpp_internal source

CREATE OR REPLACE VIEW analytic.v_public_tenders_dpp_internal
AS SELECT pt.id_zakazky,
    pt.nazev_zakazky,
    pt.zadavatel_ico,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.smluvni_cena_bez_dph_kc - NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_kc,
    (pt.smluvni_cena_bez_dph_kc - pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc) / NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_procent,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.datum_uzavreni_smlouvy - pt.datum_zahajeni_zadavaciho_rizeni AS delka_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu - pt.datum_zahajeni_zadavaciho_rizeni AS delka_lhuty_pro_podani_nabidek,
        CASE
            WHEN pt.hodnotici_kriteria ~~ '%100%'::text THEN 'Pouze cena'::text
            ELSE 'Více kritérií'::text
        END AS hodnotici_kriteria_typ,
    pt.faze_zakazky,
    pt.updated_at,
    pt.created_at,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < pt.datum_vytvoreni_zakazky THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Říjen 2021 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders_dpp pt
  WHERE (pt.faze_zakazky = ANY (ARRAY['Zadána'::text, 'Zrušena'::text])) AND pt.datum_vytvoreni_zakazky >= '2021-10-01 01:00:00+02'::timestamp with time zone AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));


-- analytic.v_public_tenders_dpp_internal_frequent_suppliers source

CREATE OR REPLACE VIEW analytic.v_public_tenders_dpp_internal_frequent_suppliers
AS SELECT count(DISTINCT pt.id_zakazky) AS pocet_zakazek,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.faze_zakazky,
    pt.nazev_smluvniho_partnera,
    sum(
        CASE
            WHEN pt.spolecnosti_oslovene_k_podani_nabidky_nazev ~~ (('%'::text || s2.nazev_smluvniho_partnera) || '%'::text) THEN 1
            ELSE 0
        END) AS pocet_podanych_nabidek,
    sum(
        CASE
            WHEN s.spolecnosti_oslovene_k_podani_nabidky_nazev = s2.nazev_smluvniho_partnera OR s.spolecnosti_oslovene_k_podani_nabidky_nazev IS NULL THEN 1
            ELSE 0
        END) AS pocet_vyhranych_zakazek,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < pt.datum_vytvoreni_zakazky THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Říjen 2021 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders_dpp pt
     LEFT JOIN LATERAL unnest(string_to_array(pt.spolecnosti_oslovene_k_podani_nabidky_nazev, '; '::text)) s(spolecnosti_oslovene_k_podani_nabidky_nazev) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.nazev_smluvniho_partnera, '; '::text)) s2(nazev_smluvniho_partnera) ON true
  WHERE (pt.faze_zakazky = ANY (ARRAY['Zadána'::text])) AND (pt.varianta_druhu_rizeni = ANY (ARRAY['Zakázka zadaná na základě výjimky'::text])) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text]))) AND pt.datum_vytvoreni_zakazky >= '2021-10-01 01:00:00+02'::timestamp with time zone
  GROUP BY pt.organizacni_jednotka_spravy, pt.nazev_smluvniho_partnera, pt.varianta_druhu_rizeni, pt.datum_uzavreni_smlouvy, pt.datum_vytvoreni_zakazky, pt.faze_zakazky, (
        CASE
            WHEN pt.datum_uzavreni_smlouvy < pt.datum_vytvoreni_zakazky THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Říjen 2021 - nyní'::text
        END);


-- analytic.v_public_tenders_dpp_opendata source

CREATE OR REPLACE VIEW analytic.v_public_tenders_dpp_opendata
AS SELECT pt.id_zakazky,
    pt.id_casti_zakazky,
    pt.systemove_cislo_zakazky,
    pt.evidencni_cislo_zakazky,
    pt.cislo_vysledku_zadavaciho_rizeni,
    pt.interni_cisla_smluv_objednavek,
    pt.nazev_zakazky,
    pt.nazev_casti_zakazky,
    pt.druh_verejne_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.typ_zakazky,
    pt.jedna_se_o_cast_vz,
    pt.zakazka_zavadejici_dns,
    pt.faze_zakazky,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.zakazka_souvisejici_s_vykonem_relevantni_cinnosti,
    pt.organizacni_jednotka_spravy,
    pt.datum_vytvoreni_zakazky,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_otevirani_nabidek_na_profilu,
    pt.lhuta_pro_doruceni_zadosti_o_ucast_na_profilu,
    pt.hodnotici_kriteria,
    pt.vysledkem_je_ramcova_dohoda,
    pt.hodnoceni_formou_elektronicke_aukce_prostrednictvim_ta,
    pt.hodnoceni_formou_elektronicke_aukce_mimo_aplikaci_ta,
    pt.spolecnosti_oslovene_k_podani_nabidky_pocet,
    pt.spolecnosti_oslovene_k_podani_nabidky_nazev,
    pt.pocet_zadosti_o_ucast,
    pt.seznam_zadosti_o_ucast,
    pt.seznam_nabidek_a_nabidkova_cena_bez_dph,
    pt.ucastnik_zadavaciho_rizeni_nazev,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
    pt.smluvni_cena_bez_dph_kc,
    pt.smluvni_cena_s_dph_kc,
    pt.datum_uzavreni_smlouvy,
    pt.skutecne_uhrazena_cena_bez_dph,
    pt.skutecne_uhrazena_cena_s_dph,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2
   FROM python.public_tenders_dpp pt
  WHERE (pt.faze_zakazky = ANY (ARRAY['Zadána'::text])) AND pt.datum_vytvoreni_zakazky >= '2021-10-01 00:00:00'::timestamp without time zone AND (pt.datum_uzavreni_smlouvy < CURRENT_DATE OR pt.datum_uzavreni_smlouvy IS NULL) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));


-- analytic.v_public_tenders_internal source

CREATE OR REPLACE VIEW analytic.v_public_tenders_internal
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT pt.id_zakazky,
    pt.nazev_zakazky,
    pto.oblast,
    pto.odbor_mhmp,
    pt.zadavatel_ico,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pt.druh_verejne_zakazky,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
    pt.smluvni_cena_s_dph_kc,
    pt.smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.smluvni_cena_s_dph_kc > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name,
        CASE
            WHEN pt.pocet_nabidek = 1 OR pt.pocet_nabidek = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek, ' nabídky')
            WHEN pt.pocet_nabidek > 4 THEN concat(pt.pocet_nabidek, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.smluvni_cena_bez_dph_kc - NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_kc,
    (pt.smluvni_cena_bez_dph_kc - pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc) / NULLIF(pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc, 0::double precision) AS rozdil_od_predp_hodnoty_procent,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.datum_uzavreni_smlouvy - pt.datum_zahajeni_zadavaciho_rizeni AS delka_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu - pt.datum_zahajeni_zadavaciho_rizeni AS delka_lhuty_pro_podani_nabidek,
        CASE
            WHEN pt.hodnotici_kriteria ~~ '%100%'::text THEN 'Pouze cena'::text
            ELSE 'Více kritérií'::text
        END AS hodnotici_kriteria_typ,
    pt.externi_zastoupeni_zadavatele_v_rizeni,
    pt.externi_zastoupeni_zadavatele_v_rizeni_nazev,
    pt.externi_zastoupeni_zadavatele_v_rizeni_ico,
    "left"(pt.hlavni_nipez_kod_zakazky, 3) AS cpv_kod,
    pt.hlavni_nipez_kod_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.faze_zakazky,
        CASE
            WHEN pto.ico IS NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
    pt.updated_at,
    pt.created_at,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     LEFT JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text, 'Zrušena'::text])) AND (pt.datum_vytvoreni_zakazky >= '2020-09-01 01:00:00+02'::timestamp with time zone OR pt.datum_vytvoreni_zakazky >= '2021-01-01 00:00:00+01'::timestamp with time zone AND pto.ico IS NOT NULL) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));


-- analytic.v_public_tenders_internal_frequent_suppliers source

CREATE OR REPLACE VIEW analytic.v_public_tenders_internal_frequent_suppliers
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT count(DISTINCT pt.id_zakazky) AS pocet_zakazek,
    pt.organizacni_jednotka_spravy,
    pt.varianta_druhu_rizeni,
    pto.oblast,
    pto.odbor_mhmp,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
    pt.datum_uzavreni_smlouvy,
    pt.datum_vytvoreni_zakazky,
    pt.faze_zakazky,
        CASE
            WHEN pto.ico IS NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END AS mhmp_po,
        CASE
            WHEN pt.varianta_druhu_rizeni = 'Neveřejná výzva k podání nabídky'::text THEN s.spolecnosti_oslovene_k_podani_nabidky_nazev
            ELSE s2.nazev_smluvniho_partnera
        END AS spolecnost,
    sum(
        CASE
            WHEN pt.spolecnosti_oslovene_k_podani_nabidky_nazev ~~ (('%'::text || s2.nazev_smluvniho_partnera) || '%'::text) THEN 1
            ELSE 0
        END) AS pocet_podanych_nabidek,
    sum(
        CASE
            WHEN s.spolecnosti_oslovene_k_podani_nabidky_nazev = s2.nazev_smluvniho_partnera OR s.spolecnosti_oslovene_k_podani_nabidky_nazev IS NULL THEN 1
            ELSE 0
        END) AS pocet_vyhranych_zakazek,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders pt
     LEFT JOIN LATERAL unnest(string_to_array(pt.spolecnosti_oslovene_k_podani_nabidky_nazev, '; '::text)) s(spolecnosti_oslovene_k_podani_nabidky_nazev) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.nazev_smluvniho_partnera, '; '::text)) s2(nazev_smluvniho_partnera) ON true
     LEFT JOIN LATERAL unnest(string_to_array(pt.ico_smluvniho_partnera, '; '::text)) s3(ico_smluvniho_partnera) ON true
     LEFT JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND (pt.varianta_druhu_rizeni = ANY (ARRAY['Neveřejná výzva k podání nabídky'::text, 'Drobná objednávka'::text, 'Přímo objednáno u jednoho dodavatele'::text, 'Zakázka zadaná na základě výjimky'::text])) AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text]))) AND (pt.datum_vytvoreni_zakazky >= '2020-09-01 01:00:00+02'::timestamp with time zone OR pt.datum_vytvoreni_zakazky >= '2021-01-01 00:00:00+01'::timestamp with time zone AND pto.ico IS NULL)
  GROUP BY pt.organizacni_jednotka_spravy, pt.varianta_druhu_rizeni, pto.oblast, pto.odbor_mhmp, (
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END), pt.datum_uzavreni_smlouvy, pt.datum_vytvoreni_zakazky, pt.faze_zakazky, (
        CASE
            WHEN pto.ico IS NULL THEN 'MHMP'::text
            ELSE 'Příspěvková organizace'::text
        END), (
        CASE
            WHEN pt.varianta_druhu_rizeni = 'Neveřejná výzva k podání nabídky'::text THEN s.spolecnosti_oslovene_k_podani_nabidky_nazev
            ELSE s2.nazev_smluvniho_partnera
        END), (
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-09-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            ELSE 'Září 2020 - nyní'::text
        END);


-- analytic.v_public_tenders_oict source

CREATE OR REPLACE VIEW analytic.v_public_tenders_oict
AS WITH inner_table AS (
         SELECT public_tenders_oict.id_zakazky,
            public_tenders_oict.systemove_cislo_zakazky,
            public_tenders_oict.nazev_zakazky,
            public_tenders_oict.rezim,
            public_tenders_oict.faze_zadavaciho_rizeni,
            public_tenders_oict.datum_zahajeni,
            public_tenders_oict.predpokladana_hodnota,
            public_tenders_oict.druh_zakazky,
            public_tenders_oict.druh_zadavaciho_rizeni,
            public_tenders_oict.strucny_popis_predmetu,
            public_tenders_oict.lhuta_pro_nabidky,
            public_tenders_oict.dodavatel_nazev,
            public_tenders_oict.dodavatel_ico,
            public_tenders_oict.dodavatel_adresa,
            public_tenders_oict.pocet_obdrzenych_nabidek,
            public_tenders_oict.datum_uzavreni_smlouvy,
            public_tenders_oict.konecna_cena_bez_dph,
            public_tenders_oict.konecna_cena_s_dph,
            public_tenders_oict.ramcova_dohoda,
            public_tenders_oict.pocet_casti,
            public_tenders_oict.datum_zruseni,
            public_tenders_oict.updated_at,
            COALESCE(public_tenders_oict.pocet_casti, 0::bigint) AS pocet_casti_,
                CASE
                    WHEN public_tenders_oict.druh_zadavaciho_rizeni = 'VZMR - pouze zveřejnění v EZAK'::text OR public_tenders_oict.druh_zadavaciho_rizeni = 'VZMR s uveřejněním výzvy'::text OR public_tenders_oict.rezim = 'VZ malého rozsahu'::text THEN 'VZMR'::text
                    ELSE public_tenders_oict.druh_zadavaciho_rizeni
                END AS druh_zadavaciho_rizeni_
           FROM python.public_tenders_oict
          WHERE public_tenders_oict.rezim <> 'mimo režim ZZVZ'::text
        )
 SELECT inner_table.id_zakazky,
    inner_table.systemove_cislo_zakazky,
    inner_table.nazev_zakazky,
    inner_table.rezim,
    inner_table.faze_zadavaciho_rizeni,
    inner_table.datum_zahajeni,
    inner_table.predpokladana_hodnota,
    inner_table.druh_zakazky,
    inner_table.strucny_popis_predmetu,
    inner_table.lhuta_pro_nabidky,
    inner_table.dodavatel_nazev,
    inner_table.dodavatel_ico,
    inner_table.dodavatel_adresa,
    inner_table.pocet_obdrzenych_nabidek,
    inner_table.datum_uzavreni_smlouvy,
    inner_table.konecna_cena_bez_dph,
    inner_table.konecna_cena_s_dph,
    inner_table.ramcova_dohoda,
    inner_table.datum_zruseni,
    inner_table.updated_at,
    inner_table.pocet_casti_ AS pocet_casti,
    inner_table.druh_zadavaciho_rizeni_ AS druh_zadavaciho_rizeni
   FROM inner_table
  WHERE inner_table.pocet_casti_ < 1;


-- analytic.v_public_tenders_oict_opendata source

CREATE OR REPLACE VIEW analytic.v_public_tenders_oict_opendata
AS WITH inner_table AS (
         SELECT public_tenders_oict.id_zakazky,
            public_tenders_oict.systemove_cislo_zakazky,
            public_tenders_oict.nazev_zakazky,
            public_tenders_oict.rezim,
            public_tenders_oict.faze_zadavaciho_rizeni,
            public_tenders_oict.datum_zahajeni,
            public_tenders_oict.predpokladana_hodnota,
            public_tenders_oict.druh_zakazky,
            public_tenders_oict.druh_zadavaciho_rizeni,
            public_tenders_oict.strucny_popis_predmetu,
            public_tenders_oict.lhuta_pro_nabidky,
            public_tenders_oict.dodavatel_nazev,
            public_tenders_oict.dodavatel_ico,
            public_tenders_oict.dodavatel_adresa,
            public_tenders_oict.pocet_obdrzenych_nabidek,
            public_tenders_oict.datum_uzavreni_smlouvy,
            public_tenders_oict.konecna_cena_bez_dph,
            public_tenders_oict.konecna_cena_s_dph,
            public_tenders_oict.ramcova_dohoda,
            public_tenders_oict.pocet_casti,
            public_tenders_oict.datum_zruseni,
            public_tenders_oict.updated_at,
            COALESCE(public_tenders_oict.pocet_casti, 0::bigint) AS pocet_casti_,
                CASE
                    WHEN public_tenders_oict.druh_zadavaciho_rizeni = 'VZMR - pouze zveřejnění v EZAK'::text OR public_tenders_oict.druh_zadavaciho_rizeni = 'VZMR s uveřejněním výzvy'::text OR public_tenders_oict.rezim = 'VZ malého rozsahu'::text THEN 'VZMR'::text
                    ELSE public_tenders_oict.druh_zadavaciho_rizeni
                END AS druh_zadavaciho_rizeni_
           FROM python.public_tenders_oict
          WHERE public_tenders_oict.rezim <> 'mimo režim ZZVZ'::text
        )
 SELECT inner_table.id_zakazky,
    inner_table.systemove_cislo_zakazky,
    inner_table.nazev_zakazky,
    inner_table.rezim,
    inner_table.faze_zadavaciho_rizeni,
    inner_table.datum_zahajeni,
    inner_table.predpokladana_hodnota,
    inner_table.druh_zakazky,
    inner_table.strucny_popis_predmetu,
    inner_table.lhuta_pro_nabidky,
    inner_table.dodavatel_nazev,
    inner_table.dodavatel_ico,
    inner_table.dodavatel_adresa,
    inner_table.pocet_obdrzenych_nabidek,
    inner_table.datum_uzavreni_smlouvy,
    inner_table.konecna_cena_bez_dph,
    inner_table.konecna_cena_s_dph,
    inner_table.ramcova_dohoda,
    inner_table.datum_zruseni,
    inner_table.updated_at,
    inner_table.pocet_casti_ AS pocet_casti,
    inner_table.druh_zadavaciho_rizeni_ AS druh_zadavaciho_rizeni
   FROM inner_table
  WHERE inner_table.pocet_casti_ < 1 AND inner_table.faze_zadavaciho_rizeni = 'Zadáno'::text;


-- analytic.v_public_tenders_opendata source

CREATE OR REPLACE VIEW analytic.v_public_tenders_opendata
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT pt.id_zakazky,
    pt.id_casti_zakazky,
    pt.systemove_cislo_zakazky,
    pt.evidencni_cislo_zakazky,
    pt.cislo_vysledku_zadavaciho_rizeni,
    pt.interni_cisla_smluv_objednavek,
    pt.nazev_zakazky,
    pt.nazev_casti_zakazky,
    pt.externi_zastoupeni_zadavatele_v_rizeni,
    pt.externi_zastoupeni_zadavatele_v_rizeni_nazev,
    pt.externi_zastoupeni_zadavatele_v_rizeni_ico,
    pt.druh_verejne_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.typ_zakazky,
    pt.varianta_druhu_rizeni,
    pt.jedna_se_o_cast_vz,
    pt.zakazka_zavadejici_dns,
    pt.faze_zakazky,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.zakazka_souvisejici_s_vykonem_relevantni_cinnosti,
    pt.hlavni_nipez_kod_zakazky,
    pt.organizacni_jednotka_spravy,
    pt.datum_vytvoreni_zakazky,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_otevirani_nabidek_na_profilu,
    pt.lhuta_pro_doruceni_zadosti_o_ucast_na_profilu,
    pt.hodnotici_kriteria,
    pt.vysledkem_je_ramcova_dohoda,
    pt.hodnoceni_formou_elektronicke_aukce_prostrednictvim_ta,
    pt.hodnoceni_formou_elektronicke_aukce_mimo_aplikaci_ta,
    pt.spolecnosti_oslovene_k_podani_nabidky_pocet,
    pt.spolecnosti_oslovene_k_podani_nabidky_nazev,
    pt.pocet_zadosti_o_ucast,
    pt.seznam_zadosti_o_ucast,
    pt.seznam_nabidek_a_nabidkova_cena_bez_dph,
    pt.ucastnik_zadavaciho_rizeni_nazev,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
    pt.smluvni_cena_bez_dph_kc,
    pt.smluvni_cena_s_dph_kc,
    pt.datum_uzavreni_smlouvy,
    pt.skutecne_uhrazena_cena_bez_dph,
    pt.skutecne_uhrazena_cena_s_dph,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     LEFT JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE pto.ico IS NULL AND (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND pt.datum_uzavreni_smlouvy >= '2020-09-01 00:00:00'::timestamp without time zone AND pt.datum_uzavreni_smlouvy < CURRENT_DATE AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));


-- analytic.v_public_tenders_opendata_funded_organisations source

CREATE OR REPLACE VIEW analytic.v_public_tenders_opendata_funded_organisations
AS WITH inhouse_ico AS (
         SELECT public_tenders_city_companies.ico
           FROM python.public_tenders_city_companies
        UNION ALL
         SELECT public_tenders_funded_organisations.ico
           FROM python.public_tenders_funded_organisations
        )
 SELECT pt.id_zakazky,
    pt.id_casti_zakazky,
    pt.systemove_cislo_zakazky,
    pt.evidencni_cislo_zakazky,
    pt.cislo_vysledku_zadavaciho_rizeni,
    pt.interni_cisla_smluv_objednavek,
    pt.nazev_zakazky,
    pt.nazev_casti_zakazky,
    pt.externi_zastoupeni_zadavatele_v_rizeni,
    pt.externi_zastoupeni_zadavatele_v_rizeni_nazev,
    pt.externi_zastoupeni_zadavatele_v_rizeni_ico,
    pt.druh_verejne_zakazky,
    pt.druh_zadavaciho_rizeni,
    pt.typ_zakazky,
    pt.varianta_druhu_rizeni,
    pt.jedna_se_o_cast_vz,
    pt.zakazka_zavadejici_dns,
    pt.faze_zakazky,
    pt.celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.zakazka_souvisejici_s_vykonem_relevantni_cinnosti,
    pt.hlavni_nipez_kod_zakazky,
    pt.organizacni_jednotka_spravy,
    pt.datum_vytvoreni_zakazky,
    pt.datum_zahajeni_zadavaciho_rizeni,
    pt.lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_otevirani_nabidek_na_profilu,
    pt.lhuta_pro_doruceni_zadosti_o_ucast_na_profilu,
    pt.hodnotici_kriteria,
    pt.vysledkem_je_ramcova_dohoda,
    pt.hodnoceni_formou_elektronicke_aukce_prostrednictvim_ta,
    pt.hodnoceni_formou_elektronicke_aukce_mimo_aplikaci_ta,
    pt.spolecnosti_oslovene_k_podani_nabidky_pocet,
    pt.spolecnosti_oslovene_k_podani_nabidky_nazev,
    pt.pocet_zadosti_o_ucast,
    pt.seznam_zadosti_o_ucast,
    pt.seznam_nabidek_a_nabidkova_cena_bez_dph,
    pt.ucastnik_zadavaciho_rizeni_nazev,
    pt.nazev_smluvniho_partnera,
    pt.ico_smluvniho_partnera,
    pt.smluvni_cena_bez_dph_kc,
    pt.smluvni_cena_s_dph_kc,
    pt.datum_uzavreni_smlouvy,
    pt.skutecne_uhrazena_cena_bez_dph,
    pt.skutecne_uhrazena_cena_s_dph,
        CASE
            WHEN (pt.ico_smluvniho_partnera IN ( SELECT inhouse_ico.ico
               FROM inhouse_ico)) THEN 'Ano'::text
            ELSE 'Ne'::text
        END AS inhouse,
        CASE
            WHEN pt.pocet_nabidek = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek
        END AS pocet_nabidek,
        CASE
            WHEN m2.mmr_group IS NOT NULL THEN m2.mmr_group::character varying
            WHEN character_length(pt.hlavni_nipez_kod_zakazky) = 2 THEN 'Neuvedeno'::character varying
            WHEN g3.cpv_group IS NOT NULL THEN g3.cpv_group::character varying
            WHEN g2.cpv_group IS NOT NULL THEN g2.cpv_group::character varying
            ELSE 'Neuvedeno'::character varying
        END AS cpv_name
   FROM python.public_tenders pt
     LEFT JOIN ( SELECT public_tenders.id_zakazky
           FROM python.public_tenders
          ORDER BY public_tenders.smluvni_cena_bez_dph_kc DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_cpv_groups g3 ON g3.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 3)
     LEFT JOIN python.public_tenders_cpv_groups g2 ON g2.cpv_code::text = "left"(pt.hlavni_nipez_kod_zakazky, 2)
     LEFT JOIN python.public_tenders_mmr_commodity_groups m2 ON m2.mmr_commodity_code::text = pt.hlavni_nipez_kod_zakazky
     JOIN python.public_tenders_funded_organisations pto ON pt.zadavatel_ico::text = pto.ico
  WHERE (pt.faze_zakazky = ANY (ARRAY['Ukončeno plnění smlouvy na základě veřejné zakázky'::text, 'Zadána'::text])) AND pt.datum_uzavreni_smlouvy >= '2020-09-01 00:00:00'::timestamp without time zone AND pt.datum_uzavreni_smlouvy < CURRENT_DATE AND (pt.vysledkem_je_ramcova_dohoda = 'Ne'::text OR (pt.varianta_druhu_rizeni = ANY (ARRAY['Výzva k plnění rámcové smlouvy'::text, 'Minitendr'::text])));


-- analytic.v_public_tenders_sck source

CREATE OR REPLACE VIEW analytic.v_public_tenders_sck
AS WITH nadrizene_vz AS (
         SELECT pts.systemove_cislo_zakazky,
            pts.nazev_zakazky
           FROM python.public_tenders_sck pts
          WHERE (pts.systemove_cislo_zakazky IN ( SELECT p.systemove_cislo_nadrizene_vz
                   FROM python.public_tenders_sck p
                  WHERE p.systemove_cislo_nadrizene_vz IS NOT NULL
                  GROUP BY p.systemove_cislo_nadrizene_vz))
        )
 SELECT pt.systemove_cislo_zakazky AS id_zakazky,
        CASE
            WHEN nvz.systemove_cislo_zakazky IS NOT NULL THEN (nvz.nazev_zakazky || ' - '::text) || pt.nazev_zakazky
            ELSE pt.nazev_zakazky
        END AS nazev_zakazky,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN 'Středočeský kraj'::text
            ELSE 'Příspěvková organizace'::text
        END AS sck_po,
    ('https://zakazky.kr-stredocesky.cz/contract_display_'::text || pt.id_zakazky) || '.html'::text AS url,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN pt.oddeleni
            ELSE pt.zadavatel_nazev
        END AS organizacni_jednotka_spravy,
    COALESCE(pt.druh_zadavaciho_rizeni, 'Neuvedeno'::text) AS varianta_druhu_rizeni,
    COALESCE(pt.druh_zakazky, 'Neuvedeno'::text) AS druh_verejne_zakazky,
    COALESCE(pt.dodavatel_nazev, 'Neuvedeno'::text) AS nazev_smluvniho_partnera,
    COALESCE(pt.dodavatel_ico, 'Neuvedeno'::text) AS ico_smluvniho_partnera,
        CASE
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek_vstupujicich_do_hodnoceni
        END AS pocet_nabidek,
    pt.datum_uzavreni_smlouvy,
    pt.konecna_cena_s_dph AS smluvni_cena_s_dph_kc,
    pt.konecna_cena_bez_dph AS smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.konecna_cena_s_dph > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
    pt.datum_zahajeni AS datum_zahajeni_zadavaciho_rizeni,
        CASE
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = 1 OR pt.pocet_nabidek_vstupujicich_do_hodnoceni = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek_vstupujicich_do_hodnoceni, ' nabídky')
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni > 4 THEN concat(pt.pocet_nabidek_vstupujicich_do_hodnoceni, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.faze_zadavaciho_rizeni AS faze_zakazky,
    pt.updated_at
   FROM python.public_tenders_sck pt
     LEFT JOIN ( SELECT public_tenders_sck.id_zakazky
           FROM python.public_tenders_sck
          ORDER BY public_tenders_sck.konecna_cena_bez_dph DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_sck_funded_organisations pto ON pt.zadavatel_ico = pto.ico
     LEFT JOIN nadrizene_vz nvz ON nvz.systemove_cislo_zakazky = pt.systemove_cislo_nadrizene_vz
  WHERE NOT (pt.systemove_cislo_zakazky IN ( SELECT nz.systemove_cislo_zakazky
           FROM nadrizene_vz nz)) AND (pt.faze_zadavaciho_rizeni = ANY (ARRAY['Zadáno'::text, 'Uzavřeno'::text])) AND pt.datum_uzavreni_smlouvy >= '2020-01-01'::date AND pt.datum_zahajeni >= '2020-01-01'::date;


-- analytic.v_public_tenders_sck_internal source

CREATE OR REPLACE VIEW analytic.v_public_tenders_sck_internal
AS WITH nadrizene_vz AS (
         SELECT pts.systemove_cislo_zakazky,
            pts.nazev_zakazky
           FROM python.public_tenders_sck pts
          WHERE (pts.systemove_cislo_zakazky IN ( SELECT p.systemove_cislo_nadrizene_vz
                   FROM python.public_tenders_sck p
                  WHERE p.systemove_cislo_nadrizene_vz IS NOT NULL
                  GROUP BY p.systemove_cislo_nadrizene_vz))
        )
 SELECT pt.systemove_cislo_zakazky AS id_zakazky,
        CASE
            WHEN nvz.systemove_cislo_zakazky IS NOT NULL THEN (nvz.nazev_zakazky || ' - '::text) || pt.nazev_zakazky
            ELSE pt.nazev_zakazky
        END AS nazev_zakazky,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN 'Středočeský kraj'::text
            ELSE 'Příspěvková organizace'::text
        END AS sck_po,
    pto.typ AS oblast,
    ('https://zakazky.kr-stredocesky.cz/contract_display_'::text || pt.id_zakazky) || '.html'::text AS url,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN pt.oddeleni
            ELSE pt.zadavatel_nazev
        END AS organizacni_jednotka_spravy,
    COALESCE(pt.druh_zadavaciho_rizeni, 'Neuvedeno'::text) AS varianta_druhu_rizeni,
    COALESCE(pt.druh_zakazky, 'Neuvedeno'::text) AS druh_verejne_zakazky,
    COALESCE(pt.dodavatel_nazev, 'Neuvedeno'::text) AS nazev_smluvniho_partnera,
    COALESCE(pt.dodavatel_ico, 'Neuvedeno'::text) AS ico_smluvniho_partnera,
        CASE
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek_vstupujicich_do_hodnoceni
        END AS pocet_nabidek,
    pt.konecna_cena_s_dph AS smluvni_cena_s_dph_kc,
    pt.konecna_cena_bez_dph AS smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.konecna_cena_s_dph > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
        CASE
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = 1 OR pt.pocet_nabidek_vstupujicich_do_hodnoceni = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek_vstupujicich_do_hodnoceni, ' nabídky')
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni > 4 THEN concat(pt.pocet_nabidek_vstupujicich_do_hodnoceni, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.predpokladana_hodnota AS celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.konecna_cena_bez_dph - NULLIF(pt.predpokladana_hodnota, 0::double precision) AS rozdil_od_predp_hodnoty_kc,
    (pt.konecna_cena_bez_dph - pt.predpokladana_hodnota) / NULLIF(pt.predpokladana_hodnota, 0::double precision) AS rozdil_od_predp_hodnoty_procent,
    pt.datum_uzavreni_smlouvy,
    pt.lhuta_pro_nabidky AS lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_zahajeni AS datum_zahajeni_zadavaciho_rizeni,
    pt.datum_uzavreni_smlouvy - pt.datum_zahajeni AS delka_zadavaciho_rizeni,
    pt.lhuta_pro_nabidky - pt.datum_zahajeni AS delka_lhuty_pro_podani_nabidek,
    pt.faze_zadavaciho_rizeni AS faze_zakazky,
    pt.updated_at,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-01-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            WHEN pt.datum_uzavreni_smlouvy >= '2020-01-01'::date THEN 'Leden 2020 - nyní'::text
            ELSE 'Neuvedeno'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders_sck pt
     LEFT JOIN ( SELECT public_tenders_sck.id_zakazky
           FROM python.public_tenders_sck
          ORDER BY public_tenders_sck.konecna_cena_bez_dph DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_sck_funded_organisations pto ON pt.zadavatel_ico = pto.ico
     LEFT JOIN nadrizene_vz nvz ON nvz.systemove_cislo_zakazky = pt.systemove_cislo_nadrizene_vz
  WHERE NOT (pt.systemove_cislo_zakazky IN ( SELECT nz.systemove_cislo_zakazky
           FROM nadrizene_vz nz)) AND (pt.faze_zadavaciho_rizeni = ANY (ARRAY['Zadáno'::text, 'Zrušeno'::text, 'Uzavřeno'::text])) AND pt.datum_zahajeni >= '2020-01-01'::date;


-- analytic.v_public_tenders_sck_internal_frequent_suppliers source

CREATE OR REPLACE VIEW analytic.v_public_tenders_sck_internal_frequent_suppliers
AS SELECT count(DISTINCT pt.id_zakazky) AS pocet_zakazek,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN pt.oddeleni
            ELSE pt.zadavatel_nazev
        END AS organizacni_jednotka_spravy,
    COALESCE(pt.druh_zadavaciho_rizeni, 'Neuvedeno'::text) AS varianta_druhu_rizeni,
    pt.datum_uzavreni_smlouvy,
    pt.faze_zadavaciho_rizeni AS faze_zakazky,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN 'Středočeský kraj'::text
            ELSE 'Příspěvková organizace'::text
        END AS sck_po,
    pto.typ AS oblast,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-01-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            WHEN pt.datum_uzavreni_smlouvy >= '2020-01-01'::date THEN 'Leden 2020 - nyní'::text
            ELSE 'Neuvedeno'::text
        END AS uzavreni_smlouvy_filtr,
    pt.updated_at
   FROM python.public_tenders_sck pt
     LEFT JOIN python.public_tenders_sck_funded_organisations pto ON pt.zadavatel_ico = pto.ico
  WHERE NOT (pt.systemove_cislo_zakazky IN ( SELECT pts.systemove_cislo_nadrizene_vz
           FROM python.public_tenders_sck pts
          WHERE pts.systemove_cislo_nadrizene_vz IS NOT NULL
          GROUP BY pts.systemove_cislo_nadrizene_vz)) AND (pt.faze_zadavaciho_rizeni = ANY (ARRAY['Zadáno'::text, 'Zrušeno'::text, 'Uzavřeno'::text])) AND pt.datum_zahajeni >= '2020-01-01'::date
  GROUP BY (
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN pt.oddeleni
            ELSE pt.zadavatel_nazev
        END), pt.druh_zadavaciho_rizeni, pt.datum_uzavreni_smlouvy, pt.faze_zadavaciho_rizeni, (
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN 'Středočeský kraj'::text
            ELSE 'Příspěvková organizace'::text
        END), pto.typ, (
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-01-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            WHEN pt.datum_uzavreni_smlouvy >= '2020-01-01'::date THEN 'Leden 2020 - nyní'::text
            ELSE 'Neuvedeno'::text
        END), pt.updated_at;
		
-- analytic.v_sreality source

CREATE OR REPLACE VIEW analytic.v_sreality
AS SELECT s.week_start_dt,
    s.week_label,
    s.week_number,
    s.offer_type,
    s.district,
    s.category,
    s.offers_count,
    s.median_price,
        CASE
            WHEN s.district IS NULL OR s.district = ''::text OR s.district = 'All'::text THEN NULL::numeric
            WHEN "right"(s.district, 1)::numeric = 0::numeric THEN 10::numeric
            ELSE "right"(s.district, 1)::numeric
        END AS district_order
   FROM python.sreality s;


-- analytic.v_ukraine_statistics source

CREATE OR REPLACE VIEW analytic.v_ukraine_statistics
AS WITH inner_table AS (
         SELECT us.kraj,
            us.okres,
            us.obec,
            us.kod_obce,
            us.obec_mc,
            us.celkem,
            us.m_do_3,
            us.z_do_3,
            us.x_do_3,
            us.m_do_6,
            us.z_do_6,
            us.x_do_6,
            us.m_do_15,
            us.z_do_15,
            us.x_do_15,
            us.m_do_18,
            us.z_do_18,
            us.x_do_18,
            us.m_do_65,
            us.z_do_65,
            us.x_do_65,
            us.m_sen,
            us.z_sen,
            us.x_sen,
            us.source,
            us.updated_at,
            to_date("left"(replace("right"(us.source, 16), '_'::text, '-'::text), 10), 'dd-mm-yyyy'::text) AS file_date
           FROM python.ukraine_statistics us
        )
 SELECT inner_table.file_date,
    inner_table.kraj,
    inner_table.okres,
    inner_table.obec,
    inner_table.obec_mc,
    inner_table.celkem,
    inner_table.m_do_3,
    inner_table.z_do_3,
    inner_table.x_do_3,
    inner_table.m_do_6,
    inner_table.z_do_6,
    inner_table.x_do_6,
    inner_table.m_do_15,
    inner_table.z_do_15,
    inner_table.x_do_15,
    inner_table.m_do_18,
    inner_table.z_do_18,
    inner_table.x_do_18,
    inner_table.m_do_65,
    inner_table.z_do_65,
    inner_table.x_do_65,
    inner_table.m_sen,
    inner_table.z_sen,
    inner_table.x_sen
   FROM inner_table
  WHERE inner_table.file_date = (( SELECT max(inner_table_1.file_date) AS max
           FROM inner_table inner_table_1));		
		   
-- analytic.v_uzis_covid19_age_groups source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_age_groups
AS SELECT covid19_cz_details.datum_hlaseni::date AS report_date,
    covid19_cz_details.vek::numeric AS age,
        CASE
            WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
            WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
            WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
            WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
            WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
            WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
            ELSE '65 a víc'::text
        END AS age_group,
    0 AS id,
    covid19_cz_details.pohlavi::character varying(10) AS gender,
        CASE
            WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN 1
            WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN 2
            WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN 3
            WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN 4
            WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN 5
            WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN 6
            ELSE 7
        END AS age_group_order,
    covid19_cz_details.kraj::character varying(100) AS region,
    codebook_regions."kraj-nazev" AS nazev_kraj
   FROM uzis.covid19_cz_details
     LEFT JOIN uzis.codebook_regions ON codebook_regions."kraj-kod" = covid19_cz_details.kraj;


-- analytic.v_uzis_covid19_age_groups_prague source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_age_groups_prague
AS SELECT covid19_cz_details.datum_hlaseni AS report_date,
    covid19_cz_details.vek AS age,
        CASE
            WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
            WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
            WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
            WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
            WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
            WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
            ELSE '65 a víc'::text
        END AS age_group,
    covid19_cz_details.pohlavi AS gender,
        CASE
            WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN 1
            WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN 2
            WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN 3
            WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN 4
            WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN 5
            WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN 6
            ELSE 7
        END AS age_group_order,
        CASE
            WHEN covid19_cz_details.kraj = 'CZ010'::text THEN 'Hlavní město Praha'::text
            ELSE 'Středočeský kraj'::text
        END AS region
   FROM uzis.covid19_cz_details
  WHERE covid19_cz_details.kraj = ANY (ARRAY['CZ010'::text, 'CZ020'::text]);


-- analytic.v_uzis_covid19_age_groups_ratios source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_age_groups_ratios
AS WITH age_groups AS (
         SELECT date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone) AS report_month,
                CASE
                    WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
                    WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
                    WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
                    WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
                    WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
                    WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
                    ELSE '65 a víc'::text
                END AS age_group,
            count(*) AS cases_count,
            covid19_cz_details.kraj AS region,
            codebook_regions."kraj-nazev" AS nazev_kraj
           FROM uzis.covid19_cz_details
             LEFT JOIN uzis.codebook_regions ON codebook_regions."kraj-kod" = covid19_cz_details.kraj
          GROUP BY covid19_cz_details.kraj, (date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone)), (
                CASE
                    WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
                    WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
                    WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
                    WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
                    WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
                    WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
                    ELSE '65 a víc'::text
                END), codebook_regions."kraj-nazev"
        ), totals AS (
         SELECT date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone) AS report_month,
            count(*) AS cases_count,
            covid19_cz_details.kraj AS region
           FROM uzis.covid19_cz_details
          GROUP BY covid19_cz_details.kraj, (date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone))
        )
 SELECT ag.report_month,
    ag.age_group,
    ag.cases_count::numeric / t.cases_count::numeric AS age_group_ratio,
        CASE
            WHEN ag.age_group = '0-5'::text THEN 1
            WHEN ag.age_group = '6-9'::text THEN 2
            WHEN ag.age_group = '10-14'::text THEN 3
            WHEN ag.age_group = '15-19'::text THEN 4
            WHEN ag.age_group = '20-25'::text THEN 5
            WHEN ag.age_group = '26-64'::text THEN 6
            ELSE 7
        END AS age_group_order,
    ag.region::character varying(100) AS region,
    ag.nazev_kraj
   FROM age_groups ag
     JOIN totals t ON ag.report_month = t.report_month AND ag.region = t.region;


-- analytic.v_uzis_covid19_central_bohemia source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_central_bohemia
AS SELECT eo.datum,
    eo.okres_nazev,
    eo.kumulativni_pocet_pozitivnich_osob,
    eo.kumulativni_pocet_zemrelych,
    eo.kumulativni_pocet_vylecenych,
        CASE
            WHEN eo.kumulativni_pocet_zemrelych IS NULL THEN 0::numeric
            ELSE eo.kumulativni_pocet_zemrelych::numeric
        END -
        CASE
            WHEN lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)::numeric
        END AS pocet_zemrelych_den,
    eo.incidence AS pocet_pozitivnich_osob_den,
        CASE
            WHEN eo.prevalence IS NULL THEN 0::numeric
            ELSE eo.prevalence::numeric
        END -
        CASE
            WHEN lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)::numeric
        END AS pocet_aktivnich_pripadu_den,
    eo.prevalence AS aktualni_pocet_aktivnich_pripadu,
    eo.aktualni_pocet_hospitalizovanych_osob,
        CASE
            WHEN eo.aktualni_pocet_hospitalizovanych_osob IS NULL THEN 0::numeric::double precision
            ELSE eo.aktualni_pocet_hospitalizovanych_osob
        END -
        CASE
            WHEN lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric::double precision
            ELSE lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)
        END AS pocet_hospitalizovanych_osob,
    eo.casova_znamka AS posledni_aktualizace,
        CASE
            WHEN eo.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS vcerejsi_den,
    eo.kumulativni_pocet_hospitalizovanych_osob,
    dp.population,
    eo.kumulativni_pocet_zemrelych + eo.kumulativni_pocet_vylecenych AS aktualni_pocet_neaktivnich_pripadu,
        CASE
            WHEN eo.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN true
            ELSE false
        END AS last_7days,
        CASE
            WHEN eo.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN true
            ELSE false
        END AS last_14days,
        CASE
            WHEN eo.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN eo.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc,
    eo.opou_nazev,
    eo.opou_kod,
    eo.obec_kod
   FROM ( SELECT covid19_municipalities.casova_znamka,
            covid19_municipalities.datum,
            covid19_municipalities.obec_kod,
            covid19_municipalities.psc,
            covid19_municipalities.opou_kod,
            covid19_municipalities.opou_nazev,
            covid19_municipalities.orp_kod,
            covid19_municipalities.orp_nazev,
            covid19_municipalities.okres_kod,
            covid19_municipalities.okres_nazev,
            covid19_municipalities.kraj_kod,
            covid19_municipalities.kraj_nazev,
            covid19_municipalities.kumulativni_pocet_pozitivnich_osob,
            covid19_municipalities.kumulativni_pocet_hospitalizovanych_osob,
            covid19_municipalities.aktualni_pocet_hospitalizovanych_osob,
            covid19_municipalities.incidence,
            covid19_municipalities.pocet_vyleceni_den,
            covid19_municipalities.kumulativni_pocet_vylecenych,
            covid19_municipalities.pocet_zemreli_den,
            covid19_municipalities.zemreli_za_hospitalizace,
            covid19_municipalities.kumulativni_pocet_zemrelych,
            covid19_municipalities.prevalence,
            row_number() OVER (PARTITION BY covid19_municipalities.obec_kod, covid19_municipalities.datum) AS rank
           FROM uzis.covid19_municipalities
          WHERE covid19_municipalities.kraj_kod = 'CZ020'::text) eo
     LEFT JOIN uzis.population_central_bohemia dp ON eo.okres_kod = dp.region_code::text
  WHERE eo.datum < date_trunc('day'::text, now()) AND eo.okres_nazev <> 'Praha'::text AND eo.rank = 1;


-- analytic.v_uzis_covid19_cz source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_cz
AS SELECT covid19_cz_daily.datum::date AS report_date,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_nakazenych IS NULL THEN 0::numeric
            ELSE covid19_cz_daily.kumulativni_pocet_nakazenych::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_nakazenych) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_nakazenych) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS infected_count_daily,
    covid19_cz_daily.kumulativni_pocet_nakazenych::numeric AS infected_count_cumulative,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_vylecenych IS NULL THEN 0::numeric
            ELSE covid19_cz_daily.kumulativni_pocet_vylecenych::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_vylecenych) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_vylecenych) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS healed_count_daily,
    covid19_cz_daily.kumulativni_pocet_vylecenych::numeric AS healed_count_cumulative,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_umrti IS NULL THEN 0::numeric
            ELSE covid19_cz_daily.kumulativni_pocet_umrti::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_umrti) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_umrti) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS deceased_count_daily,
    covid19_cz_daily.kumulativni_pocet_umrti::numeric AS deceased_count_cumulative,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_testu IS NULL THEN 0::numeric
            ELSE covid19_cz_daily.kumulativni_pocet_testu::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_testu) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_testu) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS tested_count_daily,
    covid19_cz_daily.kumulativni_pocet_testu::numeric AS tested_count_cumulative,
    (covid19_cz_daily.kumulativni_pocet_nakazenych - covid19_cz_daily.kumulativni_pocet_umrti - covid19_cz_daily.kumulativni_pocet_vylecenych)::numeric AS active_count_current,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_nakazenych IS NULL THEN 0::numeric
            ELSE (covid19_cz_daily.kumulativni_pocet_nakazenych - covid19_cz_daily.kumulativni_pocet_umrti - covid19_cz_daily.kumulativni_pocet_vylecenych)::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_nakazenych - covid19_cz_daily.kumulativni_pocet_umrti - covid19_cz_daily.kumulativni_pocet_vylecenych) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_nakazenych - covid19_cz_daily.kumulativni_pocet_umrti - covid19_cz_daily.kumulativni_pocet_vylecenych) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS active_count_daily,
        CASE
            WHEN covid19_cz_daily.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS yesterday,
        CASE
            WHEN covid19_cz_daily.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN covid19_cz_daily.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM uzis.covid19_cz_daily;


-- analytic.v_uzis_covid19_hospital_capacity source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_celkem,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_celkem,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_volna,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem,
    covid19_hospital_capacity_details.cvvhd_kapacita_volna,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;


-- analytic.v_uzis_covid19_hospital_capacity_beds source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity_beds
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_standard_kyslik'::text AS typ,
    'Běžné'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_hfno_cpap'::text AS typ,
    'Běžné'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_upv_niv'::text AS typ,
    'Běžné'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_celkem - covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.inf_luzka_kyslik_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_standard_kyslik'::text AS typ,
    'Infekční'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_celkem - covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.inf_luzka_hfno_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_hfno_cpap'::text AS typ,
    'Infekční'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.inf_luzka_upv_kapacita_celkem - covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.inf_luzka_upv_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_upv_niv'::text AS typ,
    'Infekční'::text AS oddeleni
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;


-- analytic.v_uzis_covid19_hospitalized_daily source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospitalized_daily
AS SELECT dta.datum AS reporting_date,
    dta.datum AS reporting_time,
    dta.updated_at AS data_valid_to,
    ck."kraj-nazev" AS region_name,
    dta.pocet_hosp AS current_hospitalized,
    COALESCE(dta.pocet_hosp, 0::double precision) - COALESCE(lag(dta.pocet_hosp) OVER (PARTITION BY dta.nemocnice_kraj ORDER BY dta.datum), 0::double precision) AS new_current_hospitalized,
    dta.pacient_prvni_zaznam AS new_hospitalized,
    dta.stav_bez_priznaku AS current_without_symptoms,
    dta.stav_lehky AS current_light_progress,
    dta.stav_stredni AS current_medium_progress,
    dta.stav_tezky AS current_severe_progress,
    COALESCE(dta.stav_tezky, 0::double precision) - COALESCE(lag(dta.stav_tezky) OVER (PARTITION BY dta.nemocnice_kraj ORDER BY dta.datum), 0::double precision) AS new_severe_progress,
    dta.jip,
    dta.kyslik AS oxygen_treatment_standart,
    dta.hfno,
    dta.upv,
    dta.ecmo,
    dta.tezky_upv_ecmo AS current_severe_progress_or_highly_intensive_care,
    COALESCE(dta.tezky_upv_ecmo, 0::double precision) - COALESCE(lag(dta.tezky_upv_ecmo) OVER (PARTITION BY dta.nemocnice_kraj ORDER BY dta.datum), 0::double precision) AS new_current_severe_progress_or_highly_intensive_care,
        CASE
            WHEN dta.datum = (CURRENT_DATE - '1 day'::interval) AND dta.datum < CURRENT_DATE THEN true
            ELSE false
        END AS max_reporting_date,
        CASE
            WHEN dta.datum >= (CURRENT_DATE - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN dta.datum >= date_trunc('day'::text, (( SELECT max(covid19_hospitalized_regional.datum) AS max
               FROM uzis.covid19_hospitalized_regional)) - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM uzis.covid19_hospitalized_regional dta
     JOIN uzis.codebook_regions ck ON dta.nemocnice_kraj = ck."kraj-kod";


-- analytic.v_uzis_covid19_moving_averages source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_moving_averages
AS SELECT a.report_date,
    a.infected_count_daily AS new_positive,
    avg(a.infected_count_daily) OVER (ORDER BY a.report_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_new_positive,
    a.tested_count_daily AS tested,
    avg(a.tested_count_daily) OVER (ORDER BY a.report_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_tested,
    a.deceased_count_daily AS deceased,
    avg(a.deceased_count_daily) OVER (ORDER BY a.report_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_deceased,
    b.new_hospitalized,
    b.mov_avg_new_hospitalized,
    b.new_severe_progress,
    b.mov_avg_new_severe_progress,
    c.requests_primary_diagnosis,
    c.mov_avg_requests_primary_diagnosis,
        CASE
            WHEN a.report_date >= ((( SELECT max(v_uzis_covid19_cz.report_date) AS max
               FROM analytic.v_uzis_covid19_cz)) - '4 days'::interval) THEN true
            ELSE false
        END AS last_4_days
   FROM analytic.v_uzis_covid19_cz a
     LEFT JOIN ( SELECT v_uzis_covid19_hospitalized_daily.reporting_date,
            sum(v_uzis_covid19_hospitalized_daily.new_hospitalized) AS new_hospitalized,
            avg(sum(v_uzis_covid19_hospitalized_daily.new_hospitalized)) OVER (ORDER BY v_uzis_covid19_hospitalized_daily.reporting_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_new_hospitalized,
            sum(v_uzis_covid19_hospitalized_daily.new_severe_progress) AS new_severe_progress,
            avg(sum(v_uzis_covid19_hospitalized_daily.new_severe_progress)) OVER (ORDER BY v_uzis_covid19_hospitalized_daily.reporting_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_new_severe_progress
           FROM analytic.v_uzis_covid19_hospitalized_daily
          GROUP BY v_uzis_covid19_hospitalized_daily.reporting_date) b ON a.report_date = b.reporting_date
     LEFT JOIN ( SELECT r.datum AS reporting_date,
            sum(r.primdg) AS requests_primary_diagnosis,
            avg(sum(r.primdg)) OVER (ORDER BY r.datum ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_requests_primary_diagnosis
           FROM uzis.covid19_pcr_requests r
          GROUP BY r.datum) c ON a.report_date = c.reporting_date;


-- analytic.v_uzis_covid19_municipalities source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_municipalities
AS SELECT eo.datum,
    eo.kraj_kod,
    eo.kraj_nazev,
    eo.okres_kod,
    eo.okres_nazev,
    eo.obec_kod,
    eo.obec_nazev,
    population_regions.population AS pocet_obyvatel_kraj,
    population_districts.population AS pocet_obyvatel_okres,
    eo.kumulativni_pocet_pozitivnich_osob,
    eo.kumulativni_pocet_zemrelych,
    eo.kumulativni_pocet_vylecenych,
        CASE
            WHEN eo.kumulativni_pocet_zemrelych IS NULL THEN 0::numeric
            ELSE eo.kumulativni_pocet_zemrelych
        END -
        CASE
            WHEN lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)
        END AS pocet_zemrelych_den,
    eo.incidence AS pocet_pozitivnich_osob_den,
        CASE
            WHEN eo.prevalence IS NULL THEN 0::numeric
            ELSE eo.prevalence
        END -
        CASE
            WHEN lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)
        END AS pocet_aktivnich_pripadu_den,
    eo.prevalence AS aktualni_pocet_aktivnich_pripadu,
    eo.aktualni_pocet_hospitalizovanych_osob,
        CASE
            WHEN eo.aktualni_pocet_hospitalizovanych_osob IS NULL THEN 0::numeric::double precision
            ELSE eo.aktualni_pocet_hospitalizovanych_osob
        END -
        CASE
            WHEN lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric::double precision
            ELSE lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)
        END AS pocet_hospitalizovanych_osob,
    eo.casova_znamka AS posledni_aktualizace,
        CASE
            WHEN eo.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS vcerejsi_den,
    eo.kumulativni_pocet_hospitalizovanych_osob,
    eo.kumulativni_pocet_zemrelych + eo.kumulativni_pocet_vylecenych AS aktualni_pocet_neaktivnich_pripadu,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '7 days'::interval) THEN true
            ELSE false
        END AS last_7days,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '14 days'::interval) THEN true
            ELSE false
        END AS last_14days,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM ( SELECT covid19_municipalities.datum,
            covid19_municipalities.kraj_kod,
            covid19_municipalities.kraj_nazev,
            covid19_municipalities.okres_kod,
            covid19_municipalities.okres_nazev,
            covid19_municipalities.obec_kod,
            covid19_municipalities.obec_nazev,
            covid19_municipalities.kumulativni_pocet_pozitivnich_osob,
            covid19_municipalities.kumulativni_pocet_zemrelych,
            covid19_municipalities.kumulativni_pocet_vylecenych,
            covid19_municipalities.kumulativni_pocet_hospitalizovanych_osob,
            covid19_municipalities.casova_znamka,
            covid19_municipalities.aktualni_pocet_hospitalizovanych_osob,
            covid19_municipalities.prevalence,
            covid19_municipalities.incidence
           FROM uzis.covid19_municipalities
        UNION ALL
         SELECT covid19_prague_districts.datum,
            covid19_prague_districts.krajkod AS kraj_kod,
            covid19_prague_districts.krajnazev AS kraj_nazev,
            covid19_prague_districts.okreskod AS okres_kod,
            covid19_prague_districts.okresnazev AS okres_nazev,
            covid19_prague_districts.mckod AS obec_kod,
            covid19_prague_districts.mestskacast AS obec_nazev,
            covid19_prague_districts.kumulativni_pocet_pozitivnich_osob,
            covid19_prague_districts.kumulativni_pocet_zemrelych,
            covid19_prague_districts.kumulativni_pocet_vylecenych,
            covid19_prague_districts.kumulativni_pocet_hospitalizovanych_osob,
            covid19_prague_districts.casova_znamka,
            covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob,
            covid19_prague_districts.prevalence,
            covid19_prague_districts.incidence
           FROM uzis.covid19_prague_districts) eo
     LEFT JOIN uzis.population_regions ON eo.kraj_kod = population_regions.region_code
     LEFT JOIN uzis.population_districts ON eo.okres_kod = population_districts.district_code
  WHERE eo.datum < CURRENT_DATE;


-- analytic.v_uzis_covid19_pcr_requests source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_pcr_requests
AS SELECT cpr.datum,
    cpr.krajkod,
    cpr.kraj,
    cpr.pocet,
    cpr.samoplatcu,
    cpr.cizincu,
    cpr.primdg,
    cpr.kontrolni,
    cpr.preventivni,
    cpr.epidemilogicka,
    cpr.nemocnice,
    cpr.ostatni_luzkova,
    cpr.socialni_zarizeni,
    cpr.socialni_zarizeni1,
    cpr.praktici,
    cpr.specialiste,
    cpr.khs,
    cpr.updated_at,
    cpr.updated_by,
    cpr.created_at,
    cpr.created_by
   FROM uzis.covid19_pcr_requests cpr;


-- analytic.v_uzis_covid19_population_age_regions source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_population_age_regions
AS SELECT sub.population,
    sub.region_code,
    sub.age_group,
    concat(sub.region_code, '_', sub.age_group) AS region_age_id
   FROM ( SELECT sum(cpar.population) AS population,
            cpar.region_code,
                CASE
                    WHEN cpar.age >= 5 AND cpar.age < 12 THEN '5-11'::text
                    WHEN cpar.age >= 12 AND cpar.age < 18 THEN '12-17'::text
                    WHEN cpar.age >= 18 AND cpar.age < 25 THEN '18-24'::text
                    WHEN cpar.age >= 25 AND cpar.age < 30 THEN '25-29'::text
                    WHEN cpar.age >= 30 AND cpar.age < 35 THEN '30-34'::text
                    WHEN cpar.age >= 35 AND cpar.age < 40 THEN '35-39'::text
                    WHEN cpar.age >= 40 AND cpar.age < 45 THEN '40-44'::text
                    WHEN cpar.age >= 45 AND cpar.age < 50 THEN '45-49'::text
                    WHEN cpar.age >= 50 AND cpar.age < 55 THEN '50-54'::text
                    WHEN cpar.age >= 55 AND cpar.age < 60 THEN '55-59'::text
                    WHEN cpar.age >= 60 AND cpar.age < 65 THEN '60-64'::text
                    WHEN cpar.age >= 65 AND cpar.age < 70 THEN '65-69'::text
                    WHEN cpar.age >= 70 AND cpar.age < 75 THEN '70-74'::text
                    WHEN cpar.age >= 75 AND cpar.age < 80 THEN '75-79'::text
                    WHEN cpar.age >= 80 THEN '80+'::text
                    ELSE NULL::text
                END AS age_group
           FROM uzis.population_age_regions cpar
          GROUP BY cpar.region_code, (
                CASE
                    WHEN cpar.age >= 5 AND cpar.age < 12 THEN '5-11'::text
                    WHEN cpar.age >= 12 AND cpar.age < 18 THEN '12-17'::text
                    WHEN cpar.age >= 18 AND cpar.age < 25 THEN '18-24'::text
                    WHEN cpar.age >= 25 AND cpar.age < 30 THEN '25-29'::text
                    WHEN cpar.age >= 30 AND cpar.age < 35 THEN '30-34'::text
                    WHEN cpar.age >= 35 AND cpar.age < 40 THEN '35-39'::text
                    WHEN cpar.age >= 40 AND cpar.age < 45 THEN '40-44'::text
                    WHEN cpar.age >= 45 AND cpar.age < 50 THEN '45-49'::text
                    WHEN cpar.age >= 50 AND cpar.age < 55 THEN '50-54'::text
                    WHEN cpar.age >= 55 AND cpar.age < 60 THEN '55-59'::text
                    WHEN cpar.age >= 60 AND cpar.age < 65 THEN '60-64'::text
                    WHEN cpar.age >= 65 AND cpar.age < 70 THEN '65-69'::text
                    WHEN cpar.age >= 70 AND cpar.age < 75 THEN '70-74'::text
                    WHEN cpar.age >= 75 AND cpar.age < 80 THEN '75-79'::text
                    WHEN cpar.age >= 80 THEN '80+'::text
                    ELSE NULL::text
                END)) sub;


-- analytic.v_uzis_covid19_prague_districts source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_prague_districts
AS SELECT uzis_covid19_prague_districts.datum,
        CASE
            WHEN uzis_covid19_prague_districts.mestskacast::text = 'Praha'::text THEN 'Nezařazeno'::character varying
            ELSE uzis_covid19_prague_districts.mestskacast
        END::character varying(50) AS mestskacast,
    uzis_covid19_prague_districts.kumulativni_pocet_pozitivnich_osob,
    uzis_covid19_prague_districts.kumulativni_pocet_zemrelych,
    uzis_covid19_prague_districts.kumulativni_pocet_vylecenych,
        CASE
            WHEN uzis_covid19_prague_districts.kumulativni_pocet_zemrelych IS NULL THEN 0::numeric
            ELSE uzis_covid19_prague_districts.kumulativni_pocet_zemrelych
        END -
        CASE
            WHEN lag(uzis_covid19_prague_districts.kumulativni_pocet_zemrelych) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum) IS NULL THEN 0::numeric
            ELSE lag(uzis_covid19_prague_districts.kumulativni_pocet_zemrelych) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum)
        END AS pocet_zemrelych_den,
    uzis_covid19_prague_districts.incidence AS pocet_pozitivnich_osob_den,
        CASE
            WHEN uzis_covid19_prague_districts.prevalence IS NULL THEN 0::numeric
            ELSE uzis_covid19_prague_districts.prevalence
        END -
        CASE
            WHEN lag(uzis_covid19_prague_districts.prevalence) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum) IS NULL THEN 0::numeric
            ELSE lag(uzis_covid19_prague_districts.prevalence) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum)
        END AS pocet_aktivnich_pripadu_den,
    uzis_covid19_prague_districts.prevalence AS aktualni_pocet_aktivnich_pripadu,
    uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob,
        CASE
            WHEN uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob IS NULL THEN 0::numeric
            ELSE uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob
        END -
        CASE
            WHEN lag(uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum) IS NULL THEN 0::numeric
            ELSE lag(uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum)
        END AS pocet_hospitalizovanych_osob,
    round(uzis_covid19_prague_districts.podil_65__na_kumulativnim_poctu_pozitivnich * uzis_covid19_prague_districts.kumulativni_pocet_pozitivnich_osob::double precision) AS pocet_pozitivnich_65,
    round(uzis_covid19_prague_districts.podil_65__na_kumulativnim_poctu_hospitalizovanych * uzis_covid19_prague_districts.kumulativni_pocet_hospitalizovanych_osob::double precision) AS pocet_hospitalizovanych_65,
    round(uzis_covid19_prague_districts.podil_65__na_kumulativnim_poctu_zemrelych * uzis_covid19_prague_districts.kumulativni_pocet_zemrelych::double precision) AS pocet_zemrelych_65,
    uzis_covid19_prague_districts.casova_znamka AS posledni_aktualizace,
        CASE
            WHEN uzis_covid19_prague_districts.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS vcerejsi_den,
    uzis_covid19_prague_districts.kumulativni_pocet_hospitalizovanych_osob,
        CASE
            WHEN uzis_covid19_prague_districts.mestskacast::text = 'Praha'::text THEN '100'::text
            ELSE "right"(uzis_covid19_prague_districts.mestskacast::text, length(uzis_covid19_prague_districts.mestskacast::text) - "position"(uzis_covid19_prague_districts.mestskacast::text, ' '::text))
        END AS mestskacast_order,
    dp.population,
    uzis_covid19_prague_districts.kumulativni_pocet_zemrelych + uzis_covid19_prague_districts.kumulativni_pocet_vylecenych AS aktualni_pocet_neaktivnich_pripadu,
        CASE
            WHEN uzis_covid19_prague_districts.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN true
            ELSE false
        END AS last_7days,
        CASE
            WHEN uzis_covid19_prague_districts.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN true
            ELSE false
        END AS last_14days,
        CASE
            WHEN uzis_covid19_prague_districts.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN uzis_covid19_prague_districts.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM ( SELECT covid19_prague_districts.casova_znamka,
            covid19_prague_districts.datum,
            covid19_prague_districts.mckod,
            covid19_prague_districts.mestskacast,
            covid19_prague_districts.psc,
            covid19_prague_districts.opoukod,
            covid19_prague_districts.opounazev,
            covid19_prague_districts.orpkod,
            covid19_prague_districts.orpnazev,
            covid19_prague_districts.okreskod,
            covid19_prague_districts.okresnazev,
            covid19_prague_districts.krajkod,
            covid19_prague_districts.krajnazev,
            covid19_prague_districts.kumulativni_pocet_pozitivnich_osob,
            covid19_prague_districts.kumulativni_pocet_hospitalizovanych_osob,
            covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob,
            covid19_prague_districts.zemreli_za_hospitalizace,
            covid19_prague_districts.incidence,
            covid19_prague_districts.kumulativni_pocet_vylecenych,
            covid19_prague_districts.kumulativni_pocet_zemrelych,
            covid19_prague_districts.prevalence,
            covid19_prague_districts.podil_65__na_kumulativnim_poctu_pozitivnich,
            covid19_prague_districts.podil_65__na_kumulativnim_poctu_hospitalizovanych,
            covid19_prague_districts.podil_65__na_aktualnim_poctu_hospitalizovanych,
            covid19_prague_districts.podil_65__na_incidenci,
            covid19_prague_districts.podil_65__na_kumulativnim_poctu_vylecenych,
            covid19_prague_districts.podil_65__na_kumulativnim_poctu_zemrelych,
            covid19_prague_districts.podil_65__na_prevalenci,
            row_number() OVER (PARTITION BY covid19_prague_districts.mckod, covid19_prague_districts.datum) AS rank
           FROM uzis.covid19_prague_districts) uzis_covid19_prague_districts
     LEFT JOIN uzis.population_prague_districts dp ON uzis_covid19_prague_districts.mestskacast::text = dp.district::text
  WHERE uzis_covid19_prague_districts.datum < date_trunc('day'::text, now()) AND uzis_covid19_prague_districts.rank = 1;


-- analytic.v_uzis_covid19_prague_hospitalized_positive source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_prague_hospitalized_positive
AS SELECT a.datum,
    sum(b.kumulativni_pocet_pozitivnich_osob) AS kumulativni_pocet_pozitivnich_osob,
    sum(a.kumulativni_pocet_hospitalizovanych_osob) AS kumulativni_pocet_hospitalizovanych_osob,
    sum(b.pocet_pozitivnich_osob_den) AS pocet_pozitivnich_osob_den,
    sum(a.pocet_hospitalizovanych_osob) AS pocet_hospitalizovanych_osob
   FROM analytic.v_uzis_covid19_prague_districts a
     JOIN analytic.v_uzis_covid19_prague_districts b ON b.datum = (a.datum - '10 days'::interval) AND a.mestskacast::text = b.mestskacast::text
  GROUP BY a.datum;


-- analytic.v_uzis_covid19_regions source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_regions
AS SELECT ek.datum AS "Datum",
    ek.kraj AS kraj_kod,
    codebook_regions."kraj-nazev" AS kraj_nazev,
    population_regions.population AS pocet_obyvatel,
    ek.kumulativni_pocet_pozitivnich_osob,
    ek.kumulativni_pocet_zemrelych,
    ek.kumulativni_pocet_vylecenych,
        CASE
            WHEN ek.kumulativni_pocet_zemrelych IS NULL THEN 0::numeric
            ELSE ek.kumulativni_pocet_zemrelych::numeric
        END -
        CASE
            WHEN lag(ek.kumulativni_pocet_zemrelych) OVER (PARTITION BY ek.kraj ORDER BY ek.datum) IS NULL THEN 0::numeric
            ELSE lag(ek.kumulativni_pocet_zemrelych) OVER (PARTITION BY ek.kraj ORDER BY ek.datum)::numeric
        END AS pocet_zemrelych_den,
    ek.incidence AS pocet_pozitivnich_osob_den,
        CASE
            WHEN ek.prevalence IS NULL THEN 0::numeric
            ELSE ek.prevalence::numeric
        END -
        CASE
            WHEN lag(ek.prevalence) OVER (PARTITION BY ek.kraj ORDER BY ek.datum) IS NULL THEN 0::numeric
            ELSE lag(ek.prevalence) OVER (PARTITION BY ek.kraj ORDER BY ek.datum)::numeric
        END AS pocet_aktivnich_pripadu_den,
    ek.prevalence AS aktualni_pocet_aktivnich_pripadu,
    ek.aktualni_pocet_hospitalizovanych_osob,
        CASE
            WHEN ek.aktualni_pocet_hospitalizovanych_osob IS NULL THEN 0::numeric::double precision
            ELSE ek.aktualni_pocet_hospitalizovanych_osob
        END -
        CASE
            WHEN lag(ek.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY ek.kraj ORDER BY ek.datum) IS NULL THEN 0::numeric::double precision
            ELSE lag(ek.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY ek.kraj ORDER BY ek.datum)
        END AS pocet_hospitalizovanych_osob,
    ek.casova_znamka AS posledni_aktualizace,
        CASE
            WHEN ek.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS vcerejsi_den,
    ek.kumulativni_pocet_hospitalizovanych_osob,
    ek.kumulativni_pocet_zemrelych + ek.kumulativni_pocet_vylecenych AS aktualni_pocet_neaktivnich_pripadu,
    round(ek.podil_65__na_kumulativnim_poctu_pozitivnich * ek.kumulativni_pocet_pozitivnich_osob::double precision) AS pocet_pozitivnich_65,
    round(ek.podil_65__na_kumulativnim_poctu_hospitalizovanych * ek.kumulativni_pocet_hospitalizovanych_osob) AS pocet_hospitalizovanych_65,
    round(ek.podil_65__na_kumulativnim_poctu_zemrelych * ek.kumulativni_pocet_zemrelych::double precision) AS pocet_zemrelych_65,
        CASE
            WHEN ek.datum >= (CURRENT_DATE - '7 days'::interval) THEN true
            ELSE false
        END AS last_7days,
        CASE
            WHEN ek.datum >= (CURRENT_DATE - '14 days'::interval) THEN true
            ELSE false
        END AS last_14days,
        CASE
            WHEN ek.datum >= (CURRENT_DATE - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN ek.datum >= (CURRENT_DATE - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM uzis.covid19_regions ek
     LEFT JOIN uzis.codebook_regions ON ek.kraj = codebook_regions."kraj-kod"
     LEFT JOIN uzis.population_regions ON ek.kraj = population_regions.region_code
  WHERE ek.datum < CURRENT_DATE;


-- analytic.v_uzis_covid19_regions_positive_7_14_days source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_regions_positive_7_14_days
AS SELECT v_uzis_covid19_regions."Datum",
    v_uzis_covid19_regions.kraj_kod,
    v_uzis_covid19_regions.kraj_nazev,
    v_uzis_covid19_regions.pocet_obyvatel,
    v_uzis_covid19_regions.pocet_pozitivnich_osob_den,
    avg(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) OVER (PARTITION BY v_uzis_covid19_regions.kraj_nazev ORDER BY v_uzis_covid19_regions."Datum" ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS avg_pozitivni_7_dnu,
    sum(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) OVER (PARTITION BY v_uzis_covid19_regions.kraj_nazev ORDER BY v_uzis_covid19_regions."Datum" ROWS BETWEEN 13 PRECEDING AND CURRENT ROW) AS sum_pozitivni_14_dnu,
    avg(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) OVER (PARTITION BY v_uzis_covid19_regions.kraj_nazev ORDER BY v_uzis_covid19_regions."Datum" ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) / (v_uzis_covid19_regions.pocet_obyvatel / 100000)::numeric AS avg_pozitivni_7_dnu_na_100tis,
    sum(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) OVER (PARTITION BY v_uzis_covid19_regions.kraj_nazev ORDER BY v_uzis_covid19_regions."Datum" ROWS BETWEEN 13 PRECEDING AND CURRENT ROW) / (v_uzis_covid19_regions.pocet_obyvatel / 100000)::numeric AS sum_pozitivni_14_dnu_na_100tis
   FROM analytic.v_uzis_covid19_regions
UNION ALL
 SELECT sub."Datum",
    'CZ00'::text AS kraj_kod,
    'Celá ČR'::text AS kraj_nazev,
    sub.pocet_obyvatel,
    sub.pocet_pozitivnich_osob_den,
    avg(sub.pocet_pozitivnich_osob_den) OVER (ORDER BY sub."Datum" ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS avg_pozitivni_7_dnu,
    sum(sub.pocet_pozitivnich_osob_den) OVER (ORDER BY sub."Datum" ROWS BETWEEN 13 PRECEDING AND CURRENT ROW) AS sum_pozitivni_14_dnu,
    avg(sub.pocet_pozitivnich_osob_den) OVER (ORDER BY sub."Datum" ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) / (sub.pocet_obyvatel / 100000::numeric) AS avg_pozitivni_7_dnu_na_100tis,
    sum(sub.pocet_pozitivnich_osob_den) OVER (ORDER BY sub."Datum" ROWS BETWEEN 13 PRECEDING AND CURRENT ROW) / (sub.pocet_obyvatel / 100000::numeric) AS sum_pozitivni_14_dnu_na_100tis
   FROM ( SELECT v_uzis_covid19_regions."Datum",
            'CZ00'::text AS kraj_kod,
            'Celá ČR'::text AS kraj_nazev,
            sum(v_uzis_covid19_regions.pocet_obyvatel) AS pocet_obyvatel,
            sum(v_uzis_covid19_regions.pocet_pozitivnich_osob_den) AS pocet_pozitivnich_osob_den
           FROM analytic.v_uzis_covid19_regions
          GROUP BY v_uzis_covid19_regions."Datum") sub;


-- analytic.v_uzis_covid19_tests_districts source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_tests_districts
AS SELECT eo.datum,
    eo.kraj_kod,
    eo.kraj_nazev,
    eo.okres_kod,
    eo.okres_nazev,
    eo.pocet_pozitivnich_osob_den,
    t.prirustkovy_pocet_testu_okres,
    t.prirustkovy_pocet_prvnich_testu_okres,
    t.kumulativni_pocet_testu_okres,
    t.kumulativni_pocet_prvnich_testu_okres,
    t.prirustkovy_pocet_testu_okres - t.prirustkovy_pocet_prvnich_testu_okres AS prirustkovy_pocet_naslednych_testu
   FROM ( SELECT eo_1.datum,
            eo_1.kraj_kod,
            eo_1.kraj_nazev,
            eo_1.okres_kod,
            eo_1.okres_nazev,
            sum(eo_1.incidence) AS pocet_pozitivnich_osob_den
           FROM uzis.covid19_municipalities eo_1
          WHERE eo_1.datum < CURRENT_DATE
          GROUP BY eo_1.datum, eo_1.kraj_kod, eo_1.kraj_nazev, eo_1.okres_kod, eo_1.okres_nazev
        UNION ALL
         SELECT covid19_prague_districts.datum,
            covid19_prague_districts.krajkod AS kraj_kod,
            covid19_prague_districts.krajnazev AS kraj_nazev,
            'CZ0100'::text AS okres_kod,
            covid19_prague_districts.okresnazev AS okres_nazev,
            sum(covid19_prague_districts.incidence) AS pocet_pozitivnich_osob_den
           FROM uzis.covid19_prague_districts
          WHERE covid19_prague_districts.datum < CURRENT_DATE
          GROUP BY covid19_prague_districts.datum, covid19_prague_districts.krajkod, covid19_prague_districts.krajnazev, covid19_prague_districts.okresnazev) eo
     JOIN uzis.covid19_cz_tests_regional t ON eo.datum::date = t.datum::date AND eo.kraj_kod = t.kraj_nuts_kod AND eo.okres_kod = t.okres_lau_kod;

-- analytic.v_uzis_covid19_vaccination_logistics source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_logistics
AS SELECT s.datum,
    s.updated_at,
        CASE
            WHEN s.datum >= (CURRENT_DATE - '1 day'::interval) AND s.datum < CURRENT_DATE THEN true
            ELSE false
        END AS vcerejsi_den,
    s.ockovaci_misto_id,
    s.ockovaci_misto_nazev,
    s.kraj_nazev,
    s.ockovaci_latka,
    s.prijem,
    s.prijato_od_jinych_ocm,
    s.prijem + s.prijato_od_jinych_ocm AS prijem_celkem,
    s.vydej,
    s.prijem + s.prijato_od_jinych_ocm - s.vydej AS prijem_celkem_minus_vydej,
    s.pouzite_davky,
    s.znehodnocene_davky,
    sum(s.pouzite_davky) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS pouzito_kumulativni,
    sum(s.znehodnocene_davky) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS znehodnoceno_kumulativni,
    sum(s.prijem) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS prijem_kumulativni,
    sum(s.prijato_od_jinych_ocm) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS prijato_od_jinych_ocm_kumulativni,
    sum(s.prijem) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) + sum(s.prijato_od_jinych_ocm) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS prijato_celkem_kumulativni,
    sum(s.vydej) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS vydej_kumulativni,
    sum(s.prijem) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) + sum(s.prijato_od_jinych_ocm) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) - sum(s.vydej) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) - sum(s.pouzite_davky) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) - sum(s.znehodnocene_davky) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS davek_k_dispozici
   FROM ( SELECT p.datum,
            p.updated_at,
            p.ockovaci_misto_id,
            p.ockovaci_misto_nazev,
            p.kraj_nazev,
                CASE
                    WHEN p.ockovaci_latka = 'Comirnaty'::text THEN 'Pfizer'::text
                    WHEN p.ockovaci_latka = 'VAXZEVRIA'::text THEN 'VAXZEVRIA/AstraZeneca'::text
                    WHEN p.ockovaci_latka = 'COVID-19 Vaccine Moderna'::text THEN 'Moderna'::text
                    ELSE p.ockovaci_latka
                END AS ockovaci_latka,
            COALESCE(u.pouzite_davky, 0::bigint) AS pouzite_davky,
            COALESCE(u.znehodnocene_davky, 0::bigint) AS znehodnocene_davky,
            COALESCE(d_prijato.prijato_od_jinych_ocm, 0::numeric) AS prijato_od_jinych_ocm,
            COALESCE(sum(d.pocet_davek) FILTER (WHERE d.akce = 'Příjem'::text), 0::numeric) AS prijem,
            COALESCE(sum(d.pocet_davek) FILTER (WHERE d.akce = 'Výdej'::text), 0::numeric) AS vydej
           FROM ( SELECT p_1.ockovaci_misto_id,
                    p_1.ockovaci_misto_nazev,
                    p_1.updated_at,
                    ck."kraj-nazev" AS kraj_nazev,
                    generate_series('2020-12-26 00:00:00'::timestamp without time zone, CURRENT_DATE::timestamp without time zone, '1 day'::interval) AS datum,
                    u_1.ockovaci_latka
                   FROM uzis.covid19_vaccination_points p_1
                     LEFT JOIN uzis.codebook_regions ck ON "left"(p_1.okres_nuts_kod, 5) = ck."kraj-kod"
                     CROSS JOIN unnest(ARRAY['Comirnaty'::text, 'VAXZEVRIA'::text, 'COVID-19 Vaccine Moderna'::text, 'COVID-19 Vaccine Janssen'::text]) WITH ORDINALITY u_1(ockovaci_latka, ordinality)) p
             LEFT JOIN uzis.covid19_vaccination_distribution d ON d.datum = p.datum AND d.ockovaci_misto_id = p.ockovaci_misto_id AND d.ockovaci_latka = p.ockovaci_latka
             LEFT JOIN ( SELECT covid19_vaccination_distribution.datum,
                    covid19_vaccination_distribution.cilove_ockovaci_misto_id,
                    covid19_vaccination_distribution.ockovaci_latka,
                    sum(covid19_vaccination_distribution.pocet_davek) AS prijato_od_jinych_ocm
                   FROM uzis.covid19_vaccination_distribution
                  WHERE covid19_vaccination_distribution.akce = 'Výdej'::text
                  GROUP BY covid19_vaccination_distribution.datum, covid19_vaccination_distribution.cilove_ockovaci_misto_id, covid19_vaccination_distribution.ockovaci_latka) d_prijato ON d_prijato.datum = p.datum AND d_prijato.cilove_ockovaci_misto_id = p.ockovaci_misto_id AND d_prijato.ockovaci_latka = p.ockovaci_latka
             LEFT JOIN uzis.history_covid19_vaccination_usage u ON p.datum = u.datum AND p.ockovaci_misto_id = u.ockovaci_misto_id AND p.ockovaci_latka = u.ockovaci_latka
          WHERE p.datum < CURRENT_DATE
          GROUP BY p.datum, p.updated_at, p.ockovaci_misto_id, p.ockovaci_misto_nazev, p.kraj_nazev, p.ockovaci_latka, u.pouzite_davky, u.znehodnocene_davky, d_prijato.prijato_od_jinych_ocm) s
  ORDER BY s.kraj_nazev, s.ockovaci_misto_nazev, s.datum, s.ockovaci_latka;

-- analytic.v_uzis_covid19_vaccination_prague_districts source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_prague_districts
AS SELECT v.mestska_cast,
    p.spravni_obvod,
        CASE
            WHEN v.vekova_skupina = '0-17'::text THEN '12-17'::text
            ELSE v.vekova_skupina
        END AS vekova_skupina,
    p.populace,
    count(*) FILTER (WHERE v.poradi_davky = 1) AS prvni_davky,
    count(*) FILTER (WHERE v.poradi_davky = 2) AS druhe_davky,
    count(*) FILTER (WHERE v.poradi_davky = 1)::double precision / p.populace::double precision AS prvni_davky_podil,
    count(*) FILTER (WHERE v.poradi_davky = 2)::double precision / p.populace::double precision AS druhe_davky_podil
   FROM uzis.history_covid19_vaccination_prague_details v
     LEFT JOIN ( SELECT population_prague_age_districts.mestska_cast,
            population_prague_age_districts.spravni_obvod,
                CASE
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['10-14'::text, '15-19'::text]) THEN '12-17'::text
                    WHEN population_prague_age_districts.vekova_skupina = '20-24'::text THEN '18-24'::text
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['80-84'::text, '85'::text]) THEN '80+'::text
                    ELSE population_prague_age_districts.vekova_skupina
                END AS vekova_skupina,
            sum(population_prague_age_districts.populace) AS populace
           FROM uzis.population_prague_age_districts
          GROUP BY population_prague_age_districts.mestska_cast, population_prague_age_districts.spravni_obvod, (
                CASE
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['10-14'::text, '15-19'::text]) THEN '12-17'::text
                    WHEN population_prague_age_districts.vekova_skupina = '20-24'::text THEN '18-24'::text
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['80-84'::text, '85'::text]) THEN '80+'::text
                    ELSE population_prague_age_districts.vekova_skupina
                END)) p ON v.mestska_cast = p.mestska_cast AND
        CASE
            WHEN v.vekova_skupina = '0-17'::text THEN '12-17'::text
            ELSE v.vekova_skupina
        END = p.vekova_skupina
  GROUP BY v.mestska_cast, p.spravni_obvod, v.vekova_skupina, p.populace;


-- analytic.v_uzis_covid19_vaccination_regions_details source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_regions_details
AS SELECT count(*) AS pocet_davek,
    v.datum_vakcinace,
        CASE
            WHEN v.datum_vakcinace >= (CURRENT_DATE - '7 days'::interval) AND v.datum_vakcinace < CURRENT_DATE THEN 'posledních 7 dnů'::text
            WHEN v.datum_vakcinace >= (CURRENT_DATE - '14 days'::interval) AND v.datum_vakcinace < (CURRENT_DATE - '7 days'::interval) THEN 'predchozích 7 dnů'::text
            ELSE NULL::text
        END AS "7_14_dnu",
    date_trunc('week'::text, v.datum_vakcinace)::date AS tyden,
        CASE
            WHEN date_trunc('week'::text, v.datum_vakcinace)::date = date_trunc('week'::text, CURRENT_DATE::timestamp with time zone)::date THEN 'posledni nedokonceny tyden'::text
            ELSE 'cely tyden'::text
        END AS tyden_filter_posledni,
    v.updated_at AS datum_aktualizace,
    v.vakcina,
        CASE
            WHEN v.vekova_skupina = '0-11'::text THEN '5-11'::text
            WHEN v.vekova_skupina = ANY (ARRAY['12-15'::text, '16-17'::text]) THEN '12-17'::text
            ELSE v.vekova_skupina
        END AS vekova_skupina,
        CASE
            WHEN v.vekova_skupina = '0-11'::text THEN 0
            WHEN v.vekova_skupina = ANY (ARRAY['12-15'::text, '16-17'::text]) THEN 1
            WHEN v.vekova_skupina = '18-24'::text THEN 2
            WHEN v.vekova_skupina = '25-29'::text THEN 3
            WHEN v.vekova_skupina = '30-34'::text THEN 4
            WHEN v.vekova_skupina = '35-39'::text THEN 5
            WHEN v.vekova_skupina = '40-44'::text THEN 6
            WHEN v.vekova_skupina = '45-49'::text THEN 7
            WHEN v.vekova_skupina = '50-54'::text THEN 8
            WHEN v.vekova_skupina = '55-59'::text THEN 9
            WHEN v.vekova_skupina = '60-64'::text THEN 10
            WHEN v.vekova_skupina = '65-69'::text THEN 11
            WHEN v.vekova_skupina = '70-74'::text THEN 12
            WHEN v.vekova_skupina = '75-79'::text THEN 13
            WHEN v.vekova_skupina = '80+'::text THEN 14
            WHEN v.vekova_skupina = 'Neznámá'::text THEN 15
            ELSE 15
        END AS vekova_skupina_order,
        CASE
            WHEN v.indikace_zdravotnik = 1 THEN 'Zdravotníci'::text
            WHEN v.indikace_socialni_sluzby = 1 THEN 'Socilání služby'::text
            WHEN v.indikace_pedagog = 1 THEN 'Pedagogové'::text
            WHEN v.indikace_skolstvi_ostatni = 1 THEN 'Školství ostatní'::text
            WHEN v.chronicke_onemocneni = 1 THEN 'Chronicky nemocní'::text
            WHEN v.bezpecnostni_infrastruktura = 1 THEN 'Bezpečnostní infrastruktura'::text
            ELSE NULL::text
        END AS indikacni_skupina,
    v.kraj_nazev AS kraj_ockovani,
        CASE
            WHEN c.kraj_nazev IS NULL THEN 'Neznámý'::text
            ELSE c.kraj_nazev
        END AS kraj_bydliste,
    v.pohlavi,
    v.poradi_davky,
    v.zarizeni_nazev,
    concat(v.kraj_kod, '_',
        CASE
            WHEN v.vekova_skupina = '0-11'::text THEN '5-11'::text
            WHEN v.vekova_skupina = ANY (ARRAY['12-15'::text, '16-17'::text]) THEN '12-17'::text
            ELSE v.vekova_skupina
        END) AS kraj_ockovani_vek_id,
    concat(c.kraj_kod, '_',
        CASE
            WHEN v.vekova_skupina = '0-11'::text THEN '5-11'::text
            WHEN v.vekova_skupina = ANY (ARRAY['12-15'::text, '16-17'::text]) THEN '12-17'::text
            ELSE v.vekova_skupina
        END) AS kraj_bydliste_vek_id
   FROM uzis.covid19_vaccination_regional_details v
     LEFT JOIN uzis.codebook_regions_orps c ON v.orp_bydliste = c.orp_nazev
  GROUP BY v.datum_vakcinace, v.updated_at, v.vakcina, v.vekova_skupina, v.kraj_nazev, c.kraj_nazev, v.pohlavi, v.poradi_davky, v.zarizeni_nazev, v.zrizovatel, v.kraj_kod, c.kraj_kod, (
        CASE
            WHEN v.indikace_zdravotnik = 1 THEN 'Zdravotníci'::text
            WHEN v.indikace_socialni_sluzby = 1 THEN 'Socilání služby'::text
            WHEN v.indikace_pedagog = 1 THEN 'Pedagogové'::text
            WHEN v.indikace_skolstvi_ostatni = 1 THEN 'Školství ostatní'::text
            WHEN v.chronicke_onemocneni = 1 THEN 'Chronicky nemocní'::text
            WHEN v.bezpecnostni_infrastruktura = 1 THEN 'Bezpečnostní infrastruktura'::text
            ELSE NULL::text
        END);
		
-- analytic.v_uzis_covid19_vaccinated_split source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccinated_split
AS WITH ockovani AS (
         SELECT v_uzis_covid19_vaccination_regions_details.datum_vakcinace,
            v_uzis_covid19_vaccination_regions_details.kraj_bydliste,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek) FILTER (WHERE v_uzis_covid19_vaccination_regions_details.vekova_skupina = ANY (ARRAY['65-69'::text, '70-74'::text, '75-79'::text, '80+'::text]))) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani65,
            sum(sum(
                CASE
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '12-17'::text THEN 15
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '18-24'::text THEN 21
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '25-29'::text THEN 27
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '30-34'::text THEN 32
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '35-39'::text THEN 37
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '40-44'::text THEN 42
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '45-49'::text THEN 47
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '50-54'::text THEN 52
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '55-59'::text THEN 57
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '60-64'::text THEN 62
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '65-69'::text THEN 67
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '70-74'::text THEN 72
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '75-79'::text THEN 77
                    WHEN v_uzis_covid19_vaccination_regions_details.vekova_skupina = '80+'::text THEN 85
                    ELSE NULL::integer
                END * v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) / sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS vek_prumer
           FROM analytic.v_uzis_covid19_vaccination_regions_details
          WHERE v_uzis_covid19_vaccination_regions_details.poradi_davky = 2
          GROUP BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace, v_uzis_covid19_vaccination_regions_details.kraj_bydliste
        )
 SELECT i.datum,
    date_trunc('week'::text, i.datum) AS tyden,
    date_trunc('month'::text, i.datum) AS mesic,
        CASE
            WHEN i.datum = (( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) THEN 'poslední den'::text
            ELSE NULL::text
        END AS last_day,
        CASE
            WHEN i.datum > ((( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) - '7 days'::interval) THEN 'posledních 7 dnů'::text
            ELSE NULL::text
        END AS last_7_days,
        CASE
            WHEN i.datum > ((( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) - '14 days'::interval) THEN 'posledních 14 dnů'::text
            ELSE NULL::text
        END AS last_14_days,
        CASE
            WHEN i.datum < date_trunc('week'::text, CURRENT_DATE::timestamp with time zone) THEN false
            ELSE true
        END AS unfinished_calendar_week,
    i.krajkod,
    cr."kraj-nazev" AS kraj_nazev,
    pr.population,
    par.population65,
    ockovani.pocet_uzavrenych_ockovani,
    ockovani.pocet_uzavrenych_ockovani65,
    ockovani.vek_prumer,
    i.poz_bez_ockovani + i.poz_po_prvni AS incidence_neockovani,
    (100000 * (i.poz_bez_ockovani + i.poz_po_prvni))::numeric / (pr.population::numeric - ockovani.pocet_uzavrenych_ockovani) AS incidence_neockovani_100tis,
    i.poz_po_ukonceni + i.poz_po_ukonceni_s_posilujici AS incidence_ockovani,
    (100000 * (i.poz_po_ukonceni + i.poz_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani AS incidence_ockovani_100tis,
    i.poz_vek_bez_ockovani_vek AS incidence_prumerny_vek_neockovani,
    i.poz_vek_po_ukonceni AS incidence_prumerny_vek_ockovani,
    i65.p65_bez_ockovani + i65.p65_po_prvni AS incidence65_neockovani,
    (100000 * (i65.p65_bez_ockovani + i65.p65_po_prvni))::numeric / (par.population65 - ockovani.pocet_uzavrenych_ockovani65) AS incidence65_neockovani_100tis,
    i65.p65_po_ukonceni + i65.p65_po_ukonceni_s_posilujici AS incidence65_ockovani,
    (100000 * (i65.p65_po_ukonceni + i65.p65_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani65 AS incidence65_ockovani_100tis,
    i65.p65_vek_bez_ockovani_vek AS incidence65_prumerny_vek_neockovani,
    i65.p65_vek_po_ukonceni AS incidence65_prumerny_vek_ockovani,
    h.hos_bez_ockovani + h.hos_po_prvni AS hospitalizace_neockovani,
    (100000 * (h.hos_bez_ockovani + h.hos_po_prvni))::numeric / (pr.population::numeric - ockovani.pocet_uzavrenych_ockovani) AS hospitalizace_neockovani_100tis,
    h.hos_po_ukonceni + h.hos_po_ukonceni_s_posilujici AS hospitalizace_ockovani,
    (100000 * (h.hos_po_ukonceni + h.hos_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani AS hospitalizace_ockovani_100tis,
    h.hos_vek_bez_ockovani_vek AS hospitalizace_prumerny_vek_neockovani,
    h.hos_vek_po_ukonceni1 AS hospitalizace_prumerny_vek_ockovani,
    icu.jip_bez_ockovani + icu.jip_po_prvni AS jip_neockovani,
    (100000 * (icu.jip_bez_ockovani + icu.jip_po_prvni))::numeric / (pr.population::numeric - ockovani.pocet_uzavrenych_ockovani) AS jip_neockovani_100tis,
    icu.jip_po_ukonceni + icu.jip_po_ukonceni_s_posilujici AS jip_ockovani,
    (100000 * (icu.jip_po_ukonceni + icu.jip_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani AS jip_ockovani_100tis,
    icu.jip_vek_bez_ockovani_vek AS jip_prumerny_vek_neockovani,
    icu.jip_vek_po_ukonceni AS jip_prumerny_vek_ockovani,
    d.zem_bez_ockovani + d.zem_po_prvni AS zemreli_neockovani,
    (100000 * (d.zem_bez_ockovani + d.zem_po_prvni))::numeric / (pr.population::numeric - ockovani.pocet_uzavrenych_ockovani) AS zemreli_neockovani_100tis,
    d.zem_po_ukonceni + d.zem_po_ukonceni_s_posilujici AS zemreli_ockovani,
    (100000 * (d.zem_po_ukonceni + d.zem_po_ukonceni_s_posilujici))::numeric / ockovani.pocet_uzavrenych_ockovani AS zemreli_ockovani_100tis,
    d.zem_vek_bez_ockovani_vek AS zemreli_prumerny_vek_neockovani,
    d.zem_vek_po_ukonceni AS zemreli_prumerny_vek_ockovani
   FROM uzis.covid19_vaccinated_split_incidence i
     JOIN uzis.covid19_vaccinated_split_incidence65 i65 ON i.datum = i65.datum AND i.krajkod = i65.krajkod
     JOIN uzis.covid19_vaccinated_split_hospitalized h ON i.datum = h.datum AND i.krajkod = h.krajkod
     JOIN uzis.covid19_vaccinated_split_icu icu ON i.datum = icu.datum AND i.krajkod = icu.krajkod
     JOIN uzis.covid19_vaccinated_split_deaths d ON i.datum = d.datum AND i.krajkod = d.krajkod
     LEFT JOIN uzis.population_regions pr ON i.krajkod = pr.region_code
     LEFT JOIN ( SELECT sum(population_age_regions.population) AS population65,
            population_age_regions.region_code
           FROM uzis.population_age_regions
          WHERE population_age_regions.age >= 65
          GROUP BY population_age_regions.region_code) par ON i.krajkod = par.region_code
     LEFT JOIN ockovani ON ockovani.datum_vakcinace = i.datum AND ockovani.kraj_bydliste = pr.region_name
     LEFT JOIN uzis.codebook_regions cr ON i.krajkod = cr."kraj-kod"
  WHERE i.datum >= '2021-02-01 00:00:00'::timestamp without time zone;
		

-- analytic.v_uzis_covid19_vaccinated_split_booster source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccinated_split_booster
AS WITH ockovani_druha AS (
         SELECT v_uzis_covid19_vaccination_regions_details.datum_vakcinace,
            v_uzis_covid19_vaccination_regions_details.kraj_bydliste,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek) FILTER (WHERE v_uzis_covid19_vaccination_regions_details.vekova_skupina = ANY (ARRAY['65-69'::text, '70-74'::text, '75-79'::text, '80+'::text]))) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani65
           FROM analytic.v_uzis_covid19_vaccination_regions_details
          WHERE v_uzis_covid19_vaccination_regions_details.poradi_davky = 2
          GROUP BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace, v_uzis_covid19_vaccination_regions_details.kraj_bydliste
        ), ockovani_treti AS (
         SELECT v_uzis_covid19_vaccination_regions_details.datum_vakcinace,
            v_uzis_covid19_vaccination_regions_details.kraj_bydliste,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek)) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani,
            sum(sum(v_uzis_covid19_vaccination_regions_details.pocet_davek) FILTER (WHERE v_uzis_covid19_vaccination_regions_details.vekova_skupina = ANY (ARRAY['65-69'::text, '70-74'::text, '75-79'::text, '80+'::text]))) OVER (PARTITION BY v_uzis_covid19_vaccination_regions_details.kraj_bydliste ORDER BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace) AS pocet_uzavrenych_ockovani65
           FROM analytic.v_uzis_covid19_vaccination_regions_details
          WHERE v_uzis_covid19_vaccination_regions_details.poradi_davky = 3
          GROUP BY v_uzis_covid19_vaccination_regions_details.datum_vakcinace, v_uzis_covid19_vaccination_regions_details.kraj_bydliste
        )
 SELECT i.datum,
    date_trunc('week'::text, i.datum) AS tyden,
    date_trunc('month'::text, i.datum) AS mesic,
        CASE
            WHEN i.datum = (( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) THEN 'poslední den'::text
            ELSE NULL::text
        END AS last_day,
        CASE
            WHEN i.datum > ((( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) - '7 days'::interval) THEN 'posledních 7 dnů'::text
            ELSE NULL::text
        END AS last_7_days,
        CASE
            WHEN i.datum > ((( SELECT max(covid19_vaccinated_split_incidence.datum) AS max
               FROM uzis.covid19_vaccinated_split_incidence)) - '14 days'::interval) THEN 'posledních 14 dnů'::text
            ELSE NULL::text
        END AS last_14_days,
        CASE
            WHEN i.datum < date_trunc('week'::text, CURRENT_DATE::timestamp with time zone) THEN false
            ELSE true
        END AS unfinished_calendar_week,
    i.krajkod,
    cr."kraj-nazev" AS kraj_nazev,
    pr.population,
    par.population65,
    ockovani_druha.pocet_uzavrenych_ockovani AS pocet_uzavrenych_ockovani_druha_davka,
    ockovani_treti.pocet_uzavrenych_ockovani AS pocet_uzavrenych_ockovani_treti_davka,
    i.poz_bez_ockovani + i.poz_po_prvni AS incidence_neockovani,
    (100000 * (i.poz_bez_ockovani + i.poz_po_prvni))::numeric / (pr.population::numeric - ockovani_druha.pocet_uzavrenych_ockovani) AS incidence_neockovani_100tis,
    i.poz_po_ukonceni AS incidence_druha_davka,
    i.poz_po_ukonceni_s_posilujici AS incidence_treti_davka,
    (100000 * i.poz_po_ukonceni)::numeric / (ockovani_druha.pocet_uzavrenych_ockovani - ockovani_treti.pocet_uzavrenych_ockovani) AS incidence_ockovani_druha_davka_100tis,
    (100000 * i.poz_po_ukonceni_s_posilujici)::numeric / ockovani_treti.pocet_uzavrenych_ockovani AS incidence_ockovani_treti_davka_100tis,
    h.hos_bez_ockovani + h.hos_po_prvni AS hospitalizace_neockovani,
    (100000 * (h.hos_bez_ockovani + h.hos_po_prvni))::numeric / (pr.population::numeric - ockovani_druha.pocet_uzavrenych_ockovani) AS hospitalizace_neockovani_100tis,
    h.hos_po_ukonceni AS hospitalizace_ockovani_druha_davka,
    h.hos_po_ukonceni_s_posilujici AS hospitalizace_ockovani_treti_davka,
    (100000 * h.hos_po_ukonceni)::numeric / (ockovani_druha.pocet_uzavrenych_ockovani - ockovani_treti.pocet_uzavrenych_ockovani) AS hospitalizace_ockovani_druha_davka_100tis,
    (100000 * h.hos_po_ukonceni_s_posilujici)::numeric / ockovani_treti.pocet_uzavrenych_ockovani AS hospitalizace_ockovani_treti_davka_100tis,
    icu.jip_bez_ockovani + icu.jip_po_prvni AS jip_neockovani,
    (100000 * (icu.jip_bez_ockovani + icu.jip_po_prvni))::numeric / (pr.population::numeric - ockovani_druha.pocet_uzavrenych_ockovani) AS jip_neockovani_100tis,
    icu.jip_po_ukonceni AS jip_ockovani_druha_davka,
    icu.jip_po_ukonceni_s_posilujici AS jip_ockovani_treti_davka,
    (100000 * icu.jip_po_ukonceni)::numeric / (ockovani_druha.pocet_uzavrenych_ockovani - ockovani_treti.pocet_uzavrenych_ockovani) AS jip_ockovani_druha_davka_100tis,
    (100000 * icu.jip_po_ukonceni_s_posilujici)::numeric / ockovani_treti.pocet_uzavrenych_ockovani AS jip_ockovani_treti_davka_100tis,
    d.zem_bez_ockovani + d.zem_po_prvni AS zemreli_neockovani,
    (100000 * (d.zem_bez_ockovani + d.zem_po_prvni))::numeric / (pr.population::numeric - ockovani_druha.pocet_uzavrenych_ockovani) AS zemreli_neockovani_100tis,
    d.zem_po_ukonceni AS zemreli_ockovani_druha_davka,
    d.zem_po_ukonceni_s_posilujici AS zemreli_ockovani_treti_davka,
    (100000 * d.zem_po_ukonceni)::numeric / (ockovani_druha.pocet_uzavrenych_ockovani - ockovani_treti.pocet_uzavrenych_ockovani) AS zemreli_ockovani_druha_davka_100tis,
    (100000 * d.zem_po_ukonceni_s_posilujici)::numeric / ockovani_treti.pocet_uzavrenych_ockovani AS zemreli_ockovani_treti_davka_100tis
   FROM uzis.covid19_vaccinated_split_incidence i
     JOIN uzis.covid19_vaccinated_split_incidence65 i65 ON i.datum = i65.datum AND i.krajkod = i65.krajkod
     JOIN uzis.covid19_vaccinated_split_hospitalized h ON i.datum = h.datum AND i.krajkod = h.krajkod
     JOIN uzis.covid19_vaccinated_split_icu icu ON i.datum = icu.datum AND i.krajkod = icu.krajkod
     JOIN uzis.covid19_vaccinated_split_deaths d ON i.datum = d.datum AND i.krajkod = d.krajkod
     LEFT JOIN uzis.population_regions pr ON i.krajkod = pr.region_code
     LEFT JOIN ( SELECT sum(population_age_regions.population) AS population65,
            population_age_regions.region_code
           FROM uzis.population_age_regions
          WHERE population_age_regions.age >= 65
          GROUP BY population_age_regions.region_code) par ON i.krajkod = par.region_code
     LEFT JOIN ockovani_druha ON ockovani_druha.datum_vakcinace = i.datum AND ockovani_druha.kraj_bydliste = pr.region_name
     LEFT JOIN ockovani_treti ON ockovani_treti.datum_vakcinace = i.datum AND ockovani_treti.kraj_bydliste = pr.region_name
     LEFT JOIN uzis.codebook_regions cr ON i.krajkod = cr."kraj-kod"
  WHERE i.datum >= '2021-02-01 00:00:00'::timestamp without time zone;



-- analytic.v_uzis_covid19_vaccination_regions_vaccination_points source

CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_regions_vaccination_points
AS SELECT v.zarizeni_kod,
        CASE
            WHEN (v.zarizeni_kod IN ( SELECT DISTINCT covid19_vaccination_points.nrpzs_kod
               FROM uzis.covid19_vaccination_points)) THEN 'Registrované očkovací místo'::text
            ELSE 'Ostatní'::text
        END AS typ_mista,
    v.zarizeni_nazev,
    v.kraj_nazev,
    count(*) FILTER (WHERE v.poradi_davky = 1) AS pocet_prvnich_davek,
    count(*) FILTER (WHERE v.poradi_davky = 2) AS pocet_druhych_davek,
    count(*) AS pocet_davek_celkem,
    count(*) FILTER (WHERE v.datum_vakcinace >= (CURRENT_DATE - '7 days'::interval) AND v.datum_vakcinace < CURRENT_DATE) AS pocet_davek_poslednich_7_dnu,
    count(*) FILTER (WHERE v.datum_vakcinace >= (CURRENT_DATE - '14 days'::interval) AND v.datum_vakcinace < (CURRENT_DATE - '7 days'::interval)) AS pocet_davek_predchozich_7_dnu,
    count(*) FILTER (WHERE v.vakcina = 'Comirnaty'::text) AS pocet_davek_pfizer,
    count(*) FILTER (WHERE v.vakcina = 'Spikevax'::text) AS pocet_davek_moderna,
    count(*) FILTER (WHERE v.vakcina = 'VAXZEVRIA'::text) AS pocet_davek_astra,
    count(*) FILTER (WHERE v.vakcina = 'COVID-19 Vaccine Janssen'::text) AS pocet_davek_jansen,
    count(*) FILTER (WHERE v.vakcina = 'Comirnaty'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_pfizer,
    count(*) FILTER (WHERE v.vakcina = 'Spikevax'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_moderna,
    count(*) FILTER (WHERE v.vakcina = 'VAXZEVRIA'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_astra,
    count(*) FILTER (WHERE v.vakcina = 'COVID-19 Vaccine Janssen'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_jansen,
    count(*) FILTER (WHERE v.vakcina = 'Comirnaty'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_pfizer,
    count(*) FILTER (WHERE v.vakcina = 'Spikevax'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_moderna,
    count(*) FILTER (WHERE v.vakcina = 'VAXZEVRIA'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_astra,
    count(*) FILTER (WHERE v.vakcina = 'COVID-19 Vaccine Janssen'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_jansen,
    count(*) FILTER (WHERE v.pohlavi = 'Z'::text) AS pocet_davek_zeny,
    count(*) FILTER (WHERE v.pohlavi = 'M'::text) AS pocet_davek_muzi,
    count(*) FILTER (WHERE v.vekova_skupina = '80+'::text) AS pocet_davek_80_plus,
    count(*) FILTER (WHERE v.kraj_nazev <> c.kraj_nazev) AS pocet_davek_mimo_kraj_bydliste
   FROM uzis.covid19_vaccination_regional_details v
     LEFT JOIN uzis.codebook_regions_orps c ON v.orp_bydliste = c.orp_nazev
  GROUP BY v.zarizeni_kod, v.zarizeni_nazev, v.kraj_nazev;		   