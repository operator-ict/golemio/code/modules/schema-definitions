CREATE OR REPLACE VIEW analytic.v_public_tenders_sck
AS WITH nadrizene_vz AS (
         SELECT pts.systemove_cislo_zakazky,
            pts.nazev_zakazky
           FROM python.public_tenders_sck pts
          WHERE (pts.systemove_cislo_zakazky IN ( SELECT p.systemove_cislo_nadrizene_vz
                   FROM python.public_tenders_sck p
                  WHERE p.systemove_cislo_nadrizene_vz IS NOT NULL
                  GROUP BY p.systemove_cislo_nadrizene_vz))
        )
 SELECT pt.systemove_cislo_zakazky AS id_zakazky,
        CASE
            WHEN nvz.systemove_cislo_zakazky IS NOT NULL THEN (nvz.nazev_zakazky || ' - '::text) || pt.nazev_zakazky
            ELSE pt.nazev_zakazky
        END AS nazev_zakazky,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN 'Středočeský kraj'::text
            ELSE 'Příspěvková organizace'::text
        END AS sck_po,
    ('https://zakazky.kr-stredocesky.cz/contract_display_'::text || pt.id_zakazky) || '.html'::text AS url,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN pt.oddeleni
            ELSE pt.zadavatel_nazev
        END AS organizacni_jednotka_spravy,
    COALESCE(pt.druh_zadavaciho_rizeni, 'Neuvedeno'::text) AS varianta_druhu_rizeni,
    COALESCE(pt.druh_zakazky, 'Neuvedeno'::text) AS druh_verejne_zakazky,
    COALESCE(pt.dodavatel_nazev, 'Neuvedeno'::text) AS nazev_smluvniho_partnera,
    COALESCE(pt.dodavatel_ico, 'Neuvedeno'::text) AS ico_smluvniho_partnera,
        CASE
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek_vstupujicich_do_hodnoceni
        END AS pocet_nabidek,
    pt.datum_uzavreni_smlouvy,
    pt.konecna_cena_s_dph AS smluvni_cena_s_dph_kc,
    pt.konecna_cena_bez_dph AS smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.konecna_cena_s_dph > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
    pt.datum_zahajeni AS datum_zahajeni_zadavaciho_rizeni,
        CASE
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = 1 OR pt.pocet_nabidek_vstupujicich_do_hodnoceni = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek_vstupujicich_do_hodnoceni, ' nabídky')
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni > 4 THEN concat(pt.pocet_nabidek_vstupujicich_do_hodnoceni, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.faze_zadavaciho_rizeni AS faze_zakazky,
    pt.updated_at
   FROM python.public_tenders_sck pt
     LEFT JOIN ( SELECT public_tenders_sck.id_zakazky
           FROM python.public_tenders_sck
          ORDER BY public_tenders_sck.konecna_cena_bez_dph DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_sck_funded_organisations pto ON pt.zadavatel_ico = pto.ico
     LEFT JOIN nadrizene_vz nvz ON nvz.systemove_cislo_zakazky = pt.systemove_cislo_nadrizene_vz
  WHERE NOT (pt.systemove_cislo_zakazky IN ( SELECT nz.systemove_cislo_zakazky
           FROM nadrizene_vz nz)) AND (pt.faze_zadavaciho_rizeni = ANY (ARRAY['Zadáno'::text, 'Uzavřeno'::text])) AND pt.datum_uzavreni_smlouvy >= '2020-01-01'::date AND pt.datum_zahajeni >= '2020-01-01'::date;

CREATE OR REPLACE VIEW analytic.v_public_tenders_sck_internal
AS WITH nadrizene_vz AS (
         SELECT pts.systemove_cislo_zakazky,
            pts.nazev_zakazky
           FROM python.public_tenders_sck pts
          WHERE (pts.systemove_cislo_zakazky IN ( SELECT p.systemove_cislo_nadrizene_vz
                   FROM python.public_tenders_sck p
                  WHERE p.systemove_cislo_nadrizene_vz IS NOT NULL
                  GROUP BY p.systemove_cislo_nadrizene_vz))
        )
 SELECT pt.systemove_cislo_zakazky AS id_zakazky,
        CASE
            WHEN nvz.systemove_cislo_zakazky IS NOT NULL THEN (nvz.nazev_zakazky || ' - '::text) || pt.nazev_zakazky
            ELSE pt.nazev_zakazky
        END AS nazev_zakazky,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN 'Středočeský kraj'::text
            ELSE 'Příspěvková organizace'::text
        END AS sck_po,
    pto.typ AS oblast,
    ('https://zakazky.kr-stredocesky.cz/contract_display_'::text || pt.id_zakazky) || '.html'::text AS url,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN pt.oddeleni
            ELSE pt.zadavatel_nazev
        END AS organizacni_jednotka_spravy,
    COALESCE(pt.druh_zadavaciho_rizeni, 'Neuvedeno'::text) AS varianta_druhu_rizeni,
    COALESCE(pt.druh_zakazky, 'Neuvedeno'::text) AS druh_verejne_zakazky,
    COALESCE(pt.dodavatel_nazev, 'Neuvedeno'::text) AS nazev_smluvniho_partnera,
    COALESCE(pt.dodavatel_ico, 'Neuvedeno'::text) AS ico_smluvniho_partnera,
        CASE
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = 0 THEN 1::bigint
            ELSE pt.pocet_nabidek_vstupujicich_do_hodnoceni
        END AS pocet_nabidek,
    pt.konecna_cena_s_dph AS smluvni_cena_s_dph_kc,
    pt.konecna_cena_bez_dph AS smluvni_cena_bez_dph_kc,
        CASE
            WHEN top10.id_zakazky IS NOT NULL THEN 'Y'::text
            ELSE 'N'::text
        END AS top10,
        CASE
            WHEN pt.konecna_cena_s_dph > 300000::double precision THEN 'Více než 300 000'::text
            ELSE 'Méně než 300 000'::text
        END AS price_range,
        CASE
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = 1 OR pt.pocet_nabidek_vstupujicich_do_hodnoceni = 0 THEN '1 nabídka'::text
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni = ANY (ARRAY[2::bigint, 3::bigint, 4::bigint]) THEN concat(pt.pocet_nabidek_vstupujicich_do_hodnoceni, ' nabídky')
            WHEN pt.pocet_nabidek_vstupujicich_do_hodnoceni > 4 THEN concat(pt.pocet_nabidek_vstupujicich_do_hodnoceni, ' nabídek')
            ELSE 'Neuvedeno'::text
        END AS pocet_nabidek2,
    pt.predpokladana_hodnota AS celkova_predpokladana_hodnota_zakazky_bez_dph_kc,
    pt.konecna_cena_bez_dph - NULLIF(pt.predpokladana_hodnota, 0::double precision) AS rozdil_od_predp_hodnoty_kc,
    (pt.konecna_cena_bez_dph - pt.predpokladana_hodnota) / NULLIF(pt.predpokladana_hodnota, 0::double precision) AS rozdil_od_predp_hodnoty_procent,
    pt.datum_uzavreni_smlouvy,
    pt.lhuta_pro_nabidky AS lhuta_pro_podani_nabidek_na_profilu,
    pt.datum_zahajeni AS datum_zahajeni_zadavaciho_rizeni,
    pt.datum_uzavreni_smlouvy - pt.datum_zahajeni AS delka_zadavaciho_rizeni,
    pt.lhuta_pro_nabidky - pt.datum_zahajeni AS delka_lhuty_pro_podani_nabidek,
    pt.faze_zadavaciho_rizeni AS faze_zakazky,
    pt.updated_at,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-01-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            WHEN pt.datum_uzavreni_smlouvy >= '2020-01-01'::date THEN 'Leden 2020 - nyní'::text
            ELSE 'Neuvedeno'::text
        END AS uzavreni_smlouvy_filtr
   FROM python.public_tenders_sck pt
     LEFT JOIN ( SELECT public_tenders_sck.id_zakazky
           FROM python.public_tenders_sck
          ORDER BY public_tenders_sck.konecna_cena_bez_dph DESC
         LIMIT 10) top10 ON pt.id_zakazky = top10.id_zakazky
     LEFT JOIN python.public_tenders_sck_funded_organisations pto ON pt.zadavatel_ico = pto.ico
     LEFT JOIN nadrizene_vz nvz ON nvz.systemove_cislo_zakazky = pt.systemove_cislo_nadrizene_vz
  WHERE NOT (pt.systemove_cislo_zakazky IN ( SELECT nz.systemove_cislo_zakazky
           FROM nadrizene_vz nz)) AND (pt.faze_zadavaciho_rizeni = ANY (ARRAY['Zadáno'::text, 'Zrušeno'::text, 'Uzavřeno'::text])) AND pt.datum_zahajeni >= '2020-01-01'::date;

CREATE OR REPLACE VIEW analytic.v_public_tenders_sck_internal_frequent_suppliers
AS SELECT count(DISTINCT pt.id_zakazky) AS pocet_zakazek,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN pt.oddeleni
            ELSE pt.zadavatel_nazev
        END AS organizacni_jednotka_spravy,
    COALESCE(pt.druh_zadavaciho_rizeni, 'Neuvedeno'::text) AS varianta_druhu_rizeni,
    pt.datum_uzavreni_smlouvy,
    pt.faze_zadavaciho_rizeni AS faze_zakazky,
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN 'Středočeský kraj'::text
            ELSE 'Příspěvková organizace'::text
        END AS sck_po,
    pto.typ AS oblast,
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-01-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            WHEN pt.datum_uzavreni_smlouvy >= '2020-01-01'::date THEN 'Leden 2020 - nyní'::text
            ELSE 'Neuvedeno'::text
        END AS uzavreni_smlouvy_filtr,
    pt.updated_at
   FROM python.public_tenders_sck pt
     LEFT JOIN python.public_tenders_sck_funded_organisations pto ON pt.zadavatel_ico = pto.ico
  WHERE NOT (pt.systemove_cislo_zakazky IN ( SELECT pts.systemove_cislo_nadrizene_vz
           FROM python.public_tenders_sck pts
          WHERE pts.systemove_cislo_nadrizene_vz IS NOT NULL
          GROUP BY pts.systemove_cislo_nadrizene_vz)) AND (pt.faze_zadavaciho_rizeni = ANY (ARRAY['Zadáno'::text, 'Zrušeno'::text, 'Uzavřeno'::text])) AND pt.datum_zahajeni >= '2020-01-01'::date
  GROUP BY (CASE
                WHEN pt.zadavatel_ico = '70891095'::text THEN pt.oddeleni
                ELSE pt.zadavatel_nazev
        END), 
        pt.druh_zadavaciho_rizeni, pt.datum_uzavreni_smlouvy, pt.faze_zadavaciho_rizeni, (
        CASE
            WHEN pt.zadavatel_ico = '70891095'::text THEN 'Středočeský kraj'::text
            ELSE 'Příspěvková organizace'::text
        END), pto.typ, (
        CASE
            WHEN pt.datum_uzavreni_smlouvy < '2020-01-01'::date THEN 'Uzavření smlouvy před datem vytvoření zakázky'::text
            WHEN pt.datum_uzavreni_smlouvy > pt.updated_at THEN 'Uzavření smlouvy v budoucnosti'::text
            WHEN pt.datum_uzavreni_smlouvy >= '2020-01-01'::date THEN 'Leden 2020 - nyní'::text
            ELSE 'Neuvedeno'::text
        END), pt.updated_at;
        