drop view analytic.v_pedestrians_flow_quality;
-- analytic.v_pedestrians_flow_quality source

CREATE OR REPLACE VIEW analytic.v_pedestrians_flow_quality
AS WITH timeline AS (
         SELECT "timestamp"."timestamp"
           FROM generate_series(( SELECT date_trunc('hour'::text, to_timestamp((min(flow_measurements.start_timestamp) / 1000)::double precision)) AS date_trunc
                   FROM flow_measurements), ( SELECT to_timestamp((max(flow_measurements.start_timestamp) / 1000)::double precision) AS to_timestamp
                   FROM flow_measurements), '01:00:00'::interval) "timestamp"("timestamp")
        ), category AS (
         SELECT DISTINCT flow_measurements.cube_id,
            flow_measurements.sink_id
           FROM flow_measurements
        ), timeline_category AS (
         SELECT t_1."timestamp",
            c.cube_id,
            c.sink_id
           FROM timeline t_1
             JOIN category c ON true
        ), real_nrow AS (
         SELECT date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) AS date_trunc,
            flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(*) AS n_row
           FROM flow_measurements
          GROUP BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))), flow_measurements.cube_id, flow_measurements.sink_id
        ), expected_nrow AS (
         SELECT flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(DISTINCT flow_measurements.category) * 12 AS expected_nrow
           FROM flow_measurements
          GROUP BY flow_measurements.cube_id, flow_measurements.sink_id
        )
 SELECT t."timestamp",
    t.cube_id,
    t.sink_id,
    concat(t.cube_id, ' - ', t.sink_id) AS cube_sink,
    COALESCE(r.n_row, 0::bigint) AS n_row,
    er.expected_nrow
   FROM timeline_category t
     LEFT JOIN real_nrow r ON t."timestamp" = r.date_trunc AND t.cube_id = r.cube_id AND t.sink_id = r.sink_id
     LEFT JOIN expected_nrow er ON t.cube_id = er.cube_id AND t.sink_id = er.sink_id;

drop VIEW analytic.v_uzis_covid19_vaccinated_split;

--SET search_path TO uzis,public;

drop TABLe uzis.history_covid19_hospital_capacity_regional;
--RENAME TO covid19_hospital_capacity_regional;

ALTER TABLE uzis.covid19_regions 
RENAME COLUMN datum TO "Datum";

ALTER TABLE uzis.covid19_regions 
RENAME COLUMN kraj TO "Kraj";

--drop table uzis.covid19_hospital_capacity_regional;

drop TABLE uzis.covid19_regions;