-- public.mobileappstatistics_appstore definition

CREATE TABLE public.mobileappstatistics_appstore (
	begin_day date NOT NULL, -- den za který máme data
	app_id varchar(50) NOT NULL, -- ID aplikace
	app_name varchar(50) NOT NULL, -- název aplikace
	"version" varchar(50) NOT NULL, -- verze aplikace
	event_type varchar(50) NOT NULL, -- id které určujě zda jde o placenou app/update apod
	device varchar(50) NOT NULL, -- typ zařízení na které bylo instalováno
	country varchar(50) NOT NULL, -- země původu uživatele
	event_count int4 NULL, -- počet unikátních kliknutí na button “Buy” or “Get”
	create_batch_id int8 NULL, -- ID vstupní dávky
	created_at timestamp NULL, -- Čas vložení
	created_by varchar(150) NULL, -- identikace uživatele/procesu, který záznam vložil
	update_batch_id int8 NULL, -- ID poslední dávky, která záznam modifikovala
	updated_at timestamp NULL, -- Čas poslední modifikace
	updated_by varchar(150) NULL, -- Identikace uživatele/procesu, který poslední měnil záznam
	CONSTRAINT mobileappstatistics_appstore_pk PRIMARY KEY (begin_day, app_id, app_name, version, event_type, device, country)
);
CREATE INDEX idx_mobileappstatistics_appstore_begin_day ON public.mobileappstatistics_appstore USING btree (begin_day);
CREATE INDEX mobileappstatistics_appstore_create_batch ON public.mobileappstatistics_appstore USING btree (create_batch_id);
CREATE INDEX mobileappstatistics_appstore_update_batch ON public.mobileappstatistics_appstore USING btree (update_batch_id);
COMMENT ON TABLE public.mobileappstatistics_appstore IS 'data z App Store';

-- Column comments

COMMENT ON COLUMN public.mobileappstatistics_appstore.begin_day IS 'den za který máme data';
COMMENT ON COLUMN public.mobileappstatistics_appstore.app_id IS 'ID aplikace';
COMMENT ON COLUMN public.mobileappstatistics_appstore.app_name IS 'název aplikace';
COMMENT ON COLUMN public.mobileappstatistics_appstore."version" IS 'verze aplikace';
COMMENT ON COLUMN public.mobileappstatistics_appstore.event_type IS 'id které určujě zda jde o placenou app/update apod';
COMMENT ON COLUMN public.mobileappstatistics_appstore.device IS 'typ zařízení na které bylo instalováno';
COMMENT ON COLUMN public.mobileappstatistics_appstore.country IS 'země původu uživatele';
COMMENT ON COLUMN public.mobileappstatistics_appstore.event_count IS 'počet unikátních kliknutí na button “Buy” or “Get”';
COMMENT ON COLUMN public.mobileappstatistics_appstore.create_batch_id IS 'ID vstupní dávky';
COMMENT ON COLUMN public.mobileappstatistics_appstore.created_at IS 'Čas vložení';
COMMENT ON COLUMN public.mobileappstatistics_appstore.created_by IS 'identikace uživatele/procesu, který záznam vložil';
COMMENT ON COLUMN public.mobileappstatistics_appstore.update_batch_id IS 'ID poslední dávky, která záznam modifikovala';
COMMENT ON COLUMN public.mobileappstatistics_appstore.updated_at IS 'Čas poslední modifikace';
COMMENT ON COLUMN public.mobileappstatistics_appstore.updated_by IS 'Identikace uživatele/procesu, který poslední měnil záznam';


-- public.mobileappstatistics_playstore definition

CREATE TABLE public.mobileappstatistics_playstore (
	begin_day date NOT NULL,
	package_name varchar(256) NOT NULL,
	daily_device_installs int4 NULL,
	daily_user_installs int4 NULL,
	install_events int4 NULL,
	create_batch_id int8 NULL, -- ID vstupní dávky
	created_at timestamp NULL, -- Čas vložení
	created_by varchar(150) NULL, -- identikace uživatele/procesu, který záznam vložil
	update_batch_id int8 NULL, -- ID poslední dávky, která záznam modifikovala
	updated_at timestamp NULL, -- Čas poslední modifikace
	updated_by varchar(150) NULL, -- Identikace uživatele/procesu, který poslední měnil záznam
	CONSTRAINT mobileappstatistics_playstore_pk PRIMARY KEY (begin_day, package_name)
);
CREATE INDEX idx_mobileappstatistics_playstore_begin_day ON public.mobileappstatistics_playstore USING btree (begin_day);
CREATE INDEX mobileappstatistics_playstore_create_batch ON public.mobileappstatistics_playstore USING btree (create_batch_id);
CREATE INDEX mobileappstatistics_playstore_update_batch ON public.mobileappstatistics_playstore USING btree (update_batch_id);

-- Column comments

COMMENT ON COLUMN public.mobileappstatistics_playstore.create_batch_id IS 'ID vstupní dávky';
COMMENT ON COLUMN public.mobileappstatistics_playstore.created_at IS 'Čas vložení';
COMMENT ON COLUMN public.mobileappstatistics_playstore.created_by IS 'identikace uživatele/procesu, který záznam vložil';
COMMENT ON COLUMN public.mobileappstatistics_playstore.update_batch_id IS 'ID poslední dávky, která záznam modifikovala';
COMMENT ON COLUMN public.mobileappstatistics_playstore.updated_at IS 'Čas poslední modifikace';
COMMENT ON COLUMN public.mobileappstatistics_playstore.updated_by IS 'Identikace uživatele/procesu, který poslední měnil záznam';