create schema if not exists uzis;

-- uzis.covid19_cz_details definition
CREATE TABLE if not exists uzis.covid19_cz_details (
	datum_hlaseni timestamp NULL,
	vek int8 NULL,
	pohlavi text NULL,
	kraj text NULL,
	nakaza_v_zahranici float8 NULL,
	nakaza_zeme_csu_kod text NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	create_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	update_batch_id int8 NULL
);

-- uzis.covid19_cz_daily definition
CREATE TABLE if not exists uzis.covid19_cz_daily (
	datum timestamp NULL,
	kumulativni_pocet_nakazenych int8 NULL,
	kumulativni_pocet_vylecenych int8 NULL,
	kumulativni_pocet_umrti int8 NULL,
	kumulativni_pocet_testu int8 NULL,
	kumulativni_pocet_ag_testu int8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);

-- uzis.covid19_hospital_capacity_details definition
CREATE TABLE if not exists uzis.covid19_hospital_capacity_details (
	datum timestamp NULL,
	zz_kod int8 NULL,
	zz_nazev text NULL,
	kraj_nuts_kod text NULL,
	kraj_nazev text NULL,
	luzka_standard_kyslik_kapacita_volna_covid_pozitivni float8 NULL,
	luzka_standard_kyslik_kapacita_volna_covid_negativni float8 NULL,
	luzka_standard_kyslik_kapacita_celkem float8 NULL,
	luzka_hfno_cpap_kapacita_volna_covid_pozitivni float8 NULL,
	luzka_hfno_cpap_kapacita_volna_covid_negativni float8 NULL,
	luzka_hfno_cpap_kapacita_celkem float8 NULL,
	luzka_upv_niv_kapacita_volna_covid_pozitivni float8 NULL,
	luzka_upv_niv_kapacita_volna_covid_negativni float8 NULL,
	luzka_upv_niv_kapacita_celkem float8 NULL,
	ecmo_kapacita_volna float8 NULL,
	ecmo_kapacita_celkem float8 NULL,
	cvvhd_kapacita_volna float8 NULL,
	cvvhd_kapacita_celkem float8 NULL,
	ventilatory_prenosne_kapacita_volna float8 NULL,
	ventilatory_prenosne_kapacita_celkem float8 NULL,
	ventilatory_operacni_sal_kapacita_volna float8 NULL,
	ventilatory_operacni_sal_kapacita_celkem float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);

-- uzis.covid19_hospitalized_regional definition
CREATE TABLE if not exists uzis.covid19_hospitalized_regional (
	datum timestamp NULL,
	nemocnice_kraj text NULL,
	pacient_prvni_zaznam float8 NULL,
	kum_pacient_prvni_zaznam float8 NULL,
	pocet_hosp float8 NULL,
	stav_bez_priznaku float8 NULL,
	stav_lehky float8 NULL,
	stav_stredni float8 NULL,
	stav_tezky float8 NULL,
	jip float8 NULL,
	kyslik float8 NULL,
	hfno float8 NULL,
	upv float8 NULL,
	ecmo float8 NULL,
	tezky_upv_ecmo float8 NULL,
	umrti float8 NULL,
	kum_umrti float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);

-- uzis.covid19_pcr_requests definition
CREATE TABLE if not exists uzis.covid19_pcr_requests (
	datum timestamp NULL,
	krajkod text NULL,
	kraj text NULL,
	pocet int8 NULL,
	samoplatcu int8 NULL,
	cizincu int8 NULL,
	primdg int8 NULL,
	kontrolni int8 NULL,
	preventivni int8 NULL,
	epidemilogicka int8 NULL,
	nemocnice int8 NULL,
	ostatni_luzkova int8 NULL,
	socialni_zarizeni int8 NULL,
	socialni_zarizeni1 int8 NULL,
	praktici int8 NULL,
	specialiste int8 NULL,
	khs int8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);

-- uzis.covid19_pes_regional definition
CREATE TABLE if not exists uzis.covid19_pes_regional (
	datum_zobrazeni timestamp NULL,
	datum timestamp NULL,
	kraj_kod text NULL,
	incidence14_100 float8 NULL,
	incidence_65_14_100 float8 NULL,
	simple_r float8 NULL,
	pozit_perc_test float8 NULL,
	podil_zachycen_hosp float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);

-- uzis.covid19_cz_tests_regional definition
CREATE TABLE if not exists uzis.covid19_cz_tests_regional (
	datum timestamp NULL,
	kraj_nuts_kod text NULL,
	okres_lau_kod text NULL,
	prirustkovy_pocet_testu_okres int8 NULL,
	kumulativni_pocet_testu_okres int8 NULL,
	prirustkovy_pocet_testu_kraj int8 NULL,
	kumulativni_pocet_testu_kraj int8 NULL,
	prirustkovy_pocet_prvnich_testu_okres int8 NULL,
	kumulativni_pocet_prvnich_testu_okres int8 NULL,
	prirustkovy_pocet_prvnich_testu_kraj int8 NULL,
	kumulativni_pocet_prvnich_testu_kraj int8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);

-- uzis.covid19_vaccination_distribution definition
CREATE TABLE if not exists uzis.covid19_vaccination_distribution (
	datum timestamp NULL,
	ockovaci_misto_id text NULL,
	ockovaci_misto_nazev text NULL,
	kraj_nuts_kod text NULL,
	kraj_nazev text NULL,
	cilove_ockovaci_misto_id text NULL,
	cilove_ockovaci_misto_nazev text NULL,
	cilovy_kraj_kod text NULL,
	cilovy_kraj_nazev text NULL,
	ockovaci_latka text NULL,
	vyrobce text NULL,
	akce text NULL,
	pocet_ampulek int8 NULL,
	pocet_davek int8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);

-- uzis.covid19_vaccination_points definition
CREATE TABLE if not exists uzis.covid19_vaccination_points (
	ockovaci_misto_id text NULL,
	ockovaci_misto_nazev text NULL,
	okres_nuts_kod text NULL,
	operacni_status float8 NULL,
	ockovaci_misto_adresa text NULL,
	latitude float8 NULL,
	longitude float8 NULL,
	nrpzs_kod int8 NULL,
	minimalni_kapacita int8 NULL,
	bezbarierovy_pristup float8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL,
	ockovaci_misto_typ text NULL
);

-- uzis.covid19_vaccination_usage definition
CREATE TABLE if not exists uzis.covid19_vaccination_usage (
	datum timestamp NULL,
	ockovaci_misto_id text NULL,
	ockovaci_misto_nazev text NULL,
	kraj_nuts_kod text NULL,
	kraj_nazev text NULL,
	ockovaci_latka text NULL,
	vyrobce text NULL,
	pouzite_ampulky int8 NULL,
	znehodnocene_ampulky int8 NULL,
	pouzite_davky int8 NULL,
	znehodnocene_davky int8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL
);

-- uzis.covid19_vaccination_prague_details definition
CREATE TABLE if not exists uzis.covid19_vaccination_prague_details (
	datum_vakcinace timestamp NULL,
	vakcina_kod text NULL,
	vakcina text NULL,
	zrizovatel_kod int8 NULL,
	zrizovatel text NULL,
	zarizeni_kod int8 NULL,
	zarizeni_nazev text NULL,
	poradi_davky int8 NULL,
	vekova_skupina text NULL,
	pohlavi text NULL,
	obec text NULL,
	mestska_cast text NULL,
	indikace_zdravotnik int8 NULL,
	indikace_socialni_sluzby int8 NULL,
	indikace_pracovnici_ki int8 NULL,
	indikace_pedagog int8 NULL,
	indikace_skolstvi_ostatni int8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL,
	bezpecnostni_infrastruktura int8 NULL,
	chronicke_onemocneni int8 NULL
);

-- uzis.covid19_vaccination_regional_details definition
CREATE TABLE if not exists uzis.covid19_vaccination_regional_details (
	datum_aktualizace timestamp NULL,
	datum_vakcinace timestamp NULL,
	vakcina_kod text NULL,
	vakcina text NULL,
	kraj_kod text NULL,
	kraj_nazev text NULL,
	zrizovatel_kod int8 NULL,
	zrizovatel text NULL,
	zarizeni_kod int8 NULL,
	zarizeni_nazev text NULL,
	poradi_davky int8 NULL,
	vekova_skupina text NULL,
	pohlavi text NULL,
	orp_bydliste text NULL,
	orp_bydliste_kod float8 NULL,
	indikace_zdravotnik int8 NULL,
	indikace_socialni_sluzby int8 NULL,
	indikace_ostatni int8 NULL,
	updated_at timestamp NULL,
	updated_by text NULL,
	created_at timestamp NULL,
	created_by text NULL,
	indikace_pedagog int8 NULL,
	indikace_skolstvi_ostatni int8 NULL,
	bezpecnostni_infrastruktura int8 NULL,
	chronicke_onemocneni int8 NULL
);

-- uzis.covid19_municipalities definition
CREATE TABLE if not exists uzis.covid19_municipalities (
	casova_znamka timestamp NULL,
	datum timestamp NULL,
	obec_kod int8 NULL,
	psc float8 NULL,
	opou_kod int8 NULL,
	opou_nazev text NULL,
	orp_kod int8 NULL,
	orp_nazev text NULL,
	okres_kod text NULL,
	okres_nazev text NULL,
	kraj_kod text NULL,
	kraj_nazev text NULL,
	kumulativni_pocet_pozitivnich_osob int8 NULL,
	kumulativni_pocet_hospitalizovanych_osob float8 NULL,
	aktualni_pocet_hospitalizovanych_osob float8 NULL,
	incidence int8 NULL,
	pocet_vyleceni_den int8 NULL,
	kumulativni_pocet_vylecenych int8 NULL,
	pocet_zemreli_den int8 NULL,
	zemreli_za_hospitalizace float8 NULL,
	kumulativni_pocet_zemrelych int8 NULL,
	prevalence int8 NULL,
	created_at timestamptz(0) NULL,
	created_by varchar(150) NULL,
	create_batch_id int8 NULL,
	updated_at timestamptz(0) NULL,
	updated_by varchar(150) NULL,
	update_batch_id int8 NULL,
	obec_nazev varchar NULL
);

-- uzis.covid19_prague_districts definition
CREATE TABLE if not exists uzis.covid19_prague_districts (
	casova_znamka timestamptz NOT NULL,
	datum timestamptz NOT NULL,
	mckod numeric NULL,
	mestskacast varchar(50) NULL,
	psc numeric NULL,
	opoukod numeric NULL,
	opounazev varchar(50) NULL,
	orpkod numeric NULL,
	orpnazev varchar(50) NULL,
	okreskod varchar(50) NULL,
	okresnazev varchar(50) NULL,
	krajkod varchar(50) NULL,
	krajnazev varchar(50) NULL,
	kumulativni_pocet_pozitivnich_osob numeric NULL,
	kumulativni_pocet_hospitalizovanych_osob numeric NULL,
	aktualni_pocet_hospitalizovanych_osob numeric NULL,
	zemreli_za_hospitalizace numeric NULL,
	incidence numeric NULL,
	kumulativni_pocet_vylecenych numeric NULL,
	kumulativni_pocet_zemrelych numeric NULL,
	prevalence numeric NULL,
	podil_65__na_kumulativnim_poctu_pozitivnich float8 NULL,
	podil_65__na_kumulativnim_poctu_hospitalizovanych float8 NULL,
	podil_65__na_aktualnim_poctu_hospitalizovanych float8 NULL,
	podil_65__na_incidenci float8 NULL,
	podil_65__na_kumulativnim_poctu_vylecenych float8 NULL,
	podil_65__na_kumulativnim_poctu_zemrelych float8 NULL,
	podil_65__na_prevalenci float8 NULL,
	created_at timestamptz(0) NULL,
	created_by varchar(150) NULL,
	create_batch_id int8 NULL,
	updated_at timestamptz(0) NULL,
	updated_by varchar(150) NULL,
	update_batch_id int8 NULL
);

-- uzis.codebook_regions definition
CREATE TABLE if not exists uzis.codebook_regions (
	"kraj-kod" text NULL,
	"kraj-nazev" text NULL
);

-- uzis.codebook_regions_orps definition
CREATE TABLE if not exists uzis.codebook_regions_orps (
	orp_nazev text NULL,
	kraj_kod text NULL,
	kraj_nazev text NULL
);

-- uzis.population_districts definition
CREATE TABLE if not exists uzis.population_districts (
	"index" int8 NULL,
	district_code text NULL,
	district_name text NULL,
	population int8 NULL
);
CREATE INDEX if not exists ix_uzis_czso_population_districts_2020_index ON uzis.population_districts USING btree (index);

-- uzis.population_regions definition
CREATE TABLE if not exists uzis.population_regions (
	"index" int8 NULL,
	region_code text NULL,
	region_name text NULL,
	population int8 NULL
);
CREATE INDEX if not exists ix_uzis_czso_population_regions_2020_index ON uzis.population_regions USING btree (index);

-- uzis.population_age_regions definition
CREATE TABLE if not exists uzis.population_age_regions (
	region_code text NULL,
	age int8 NULL,
	population int8 NULL
);

-- uzis.population_prague_age_districts definition
CREATE TABLE if not exists uzis.population_prague_age_districts (
	mestska_cast text NULL,
	spravni_obvod text NULL,
	vekova_skupina text NULL,
	populace int4 NULL
);

-- uzis.population_prague_districts definition
CREATE TABLE if not exists uzis.population_prague_districts (
	district varchar NOT NULL,
	population numeric NOT NULL,
	CONSTRAINT prague_districts_population_2019_pk PRIMARY KEY (district)
);

-- uzis.population_central_bohemia definition
CREATE TABLE if not exists uzis.population_central_bohemia (
	region_code varchar NOT NULL,
	region_name varchar NOT NULL,
	population numeric NOT NULL,
	CONSTRAINT central_bohemia_population_2019_pk PRIMARY KEY (region_code)
);


-- analytic.v_uzis_covid19_age_groups source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_age_groups
AS SELECT covid19_cz_details.datum_hlaseni::date AS report_date,
    covid19_cz_details.vek::numeric AS age,
        CASE
            WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
            WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
            WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
            WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
            WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
            WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
            ELSE '65 a víc'::text
        END AS age_group,
    0 AS id,
    covid19_cz_details.pohlavi::character varying(10) AS gender,
        CASE
            WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN 1
            WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN 2
            WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN 3
            WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN 4
            WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN 5
            WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN 6
            ELSE 7
        END AS age_group_order,
    covid19_cz_details.kraj::character varying(100) AS region,
    codebook_regions."kraj-nazev" AS nazev_kraj
   FROM uzis.covid19_cz_details
     LEFT JOIN uzis.codebook_regions ON codebook_regions."kraj-kod" = covid19_cz_details.kraj;
	 
-- analytic.v_uzis_covid19_age_groups_ratios source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_age_groups_ratios
AS WITH age_groups AS (
         SELECT date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone) AS report_month,
                CASE
                    WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
                    WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
                    WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
                    WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
                    WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
                    WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
                    ELSE '65 a víc'::text
                END AS age_group,
            count(*) AS cases_count,
            covid19_cz_details.kraj AS region,
            codebook_regions."kraj-nazev" AS nazev_kraj
           FROM uzis.covid19_cz_details
             LEFT JOIN uzis.codebook_regions ON codebook_regions."kraj-kod" = covid19_cz_details.kraj
          GROUP BY covid19_cz_details.kraj, (date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone)), (
                CASE
                    WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
                    WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
                    WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
                    WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
                    WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
                    WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
                    ELSE '65 a víc'::text
                END), codebook_regions."kraj-nazev"
        ), totals AS (
         SELECT date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone) AS report_month,
            count(*) AS cases_count,
            covid19_cz_details.kraj AS region
           FROM uzis.covid19_cz_details
          GROUP BY covid19_cz_details.kraj, (date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone))
        )
 SELECT ag.report_month,
    ag.age_group,
    ag.cases_count::numeric / t.cases_count::numeric AS age_group_ratio,
        CASE
            WHEN ag.age_group = '0-5'::text THEN 1
            WHEN ag.age_group = '6-9'::text THEN 2
            WHEN ag.age_group = '10-14'::text THEN 3
            WHEN ag.age_group = '15-19'::text THEN 4
            WHEN ag.age_group = '20-25'::text THEN 5
            WHEN ag.age_group = '26-64'::text THEN 6
            ELSE 7
        END AS age_group_order,
    ag.region::character varying(100) AS region,
    ag.nazev_kraj
   FROM age_groups ag
     JOIN totals t ON ag.report_month = t.report_month AND ag.region = t.region;	 
	 
-- analytic.v_uzis_covid19_cz source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_cz
AS SELECT covid19_cz_daily.datum::date AS report_date,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_nakazenych IS NULL THEN 0::numeric
            ELSE covid19_cz_daily.kumulativni_pocet_nakazenych::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_nakazenych) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_nakazenych) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS infected_count_daily,
    covid19_cz_daily.kumulativni_pocet_nakazenych::numeric AS infected_count_cumulative,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_vylecenych IS NULL THEN 0::numeric
            ELSE covid19_cz_daily.kumulativni_pocet_vylecenych::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_vylecenych) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_vylecenych) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS healed_count_daily,
    covid19_cz_daily.kumulativni_pocet_vylecenych::numeric AS healed_count_cumulative,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_umrti IS NULL THEN 0::numeric
            ELSE covid19_cz_daily.kumulativni_pocet_umrti::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_umrti) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_umrti) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS deceased_count_daily,
    covid19_cz_daily.kumulativni_pocet_umrti::numeric AS deceased_count_cumulative,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_testu IS NULL THEN 0::numeric
            ELSE covid19_cz_daily.kumulativni_pocet_testu::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_testu) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_testu) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS tested_count_daily,
    covid19_cz_daily.kumulativni_pocet_testu::numeric AS tested_count_cumulative,
    (covid19_cz_daily.kumulativni_pocet_nakazenych - covid19_cz_daily.kumulativni_pocet_umrti - covid19_cz_daily.kumulativni_pocet_vylecenych)::numeric AS active_count_current,
        CASE
            WHEN covid19_cz_daily.kumulativni_pocet_nakazenych IS NULL THEN 0::numeric
            ELSE (covid19_cz_daily.kumulativni_pocet_nakazenych - covid19_cz_daily.kumulativni_pocet_umrti - covid19_cz_daily.kumulativni_pocet_vylecenych)::numeric
        END -
        CASE
            WHEN lag(covid19_cz_daily.kumulativni_pocet_nakazenych - covid19_cz_daily.kumulativni_pocet_umrti - covid19_cz_daily.kumulativni_pocet_vylecenych) OVER (ORDER BY covid19_cz_daily.datum) IS NULL THEN 0::numeric
            ELSE lag(covid19_cz_daily.kumulativni_pocet_nakazenych - covid19_cz_daily.kumulativni_pocet_umrti - covid19_cz_daily.kumulativni_pocet_vylecenych) OVER (ORDER BY covid19_cz_daily.datum)::numeric
        END AS active_count_daily,
        CASE
            WHEN covid19_cz_daily.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS yesterday,
        CASE
            WHEN covid19_cz_daily.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN covid19_cz_daily.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM uzis.covid19_cz_daily;	 
   
-- analytic.v_uzis_covid19_hospitalized_daily source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospitalized_daily
AS SELECT dta.datum AS reporting_date,
    dta.datum AS reporting_time,
    dta.updated_at AS data_valid_to,
    ck."kraj-nazev" AS region_name,
    dta.pocet_hosp AS current_hospitalized,
    COALESCE(dta.pocet_hosp, 0::double precision) - COALESCE(lag(dta.pocet_hosp) OVER (PARTITION BY dta.nemocnice_kraj ORDER BY dta.datum), 0::double precision) AS new_current_hospitalized,
    dta.pacient_prvni_zaznam AS new_hospitalized,
    dta.stav_bez_priznaku AS current_without_symptoms,
    dta.stav_lehky AS current_light_progress,
    dta.stav_stredni AS current_medium_progress,
    dta.stav_tezky AS current_severe_progress,
    COALESCE(dta.stav_tezky, 0::double precision) - COALESCE(lag(dta.stav_tezky) OVER (PARTITION BY dta.nemocnice_kraj ORDER BY dta.datum), 0::double precision) AS new_severe_progress,
    dta.jip,
    dta.kyslik AS oxygen_treatment_standart,
    dta.hfno,
    dta.upv,
    dta.ecmo,
    dta.tezky_upv_ecmo AS current_severe_progress_or_highly_intensive_care,
    COALESCE(dta.tezky_upv_ecmo, 0::double precision) - COALESCE(lag(dta.tezky_upv_ecmo) OVER (PARTITION BY dta.nemocnice_kraj ORDER BY dta.datum), 0::double precision) AS new_current_severe_progress_or_highly_intensive_care,
        CASE
            WHEN dta.datum = (CURRENT_DATE - '1 day'::interval) AND dta.datum < CURRENT_DATE THEN true
            ELSE false
        END AS max_reporting_date,
        CASE
            WHEN dta.datum >= (CURRENT_DATE - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN dta.datum >= date_trunc('day'::text, (( SELECT max(covid19_hospitalized_regional.datum) AS max
               FROM uzis.covid19_hospitalized_regional)) - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM uzis.covid19_hospitalized_regional dta
     JOIN uzis.codebook_regions ck ON dta.nemocnice_kraj = ck."kraj-kod";   
-- analytic.v_uzis_covid19_moving_averages source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_moving_averages
AS SELECT a.report_date,
    a.infected_count_daily AS new_positive,
    avg(a.infected_count_daily) OVER (ORDER BY a.report_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_new_positive,
    a.tested_count_daily AS tested,
    avg(a.tested_count_daily) OVER (ORDER BY a.report_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_tested,
    a.deceased_count_daily AS deceased,
    avg(a.deceased_count_daily) OVER (ORDER BY a.report_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_deceased,
    b.new_hospitalized,
    b.mov_avg_new_hospitalized,
    b.new_severe_progress,
    b.mov_avg_new_severe_progress,
    c.requests_primary_diagnosis,
    c.mov_avg_requests_primary_diagnosis,
        CASE
            WHEN a.report_date >= ((( SELECT max(v_uzis_covid19_cz.report_date) AS max
               FROM analytic.v_uzis_covid19_cz)) - '4 days'::interval) THEN true
            ELSE false
        END AS last_4_days
   FROM analytic.v_uzis_covid19_cz a
     LEFT JOIN ( SELECT v_uzis_covid19_hospitalized_daily.reporting_date,
            sum(v_uzis_covid19_hospitalized_daily.new_hospitalized) AS new_hospitalized,
            avg(sum(v_uzis_covid19_hospitalized_daily.new_hospitalized)) OVER (ORDER BY v_uzis_covid19_hospitalized_daily.reporting_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_new_hospitalized,
            sum(v_uzis_covid19_hospitalized_daily.new_severe_progress) AS new_severe_progress,
            avg(sum(v_uzis_covid19_hospitalized_daily.new_severe_progress)) OVER (ORDER BY v_uzis_covid19_hospitalized_daily.reporting_date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_new_severe_progress
           FROM analytic.v_uzis_covid19_hospitalized_daily
          GROUP BY v_uzis_covid19_hospitalized_daily.reporting_date) b ON a.report_date = b.reporting_date
     LEFT JOIN ( SELECT r.datum AS reporting_date,
            sum(r.primdg) AS requests_primary_diagnosis,
            avg(sum(r.primdg)) OVER (ORDER BY r.datum ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS mov_avg_requests_primary_diagnosis
           FROM uzis.covid19_pcr_requests r
          GROUP BY r.datum) c ON a.report_date = c.reporting_date;   
		  
-- analytic.v_uzis_covid19_pes_regional source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_pes_regional
AS SELECT p.datum,
    p.updated_at,
    p.kraj_kod,
    p.incidence14_100,
    p.incidence_65_14_100,
    p.simple_r,
    p.podil_zachycen_hosp,
    k."kraj-nazev" AS kraj_nazev
   FROM uzis.covid19_pes_regional p
     LEFT JOIN uzis.codebook_regions k ON p.kraj_kod = k."kraj-kod"
  WHERE p.datum >= '2021-01-05 00:00:00'::timestamp without time zone;		  
  
-- analytic.v_uzis_covid19_age_groups_prague source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_age_groups_prague
AS SELECT covid19_cz_details.datum_hlaseni AS report_date,
    covid19_cz_details.vek AS age,
        CASE
            WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
            WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
            WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
            WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
            WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
            WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
            ELSE '65 a víc'::text
        END AS age_group,
    covid19_cz_details.pohlavi AS gender,
        CASE
            WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN 1
            WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN 2
            WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN 3
            WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN 4
            WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN 5
            WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN 6
            ELSE 7
        END AS age_group_order,
        CASE
            WHEN covid19_cz_details.kraj = 'CZ010'::text THEN 'Hlavní město Praha'::text
            ELSE 'Středočeský kraj'::text
        END AS region
   FROM uzis.covid19_cz_details
  WHERE covid19_cz_details.kraj = ANY (ARRAY['CZ010'::text, 'CZ020'::text]);  
  
-- analytic.v_uzis_covid19_age_groups_prague_ratios source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_age_groups_prague_ratios
AS WITH age_groups AS (
         SELECT date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone) AS report_month,
                CASE
                    WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
                    WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
                    WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
                    WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
                    WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
                    WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
                    ELSE '65 a víc'::text
                END AS age_group,
            count(*) AS cases_count,
                CASE
                    WHEN covid19_cz_details.kraj = 'CZ010'::text THEN 'Hlavní město Praha'::text
                    ELSE 'Středočeský kraj'::text
                END AS region
           FROM uzis.covid19_cz_details
          WHERE covid19_cz_details.kraj = ANY (ARRAY['CZ010'::text, 'CZ020'::text])
          GROUP BY covid19_cz_details.kraj, (date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone)), (
                CASE
                    WHEN covid19_cz_details.vek::numeric <= 5::numeric THEN '0-5'::text
                    WHEN covid19_cz_details.vek::numeric >= 6::numeric AND covid19_cz_details.vek::numeric <= 9::numeric THEN '6-9'::text
                    WHEN covid19_cz_details.vek::numeric >= 10::numeric AND covid19_cz_details.vek::numeric <= 14::numeric THEN '10-14'::text
                    WHEN covid19_cz_details.vek::numeric >= 15::numeric AND covid19_cz_details.vek::numeric <= 19::numeric THEN '15-19'::text
                    WHEN covid19_cz_details.vek::numeric >= 20::numeric AND covid19_cz_details.vek::numeric <= 25::numeric THEN '20-25'::text
                    WHEN covid19_cz_details.vek::numeric >= 26::numeric AND covid19_cz_details.vek::numeric <= 64::numeric THEN '26-64'::text
                    ELSE '65 a víc'::text
                END)
        ), totals AS (
         SELECT date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone) AS report_month,
            count(*) AS cases_count,
                CASE
                    WHEN covid19_cz_details.kraj = 'CZ010'::text THEN 'Hlavní město Praha'::text
                    ELSE 'Středočeský kraj'::text
                END AS region
           FROM uzis.covid19_cz_details
          WHERE covid19_cz_details.kraj = ANY (ARRAY['CZ010'::text, 'CZ020'::text])
          GROUP BY covid19_cz_details.kraj, (date_trunc('month'::text, covid19_cz_details.datum_hlaseni::timestamp with time zone))
        )
 SELECT ag.report_month,
    ag.age_group,
    ag.cases_count::numeric / t.cases_count::numeric AS age_group_ratio,
        CASE
            WHEN ag.age_group = '0-5'::text THEN 1
            WHEN ag.age_group = '6-9'::text THEN 2
            WHEN ag.age_group = '10-14'::text THEN 3
            WHEN ag.age_group = '15-19'::text THEN 4
            WHEN ag.age_group = '20-25'::text THEN 5
            WHEN ag.age_group = '26-64'::text THEN 6
            ELSE 7
        END AS age_group_order,
    ag.region
   FROM age_groups ag
     JOIN totals t ON ag.report_month = t.report_month AND ag.region = t.region;  

-- analytic.v_uzis_covid19_prague_districts source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_prague_districts
AS SELECT uzis_covid19_prague_districts.datum,
        CASE
            WHEN uzis_covid19_prague_districts.mestskacast::text = 'Praha'::text THEN 'Nezařazeno'::character varying
            ELSE uzis_covid19_prague_districts.mestskacast
        END::character varying(50) AS mestskacast,
    uzis_covid19_prague_districts.kumulativni_pocet_pozitivnich_osob,
    uzis_covid19_prague_districts.kumulativni_pocet_zemrelych,
    uzis_covid19_prague_districts.kumulativni_pocet_vylecenych,
        CASE
            WHEN uzis_covid19_prague_districts.kumulativni_pocet_zemrelych IS NULL THEN 0::numeric
            ELSE uzis_covid19_prague_districts.kumulativni_pocet_zemrelych
        END -
        CASE
            WHEN lag(uzis_covid19_prague_districts.kumulativni_pocet_zemrelych) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum) IS NULL THEN 0::numeric
            ELSE lag(uzis_covid19_prague_districts.kumulativni_pocet_zemrelych) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum)
        END AS pocet_zemrelych_den,
    uzis_covid19_prague_districts.incidence AS pocet_pozitivnich_osob_den,
        CASE
            WHEN uzis_covid19_prague_districts.prevalence IS NULL THEN 0::numeric
            ELSE uzis_covid19_prague_districts.prevalence
        END -
        CASE
            WHEN lag(uzis_covid19_prague_districts.prevalence) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum) IS NULL THEN 0::numeric
            ELSE lag(uzis_covid19_prague_districts.prevalence) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum)
        END AS pocet_aktivnich_pripadu_den,
    uzis_covid19_prague_districts.prevalence AS aktualni_pocet_aktivnich_pripadu,
    uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob,
        CASE
            WHEN uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob IS NULL THEN 0::numeric
            ELSE uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob
        END -
        CASE
            WHEN lag(uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum) IS NULL THEN 0::numeric
            ELSE lag(uzis_covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY uzis_covid19_prague_districts.mestskacast ORDER BY uzis_covid19_prague_districts.datum)
        END AS pocet_hospitalizovanych_osob,
    round(uzis_covid19_prague_districts.podil_65__na_kumulativnim_poctu_pozitivnich * uzis_covid19_prague_districts.kumulativni_pocet_pozitivnich_osob::double precision) AS pocet_pozitivnich_65,
    round(uzis_covid19_prague_districts.podil_65__na_kumulativnim_poctu_hospitalizovanych * uzis_covid19_prague_districts.kumulativni_pocet_hospitalizovanych_osob::double precision) AS pocet_hospitalizovanych_65,
    round(uzis_covid19_prague_districts.podil_65__na_kumulativnim_poctu_zemrelych * uzis_covid19_prague_districts.kumulativni_pocet_zemrelych::double precision) AS pocet_zemrelych_65,
    uzis_covid19_prague_districts.casova_znamka AS posledni_aktualizace,
        CASE
            WHEN uzis_covid19_prague_districts.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS vcerejsi_den,
    uzis_covid19_prague_districts.kumulativni_pocet_hospitalizovanych_osob,
        CASE
            WHEN uzis_covid19_prague_districts.mestskacast::text = 'Praha'::text THEN '100'::text
            ELSE "right"(uzis_covid19_prague_districts.mestskacast::text, length(uzis_covid19_prague_districts.mestskacast::text) - "position"(uzis_covid19_prague_districts.mestskacast::text, ' '::text))
        END AS mestskacast_order,
    dp.population,
    uzis_covid19_prague_districts.kumulativni_pocet_zemrelych + uzis_covid19_prague_districts.kumulativni_pocet_vylecenych AS aktualni_pocet_neaktivnich_pripadu,
        CASE
            WHEN uzis_covid19_prague_districts.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN true
            ELSE false
        END AS last_7days,
        CASE
            WHEN uzis_covid19_prague_districts.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN true
            ELSE false
        END AS last_14days,
        CASE
            WHEN uzis_covid19_prague_districts.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN uzis_covid19_prague_districts.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM ( SELECT covid19_prague_districts.casova_znamka,
            covid19_prague_districts.datum,
            covid19_prague_districts.mckod,
            covid19_prague_districts.mestskacast,
            covid19_prague_districts.psc,
            covid19_prague_districts.opoukod,
            covid19_prague_districts.opounazev,
            covid19_prague_districts.orpkod,
            covid19_prague_districts.orpnazev,
            covid19_prague_districts.okreskod,
            covid19_prague_districts.okresnazev,
            covid19_prague_districts.krajkod,
            covid19_prague_districts.krajnazev,
            covid19_prague_districts.kumulativni_pocet_pozitivnich_osob,
            covid19_prague_districts.kumulativni_pocet_hospitalizovanych_osob,
            covid19_prague_districts.aktualni_pocet_hospitalizovanych_osob,
            covid19_prague_districts.zemreli_za_hospitalizace,
            covid19_prague_districts.incidence,
            covid19_prague_districts.kumulativni_pocet_vylecenych,
            covid19_prague_districts.kumulativni_pocet_zemrelych,
            covid19_prague_districts.prevalence,
            covid19_prague_districts.podil_65__na_kumulativnim_poctu_pozitivnich,
            covid19_prague_districts.podil_65__na_kumulativnim_poctu_hospitalizovanych,
            covid19_prague_districts.podil_65__na_aktualnim_poctu_hospitalizovanych,
            covid19_prague_districts.podil_65__na_incidenci,
            covid19_prague_districts.podil_65__na_kumulativnim_poctu_vylecenych,
            covid19_prague_districts.podil_65__na_kumulativnim_poctu_zemrelych,
            covid19_prague_districts.podil_65__na_prevalenci,
            row_number() OVER (PARTITION BY covid19_prague_districts.mckod, covid19_prague_districts.datum) AS rank
           FROM uzis.covid19_prague_districts) uzis_covid19_prague_districts
     LEFT JOIN uzis.population_prague_districts dp ON uzis_covid19_prague_districts.mestskacast::text = dp.district::text
  WHERE uzis_covid19_prague_districts.datum < date_trunc('day'::text, now()) AND uzis_covid19_prague_districts.rank = 1;	 

-- analytic.v_uzis_covid19_prague_hospitalized_positive source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_prague_hospitalized_positive
AS SELECT a.datum,
    sum(b.kumulativni_pocet_pozitivnich_osob) AS kumulativni_pocet_pozitivnich_osob,
    sum(a.kumulativni_pocet_hospitalizovanych_osob) AS kumulativni_pocet_hospitalizovanych_osob,
    sum(b.pocet_pozitivnich_osob_den) AS pocet_pozitivnich_osob_den,
    sum(a.pocet_hospitalizovanych_osob) AS pocet_hospitalizovanych_osob
   FROM analytic.v_uzis_covid19_prague_districts a
     JOIN analytic.v_uzis_covid19_prague_districts b ON b.datum = (a.datum - '10 days'::interval) AND a.mestskacast::text = b.mestskacast::text
  GROUP BY a.datum;  
  
-- analytic.v_uzis_covid19_hospital_capacity source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS luzka_standard_kyslik_kapacita_zaplnena,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS luzka_hfno_cpap_kapacita_zaplnena,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS luzka_upv_niv_kapacita_zaplnena,
    COALESCE(covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni, 0::double precision) AS luzka_kapacita_covid_pozitivni,
    COALESCE(covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni, 0::double precision) AS luzka_kapacita_covid_negativni,
    COALESCE(covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem, 0::double precision) AS luzka_kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_volna,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem - covid19_hospital_capacity_details.ecmo_kapacita_volna AS ecmo_kapacita_zaplnena,
    covid19_hospital_capacity_details.cvvhd_kapacita_volna,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem - covid19_hospital_capacity_details.cvvhd_kapacita_volna AS cvvhd_kapacita_zaplnena,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna AS ventilatory_prenosne_kapacita_zaplnena,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna AS ventilatory_operacni_sal_kapacita_zaplnena,
    COALESCE(covid19_hospital_capacity_details.ecmo_kapacita_volna, 0::double precision) + COALESCE(covid19_hospital_capacity_details.cvvhd_kapacita_volna, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna, 0::double precision) AS pristroje_kapacita_volna,
    COALESCE(covid19_hospital_capacity_details.ecmo_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.cvvhd_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem, 0::double precision) + COALESCE(covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem, 0::double precision) AS pristroje_kapacita_celkem
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;  
  
-- analytic.v_uzis_covid19_hospital_capacity_beds source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity_beds
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_celkem - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_standard_kyslik_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_standard_kyslik'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_celkem - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_hfno_cpap_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_hfno_cpap'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni AS kapacita_volna_covid_pozitivni_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS kapacita_volna_covid_negativni_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem AS kapacita_celkem_luzka,
    covid19_hospital_capacity_details.luzka_upv_niv_kapacita_celkem - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_pozitivni - covid19_hospital_capacity_details.luzka_upv_niv_kapacita_volna_covid_negativni AS kapacita_zaplnena_luzka,
    'luzka_upv_niv'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;  
  
-- analytic.v_uzis_covid19_hospital_capacity_technologies source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_hospital_capacity_technologies
AS SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.ecmo_kapacita_volna AS kapacita_volna,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem AS kapacita_celkem,
    covid19_hospital_capacity_details.ecmo_kapacita_celkem - covid19_hospital_capacity_details.ecmo_kapacita_volna AS kapacita_zaplnena,
    'ecmo'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.cvvhd_kapacita_volna AS kapacita_volna,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem AS kapacita_celkem,
    covid19_hospital_capacity_details.cvvhd_kapacita_celkem - covid19_hospital_capacity_details.cvvhd_kapacita_volna AS kapacita_zaplnena,
    'cvvhd'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna AS kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem AS kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_prenosne_kapacita_volna AS kapacita_zaplnena,
    'ventilatory_prenosne'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone
UNION ALL
 SELECT covid19_hospital_capacity_details.datum,
        CASE
            WHEN covid19_hospital_capacity_details.datum = max(covid19_hospital_capacity_details.datum) OVER (PARTITION BY covid19_hospital_capacity_details.zz_nazev) THEN true
            ELSE false
        END AS posledni_den,
    covid19_hospital_capacity_details.zz_nazev,
    covid19_hospital_capacity_details.kraj_nazev,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna AS kapacita_volna,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem AS kapacita_celkem,
    covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_celkem - covid19_hospital_capacity_details.ventilatory_operacni_sal_kapacita_volna AS kapacita_zaplnena,
    'ventilatory_operacni_sal'::text AS typ
   FROM uzis.covid19_hospital_capacity_details
  WHERE covid19_hospital_capacity_details.datum >= '2021-04-01 00:00:00'::timestamp without time zone;  

-- analytic.v_uzis_covid19_central_bohemia source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_central_bohemia
AS SELECT eo.datum,
    eo.okres_nazev,
    eo.kumulativni_pocet_pozitivnich_osob,
    eo.kumulativni_pocet_zemrelych,
    eo.kumulativni_pocet_vylecenych,
        CASE
            WHEN eo.kumulativni_pocet_zemrelych IS NULL THEN 0::numeric
            ELSE eo.kumulativni_pocet_zemrelych::numeric
        END -
        CASE
            WHEN lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)::numeric
        END AS pocet_zemrelych_den,
    eo.incidence AS pocet_pozitivnich_osob_den,
        CASE
            WHEN eo.prevalence IS NULL THEN 0::numeric
            ELSE eo.prevalence::numeric
        END -
        CASE
            WHEN lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)::numeric
        END AS pocet_aktivnich_pripadu_den,
    eo.prevalence AS aktualni_pocet_aktivnich_pripadu,
    eo.aktualni_pocet_hospitalizovanych_osob,
        CASE
            WHEN eo.aktualni_pocet_hospitalizovanych_osob IS NULL THEN 0::numeric::double precision
            ELSE eo.aktualni_pocet_hospitalizovanych_osob
        END -
        CASE
            WHEN lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric::double precision
            ELSE lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)
        END AS pocet_hospitalizovanych_osob,
    eo.casova_znamka AS posledni_aktualizace,
        CASE
            WHEN eo.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS vcerejsi_den,
    eo.kumulativni_pocet_hospitalizovanych_osob,
    dp.population,
    eo.kumulativni_pocet_zemrelych + eo.kumulativni_pocet_vylecenych AS aktualni_pocet_neaktivnich_pripadu,
        CASE
            WHEN eo.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN true
            ELSE false
        END AS last_7days,
        CASE
            WHEN eo.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN true
            ELSE false
        END AS last_14days,
        CASE
            WHEN eo.datum >= date_trunc('day'::text, now() - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN eo.datum >= date_trunc('day'::text, now() - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc,
    eo.opou_nazev,
    eo.opou_kod,
    eo.obec_kod
   FROM ( SELECT covid19_municipalities.casova_znamka,
            covid19_municipalities.datum,
            covid19_municipalities.obec_kod,
            covid19_municipalities.psc,
            covid19_municipalities.opou_kod,
            covid19_municipalities.opou_nazev,
            covid19_municipalities.orp_kod,
            covid19_municipalities.orp_nazev,
            covid19_municipalities.okres_kod,
            covid19_municipalities.okres_nazev,
            covid19_municipalities.kraj_kod,
            covid19_municipalities.kraj_nazev,
            covid19_municipalities.kumulativni_pocet_pozitivnich_osob,
            covid19_municipalities.kumulativni_pocet_hospitalizovanych_osob,
            covid19_municipalities.aktualni_pocet_hospitalizovanych_osob,
            covid19_municipalities.incidence,
            covid19_municipalities.pocet_vyleceni_den,
            covid19_municipalities.kumulativni_pocet_vylecenych,
            covid19_municipalities.pocet_zemreli_den,
            covid19_municipalities.zemreli_za_hospitalizace,
            covid19_municipalities.kumulativni_pocet_zemrelych,
            covid19_municipalities.prevalence,
            row_number() OVER (PARTITION BY covid19_municipalities.obec_kod, covid19_municipalities.datum) AS rank
           FROM uzis.covid19_municipalities
          WHERE covid19_municipalities.kraj_kod = 'CZ020'::text) eo
     LEFT JOIN uzis.population_central_bohemia dp ON eo.okres_kod = dp.region_code::text
  WHERE eo.datum < date_trunc('day'::text, now()) AND eo.okres_nazev <> 'Praha'::text AND eo.rank = 1;  
  
-- analytic.v_uzis_covid19_municipalities source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_municipalities
AS SELECT eo.datum,
    eo.kraj_kod,
    eo.kraj_nazev,
    eo.okres_kod,
    eo.okres_nazev,
    eo.obec_kod,
    eo.obec_nazev,
    population_regions.population AS pocet_obyvatel_kraj,
    population_districts.population AS pocet_obyvatel_okres,
    eo.kumulativni_pocet_pozitivnich_osob,
    eo.kumulativni_pocet_zemrelych,
    eo.kumulativni_pocet_vylecenych,
        CASE
            WHEN eo.kumulativni_pocet_zemrelych IS NULL THEN 0::numeric
            ELSE eo.kumulativni_pocet_zemrelych::numeric
        END -
        CASE
            WHEN lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.kumulativni_pocet_zemrelych) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)::numeric
        END AS pocet_zemrelych_den,
    eo.incidence AS pocet_pozitivnich_osob_den,
        CASE
            WHEN eo.prevalence IS NULL THEN 0::numeric
            ELSE eo.prevalence::numeric
        END -
        CASE
            WHEN lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric
            ELSE lag(eo.prevalence) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)::numeric
        END AS pocet_aktivnich_pripadu_den,
    eo.prevalence AS aktualni_pocet_aktivnich_pripadu,
    eo.aktualni_pocet_hospitalizovanych_osob,
        CASE
            WHEN eo.aktualni_pocet_hospitalizovanych_osob IS NULL THEN 0::numeric::double precision
            ELSE eo.aktualni_pocet_hospitalizovanych_osob
        END -
        CASE
            WHEN lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum) IS NULL THEN 0::numeric::double precision
            ELSE lag(eo.aktualni_pocet_hospitalizovanych_osob) OVER (PARTITION BY eo.obec_kod ORDER BY eo.datum)
        END AS pocet_hospitalizovanych_osob,
    eo.casova_znamka AS posledni_aktualizace,
        CASE
            WHEN eo.datum = (date_trunc('day'::text, now()) - '1 day'::interval) THEN true
            ELSE false
        END AS vcerejsi_den,
    eo.kumulativni_pocet_hospitalizovanych_osob,
    eo.kumulativni_pocet_zemrelych + eo.kumulativni_pocet_vylecenych AS aktualni_pocet_neaktivnich_pripadu,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '7 days'::interval) THEN true
            ELSE false
        END AS last_7days,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '14 days'::interval) THEN true
            ELSE false
        END AS last_14days,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '7 days'::interval) THEN 'Posledních 7 dní'::text
            ELSE NULL::text
        END AS last_7days_desc,
        CASE
            WHEN eo.datum >= (CURRENT_DATE - '14 days'::interval) THEN 'Posledních 14 dní'::text
            ELSE NULL::text
        END AS last_14days_desc
   FROM uzis.covid19_municipalities eo
     LEFT JOIN uzis.population_regions ON eo.kraj_kod = population_regions.region_code
     LEFT JOIN uzis.population_districts ON eo.okres_kod = population_districts.district_code
  WHERE eo.datum < CURRENT_DATE;  
  
-- analytic.v_uzis_covid19_tests_districts source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_tests_districts
AS SELECT eo.datum,
    eo.kraj_kod,
    eo.kraj_nazev,
    eo.okres_kod,
    eo.okres_nazev,
    eo.pocet_pozitivnich_osob_den,
    t.prirustkovy_pocet_testu_okres,
    t.prirustkovy_pocet_prvnich_testu_okres,
    t.kumulativni_pocet_testu_okres,
    t.kumulativni_pocet_prvnich_testu_okres,
    t.prirustkovy_pocet_testu_okres - t.prirustkovy_pocet_prvnich_testu_okres AS prirustkovy_pocet_naslednych_testu
   FROM ( SELECT eo_1.datum,
            eo_1.kraj_kod,
            eo_1.kraj_nazev,
            eo_1.okres_kod,
            eo_1.okres_nazev,
            sum(eo_1.incidence) AS pocet_pozitivnich_osob_den
           FROM uzis.covid19_municipalities eo_1
          WHERE eo_1.datum < CURRENT_DATE
          GROUP BY eo_1.datum, eo_1.kraj_kod, eo_1.kraj_nazev, eo_1.okres_kod, eo_1.okres_nazev) eo
     JOIN uzis.covid19_cz_tests_regional t ON eo.datum::date = t.datum::date AND eo.kraj_kod = t.kraj_nuts_kod AND eo.okres_kod = t.okres_lau_kod;
	 
-- analytic.v_uzis_covid19_pcr_requests source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_pcr_requests
AS SELECT cpr.datum,
    cpr.krajkod,
    cpr.kraj,
    cpr.pocet,
    cpr.samoplatcu,
    cpr.cizincu,
    cpr.primdg,
    cpr.kontrolni,
    cpr.preventivni,
    cpr.epidemilogicka,
    cpr.nemocnice,
    cpr.ostatni_luzkova,
    cpr.socialni_zarizeni,
    cpr.socialni_zarizeni1,
    cpr.praktici,
    cpr.specialiste,
    cpr.khs,
    cpr.updated_at,
    cpr.updated_by,
    cpr.created_at,
    cpr.created_by
   FROM uzis.covid19_pcr_requests cpr;	 
   
-- analytic.v_uzis_covid19_vaccination_logistics source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_logistics
AS SELECT s.datum,
    s.updated_at,
        CASE
            WHEN s.datum >= (CURRENT_DATE - '1 day'::interval) AND s.datum < CURRENT_DATE THEN true
            ELSE false
        END AS vcerejsi_den,
    s.ockovaci_misto_id,
    s.ockovaci_misto_nazev,
    s.kraj_nazev,
    s.ockovaci_latka,
    s.prijem,
    s.prijato_od_jinych_ocm,
    s.prijem + s.prijato_od_jinych_ocm AS prijem_celkem,
    s.vydej,
    s.prijem + s.prijato_od_jinych_ocm - s.vydej AS prijem_celkem_minus_vydej,
    s.pouzite_davky,
    s.znehodnocene_davky,
    sum(s.pouzite_davky) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS pouzito_kumulativni,
    sum(s.znehodnocene_davky) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS znehodnoceno_kumulativni,
    sum(s.prijem) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS prijem_kumulativni,
    sum(s.prijato_od_jinych_ocm) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS prijato_od_jinych_ocm_kumulativni,
    sum(s.prijem) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) + sum(s.prijato_od_jinych_ocm) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS prijato_celkem_kumulativni,
    sum(s.vydej) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS vydej_kumulativni,
    sum(s.prijem) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) + sum(s.prijato_od_jinych_ocm) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) - sum(s.vydej) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) - sum(s.pouzite_davky) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) - sum(s.znehodnocene_davky) OVER (PARTITION BY s.ockovaci_misto_id, s.ockovaci_latka ORDER BY s.datum) AS davek_k_dispozici
   FROM ( SELECT p.datum,
            p.updated_at,
            p.ockovaci_misto_id,
            p.ockovaci_misto_nazev,
            p.kraj_nazev,
                CASE
                    WHEN p.ockovaci_latka = 'Comirnaty'::text THEN 'Pfizer'::text
                    WHEN p.ockovaci_latka = 'VAXZEVRIA'::text THEN 'VAXZEVRIA/AstraZeneca'::text
                    WHEN p.ockovaci_latka = 'COVID-19 Vaccine Moderna'::text THEN 'Moderna'::text
                    ELSE p.ockovaci_latka
                END AS ockovaci_latka,
            COALESCE(u.pouzite_davky, 0::bigint) AS pouzite_davky,
            COALESCE(u.znehodnocene_davky, 0::bigint) AS znehodnocene_davky,
            COALESCE(d_prijato.prijato_od_jinych_ocm, 0::numeric) AS prijato_od_jinych_ocm,
            COALESCE(sum(d.pocet_davek) FILTER (WHERE d.akce = 'Příjem'::text), 0::numeric) AS prijem,
            COALESCE(sum(d.pocet_davek) FILTER (WHERE d.akce = 'Výdej'::text), 0::numeric) AS vydej
           FROM ( SELECT p_1.ockovaci_misto_id,
                    p_1.ockovaci_misto_nazev,
                    p_1.updated_at,
                    ck."kraj-nazev" AS kraj_nazev,
                    generate_series('2020-12-26 00:00:00'::timestamp without time zone, CURRENT_DATE::timestamp without time zone, '1 day'::interval) AS datum,
                    u_1.ockovaci_latka
                   FROM uzis.covid19_vaccination_points p_1
                     LEFT JOIN uzis.codebook_regions ck ON "left"(p_1.okres_nuts_kod, 5) = ck."kraj-kod"
                     CROSS JOIN unnest(ARRAY['Comirnaty'::text, 'VAXZEVRIA'::text, 'COVID-19 Vaccine Moderna'::text, 'COVID-19 Vaccine Janssen'::text]) WITH ORDINALITY u_1(ockovaci_latka, ordinality)) p
             LEFT JOIN uzis.covid19_vaccination_distribution d ON d.datum = p.datum AND d.ockovaci_misto_id = p.ockovaci_misto_id AND d.ockovaci_latka = p.ockovaci_latka
             LEFT JOIN ( SELECT covid19_vaccination_distribution.datum,
                    covid19_vaccination_distribution.cilove_ockovaci_misto_id,
                    covid19_vaccination_distribution.ockovaci_latka,
                    sum(covid19_vaccination_distribution.pocet_davek) AS prijato_od_jinych_ocm
                   FROM uzis.covid19_vaccination_distribution
                  WHERE covid19_vaccination_distribution.akce = 'Výdej'::text
                  GROUP BY covid19_vaccination_distribution.datum, covid19_vaccination_distribution.cilove_ockovaci_misto_id, covid19_vaccination_distribution.ockovaci_latka) d_prijato ON d_prijato.datum = p.datum AND d_prijato.cilove_ockovaci_misto_id = p.ockovaci_misto_id AND d_prijato.ockovaci_latka = p.ockovaci_latka
             LEFT JOIN uzis.covid19_vaccination_usage u ON p.datum = u.datum AND p.ockovaci_misto_id = u.ockovaci_misto_id AND p.ockovaci_latka = u.ockovaci_latka
          WHERE p.datum < CURRENT_DATE
          GROUP BY p.datum, p.updated_at, p.ockovaci_misto_id, p.ockovaci_misto_nazev, p.kraj_nazev, p.ockovaci_latka, u.pouzite_davky, u.znehodnocene_davky, d_prijato.prijato_od_jinych_ocm) s
  ORDER BY s.kraj_nazev, s.ockovaci_misto_nazev, s.datum, s.ockovaci_latka;   
  
-- analytic.v_uzis_covid19_population_age_regions source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_population_age_regions
AS SELECT sub.population,
    sub.region_code,
    sub.age_group,
    concat(sub.region_code, '_', sub.age_group) AS region_age_id
   FROM ( SELECT sum(cpar.population) AS population,
            cpar.region_code,
                CASE
                    WHEN cpar.age < 18 THEN '0-17'::text
                    WHEN cpar.age >= 18 AND cpar.age < 25 THEN '18-24'::text
                    WHEN cpar.age >= 25 AND cpar.age < 30 THEN '25-29'::text
                    WHEN cpar.age >= 30 AND cpar.age < 35 THEN '30-34'::text
                    WHEN cpar.age >= 35 AND cpar.age < 40 THEN '35-39'::text
                    WHEN cpar.age >= 40 AND cpar.age < 45 THEN '40-44'::text
                    WHEN cpar.age >= 45 AND cpar.age < 50 THEN '45-49'::text
                    WHEN cpar.age >= 50 AND cpar.age < 55 THEN '50-54'::text
                    WHEN cpar.age >= 55 AND cpar.age < 60 THEN '55-59'::text
                    WHEN cpar.age >= 60 AND cpar.age < 65 THEN '60-64'::text
                    WHEN cpar.age >= 65 AND cpar.age < 70 THEN '65-69'::text
                    WHEN cpar.age >= 70 AND cpar.age < 75 THEN '70-74'::text
                    WHEN cpar.age >= 75 AND cpar.age < 80 THEN '75-79'::text
                    WHEN cpar.age >= 80 THEN '80+'::text
                    ELSE NULL::text
                END AS age_group
           FROM uzis.population_age_regions cpar
          GROUP BY cpar.region_code, (
                CASE
                    WHEN cpar.age < 18 THEN '0-17'::text
                    WHEN cpar.age >= 18 AND cpar.age < 25 THEN '18-24'::text
                    WHEN cpar.age >= 25 AND cpar.age < 30 THEN '25-29'::text
                    WHEN cpar.age >= 30 AND cpar.age < 35 THEN '30-34'::text
                    WHEN cpar.age >= 35 AND cpar.age < 40 THEN '35-39'::text
                    WHEN cpar.age >= 40 AND cpar.age < 45 THEN '40-44'::text
                    WHEN cpar.age >= 45 AND cpar.age < 50 THEN '45-49'::text
                    WHEN cpar.age >= 50 AND cpar.age < 55 THEN '50-54'::text
                    WHEN cpar.age >= 55 AND cpar.age < 60 THEN '55-59'::text
                    WHEN cpar.age >= 60 AND cpar.age < 65 THEN '60-64'::text
                    WHEN cpar.age >= 65 AND cpar.age < 70 THEN '65-69'::text
                    WHEN cpar.age >= 70 AND cpar.age < 75 THEN '70-74'::text
                    WHEN cpar.age >= 75 AND cpar.age < 80 THEN '75-79'::text
                    WHEN cpar.age >= 80 THEN '80+'::text
                    ELSE NULL::text
                END)) sub;  
				
-- analytic.v_uzis_covid19_vaccination_prague_districts source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_prague_districts
AS SELECT v.mestska_cast,
    p.spravni_obvod,
    v.vekova_skupina,
    p.populace,
    count(*) FILTER (WHERE v.poradi_davky = 1) AS prvni_davky,
    count(*) FILTER (WHERE v.poradi_davky = 2) AS druhe_davky,
    count(*) FILTER (WHERE v.poradi_davky = 1)::double precision / p.populace::double precision AS prvni_davky_podil,
    count(*) FILTER (WHERE v.poradi_davky = 2)::double precision / p.populace::double precision AS druhe_davky_podil
   FROM uzis.covid19_vaccination_prague_details v
     LEFT JOIN ( SELECT population_prague_age_districts.mestska_cast,
            population_prague_age_districts.spravni_obvod,
                CASE
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['0-4'::text, '5-9'::text, '10-14'::text, '15-19'::text]) THEN '0-17'::text
                    WHEN population_prague_age_districts.vekova_skupina = '20-24'::text THEN '18-24'::text
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['80-84'::text, '85'::text]) THEN '80+'::text
                    ELSE population_prague_age_districts.vekova_skupina
                END AS vekova_skupina,
            sum(population_prague_age_districts.populace) AS populace
           FROM uzis.population_prague_age_districts
          GROUP BY population_prague_age_districts.mestska_cast, population_prague_age_districts.spravni_obvod, (
                CASE
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['0-4'::text, '5-9'::text, '10-14'::text, '15-19'::text]) THEN '0-17'::text
                    WHEN population_prague_age_districts.vekova_skupina = '20-24'::text THEN '18-24'::text
                    WHEN population_prague_age_districts.vekova_skupina = ANY (ARRAY['80-84'::text, '85'::text]) THEN '80+'::text
                    ELSE population_prague_age_districts.vekova_skupina
                END)) p ON v.mestska_cast = p.mestska_cast AND v.vekova_skupina = p.vekova_skupina
  GROUP BY v.mestska_cast, p.spravni_obvod, v.vekova_skupina, p.populace;				
  
-- analytic.v_uzis_covid19_vaccination_regions_details source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_regions_details
AS SELECT count(*) AS pocet_davek,
    v.datum_vakcinace,
        CASE
            WHEN v.datum_vakcinace >= (CURRENT_DATE - '7 days'::interval) AND v.datum_vakcinace < CURRENT_DATE THEN 'posledních 7 dnů'::text
            WHEN v.datum_vakcinace >= (CURRENT_DATE - '14 days'::interval) AND v.datum_vakcinace < (CURRENT_DATE - '7 days'::interval) THEN 'predchozích 7 dnů'::text
            ELSE NULL::text
        END AS "7_14_dnu",
    date_trunc('week'::text, v.datum_vakcinace)::date AS tyden,
        CASE
            WHEN date_trunc('week'::text, v.datum_vakcinace)::date = date_trunc('week'::text, CURRENT_DATE::timestamp with time zone)::date THEN 'posledni nedokonceny tyden'::text
            ELSE 'cely tyden'::text
        END AS tyden_filter_posledni,
    v.updated_at AS datum_aktualizace,
        CASE
            WHEN v.vakcina = 'COVID-19 Vaccine Moderna'::text THEN 'Moderna'::text
            WHEN v.vakcina = 'Comirnaty'::text THEN 'Pfizer/BioNTech'::text
            ELSE v.vakcina
        END AS vakcina,
    v.vekova_skupina,
        CASE
            WHEN v.vekova_skupina = '0-17'::text THEN 1
            WHEN v.vekova_skupina = '18-24'::text THEN 2
            WHEN v.vekova_skupina = '25-29'::text THEN 3
            WHEN v.vekova_skupina = '30-34'::text THEN 4
            WHEN v.vekova_skupina = '35-39'::text THEN 5
            WHEN v.vekova_skupina = '40-44'::text THEN 6
            WHEN v.vekova_skupina = '45-49'::text THEN 7
            WHEN v.vekova_skupina = '50-54'::text THEN 8
            WHEN v.vekova_skupina = '55-59'::text THEN 9
            WHEN v.vekova_skupina = '60-64'::text THEN 10
            WHEN v.vekova_skupina = '65-69'::text THEN 11
            WHEN v.vekova_skupina = '70-74'::text THEN 12
            WHEN v.vekova_skupina = '75-79'::text THEN 13
            WHEN v.vekova_skupina = '80+'::text THEN 14
            WHEN v.vekova_skupina = 'Neznámá'::text THEN 15
            ELSE 15
        END AS vekova_skupina_order,
        CASE
            WHEN v.indikace_zdravotnik = 1 THEN 'Zdravotníci'::text
            WHEN v.indikace_socialni_sluzby = 1 THEN 'Socilání služby'::text
            WHEN v.indikace_pedagog = 1 THEN 'Pedagogové'::text
            WHEN v.indikace_skolstvi_ostatni = 1 THEN 'Školství ostatní'::text
            WHEN v.chronicke_onemocneni = 1 THEN 'Chronicky nemocní'::text
            WHEN v.bezpecnostni_infrastruktura = 1 THEN 'Bezpečnostní infrastruktura'::text
            ELSE NULL::text
        END AS indikacni_skupina,
    v.kraj_nazev AS kraj_ockovani,
        CASE
            WHEN c.kraj_nazev IS NULL THEN 'Neznámý'::text
            ELSE c.kraj_nazev
        END AS kraj_bydliste,
    v.pohlavi,
    v.poradi_davky,
    v.zarizeni_nazev,
    v.zrizovatel,
        CASE
            WHEN v.zrizovatel = ANY (ARRAY['MZ'::text, 'Ostatní centrální orgány'::text]) THEN 2
            ELSE 1
        END AS zrizovatel_order,
    concat(v.kraj_kod, '_', v.vekova_skupina) AS kraj_ockovani_vek_id,
    concat(c.kraj_kod, '_', v.vekova_skupina) AS kraj_bydliste_vek_id
   FROM uzis.covid19_vaccination_regional_details v
     LEFT JOIN uzis.codebook_regions_orps c ON v.orp_bydliste = c.orp_nazev
  GROUP BY v.datum_vakcinace, v.updated_at, v.vakcina, v.vekova_skupina, v.kraj_nazev, c.kraj_nazev, v.pohlavi, v.poradi_davky, v.zarizeni_nazev, v.zrizovatel, v.kraj_kod, c.kraj_kod, (
        CASE
            WHEN v.indikace_zdravotnik = 1 THEN 'Zdravotníci'::text
            WHEN v.indikace_socialni_sluzby = 1 THEN 'Socilání služby'::text
            WHEN v.indikace_pedagog = 1 THEN 'Pedagogové'::text
            WHEN v.indikace_skolstvi_ostatni = 1 THEN 'Školství ostatní'::text
            WHEN v.chronicke_onemocneni = 1 THEN 'Chronicky nemocní'::text
            WHEN v.bezpecnostni_infrastruktura = 1 THEN 'Bezpečnostní infrastruktura'::text
            ELSE NULL::text
        END);  
		
-- analytic.v_uzis_covid19_vaccination_regions_vaccination_points source
CREATE OR REPLACE VIEW analytic.v_uzis_covid19_vaccination_regions_vaccination_points
AS SELECT v.zarizeni_kod,
        CASE
            WHEN (v.zarizeni_kod IN ( SELECT DISTINCT covid19_vaccination_points.nrpzs_kod
               FROM uzis.covid19_vaccination_points)) THEN 'Registrované očkovací místo'::text
            ELSE 'Ostatní'::text
        END AS typ_mista,
    v.zarizeni_nazev,
    v.kraj_nazev,
    count(*) FILTER (WHERE v.poradi_davky = 1) AS pocet_prvnich_davek,
    count(*) FILTER (WHERE v.poradi_davky = 2) AS pocet_druhych_davek,
    count(*) AS pocet_davek_celkem,
    count(*) FILTER (WHERE v.datum_vakcinace >= (CURRENT_DATE - '7 days'::interval) AND v.datum_vakcinace < CURRENT_DATE) AS pocet_davek_poslednich_7_dnu,
    count(*) FILTER (WHERE v.datum_vakcinace >= (CURRENT_DATE - '14 days'::interval) AND v.datum_vakcinace < (CURRENT_DATE - '7 days'::interval)) AS pocet_davek_predchozich_7_dnu,
    count(*) FILTER (WHERE v.vakcina = 'Comirnaty'::text) AS pocet_davek_pfizer,
    count(*) FILTER (WHERE v.vakcina = 'COVID-19 Vaccine Moderna'::text) AS pocet_davek_moderna,
    count(*) FILTER (WHERE v.vakcina = 'Comirnaty'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_pfizer,
    count(*) FILTER (WHERE v.vakcina = 'COVID-19 Vaccine Moderna'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_moderna,
    count(*) FILTER (WHERE v.vakcina = 'VAXZEVRIA'::text AND v.poradi_davky = 1) AS pocet_prvnich_davek_astra,
    count(*) FILTER (WHERE v.vakcina = 'Comirnaty'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_pfizer,
    count(*) FILTER (WHERE v.vakcina = 'COVID-19 Vaccine Moderna'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_moderna,
    count(*) FILTER (WHERE v.vakcina = 'VAXZEVRIA'::text AND v.poradi_davky = 2) AS pocet_druhych_davek_astra,
    count(*) FILTER (WHERE v.pohlavi = 'F'::text) AS pocet_davek_zeny,
    count(*) FILTER (WHERE v.pohlavi = 'M'::text) AS pocet_davek_muzi,
    count(*) FILTER (WHERE v.vekova_skupina = '80+'::text) AS pocet_davek_80_plus,
    count(*) FILTER (WHERE v.kraj_nazev <> c.kraj_nazev) AS pocet_davek_mimo_kraj_bydliste
   FROM uzis.covid19_vaccination_regional_details v
     LEFT JOIN uzis.codebook_regions_orps c ON v.orp_bydliste = c.orp_nazev
  GROUP BY v.zarizeni_kod, v.zarizeni_nazev, v.kraj_nazev;		