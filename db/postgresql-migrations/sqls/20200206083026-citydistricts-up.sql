/* Replace with your SQL commands */

INSERT INTO meta.dataset(
	--	id_dataset, --nevyplňovat - autoincrement
	code_dataset, 
	name_dataset, 
	description, 
	retention_days, 
	h_retention_days, 
	created_at
	)
	VALUES (
	--id_dataset, 
	'common_tables',		--code_dataset, 
	'common_tables',	--name_dataset, 
	'obecné číselníky, společné tabulky',	--descr, 
	null,			--retention_days, 
	null,			--h_retention_days, 
	now() 			--created_at
	)
ON CONFLICT DO NOTHING;

CREATE TABLE public.citydistricts (
	--id serial NOT NULL,
	geom geometry(POLYGON, 4326) NULL,
	objectid int4 NULL,		-- "OBJECTID"
	create_date varchar NULL,		-- "DAT_VZNIK"
	chnage_date varchar NULL,		-- "DAT_ZMENA"
	area float8 NULL,		-- "PLOCHA"
	id int4 NULL,		-- "ID"
	zip int4 NULL,		-- "KOD_MC"
	district_name varchar NULL,		-- "NAZEV_MC"
	kod_mo int4 NULL,		-- "KOD_MO"
	kod_so varchar NULL,		-- "KOD_SO"
	tid_tm_district_p int4 NULL,		-- "TID_TMMESTSKECASTI_P"
	provider varchar NULL,		-- "POSKYT"
	id_provider int4 NULL,		-- "ID_POSKYT"
	chnage_statut varchar NULL,		-- "STAV_ZMENA"
	name_1 varchar NULL,		-- "NAZEV_1"
	shape_length float8 NULL,		-- "Shape_Length"
	shape_area float8 NULL,		-- "Shape_Area"
	CONSTRAINT citydistricts_pkey PRIMARY KEY (id)
);
CREATE INDEX sidx_citydistricts_geom ON public.citydistricts USING gist (geom);

-- zaevidování do meta.extract
INSERT INTO meta."extract"(
	name_extract, 
	id_dataset, 
	id_db, 
	description, 
	schema_extract, 
	retention_days, 
	h_retention_days, 
	created_at)
	
	VALUES (
	'citydistricts',		-- name_extract, 
	(select id_dataset from meta.dataset where code_dataset = 'common_tables'),		--;id_dataset, 
	1,		--	1 - postgres, 2 mongodb_db	id_db, 
	'Seznam městských částí Prahy',	--description, 
	'public',	--schema_extract, 
	null,	--retention_days, 
	null,	--h_retention_days, 
	now());
--------------------------------------------------------------------------------------
