DROP VIEW analytic.v_camea_bikecounters_24h_missing;

DROP VIEW analytic.v_camea_bikecounters_all_data_history;

DROP VIEW analytic.v_camea_bikecounters_all_data_table_today;

DROP VIEW analytic.v_camea_bikecounters_all_data_today;

DROP VIEW analytic.v_camea_bikecounters_data_disbalance;

DROP VIEW analytic.v_camea_bikecounters_data_quality;

DROP VIEW analytic.v_camea_bikecounters_last_update;

DROP VIEW analytic.v_camea_bikecounters_name_table;

DROP VIEW analytic.v_camea_data_disbalance;

DROP VIEW analytic.v_camea_data_quality;

DROP VIEW analytic.v_camea_last_update;

-- DROP VIEW IF EXISTS analytic.v_bicyclecounters_24h_missing; done manually bcs it is missing in migrations

DROP TABLE analytic.camea_bikecounters_in_contract;
