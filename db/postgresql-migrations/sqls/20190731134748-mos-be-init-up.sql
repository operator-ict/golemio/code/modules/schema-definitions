CREATE TABLE public.mos_be_accounts
(
  "count" integer,
  "day" character varying(255) NOT NULL,

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),
  CONSTRAINT mos_be_accounts_pkey PRIMARY KEY ("day")
)
WITH (
  OIDS=FALSE
);

CREATE TABLE public.mos_be_coupons
(
  coupon_custom_status_id integer,
  coupon_id integer NOT NULL,
  created character varying(255),
  customer_id integer,
  customer_profile_name character varying(255),
  "description" text,
  price numeric,
  seller_id integer,
  tariff_id integer,
  tariff_int_name character varying(255),
  tariff_name character varying(255),
  tariff_profile_name character varying(255),
  valid_from character varying(255),
  valid_till character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),
  CONSTRAINT mos_be_coupons_pkey PRIMARY KEY (coupon_id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE public.mos_be_customers
(
  customer_id integer NOT NULL,
  date_of_birth character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),
  CONSTRAINT mos_be_customers_pkey PRIMARY KEY (customer_id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE public.mos_be_zones
(
  coupon_id integer NOT NULL,
  zone_name character varying(255),

  create_batch_id bigint,
  created_at timestamp with time zone,
  created_by character varying(150),
  update_batch_id bigint,
  updated_at timestamp with time zone,
  updated_by character varying(150),
  CONSTRAINT mos_be_zones_pkey PRIMARY KEY (coupon_id)
)
WITH (
  OIDS=FALSE
);
