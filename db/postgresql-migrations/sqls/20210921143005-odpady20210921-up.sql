create or replace view analytic.v_containers_measurements as
select container_code,
        station_code,
        measured_at_utc,
        measured_at_utc::date as measured_at_utc_date,
        split_part(measured_at_utc::varchar(50) , ' ', 2) as measured_at_utc_time,
        percent_calculated,
        (coalesce(percent_calculated::decimal, 0)) / 100 as percent_calculated_decimal
from public.containers_measurement
where container_code in (select distinct sensoneo_code from containers_containers where sensoneo_code is not null and sensoneo_code != code);
