ALTER TABLE "public"."vehiclepositions_positions"
    DROP COLUMN "this_stop_sequence";

ALTER TABLE "public"."vehiclepositions_stops"
    DROP COLUMN "stop_id",
    DROP COLUMN "stop_sequence",
    DROP COLUMN "arrival_timestamp_real",
    DROP COLUMN "departure_timestamp_real";

ALTER TABLE "public"."vehiclepositions_trips"
    DROP COLUMN "is_canceled",
    DROP COLUMN "end_timestamp";

DROP MATERIALIZED VIEW "public"."v_ropidgtfs_trips_minmaxsequences";
CREATE MATERIALIZED VIEW "public"."v_ropidgtfs_trips_minmaxsequences" AS 
    SELECT ropidgtfs_stop_times.trip_id,
        max(ropidgtfs_stop_times.stop_sequence) AS max_stop_sequence,
        min(ropidgtfs_stop_times.stop_sequence) AS min_stop_sequence
    FROM ropidgtfs_stop_times
    GROUP BY ropidgtfs_stop_times.trip_id;