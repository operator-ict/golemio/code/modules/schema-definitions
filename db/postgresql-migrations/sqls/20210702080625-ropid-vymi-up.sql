CREATE SEQUENCE public.ropidvymi_events_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE SEQUENCE public.ropidvymi_events_routes_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE SEQUENCE public.ropidvymi_events_stops_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE public.ropidvymi_events
(
    id int NOT NULL UNIQUE DEFAULT nextval('ropidvymi_events_id_seq'::regclass),
    vymi_id int NOT NULL,
    vymi_id_dtb int NOT NULL,
    "state" int NOT NULL,
    pa_01 varchar(150) NOT NULL,
    pa_02 varchar(150) NOT NULL,
    pa_03 varchar(150) NOT NULL,
    pa_04 varchar(150) NOT NULL,
    record_type int NOT NULL,
    event_type bigint,
    channels int,
    title varchar(255),
    time_from TIMESTAMP WITH TIME ZONE NOT NULL,
    urgency_type int NOT NULL,
    time_to TIMESTAMP WITH TIME ZONE,
    expiration_date TIMESTAMP WITH TIME ZONE,
    transportation_type int NOT NULL,
    "priority" int,
    cause varchar(1000),
    link varchar(255),
    ropid_action varchar(100000),
    dpp_action varchar(100000),
    "description" varchar(1000),

    -- audit fields
    create_batch_id bigint,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id bigint,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT "ropidvymi_events_pkey" PRIMARY KEY (id)
);

CREATE TABLE public.ropidvymi_events_routes
(
    id int NOT NULL UNIQUE DEFAULT nextval('ropidvymi_events_routes_id_seq'::regclass),
    event_id int NOT NULL,
    gtfs_route_id varchar(255),
    vymi_id int NOT NULL,
    vymi_id_dtb int NOT NULL,
    "number" int NOT NULL,
    "name" varchar(255) NOT NULL,
    route_type int NOT NULL,
    valid_from TIMESTAMP WITH TIME ZONE,
    valid_to TIMESTAMP WITH TIME ZONE,
    "text" varchar(10000),

    -- audit fields
    create_batch_id bigint,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id bigint,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT "ropidvymi_events_routes_pkey" PRIMARY KEY (id),
    CONSTRAINT "ropidvymi_events_routes_fkey" FOREIGN KEY (event_id)
        REFERENCES public.ropidvymi_events (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

CREATE TABLE public.ropidvymi_events_stops
(
    id int NOT NULL UNIQUE DEFAULT nextval('ropidvymi_events_stops_id_seq'::regclass),
    event_id int NOT NULL,
    gtfs_stop_id varchar(255),
    vymi_id int NOT NULL,
    vymi_id_dtb int NOT NULL,
    node_number int NOT NULL,
    stop_number int NOT NULL,
    stop_type int NOT NULL,
    valid_from TIMESTAMP WITH TIME ZONE,
    valid_to TIMESTAMP WITH TIME ZONE,
    "text" varchar(10000),

    -- audit fields
    create_batch_id bigint,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id bigint,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT "ropidvymi_events_stops_pkey" PRIMARY KEY (id),
    CONSTRAINT "ropidvymi_events_stops_fkey" FOREIGN KEY (event_id)
        REFERENCES public.ropidvymi_events (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

CREATE INDEX ropidvymi_events_routes_idx
    ON public.ropidvymi_events_routes
    USING btree (event_id, gtfs_route_id);

CREATE INDEX ropidvymi_events_stops_idx
    ON public.ropidvymi_events_stops
    USING btree (event_id, gtfs_stop_id);
