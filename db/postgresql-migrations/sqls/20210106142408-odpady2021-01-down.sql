drop view analytic.v_containers_next_pick;
drop VIEW analytic.v_containers_5d_same;
drop FUNCTION analytic.container_last_correct;

drop view analytic.v_containers_24h_missing;
drop VIEW analytic.v_containers_full_share;

drop FUNCTION analytic.set_containers_full_hour;
drop table public.containers_full_hour;

drop VIEW analytic.v_containers_full;
drop view analytic.v_containers_picks;
drop view analytic.v_containers_measurements;
drop view analytic.v_containers_containers;

drop table analytic.containers_districts;