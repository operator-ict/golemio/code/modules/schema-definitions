CREATE OR REPLACE VIEW analytic.v_mos_coupons_all_channels
AS
with
mobapp as (
	SELECT 
		mbc.created::date AS on_date,
	    count(*) AS count_mobapp_by_card,
	    sum(mbc.price) AS price_mobapp_by_card
	FROM public.mos_be_coupons mbc
    WHERE mbc.order_payment_type = 1 AND mbc.order_status = 2 AND mbc.seller_id = 1 AND coalesce(mbc.created_by_id::text,'') = '188'::text
	GROUP BY (mbc.created::date)
--	ORDER BY (tbl.created::date)
)
,
eshop_transfer as (
	SELECT 
		mbc.created::date AS on_date,
	    count(*) AS count_eshop_by_transfer,
    	sum(mbc.price) AS price_eshop_by_transfer
	FROM PUBLIC.mos_be_coupons mbc
    WHERE mbc.order_payment_type = 2 AND mbc.order_status = 3 AND mbc.seller_id = 1 AND coalesce(mbc.created_by_id::text,'') <> '188'::text
  	GROUP BY (mbc.created::date)
),
eshop_card as  (
	SELECT 
		mbc.created::date AS on_date,
	    count(*) AS count_eshop_by_card,
    	sum(mbc.price) AS price_eshop_by_card
	FROM PUBLIC.mos_be_coupons mbc
	WHERE mbc.order_payment_type = 1 AND mbc.order_status = 2 AND mbc.seller_id = 1 AND coalesce(mbc.created_by_id::text,'') <> '188'::text
  	GROUP BY (mbc.created::date)
),
counter_cash as  (
	SELECT 
		mbc.created::date AS on_date,
	    count(*) AS count_counter_by_cash,
    	sum(mbc.price) AS price_counter_by_cash
	FROM PUBLIC.mos_be_coupons mbc
	WHERE mbc.order_payment_type = 4 AND mbc.order_status = 5 AND mbc.seller_id = 1 AND coalesce(mbc.created_by_id::text,'') <> '188'::text
  	GROUP BY (mbc.created::date)
),
counter_card as  (
	SELECT 
		mbc.created::date AS on_date,
	    count(*) AS count_counter_by_card,
    	sum(mbc.price) AS price_counter_by_card
	FROM PUBLIC.mos_be_coupons mbc
	WHERE mbc.order_payment_type = 3 AND mbc.order_status = 2 AND mbc.seller_id = 1 AND coalesce(mbc.created_by_id::text,'') <> '188'::text
  	GROUP BY (mbc.created::date)
),
calendar as (
	SELECT generate_series(
		min(mbc.created)::date::timestamp with time zone, 
		now()::date::timestamp with time zone - interval '1 day', '1 day'::interval)::date AS on_date
	from public.mos_be_coupons mbc 
)
SELECT calendar.on_date,--mobappcard.on_date,
    COALESCE(eshcard.count_eshop_by_card, 0::bigint) AS count_eshop_by_card,
    COALESCE(eshcard.price_eshop_by_card, 0::numeric) AS price_eshop_by_card,
    COALESCE(eshtrans.count_eshop_by_transfer, 0::bigint) AS count_eshop_by_transfer,
    COALESCE(eshtrans.price_eshop_by_transfer, 0::numeric) AS price_eshop_by_transfer,
    COALESCE(ctcard.count_counter_by_card, 0::bigint) AS count_counter_by_card,
    COALESCE(ctcard.price_counter_by_card, 0::numeric) AS price_counter_by_card,
    COALESCE(ctcash.count_counter_by_cash, 0::bigint) AS count_counter_by_cash,
    COALESCE(ctcash.price_counter_by_cash, 0::numeric) AS price_counter_by_cash,
    COALESCE(mobappcard.count_mobapp_by_card, 0::bigint) AS count_mobapp_by_card,
    COALESCE(mobappcard.price_mobapp_by_card, 0::numeric) AS price_mobapp_by_card,
    COALESCE(eshcard.price_eshop_by_card, 0::numeric) + COALESCE(eshtrans.price_eshop_by_transfer, 0::numeric) + COALESCE(ctcard.price_counter_by_card, 0::numeric) + COALESCE(ctcash.price_counter_by_cash, 0::numeric) + COALESCE(mobappcard.price_mobapp_by_card, 0::numeric) AS price_all_channels
   FROM  calendar
   	 full join counter_card ctcard USING (on_date)
     FULL JOIN counter_cash ctcash USING (on_date)
     FULL JOIN eshop_card eshcard USING (on_date)
     FULL JOIN eshop_transfer eshtrans USING (on_date)
     FULL JOIN mobapp mobappcard USING (on_date);
     