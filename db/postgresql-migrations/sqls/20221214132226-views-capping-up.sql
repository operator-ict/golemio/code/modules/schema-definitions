DROP VIEW analytic.v_ropidbi_capping;
DROP VIEW analytic.v_ropidbi_capping_more_rides;

CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_agregation
AS SELECT count(DISTINCT m.account_id) AS dis_acounts,
    count(m.account_id) AS accounts,
    m.cptp,
    date_trunc('month'::text, a.date) AS date_month,
    date_part('hour'::text, a.date) AS hour,
    p.active_users,
    count(m.ticket_id) AS nu_tickets
   FROM mos_ma_ticketpurchases m
     JOIN mos_ma_ticketactivations a ON m.ticket_id = a.ticket_id
     LEFT JOIN ( SELECT DISTINCT v.account_id,
            v.active_users
           FROM ( SELECT  m_1.account_id,
                        CASE
                            WHEN m_1.cptp = 124 OR m_1.cptp = 624 THEN 'active'::text
                            ELSE 'non active'::text
                        END AS active_users
                   FROM mos_ma_ticketpurchases m_1
                     JOIN mos_ma_ticketactivations a_1 ON m_1.ticket_id = a_1.ticket_id
                  WHERE a_1.date >= '2022-04-01 00:00:00+02'::timestamp with time zone) v
          WHERE v.active_users = 'active'::text) p ON m.account_id::text = p.account_id::text
  WHERE a.date >= '2022-04-01 00:00:00+02'::timestamp with time zone
  GROUP BY (date_trunc('month'::text, a.date)), (date_part('hour'::text, a.date)), m.cptp, p.active_users;

CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_daily_counts
AS SELECT a.date::date AS date,
    mmt.cptp,
    count(mmt.ticket_id) AS nu_tickets,
    count(DISTINCT mmt.account_id) AS nu_accounts
   FROM mos_ma_ticketpurchases mmt
     JOIN mos_ma_ticketactivations a ON mmt.ticket_id = a.ticket_id
  WHERE a.date >= '2022-04-01 00:00:00+02'::timestamp with time zone AND (mmt.cptp = 124 OR mmt.cptp = 624)
  GROUP BY (a.date::date), mmt.cptp
  ORDER BY (a.date::date) DESC;

CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_distinct_accounts
AS SELECT mmt.cptp,
    date_trunc('month'::text, a.date) AS datum_mesic,
    mmt.account_id
   FROM mos_ma_ticketpurchases mmt
     JOIN mos_ma_ticketactivations a ON mmt.ticket_id = a.ticket_id
  WHERE a.date >= '2022-04-01 00:00:00+02'::timestamp with time zone AND (mmt.cptp = 124 OR mmt.cptp = 624)
  GROUP BY (date_trunc('month'::text, a.date)), mmt.account_id, mmt.cptp;

CREATE OR REPLACE VIEW analytic.v_ropidbi_capping_more_rides
AS SELECT v.date::date AS date,
    v.cptp,
    v.account_id,
    v.n_tickets
   FROM ( SELECT mmt.cptp,
            count(mmt.ticket_id) AS n_tickets,
            mmt.account_id,
            a.date,
            row_number() OVER (PARTITION BY (a.date::date), mmt.account_id) AS row_number
           FROM mos_ma_ticketpurchases mmt
             JOIN mos_ma_ticketactivations a ON mmt.ticket_id = a.ticket_id
          WHERE (mmt.cptp = 124 OR mmt.cptp = 624) AND mmt.date >= '2022-04-01 00:00:00+02'::timestamp with time zone
          GROUP BY mmt.account_id, a.date, mmt.cptp) v
  WHERE v.row_number = 1;

