-- Table: public.vehiclepositions_positions

CREATE TABLE public.vehiclepositions_positions
(
  created timestamp with time zone,
  delay integer,
  delay_stop_arrival integer,
  delay_stop_departure integer,
  gtfs_next_stop_id character varying(255),
  gtfs_shape_dist_traveled numeric,
  is_canceled boolean,
  lat numeric,
  lng numeric,
  origin_time time without time zone,
  origin_timestamp timestamp with time zone,
  tracking integer,
  trips_id character varying(255)
)
WITH (
  OIDS=FALSE
);

-- Index: public.vehiclepositions_positions_origin_time

CREATE INDEX vehiclepositions_positions_origin_time
  ON public.vehiclepositions_positions
  USING btree
  (origin_time);

-- Index: public.vehiclepositions_positions_trips_id

CREATE INDEX vehiclepositions_positions_trips_id
  ON public.vehiclepositions_positions
  USING btree
  (trips_id COLLATE pg_catalog."default");

-- Table: public.vehiclepositions_stops

CREATE TABLE public.vehiclepositions_stops
(
  arrival_time time without time zone,
  arrival_timestamp timestamp with time zone,
  cis_stop_id integer NOT NULL,
  cis_stop_platform_code character varying(255) NOT NULL,
  cis_stop_sequence integer NOT NULL,
  created timestamp with time zone,
  delay_arrival integer,
  delay_departure integer,
  delay_type integer,
  departure_time time without time zone,
  departure_timestamp timestamp with time zone,
  modified timestamp with time zone,
  trips_id character varying(255) NOT NULL,
  CONSTRAINT vehiclepositions_stops_pkey PRIMARY KEY (cis_stop_id, cis_stop_platform_code, cis_stop_sequence, trips_id)
)
WITH (
  OIDS=FALSE
);

-- Table: public.vehiclepositions_trips

CREATE TABLE public.vehiclepositions_trips
(
  cis_id bigint,
  cis_number integer,
  cis_order integer,
  cis_short_name character varying(255),
  created timestamp with time zone,
  gtfs_route_id character varying(255),
  gtfs_route_short_name character varying(255),
  gtfs_trip_id character varying(255),
  id character varying(255) NOT NULL,
  modified timestamp with time zone,
  start_cis_stop_id integer,
  start_cis_stop_platform_code character varying(255),
  start_time time without time zone,
  start_timestamp timestamp with time zone,
  vehicle_type integer,
  wheelchair_accessible boolean,
  CONSTRAINT vehiclepositions_trips_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
