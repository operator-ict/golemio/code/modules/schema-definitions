drop VIEW analytic.v_healthy_classroom_mv_data_technology_comparison;
drop MATERIALIZED VIEW analytic.mv_healthy_classroom_technology_comparison;
drop VIEW analytic.v_healthy_classroom_source_filtered_technology_comparison;

-- healthy_classroom_test.healthy_classroom_classrooms definition

-- Drop table

-- DROP TABLE healthy_classroom_test.healthy_classroom_classrooms;

CREATE TABLE public.healthy_classroom_classrooms (
	classroom_id varchar(10) NOT NULL,
	device_id varchar(50) NOT NULL,
	cardinal_direction varchar(50) NULL,
	purpose varchar(100) NULL,
	floor_nr int4 NULL,
	new_technlogy bool NULL,
	CONSTRAINT healthy_classroom_classrooms_pk PRIMARY KEY (device_id)
);

-- healthy_classroom_test.v_healthy_classroom_source_filtered_technology_comparison source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_source_filtered_technology_comparison
AS WITH dta AS (
         SELECT DISTINCT i.generate_series,
            d.five_min_timestamp,
            i.device_id::text AS device_id,
            d.co2,
            d.humidity,
            d.temperature,
            date_part('hour'::text, i.generate_series) AS measure_hour,
            d.air_quality_level
           FROM ( SELECT generate_series.generate_series,
                    dev.device_id
                   FROM generate_series('2021-10-01 00:00:00+02'::timestamp with time zone, now(), '00:05:00'::interval) generate_series(generate_series)
                     CROSS JOIN ( SELECT DISTINCT v_healthy_classroom_source_filtered.device_id
                           FROM analytic.v_healthy_classroom_source_filtered) dev) i
             LEFT JOIN analytic.v_healthy_classroom_source_filtered d ON i.generate_series = d.five_min_timestamp AND i.device_id::text = d.device_id::text
          ORDER BY i.generate_series
        )
 SELECT DISTINCT c.generate_series,
    c.measure_hour,
    c.device_id,
    c.co2,
    max(c.co2::text) OVER (PARTITION BY c.co2_grp, c.device_id ORDER BY c.generate_series) AS co2_all,
    c.humidity,
    max(c.humidity::text) OVER (PARTITION BY c.humidity_grp, c.device_id ORDER BY c.generate_series) AS humidity_all,
    c.temperature,
    max(c.temperature::text) OVER (PARTITION BY c.temperature_grp, c.device_id ORDER BY c.generate_series) AS temperature_all,
    c.air_quality_level,
    max(c.air_quality_level::text) OVER (PARTITION BY c.air_quality_grp, c.device_id ORDER BY c.generate_series) AS air_quality_level_all,
    cr.classroom_id,
    cr.cardinal_direction,
    cr.floor_nr,
    cr.new_technlogy
   FROM ( SELECT dta.generate_series,
            dta.device_id,
            dta.co2,
            dta.measure_hour,
            dta.humidity,
            dta.temperature,
            dta.air_quality_level,
            sum(
                CASE
                    WHEN dta.co2 IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS co2_grp,
            sum(
                CASE
                    WHEN dta.humidity IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS humidity_grp,
            sum(
                CASE
                    WHEN dta.temperature IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS temperature_grp,
            sum(
                CASE
                    WHEN dta.air_quality_level IS NOT NULL THEN 1
                    ELSE NULL::integer
                END) OVER (PARTITION BY dta.device_id ORDER BY dta.generate_series) AS air_quality_grp
           FROM dta) c
     LEFT JOIN public.healthy_classroom_classrooms cr ON c.device_id = cr.device_id::text
  WHERE cr.classroom_id::text = ANY (ARRAY['U303'::character varying, 'U304'::character varying, 'U305'::character varying, 'U306'::character varying, 'U307'::character varying]::text[])
  ORDER BY c.generate_series, c.measure_hour;
  
-- healthy_classroom_test.mv_healthy_classroom_technology_comparison source

CREATE MATERIALIZED VIEW analytic.mv_healthy_classroom_technology_comparison
TABLESPACE pg_default
AS SELECT n.generate_series,
    n.classroom_id,
    o.device_id,
    n.device_id AS device_id_new,
    o.co2,
    n.co2 AS co2_new,
    o.co2_all,
    n.co2_all AS co2_all_new,
    o.humidity,
    n.humidity AS humidity_new,
    o.humidity_all,
    n.humidity_all AS humidity_all_new,
    o.temperature_all,
    n.temperature_all AS temperature_all_new,
    o.temperature,
    n.temperature AS temperature_new
   FROM analytic.v_healthy_classroom_source_filtered_technology_comparison n
     JOIN ( SELECT v_healthy_classroom_source_filtered_technology_comparison.generate_series,
            v_healthy_classroom_source_filtered_technology_comparison.measure_hour,
            v_healthy_classroom_source_filtered_technology_comparison.device_id,
            v_healthy_classroom_source_filtered_technology_comparison.co2,
            v_healthy_classroom_source_filtered_technology_comparison.co2_all,
            v_healthy_classroom_source_filtered_technology_comparison.humidity,
            v_healthy_classroom_source_filtered_technology_comparison.humidity_all,
            v_healthy_classroom_source_filtered_technology_comparison.temperature,
            v_healthy_classroom_source_filtered_technology_comparison.temperature_all,
            v_healthy_classroom_source_filtered_technology_comparison.air_quality_level,
            v_healthy_classroom_source_filtered_technology_comparison.air_quality_level_all,
            v_healthy_classroom_source_filtered_technology_comparison.classroom_id,
            v_healthy_classroom_source_filtered_technology_comparison.cardinal_direction,
            v_healthy_classroom_source_filtered_technology_comparison.floor_nr,
            v_healthy_classroom_source_filtered_technology_comparison.new_technlogy
           FROM analytic.v_healthy_classroom_source_filtered_technology_comparison
          WHERE v_healthy_classroom_source_filtered_technology_comparison.new_technlogy = false) o ON n.classroom_id::text = o.classroom_id::text AND n.generate_series = o.generate_series
  WHERE n.new_technlogy = true AND n.co2_all IS NOT NULL
WITH DATA;  

-- healthy_classroom_test.v_healthy_classroom_mv_data_technology_comparison source

CREATE OR REPLACE VIEW analytic.v_healthy_classroom_mv_data_technology_comparison
AS SELECT mv_healthy_classroom_technology_comparison.generate_series,
    mv_healthy_classroom_technology_comparison.classroom_id,
    mv_healthy_classroom_technology_comparison.device_id,
    mv_healthy_classroom_technology_comparison.device_id_new,
    mv_healthy_classroom_technology_comparison.co2,
    mv_healthy_classroom_technology_comparison.co2_new,
    to_number(mv_healthy_classroom_technology_comparison.co2_all, '9,999.99'::text) AS co2_all,
    to_number(mv_healthy_classroom_technology_comparison.co2_all_new, '9,999.99'::text) AS co2_all_new,
    mv_healthy_classroom_technology_comparison.humidity,
    mv_healthy_classroom_technology_comparison.humidity_new,
    to_number(mv_healthy_classroom_technology_comparison.humidity_all, '9,999.99'::text) AS humidity_all,
    to_number(mv_healthy_classroom_technology_comparison.humidity_all_new, '9,999.99'::text) AS humidity_all_new,
    to_number(mv_healthy_classroom_technology_comparison.temperature_all, '9,999.99'::text) AS temperature_all,
    to_number(mv_healthy_classroom_technology_comparison.temperature_all_new, '9,999.99'::text) AS temperature_all_new,
    mv_healthy_classroom_technology_comparison.temperature,
    mv_healthy_classroom_technology_comparison.temperature_new
   FROM analytic.mv_healthy_classroom_technology_comparison;