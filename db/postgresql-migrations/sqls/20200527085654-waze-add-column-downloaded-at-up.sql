
ALTER TABLE public.wazeccp_alerts ADD downloaded_at bigint;
ALTER TABLE public.wazeccp_irregularities ADD downloaded_at bigint;
ALTER TABLE public.wazeccp_jams ADD downloaded_at bigint;
