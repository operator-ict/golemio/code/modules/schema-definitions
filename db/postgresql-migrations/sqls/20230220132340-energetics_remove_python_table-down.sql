CREATE TABLE python.consumption_electricity_oict_api2 (
	timeutc text NULL,
	timelocal text NULL,
	value_txt text NULL,
	addr text NULL,
	meter text NULL,
	meternumber text NULL,
	var text NULL,
	type_txt text NULL,
	commodity text NULL,
	unit text NULL,
	ismanualentry bool NULL
);
