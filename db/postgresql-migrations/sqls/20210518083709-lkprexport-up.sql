create view public.v_lkpr_export AS
select  
coalesce (id,route_id) route_id,
name,
route_name ,
from_name ,
to_name ,
route_num ,
direction,
order_idx ,
to_timestamp(update_time/1000) update_time,
time actual_time,
round(length::numeric*3.6/time) actual_speed,
historic_time,
round(length::numeric*3.6/historic_time) historic_speed,
length
from analytic.v_lkpr_route_details vrd
full  join public.wazett_route_lives wrl
on vrd.id = wrl.route_id 
where (name like 'TN-%' or route_code like 'TN-%')
and to_timestamp(update_time/1000) >= current_date - interval '1 days';
