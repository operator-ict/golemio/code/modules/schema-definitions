-- fix naming
ALTER TABLE public.vehiclepositions_runs_messages RENAME COLUMN actual_stop_aws_id TO actual_stop_asw_id;
ALTER TABLE public.vehiclepositions_runs_messages RENAME COLUMN last_stop_aws_id TO last_stop_asw_id;
ALTER TABLE public.vehiclepositions_runs_messages RENAME COLUMN pkt TO packet_number;
ALTER TABLE public.vehiclepositions_runs_messages RENAME COLUMN tm TO msg_timestamp;

-- add last msg timestamp
ALTER TABLE public.vehiclepositions_runs ADD COLUMN msg_last_timestamp TIMESTAMP WITH TIME ZONE;
