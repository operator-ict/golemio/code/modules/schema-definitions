DROP VIEW analytic.v_ropidbi_ticket_with_rollingmean;

DROP TABLE analytic.pid_cenik;

CREATE TABLE analytic.pid_price_list (
	tarif varchar(50) NULL,
	platnost_min int4 NULL,
	platnost_text varchar(50) NULL,
	cislo_tarifu_cptp int4 NULL,
	druh varchar(50) NULL,
	zpusob_urceni_konce_platnosti int4 NULL,
	pasma varchar(50) NULL,
	zony varchar(50) NULL,
	zpusob_nabidky int4 NULL,
	cena_s_dph int4 NULL,
	nabizeno_od varchar(50) NULL,
	nabizeno_do varchar(50) NULL,
	stav varchar(50) NULL,
	zdroj varchar(50) NULL
);

CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_with_rollingmean
AS SELECT   tab.date,
    		count(tab.tariff_name) AS ticket_count,
    		sum(tab.cena_s_dph) AS total_sale,
    		avg(count(tab.tariff_name)) OVER (ORDER BY tab.date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS roll_ticket_count,
    		avg(sum(tab.cena_s_dph)) OVER (ORDER BY tab.date ROWS BETWEEN 6 PRECEDING AND CURRENT ROW) AS roll_sale
FROM (SELECT  (to_char(pur.date, 'yyyy-mm-dd')) AS date,
        pur.tariff_name,
        pur.cptp,
        cen.cena_s_dph
FROM mos_ma_ticketpurchases pur
LEFT JOIN (SELECT   tarif,
					cislo_tarifu_cptp,
					cena_s_dph,
					nabizeno_od,
					nabizeno_do
           FROM analytic.pid_price_list) cen
ON pur.tariff_name::text = cen.tarif::text AND pur.cptp = cen.cislo_tarifu_cptp
AND ((to_char(pur.date, 'yyyy-mm-dd')) BETWEEN nabizeno_od AND nabizeno_do)) tab
GROUP BY tab.date;
