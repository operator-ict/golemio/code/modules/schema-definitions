/* Replace with your SQL commands */
DROP VIEW IF EXISTS analytic.v_ropidbi_ticket_zones;

-- analytic.v_ropidbi_ticket_zones source
CREATE
OR REPLACE VIEW analytic.v_ropidbi_ticket_zones AS
SELECT
	created valid_from,
	customer_profile_name customer,
	tariff_profile_name tarif,
	zona,
	zone_order,
	count (*)
FROM
	(
		SELECT
			c.created :: date,
			c.coupon_id,
			c.tariff_profile_name,
			c.customer_profile_name,
			z.zona,
			z.zone_order
		FROM
			mos_be_coupons c
			JOIN (
				SELECT
					z.coupon_id,
					CASE
						WHEN z.zone_name :: text = ANY (
							ARRAY ['Praha'::character varying::text, '0', '0+B'::character varying::text]
						) THEN 'P+0+B' :: character varying
						ELSE z.zone_name
					END AS zona,
					CASE
						WHEN z.zone_name :: text = ANY (
							ARRAY ['Praha'::character varying::text, '0', '0+B'::character varying::text]
						) THEN '0' :: character varying
						ELSE z.zone_name
					END AS zone_order
				FROM
					mos_be_zones z
				GROUP BY
					1,
					2,
					3
			) z ON c.coupon_id = z.coupon_id
	) tab
GROUP BY
	1,
	2,
	3,
	4,
	5
ORDER BY
	1 DESC;