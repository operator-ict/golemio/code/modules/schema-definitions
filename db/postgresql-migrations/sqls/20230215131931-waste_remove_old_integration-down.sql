CREATE TABLE python.waste_assigned_pick_dates (
	container_code varchar(16) NULL,
	pick_date date NULL,
	pick_at timestamptz NULL,
	is_exact_pick_date bool NULL,
	is_tolerated_pick_date bool NULL,
	is_pick_assigned bool NULL,
	tolerance interval NULL,
	trash_type varchar(20) NULL,
	category varchar(20) NULL,
	CONSTRAINT cnstr_waste_assigned_pick_dates UNIQUE (container_code, pick_date)
);

CREATE TABLE python.waste_assigned_picks (
	container_code varchar(16) NULL,
	pick_at timestamptz NULL,
	"date" date NULL,
	is_assigned bool NULL,
	CONSTRAINT cnstr_waste_assigned_picks UNIQUE (container_code, pick_at)
);

CREATE TABLE analytic.containers_districts (
	district varchar(50) NULL,
	district_cz varchar(50) NULL
);

CREATE TABLE analytic.containers_code_black_list (
	container_id varchar(50) NOT NULL,
	exclusion_date date NULL,
	CONSTRAINT containers_code_black_list_pkey PRIMARY KEY (container_id)
);

CREATE OR REPLACE VIEW analytic.v_containers_picks
AS SELECT cp.container_code,
    cc.total_volume,
        CASE
            WHEN cc.trash_type = 1 THEN 411
            WHEN cc.trash_type = 3 THEN 120
            WHEN cc.trash_type = 4 THEN 55
            WHEN cc.trash_type = 5 THEN 76
            WHEN cc.trash_type = 6 THEN 39
            WHEN cc.trash_type = 7 THEN 411
            ELSE NULL::integer
        END AS koeficient,
    cp.pick_at_utc,
    cp.percent_before,
    cp.percent_now,
    cp.event_driven,
    wpd.category
   FROM containers_picks cp
     JOIN containers_containers cc ON cp.container_code::text = cc.code::text
     LEFT JOIN python.waste_assigned_pick_dates wpd ON cp.container_code::text = wpd.container_code::text AND cp.pick_at_utc = wpd.pick_at
  WHERE (cp.container_code::text IN ( SELECT DISTINCT containers_containers.code
           FROM containers_containers));

CREATE OR REPLACE VIEW analytic.v_containers_pick_dates
AS WITH meas AS (
         SELECT containers_measurement.container_code,
            containers_measurement.measured_at_utc,
            containers_measurement.percent_calculated,
            rank() OVER (PARTITION BY containers_measurement.container_code, (date_trunc('day'::text, containers_measurement.measured_at_utc)) ORDER BY containers_measurement.measured_at_utc) AS rank
           FROM containers_measurement
        ), first_meas AS (
         SELECT cm.container_code,
            min(cm.measured_at::date) AS min_meas
           FROM containers_measurement cm
          GROUP BY cm.container_code
        ), pick AS (
         SELECT cc.code AS container_id,
            concat(pd.pick_date::date, ' 7:00')::timestamp with time zone AS pick_date,
            'Pražské služby - hlášený svoz'::text AS pick_event,
            2 AS rank
           FROM containers_picks_dates pd
             LEFT JOIN ( SELECT containers_containers.id,
                    containers_containers.sensoneo_code AS code
                   FROM containers_containers
                  WHERE containers_containers.sensoneo_code IS NOT NULL AND containers_containers.sensoneo_code::text <> containers_containers.code::text) cc ON pd.container_id::text = cc.id::text
             LEFT JOIN first_meas fm ON fm.container_code::text = cc.code::text
          WHERE pd.pick_date >= (( SELECT min(containers_measurement.measured_at_utc) AS min
                   FROM containers_measurement)) AND pd.pick_date <= now() AND pd.pick_date >= fm.min_meas
        )
 SELECT COALESCE(meas.container_code, pick.container_id) AS container_id,
    COALESCE(meas.measured_at_utc::timestamp with time zone, pick.pick_date) AS measured_at_utc,
    COALESCE(meas.percent_calculated, 0) AS percent_calculated,
    pick.pick_event
   FROM meas
     FULL JOIN pick ON meas.container_code::text = pick.container_id::text AND meas.measured_at_utc::date = pick.pick_date::date AND meas.rank = pick.rank;

CREATE OR REPLACE VIEW analytic.v_containers_next_pick
AS SELECT cc.code AS container_id,
    min(pd.pick_date) AS next_pick
   FROM containers_picks_dates pd
     JOIN containers_containers cc ON pd.container_id::text = cc.id::text
  WHERE pd.pick_date > now() AND (cc.code::text IN ( SELECT DISTINCT containers_measurement.container_code
           FROM containers_measurement))
  GROUP BY cc.code;

CREATE OR REPLACE VIEW analytic.v_containers_measurements
AS SELECT containers_measurement.container_code,
    containers_measurement.station_code,
    containers_measurement.measured_at_utc,
    containers_measurement.measured_at_utc::date AS measured_at_utc_date,
    split_part(containers_measurement.measured_at_utc::character varying(50)::text, ' '::text, 2) AS measured_at_utc_time,
    containers_measurement.percent_calculated,
    COALESCE(containers_measurement.percent_calculated::numeric, 0::numeric) / 100::numeric AS percent_calculated_decimal
   FROM containers_measurement
  WHERE (containers_measurement.container_code::text IN ( SELECT DISTINCT containers_containers.sensoneo_code
           FROM containers_containers
          WHERE containers_containers.sensoneo_code IS NOT NULL AND containers_containers.sensoneo_code::text <> containers_containers.code::text));

CREATE OR REPLACE VIEW analytic.v_containers_full_days
AS WITH base AS (
         SELECT containers_measurement.container_code AS container_id,
            containers_measurement.measured_at,
            containers_measurement.percent_calculated
           FROM containers_measurement
        ), status_tbl AS (
         SELECT base.container_id,
            base.measured_at,
            base.percent_calculated,
                CASE
                    WHEN base.percent_calculated > 95 THEN
                    CASE
                        WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) < 96 AND (base.measured_at::date - lead(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 THEN 'begin_till_midnight'::text
                        WHEN (base.measured_at::date - lag(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 AND lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) > 95 THEN 'begin_from_midnight'::text
                        WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) < 96 THEN 'begin'::text
                        WHEN (base.measured_at::date - lead(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 THEN 'end_at_midnight'::text
                        ELSE 'body'::text
                    END
                    WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) > 95 THEN
                    CASE
                        WHEN (base.measured_at::date - lag(base.measured_at::date, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at)) <> 0 THEN 'end_from_midnight'::text
                        ELSE 'end'::text
                    END
                    ELSE 'ok'::text
                END AS status
           FROM base
          ORDER BY base.container_id, base.measured_at
        ), start_end AS (
         SELECT status_tbl.container_id,
            status_tbl.measured_at,
            status_tbl.percent_calculated,
            status_tbl.status,
                CASE
                    WHEN status_tbl.status = 'end_from_midnight'::text THEN date_trunc('day'::text, status_tbl.measured_at)
                    WHEN status_tbl.status = 'begin'::text THEN status_tbl.measured_at
                    WHEN status_tbl.status = 'begin_till_midnight'::text THEN status_tbl.measured_at
                    WHEN status_tbl.status = 'begin_from_midnight'::text THEN date_trunc('day'::text, status_tbl.measured_at)
                    ELSE NULL::timestamp without time zone
                END AS start_time,
                CASE
                    WHEN status_tbl.status ~~ 'begin%'::text AND (status_tbl.measured_at::date - lead(status_tbl.measured_at::date, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at)) <> 0 THEN date_trunc('day'::text, status_tbl.measured_at) + '1 day'::interval
                    WHEN status_tbl.status ~~ 'begin%'::text AND lead(status_tbl.status, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at) = 'end_at_midnight'::text THEN date_trunc('day'::text, status_tbl.measured_at) + '1 day'::interval
                    WHEN status_tbl.status ~~ 'begin%'::text AND lead(status_tbl.status, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at) = 'end'::text THEN lead(status_tbl.measured_at, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at)
                    WHEN status_tbl.status = 'end_from_midnight'::text THEN status_tbl.measured_at
                    ELSE NULL::timestamp without time zone
                END AS end_time
           FROM status_tbl
          WHERE status_tbl.status = ANY (ARRAY['begin_till_midnight'::text, 'begin_from_midnight'::text, 'begin'::text, 'end_at_midnight'::text, 'end_from_midnight'::text, 'end'::text])
          ORDER BY status_tbl.container_id, status_tbl.measured_at
        )
 SELECT start_end.container_id,
    date_trunc('day'::text, start_end.start_time) AS date,
    start_end.start_time,
    start_end.end_time
   FROM start_end
  WHERE start_end.status = 'end_from_midnight'::text OR start_end.status ~~ 'begin%'::text;

CREATE OR REPLACE VIEW analytic.v_containers_full
AS WITH base AS (
         SELECT containers_measurement.container_code AS container_id,
            containers_measurement.measured_at,
            containers_measurement.percent_calculated
           FROM containers_measurement
        ), status_tbl AS (
         SELECT base.container_id,
            base.measured_at,
            base.percent_calculated,
                CASE
                    WHEN base.percent_calculated > 95 THEN
                    CASE
                        WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) < 96 THEN 'begin'::text
                        ELSE 'body'::text
                    END
                    WHEN lag(base.percent_calculated, 1) OVER (PARTITION BY base.container_id ORDER BY base.measured_at) > 95 THEN 'end'::text
                    ELSE 'ok'::text
                END AS status
           FROM base
          ORDER BY base.container_id, base.measured_at
        ), start_end AS (
         SELECT status_tbl.container_id,
            status_tbl.measured_at,
            status_tbl.percent_calculated,
            status_tbl.status,
                CASE
                    WHEN status_tbl.status = 'begin'::text THEN status_tbl.measured_at
                    ELSE NULL::timestamp without time zone
                END AS start_time,
                CASE
                    WHEN status_tbl.status = 'begin'::text THEN COALESCE(lead(status_tbl.measured_at, 1) OVER (PARTITION BY status_tbl.container_id ORDER BY status_tbl.measured_at), date_trunc('day'::text, status_tbl.measured_at) + '1 day'::interval)::timestamp with time zone
                    ELSE NULL::timestamp with time zone
                END AS end_time
           FROM status_tbl
          WHERE status_tbl.status = ANY (ARRAY['begin'::text, 'end'::text])
          ORDER BY status_tbl.container_id, status_tbl.measured_at
        )
 SELECT start_end.container_id,
    date_trunc('day'::text, start_end.start_time) AS date,
    start_end.start_time,
    start_end.end_time
   FROM start_end
  WHERE start_end.status = 'begin'::text;

CREATE OR REPLACE VIEW analytic.v_containers_containers
AS WITH prep AS (
         SELECT containers_containers.id,
            containers_containers.code,
            containers_containers.knsko_id,
            containers_containers.knsko_code,
            containers_containers.station_code,
            containers_containers.total_volume,
            containers_containers.prediction,
            containers_containers.bin_type,
            containers_containers.installed_at,
            containers_containers.network,
            containers_containers.cleaning_frequency_interval,
            containers_containers.cleaning_frequency_frequency,
            containers_containers.company,
            containers_containers.container_type,
            containers_containers.trash_type,
            containers_containers.source,
            containers_containers.create_batch_id,
            containers_containers.created_at,
            containers_containers.created_by,
            containers_containers.update_batch_id,
            containers_containers.updated_at,
            containers_containers.updated_by,
            containers_containers.sensor_id,
            containers_containers.pick_days,
            containers_containers.sensoneo_code
           FROM containers_containers
          WHERE containers_containers.sensoneo_code IS NOT NULL AND containers_containers.sensoneo_code::text <> containers_containers.code::text
        ), unn AS (
         SELECT prep.sensoneo_code AS code,
            unnest(string_to_array(prep.pick_days::text, ','::text))::integer AS day_num
           FROM prep
        ), translate AS (
         SELECT unn.code,
            unn.day_num,
                CASE
                    WHEN unn.day_num = 1 THEN 'Po'::text
                    WHEN unn.day_num = 2 THEN 'Út'::text
                    WHEN unn.day_num = 3 THEN 'St'::text
                    WHEN unn.day_num = 4 THEN 'Čt'::text
                    WHEN unn.day_num = 5 THEN 'Pá'::text
                    WHEN unn.day_num = 6 THEN 'So'::text
                    WHEN unn.day_num = 7 THEN 'Ne'::text
                    ELSE NULL::text
                END AS day_name
           FROM unn
        ), aa AS (
         SELECT translate.code,
            string_agg(translate.day_name, ', '::text ORDER BY translate.day_num) AS pick_days
           FROM translate
          GROUP BY translate.code
        )
 SELECT cc.sensoneo_code AS container_id,
    cc.total_volume,
    cc.network,
    cc.container_type,
    cc.bin_type,
        CASE
            WHEN cc.trash_type = 1 THEN 'glass coloured'::text
            WHEN cc.trash_type = 2 THEN 'electronics'::text
            WHEN cc.trash_type = 3 THEN 'metal'::text
            WHEN cc.trash_type = 4 THEN 'bevarage cartons'::text
            WHEN cc.trash_type = 5 THEN 'paper'::text
            WHEN cc.trash_type = 6 THEN 'plastic'::text
            WHEN cc.trash_type = 7 THEN 'glass clear'::text
            WHEN cc.trash_type = 8 THEN 'textile'::text
            ELSE NULL::text
        END AS trash_type,
        CASE
            WHEN cc.trash_type = 1 THEN 'Sklo barevné'::text
            WHEN cc.trash_type = 2 THEN 'Elektronika'::text
            WHEN cc.trash_type = 3 THEN 'Kovy'::text
            WHEN cc.trash_type = 4 THEN 'Nápojové kartony'::text
            WHEN cc.trash_type = 5 THEN 'Papír'::text
            WHEN cc.trash_type = 6 THEN 'Plast'::text
            WHEN cc.trash_type = 7 THEN 'Sklo čiré'::text
            WHEN cc.trash_type = 8 THEN 'Textil'::text
            ELSE NULL::text
        END AS typ_odpadu,
        CASE
            WHEN cc.trash_type = 1 THEN 411
            WHEN cc.trash_type = 3 THEN 120
            WHEN cc.trash_type = 4 THEN 55
            WHEN cc.trash_type = 5 THEN 76
            WHEN cc.trash_type = 6 THEN 39
            WHEN cc.trash_type = 7 THEN 411
            ELSE NULL::integer
        END AS koeficient,
    cc.cleaning_frequency_frequency,
    cc.cleaning_frequency_interval,
    cs.latitude,
    cs.longitude,
        CASE
            WHEN cs.address::text = ' Francouzská  240/76'::text THEN "substring"(cs.address::text, 2)::character varying
            ELSE cs.address
        END AS address,
    cd.district_cz,
    concat(1, "right"(split_part(cs.district::text, '-'::text, 2), 1), '000') AS psc,
    cc.sensor_id,
    cc.updated_at,
    aa.pick_days,
    row_number() OVER (PARTITION BY cs.address, cc.trash_type) AS row_number
   FROM prep cc
     LEFT JOIN containers_stations cs ON cc.station_code::text = cs.code::text
     LEFT JOIN analytic.containers_districts cd ON cs.district::text = cd.district::text
     LEFT JOIN aa ON cc.sensoneo_code::text = aa.code::text;

CREATE OR REPLACE VIEW analytic.v_containers_5d_same
AS SELECT s.container_id,
    s.min_percent,
    s.max_percent,
    analytic.container_last_correct(s.container_id::text) AS last_correct
   FROM ( SELECT containers_measurement.container_code AS container_id,
            min(containers_measurement.percent_calculated) AS min_percent,
            max(containers_measurement.percent_calculated) AS max_percent
           FROM containers_measurement
          WHERE containers_measurement.measured_at > (CURRENT_DATE - 5) AND NOT (containers_measurement.container_code::text IN ( SELECT "24h_missing".container_id
                   FROM ( SELECT containers_measurement_1.container_code AS container_id,
                            max(containers_measurement_1.measured_at) AS measured_at
                           FROM containers_measurement containers_measurement_1
                          GROUP BY containers_measurement_1.container_code
                         HAVING COALESCE(max(containers_measurement_1.measured_at), '2000-01-01 00:00:00'::timestamp without time zone) < (CURRENT_DATE - 1)) "24h_missing"))
          GROUP BY containers_measurement.container_code
         HAVING abs(min(containers_measurement.percent_calculated) - max(containers_measurement.percent_calculated)) <= 3) s;

CREATE OR REPLACE VIEW analytic.v_containers_24h_missing
AS SELECT cm.container_code AS container_id,
    max(cm.measured_at) AS measured_at
   FROM containers_measurement cm
     RIGHT JOIN ( SELECT containers_containers.id,
            containers_containers.code,
            containers_containers.knsko_id,
            containers_containers.knsko_code,
            containers_containers.station_code,
            containers_containers.total_volume,
            containers_containers.prediction,
            containers_containers.bin_type,
            containers_containers.installed_at,
            containers_containers.network,
            containers_containers.cleaning_frequency_interval,
            containers_containers.cleaning_frequency_frequency,
            containers_containers.company,
            containers_containers.container_type,
            containers_containers.trash_type,
            containers_containers.source,
            containers_containers.create_batch_id,
            containers_containers.created_at,
            containers_containers.created_by,
            containers_containers.update_batch_id,
            containers_containers.updated_at,
            containers_containers.updated_by,
            containers_containers.sensor_id,
            containers_containers.pick_days,
            containers_containers.sensoneo_code
           FROM containers_containers
          WHERE containers_containers.code::text <> containers_containers.sensoneo_code::text AND containers_containers.sensoneo_code IS NOT NULL AND NOT (containers_containers.sensoneo_code::text IN ( SELECT containers_code_black_list.container_id
                   FROM analytic.containers_code_black_list))) cc ON cc.sensoneo_code::text = cm.container_code::text
  GROUP BY cm.container_code
 HAVING max(cm.measured_at) < (now() - '1 day'::interval) OR max(cm.measured_at) IS NULL
  ORDER BY (max(cm.measured_at));

CREATE OR REPLACE VIEW public.v_containers_measurement_export
AS SELECT cc.container_id,
    cc.address,
    cc.district_cz,
    cc.trash_type,
    cm.measured_at::date AS date,
    cm.measured_at::time without time zone AS "time",
    cm.temperature,
    cm.percent_calculated
   FROM containers_measurement cm
     JOIN analytic.v_containers_containers cc ON cc.container_id::text = cm.container_code::text;

CREATE OR REPLACE VIEW public.v_containers_picks_export
AS SELECT DISTINCT cc.code,
    cs.address,
    initcap(replace(cs.district::text, '-'::text, ' '::text)) AS district_cz,
    cc.trash_type,
    cp.pick_at::date AS date,
    cp.pick_at::time without time zone AS "time",
    cp.percent_before,
    cp.percent_now,
    cp.event_driven,
    cm.temperature
   FROM containers_picks cp
     JOIN containers_containers cc ON cc.code::text = cp.container_code::text
     LEFT JOIN containers_stations cs ON cs.code::text = cc.station_code::text
     LEFT JOIN containers_measurement cm ON cc.code::text = cm.container_code::text AND cp.pick_at = cm.measured_at
  WHERE cp.pick_at >= '2019-04-15 03:06:15'::timestamp without time zone;
