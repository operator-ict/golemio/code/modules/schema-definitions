CREATE SCHEMA IF NOT EXISTS demographics;

CREATE TABLE demographics.census_age (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	vek_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_age_pkey PRIMARY KEY (uzemi_kod, vek_txt)
);

CREATE TABLE demographics.census_age_10_gender (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	vek_txt varchar(50) NOT NULL,
	pohlavi_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	atribut varchar(50) NULL,
	uroven varchar(50) NULL,
	CONSTRAINT census_age_10_gender_pkey PRIMARY KEY (uzemi_kod, vek_txt, pohlavi_txt)
);

CREATE TABLE demographics.census_age_5_gender (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	vek_txt varchar(50) NOT NULL,
	pohlavi_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	atribut varchar(50) NULL,
	uroven varchar(50) NULL,
	CONSTRAINT census_age_5_gender_pkey PRIMARY KEY (uzemi_kod, vek_txt, pohlavi_txt)
);

CREATE TABLE demographics.census_citizenship (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	obcanstvi_txt varchar(64) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_citizenship_pkey PRIMARY KEY (uzemi_kod, obcanstvi_txt)
);

CREATE TABLE demographics.census_education (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(64) NULL,
	vzdelani_txt varchar(64) NOT NULL,
	uzemi_txt varchar(50) NULL,
	atribut varchar(50) NULL,
	uroven varchar(50) NULL,
	CONSTRAINT census_education_pkey PRIMARY KEY (uzemi_kod, vzdelani_txt)
);

CREATE TABLE demographics.census_ethnicity (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	narodnost_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_ethnicity_pkey PRIMARY KEY (uzemi_kod, narodnost_txt)
);

CREATE TABLE demographics.census_fertility (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	pocetdeti_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_fertility_pkey PRIMARY KEY (uzemi_kod, pocetdeti_txt)
);

CREATE TABLE demographics.census_gender (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	pohlavi_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_gender_pkey PRIMARY KEY (uzemi_kod, pohlavi_txt)
);

CREATE TABLE demographics.census_gender_average_age (
	prumerny_vek float4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	pohlavi_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_gender_average_age_pkey PRIMARY KEY (uzemi_kod, pohlavi_txt)
);

CREATE TABLE demographics.census_housing_arrangement (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	bydleni_txt varchar(64) NOT NULL,
	pohlavi_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_housing_arrangement_pkey PRIMARY KEY (uzemi_kod, bydleni_txt, pohlavi_txt)
);

CREATE TABLE demographics.census_marital_status (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	stav_txt varchar(64) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_marital_status_pkey PRIMARY KEY (uzemi_kod, stav_txt)
);

CREATE TABLE demographics.census_mother_tongue_combination (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	jazyk_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_mother_tongue_combination_pkey PRIMARY KEY (uzemi_kod, jazyk_txt)
);

CREATE TABLE demographics.census_mother_tongue_exclusive (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	jazyk_txt varchar(50) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_mother_tongue_exclusive_pkey PRIMARY KEY (uzemi_kod, jazyk_txt)
);

CREATE TABLE demographics.census_religion (
	pocet_obyvatel int4 NULL,
	uzemi_cis int4 NULL,
	uzemi_kod int4 NOT NULL,
	rok_scitani int4 NULL,
	datum_scitani varchar(50) NULL,
	popis varchar(50) NULL,
	vira_txt varchar(64) NOT NULL,
	uzemi_txt varchar(50) NULL,
	uroven varchar(50) NULL,
	atribut varchar(50) NULL,
	CONSTRAINT census_religion_pkey PRIMARY KEY (uzemi_kod, vira_txt)
);

CREATE TABLE demographics.code_list_cr_csu (
	uzemi_cis_mc_obec int4 NULL,
	uzemi_kod_mc_obec int4 NOT NULL,
	uzemi_txt_mc_obec varchar(50) NULL,
	uzemi_cis_obec int4 NULL,
	uzemi_kod_obec int4 NULL,
	uzemi_txt_obec varchar(50) NULL,
	uzemi_cis_orp int4 NULL,
	uzemi_kod_orp int4 NULL,
	uzemi_txt_orp varchar(50) NULL,
	uzemi_cis_orp_praha int4 NULL,
	uzemi_kod_orp_praha float4 NULL,
	uzemi_txt_orp_praha varchar(50) NULL,
	uzemi_cis_okres int4 NULL,
	uzemi_kod_okres int4 NULL,
	uzemi_txt_okres varchar(50) NULL,
	uzemi_cis_kraj int4 NULL,
	uzemi_kod_kraj int4 NULL,
	uzemi_txt_kraj varchar(50) NULL,
	uzemi_cis_region int4 NULL,
	uzemi_kod_region int4 NULL,
	uzemi_txt_region varchar(50) NULL,
	CONSTRAINT code_list_cr_csu_pkey PRIMARY KEY (uzemi_kod_mc_obec)
);

CREATE OR REPLACE VIEW analytic.v_prague_portal_census_average_age
AS SELECT cgaa.uzemi_cis,
    cgaa.uzemi_kod,
    cgaa.uzemi_txt,
    cgaa.prumerny_vek,
    cgaa.pohlavi_txt,
    cgaa.atribut,
    cgaa.uroven
   FROM demographics.census_gender_average_age cgaa
     LEFT JOIN demographics.code_list_cr_csu clcc ON clcc.uzemi_kod_mc_obec = cgaa.uzemi_kod
  WHERE clcc.uzemi_txt_obec::text = 'Praha'::text AND cgaa.uzemi_cis = 44
UNION
 SELECT cgaa.uzemi_cis,
    cgaa.uzemi_kod,
    cgaa.uzemi_txt,
    cgaa.prumerny_vek,
    cgaa.pohlavi_txt,
    cgaa.atribut,
    cgaa.uroven
   FROM demographics.census_gender_average_age cgaa
     LEFT JOIN demographics.code_list_cr_csu clcc ON clcc.uzemi_kod_obec = cgaa.uzemi_kod
  WHERE clcc.uzemi_txt_obec::text = 'Praha'::text AND cgaa.uzemi_cis = 43
UNION
 SELECT cgaa.uzemi_cis,
    cgaa.uzemi_kod,
    cgaa.uzemi_txt,
    cgaa.prumerny_vek,
    cgaa.pohlavi_txt,
    cgaa.atribut,
    cgaa.uroven
   FROM demographics.census_gender_average_age cgaa
     LEFT JOIN demographics.code_list_cr_csu clcc ON clcc.uzemi_kod_orp_praha = cgaa.uzemi_kod::double precision
  WHERE clcc.uzemi_txt_obec::text = 'Praha'::text AND cgaa.uzemi_cis = 72
  ORDER BY 2;

CREATE OR REPLACE VIEW analytic.v_prague_portal_census
AS SELECT c.uzemi_kod,
    c.uzemi_txt,
    clcc.uzemi_kod_orp_praha,
    clcc.uzemi_txt_orp_praha,
    c.atribut,
    c.skupina,
    c.podskupina,
    c.pocet_obyvatel
   FROM demographics.code_list_cr_csu clcc
     LEFT JOIN ( SELECT cag.uzemi_kod,
            cag.uzemi_txt,
            cag.pocet_obyvatel,
            cag.pohlavi_txt AS skupina,
            cag.vek_txt AS podskupina,
            cag.atribut
           FROM demographics.census_age_10_gender cag
        UNION
         SELECT cc.uzemi_kod,
            cc.uzemi_txt,
            cc.pocet_obyvatel,
            cc.obcanstvi_txt AS skupina,
            NULL::character varying AS poskupina,
            cc.atribut
           FROM demographics.census_citizenship cc
        UNION
         SELECT ce.uzemi_kod,
            ce.uzemi_txt,
            ce.pocet_obyvatel,
            ce.vzdelani_txt AS skupina,
            NULL::character varying AS poskupina,
            ce.atribut
           FROM demographics.census_education ce
        UNION
         SELECT ce2.uzemi_kod,
            ce2.uzemi_txt,
            ce2.pocet_obyvatel,
            ce2.narodnost_txt AS skupina,
            NULL::character varying AS poskupina,
            ce2.atribut
           FROM demographics.census_ethnicity ce2
        UNION
         SELECT cf.uzemi_kod,
            cf.uzemi_txt,
            cf.pocet_obyvatel,
            cf.pocetdeti_txt AS skupina,
            NULL::character varying AS poskupina,
            cf.atribut
           FROM demographics.census_fertility cf
        UNION
         SELECT cms.uzemi_kod,
            cms.uzemi_txt,
            cms.pocet_obyvatel,
            cms.stav_txt AS skupina,
            NULL::character varying AS poskupina,
            cms.atribut
           FROM demographics.census_marital_status cms
        UNION
         SELECT cmtc.uzemi_kod,
            cmtc.uzemi_txt,
            cmtc.pocet_obyvatel,
            cmtc.jazyk_txt AS skupina,
            NULL::character varying AS poskupina,
            cmtc.atribut
           FROM demographics.census_mother_tongue_exclusive cmtc
        UNION
         SELECT cha.uzemi_kod,
            cha.uzemi_txt,
            cha.pocet_obyvatel,
            cha.bydleni_txt AS skupina,
            NULL::character varying AS poskupina,
            cha.atribut
           FROM demographics.census_housing_arrangement cha) c ON c.uzemi_kod = clcc.uzemi_kod_mc_obec
  WHERE clcc.uzemi_txt_obec::text = 'Praha'::text
  ORDER BY c.uzemi_kod, c.atribut;
