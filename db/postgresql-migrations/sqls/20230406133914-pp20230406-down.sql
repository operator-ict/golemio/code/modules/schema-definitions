CREATE TABLE public.wazett_feeds (
	id int4 NOT NULL,
	area_name text NULL,
	broadcaster_id text NULL,
	"name" text NULL,
	bbox jsonb NULL,
	ismetric bool NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_feeds_pkey PRIMARY KEY (id)
);

CREATE TABLE public.wazett_jams_stats (
	feed_id int4 NOT NULL,
	update_time int8 NOT NULL,
	jam_level int4 NOT NULL,
	wazers_count numeric NULL,
	length_of_jams int4 NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_jams_stats_pkey PRIMARY KEY (feed_id, update_time, jam_level)
);

CREATE TABLE public.wazett_routes (
	id int8 NOT NULL,
	feed_id int4 NULL,
	from_name text NULL,
	to_name text NULL,
	"name" text NULL,
	bbox jsonb NULL,
	last_update int8 NULL,
	line public.geometry(linestring, 4326) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_routes_pkey PRIMARY KEY (id)
);

CREATE TABLE public.wazett_route_lives (
	route_id int8 NOT NULL,
	update_time int8 NOT NULL,
	"time" int4 NULL,
	length int4 NULL,
	historic_time int4 NULL,
	jam_level int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL
);

CREATE TABLE public.wazett_subroutes (
	id serial4 NOT NULL,
	route_id int8 NULL,
	line public.geometry(linestring, 4326) NULL,
	from_name text NULL,
	to_name text NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_subroutes_pkey PRIMARY KEY (id)
);

CREATE TABLE public.wazett_subroute_lives (
	route_id int8 NOT NULL,
	update_time int8 NOT NULL,
	subroute_id int4 NOT NULL,
	"time" int4 NULL,
	length int4 NULL,
	historic_time int4 NULL,
	jam_level int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazett_subroute_lives_pkey PRIMARY KEY (update_time, route_id, subroute_id)
);