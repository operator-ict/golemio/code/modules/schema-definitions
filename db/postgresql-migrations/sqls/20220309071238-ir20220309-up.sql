create schema if not exists keboola;

CREATE TABLE keboola.internal_reports_covid (
	id varchar(255) primary key,
	idprofile int8 NOT NULL,
	"date" date NOT NULL,
	sessions int4 NULL,
	users int4 NULL,
	sessionduration numeric(12, 2) NULL,
	newusers int4 NULL
);

CREATE TABLE keboola.internal_reports_golemio (
	id varchar(255) primary key,
	idprofile int8 NULL,
	"date" date NOT NULL,
	sessions int4 NULL,
	users int4 NULL,
	sessionduration numeric(12, 2) NULL,
	newusers int4 NULL
);

CREATE TABLE keboola.internal_reports_golemio_bi (
	id varchar(255) primary key,
	idprofile int8 NULL,
	"date" date NOT NULL,
	sessions int4 NULL,
	users int4 NULL,
	sessionduration numeric(12, 2) NULL,
	newusers int4 NULL
);

CREATE TABLE keboola.internal_reports_moje_praha (
	id varchar(255) primary key,
	idprofile int8 NOT NULL,
	"date" date NOT NULL,
	sessions int4 NULL,
	users int4 NULL,
	sessionduration numeric(12, 2) NULL,
	newusers int4 NULL
);

CREATE TABLE keboola.internal_reports_mojepraha_events (
	event_date date NOT NULL,
	event_name varchar(255) NULL,
	event_count int4 NULL,
	users int4 NULL
);
ALTER TABLE keboola.internal_reports_mojepraha_events 
ADD CONSTRAINT internal_reports_mojepraha_events_pk PRIMARY KEY (event_date,event_name);


CREATE TABLE keboola.internal_reports_operatorict (
	id varchar(255) primary key,
	idprofile int8 NOT NULL,
	"date" date NOT NULL,
	sessions int4 NULL,
	users int4 NULL,
	sessionduration numeric(12, 2) NULL,
	newusers int4 NOT NULL
);

CREATE TABLE keboola.internal_reports_pragozor (
	id varchar(255) primary key,
	idprofile int8 NOT NULL,
	"date" date NOT NULL,
	sessions int4 NULL,
	users int4 NULL,
	sessionduration numeric(12, 2) NULL,
	newusers int4 NULL
);

CREATE TABLE keboola.internal_reports_smart_prague (
	id varchar(255) primary key,
	idprofile int8 NOT NULL,
	"date" date NOT NULL,
	sessions int4 NULL,
	users int4 NULL,
	sessionduration numeric(12, 2) NULL,
	newusers int4 NULL
);

CREATE TABLE keboola.internal_reports_zmenteteo_events (
	event_date date NOT NULL,
	event_name varchar(255) NULL,
	event_count int4 NULL,
	users int4 NULL
);

ALTER TABLE keboola.internal_reports_zmenteteo_events 
ADD CONSTRAINT internal_reports_zmenteteo_events_pk PRIMARY KEY (event_date,event_name);
