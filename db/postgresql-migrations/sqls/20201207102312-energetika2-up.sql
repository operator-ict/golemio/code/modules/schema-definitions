/* 
Pavel Procházka, 7.12.2020
tabulka a view ve schématu analytic
*/

CREATE TABLE analytic.consumption_electricity_daily_reserves (
	object_name varchar(255) NULL,
	device_id varchar(255) NULL,
	relevance_year int4 NULL,
	relevance_month int4 NULL,
	daily_reserved_capacity_kwh numeric NULL
);

CREATE OR REPLACE VIEW analytic.v_consumption_energy_reserve_day_electricity
AS SELECT date_trunc('day'::text, c.time_utc) AS den,
    c.time_utc,
    c.addr,
    c.unit,
    er.object_name,
    er.daily_reserved_capacity_kwh,
    c.value,
        CASE
            WHEN (c.value / er.daily_reserved_capacity_kwh) > 1::numeric THEN '100% a více'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.8 AND (c.value / er.daily_reserved_capacity_kwh) <= 1::numeric THEN '80 - 100%'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.6 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.8 THEN '60 - 80%'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.4 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.6 THEN '40 - 60%'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.2 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.4 THEN '20 - 40%'::text
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0::numeric AND (c.value / er.daily_reserved_capacity_kwh) <= 0.2 THEN '0 - 20%'::text
            WHEN c.value = 0::numeric THEN '0 - 20%'::text
            ELSE 'N/A'::text
        END AS ratio_group,
        CASE
            WHEN (c.value / er.daily_reserved_capacity_kwh) > 1::numeric THEN 6
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.8 AND (c.value / er.daily_reserved_capacity_kwh) <= 1::numeric THEN 5
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.6 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.8 THEN 4
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.4 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.6 THEN 3
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0.2 AND (c.value / er.daily_reserved_capacity_kwh) <= 0.4 THEN 2
            WHEN (c.value / er.daily_reserved_capacity_kwh) >= 0::numeric AND (c.value / er.daily_reserved_capacity_kwh) <= 0.2 THEN 1
            WHEN c.value = 0::numeric THEN 1
            ELSE 0
        END AS ratio_order
   FROM consumption_energy_consumption c
     JOIN analytic.consumption_electricity_daily_reserves er ON c.addr::text = er.device_id::text AND er.relevance_year::double precision = date_part('year'::text, c.time_utc) AND er.relevance_month::double precision = date_part('month'::text, c.time_utc)
  WHERE (c.addr::text = ANY (ARRAY['/2.3/EF1'::character varying, '/2.7/EF1'::character varying, '/2.8/EF1'::character varying, '/2.9/EF1'::character varying, '/2.10.1/EF1'::character varying]::text[])) AND c.var::text = 'EFwActi'::text;

INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,1,140),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,1,165),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,2,160),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,3,165),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,4,146),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,5,141),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,6,140),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,7,160),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,8,160),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,9,160);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,10,140),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,11,140),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2020,12,140),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,1,165),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,2,165),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,3,165),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,4,165),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,5,165),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,6,175),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,7,175);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,8,170),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,9,165),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,10,165),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,11,150),
	 ('Domov pro seniory Elišky Purkyňové - Cvičebná 9','/2.7/EF1',2019,12,154),
	 ('DS Malešice','/2.8/EF1',2020,1,250),
	 ('DS Malešice','/2.8/EF1',2020,2,250),
	 ('DS Malešice','/2.8/EF1',2020,3,250),
	 ('DS Malešice','/2.8/EF1',2020,4,250),
	 ('DS Malešice','/2.8/EF1',2020,5,250);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('DS Malešice','/2.8/EF1',2020,6,250),
	 ('DS Malešice','/2.8/EF1',2020,7,250),
	 ('DS Malešice','/2.8/EF1',2020,8,250),
	 ('DS Malešice','/2.8/EF1',2020,9,250),
	 ('DS Malešice','/2.8/EF1',2020,10,250),
	 ('DS Malešice','/2.8/EF1',2020,11,250),
	 ('DS Malešice','/2.8/EF1',2020,12,250),
	 ('DS Malešice','/2.8/EF1',2019,1,250),
	 ('DS Malešice','/2.8/EF1',2019,2,250),
	 ('DS Malešice','/2.8/EF1',2019,3,250);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('DS Malešice','/2.8/EF1',2019,4,250),
	 ('DS Malešice','/2.8/EF1',2019,5,250),
	 ('DS Malešice','/2.8/EF1',2019,6,250),
	 ('DS Malešice','/2.8/EF1',2019,7,250),
	 ('DS Malešice','/2.8/EF1',2019,8,250),
	 ('DS Malešice','/2.8/EF1',2019,9,250),
	 ('DS Malešice','/2.8/EF1',2019,10,250),
	 ('DS Malešice','/2.8/EF1',2019,11,250),
	 ('DS Malešice','/2.8/EF1',2019,12,250),
	 ('DS Chodov','/2.9/EF1',2020,1,240);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('DS Chodov','/2.9/EF1',2020,2,240),
	 ('DS Chodov','/2.9/EF1',2020,3,240),
	 ('DS Chodov','/2.9/EF1',2020,4,240),
	 ('DS Chodov','/2.9/EF1',2020,5,240),
	 ('DS Chodov','/2.9/EF1',2020,6,240),
	 ('DS Chodov','/2.9/EF1',2020,7,240),
	 ('DS Chodov','/2.9/EF1',2020,8,240),
	 ('DS Chodov','/2.9/EF1',2020,9,240),
	 ('DS Chodov','/2.9/EF1',2020,10,240),
	 ('DS Chodov','/2.9/EF1',2020,11,240);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('DS Chodov','/2.9/EF1',2020,12,240),
	 ('DS Chodov','/2.9/EF1',2019,1,240),
	 ('DS Chodov','/2.9/EF1',2019,2,240),
	 ('DS Chodov','/2.9/EF1',2019,3,240),
	 ('DS Chodov','/2.9/EF1',2019,4,240),
	 ('DS Chodov','/2.9/EF1',2019,5,240),
	 ('DS Chodov','/2.9/EF1',2019,6,240),
	 ('DS Chodov','/2.9/EF1',2019,7,240),
	 ('DS Chodov','/2.9/EF1',2019,8,240),
	 ('DS Chodov','/2.9/EF1',2019,9,240);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('DS Chodov','/2.9/EF1',2019,10,240),
	 ('DS Chodov','/2.9/EF1',2019,11,240),
	 ('DS Chodov','/2.9/EF1',2019,12,240),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,1,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,2,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,3,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,4,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,5,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,6,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,7,200);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,8,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,9,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,10,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,11,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2020,12,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,1,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,2,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,3,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,4,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,5,200);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,6,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,7,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,8,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,9,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,10,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,11,200),
	 ('Jedličkův ústav Nová budova','/2.10.1/EF1',2019,12,200),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,2,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,3,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,4,140);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,5,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,6,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,7,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,8,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,9,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,10,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,11,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2020,12,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,1,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,2,140);
INSERT INTO analytic.consumption_electricity_daily_reserves (object_name,device_id,relevance_year,relevance_month,daily_reserved_capacity_kwh) VALUES
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,3,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,4,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,5,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,6,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,7,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,8,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,9,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,10,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,11,140),
	 ('﻿Gymnázium a Hudební škola hlavního města Prahy','/2.3/EF1',2019,12,140);