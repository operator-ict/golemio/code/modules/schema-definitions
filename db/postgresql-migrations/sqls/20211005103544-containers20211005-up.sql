create table analytic.containers_code_black_list
(container_id varchar(50) primary key,
exclusion_date date
);

CREATE OR REPLACE VIEW analytic.v_containers_24h_missing AS
select
	cm.container_code as container_id,
	max(cm.measured_at) as measured_at 
from containers_measurement cm 
right join (
	select * from containers_containers
	where code != sensoneo_code 
	and sensoneo_code is not null
	and sensoneo_code not in (select container_id from analytic.containers_code_black_list)
	) cc on cc.sensoneo_code = cm.container_code 
GROUP BY cm.container_code
HAVING 
	max(cm.measured_at) < (now() - '1 day'::interval) 
	OR 
	max(cm.measured_at) IS null
order by measured_at;

