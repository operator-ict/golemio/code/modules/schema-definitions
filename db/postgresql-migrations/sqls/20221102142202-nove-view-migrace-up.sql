CREATE OR REPLACE VIEW analytic.v_ropidbi_ticket_sales_monthly_by_zones
AS WITH purchases_w_prices AS (
         SELECT p.ticket_id,
            p.tariff_name,
            p.date::date AS date,
            p.zone_count,
            COALESCE(pid.zony, 'Nezařazeno'::character varying(50)) AS zony,
            COALESCE(pid.cena_s_dph, 0) AS price
           FROM public.mos_ma_ticketpurchases p
             LEFT JOIN analytic.pid_price_list pid ON p.tariff_name::text = pid.tarif::text AND p.cptp = pid.cislo_tarifu_cptp AND to_char(p.date::date, 'yyyy-mm-dd'::text) >= pid.nabizeno_od::text AND to_char(p.date::date, 'yyyy-mm-dd'::text) <= pid.nabizeno_do::text
        ), activations AS (
         SELECT mos_ma_ticketactivations.ticket_id,
            btrim(unnest(string_to_array(mos_ma_ticketactivations.zones::text, ','::text))) AS zones
           FROM public.mos_ma_ticketactivations
          WHERE mos_ma_ticketactivations.zones::text <> ''::text AND mos_ma_ticketactivations.zones IS NOT NULL
          GROUP BY mos_ma_ticketactivations.ticket_id, mos_ma_ticketactivations.zones
        ), main AS (
         SELECT c.ticket_id,
            c.tariff_name,
            c.date,
            c.zone_count,
            c.zony,
            c.price,
            string_agg(c.zones, ', '::text) AS zones
           FROM ( SELECT DISTINCT p.ticket_id,
                    p.tariff_name,
                    to_char(p.date::date, 'YYYY-MM-01'::text) AS date,
                    p.zone_count,
                    p.zony,
                    p.price,
                        CASE
                            WHEN a.zones = ANY (ARRAY['P'::character varying::text, '0'::text, 'B'::character varying::text]) THEN 'P+0+B'::character varying::text
                            WHEN a.zones IS NULL THEN 'Nezařazeno'::text
                            ELSE a.zones
                        END AS zones,
                        CASE
                            WHEN a.zones = ANY (ARRAY['P'::character varying::text, '0'::text, 'B'::character varying::text]) THEN 0
                            WHEN a.zones IS NULL THEN '-1'::integer
                            ELSE a.zones::integer
                        END AS zone_order
                   FROM purchases_w_prices p
                     LEFT JOIN activations a ON p.ticket_id = a.ticket_id
                  ORDER BY p.ticket_id, (
                        CASE
                            WHEN a.zones = ANY (ARRAY['P'::character varying::text, '0'::text, 'B'::character varying::text]) THEN 0
                            WHEN a.zones IS NULL THEN '-1'::integer
                            ELSE a.zones::integer
                        END)) c
          GROUP BY c.ticket_id, c.tariff_name, c.date, c.zone_count, c.zony, c.price
        )
 SELECT main.tariff_name,
    main.date,
    main.zone_count,
    main.zony,
    main.zones,
    count(*) AS tickets_count,
    sum(main.price) AS sales
   FROM main
  GROUP BY main.tariff_name, main.date, main.zone_count, main.zony, main.zones
  ORDER BY main.tariff_name, main.zony, main.zone_count, main.zones, main.date;

CREATE OR REPLACE VIEW analytic.v_ropidbi_coupon_sales_monthly_by_zones
AS WITH mos AS (
         SELECT mbc.coupon_id,
            mbc.created::date AS created,
            mbc.customer_profile_name,
                CASE
                    WHEN mbc.tariff_profile_name::text = ANY (ARRAY['Bezplatná4'::text, 'Bezplatná5'::text, 'Bezplatná6'::text]) THEN 'Bezplatná'::text
                    ELSE mbc.tariff_profile_name::text
                END AS tariff_profile_name,
            mbc.price,
                CASE
                    WHEN mbc.created_by_id::integer = 188 THEN 'Mobilní aplikace'::text
                    WHEN mbc.order_payment_type = ANY (ARRAY[1, 2]) THEN 'Eshop'::text
                    WHEN mbc.order_payment_type = ANY (ARRAY[3, 4]) THEN 'Přepážka'::text
                    ELSE 'Nezařazeno'::text
                END AS channel,
                CASE
                    WHEN mbc.seller_id = 1 THEN 'OICT'::text
                    WHEN mbc.seller_id = 29 THEN 'DPP'::text
                    ELSE 'Nezařazeno'::text
                END AS seller
           FROM public.mos_be_coupons mbc
        ), zones AS (
         SELECT z.coupon_id,
            string_agg(z.zona::text, ', '::text) AS zones,
            count(*) AS zones_count
           FROM ( SELECT mos_be_zones.coupon_id,
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN 'P+0+B'::character varying
                            ELSE mos_be_zones.zone_name
                        END AS zona,
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN 0
                            ELSE mos_be_zones.zone_name::integer
                        END AS zone_order
                   FROM public.mos_be_zones
                  WHERE mos_be_zones.zone_name::text <> ''::text AND mos_be_zones.zone_name::text IS NOT NULL
                  GROUP BY mos_be_zones.coupon_id, (
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN 'P+0+B'::character varying
                            ELSE mos_be_zones.zone_name
                        END), (
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN 0
                            ELSE mos_be_zones.zone_name::integer
                        END)
                  ORDER BY mos_be_zones.coupon_id, (
                        CASE
                            WHEN mos_be_zones.zone_name::text = ANY (ARRAY['Praha'::character varying::text, '0'::text, '0+B'::character varying::text]) THEN 0
                            ELSE mos_be_zones.zone_name::integer
                        END)) z
          GROUP BY z.coupon_id
        ), main AS (
         SELECT c.coupon_id,
            to_char(c.created::date, 'YYYY-MM-01'::text)::date AS purchase_date,
            c.tariff_profile_name,
            c.customer_profile_name,
            c.price,
            c.channel,
            c.seller,
                CASE
                    WHEN zones.zones IS NOT NULL THEN zones.zones
                    ELSE 'Nezařazeno'::text
                END AS zones,
                CASE
                    WHEN zones.zones_count IS NOT NULL AND zones.zones <> 'P+0+B'::text THEN zones.zones_count::text
                    WHEN zones.zones = 'P+0+B'::text THEN 'P+0+B'::text
                    ELSE 0::text
                END AS zones_count
           FROM mos c
             LEFT JOIN zones ON c.coupon_id = zones.coupon_id
          ORDER BY c.coupon_id
        )
 SELECT m.tariff_profile_name,
    m.customer_profile_name,
    m.channel,
    m.seller,
    m.purchase_date,
    m.zones,
    m.zones_count,
    count(*) AS coupons_count,
    sum(m.price) AS sales
   FROM main m
  GROUP BY m.tariff_profile_name, m.customer_profile_name, m.channel, m.seller, m.purchase_date, m.zones_count, m.zones
  ORDER BY m.tariff_profile_name, m.customer_profile_name, m.channel, m.seller, m.zones_count, m.zones, m.purchase_date;  